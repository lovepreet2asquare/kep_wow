﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class AgreementAccess: IAgreement
    {
        AgreementData agreementData = new AgreementData();
        public AgreementResponse SelectAll(AgreementModel model)
        {
            Int32 returnResult = 0;
            AgreementResponse response = new AgreementResponse();
            response._agreementModel = new AgreementModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);

            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                DataSet ds = agreementData.SelectAll(model, out returnResult);
                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        response._agreementModel._agreementlist = AgreementList(ds.Tables[0], returnResult);
                        returnResult = 2;
                    }
                }
                else
                {
                    response.ReturnCode = 0;
                    response.ReturnMessage = "No Data Found";
                }

                response._agreementModel._StatusList = UtilityAccess.StatusList(0);
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AgreementAccess", "SelectAll");
                return response;
            }
        }
        private List<AgreementModel> AgreementList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<AgreementModel> data = new List<AgreementModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new AgreementModel
                            {
                                UserId = Convert.ToInt32(row["UserId"]),
                                AgreementId = Convert.ToInt32(row["AgreementId"]),
                                EncryptedAgreementId =UtilityAccess.Encrypt(Convert.ToString(row["AgreementId"])),
                                Title = Convert.ToString(row["Title"]),
                                ModifyDate = Convert.ToString(row["ModifyUtcDate"]),
                                ModifyTime = Convert.ToString(row["ModifyTime"]),
                                DocumentPath=Convert.ToString(row["DocumentPath"]),
                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AgreementModel", "AgreementList");
                ReturnResult = -1;
                return null;
            }
        }
        public AgreementResponse Select(AgreementModel model)
        {
            AgreementResponse response= new AgreementResponse();
            response._agreementModel = new AgreementModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            AgreementModel info= null;
            DataSet ds = null;
            int returnResult = 0;
            if (model != null && !String.IsNullOrEmpty(model.EncryptedAgreementId))
                model.AgreementId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedAgreementId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            if (model.Privacy == 1)
            {
                ds = agreementData.SelectFooter(model, out returnResult);
            }
            else
            {
                ds = agreementData.Select(model, out returnResult);
            }
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        response._agreementModel.AgreementId = Convert.ToInt32(row["AgreementId"]??0);
                        //response._agreementModel.EncryptedAgreementId =UtilityAccess.Encrypt(Convert.ToString(row["AgreementId"]??0));
                        response._agreementModel.Title = Convert.ToString(row["Title"]);
                        response._agreementModel.Description= Convert.ToString(row["Description"]);
                        response._agreementModel.DateTime= Convert.ToString(row["ModifyUtcDate"]);
                        response._agreementModel.ModifyDate= Convert.ToString(row["ModifyDate"]);
                        response._agreementModel.ModifyTime= Convert.ToString(row["ModifyTime"]);
                        response._agreementModel.DocumentPath= Convert.ToString(row["DocumentPath"]);
                        response._agreementModel.DocumentName= Convert.ToString(row["DocumentName"]);
                        response._agreementModel.EncryptedAgreementId= UtilityAccess.Encrypt(model.AgreementId.ToString());
                      
                    }
                }
               response.ReturnCode = returnResult;
               response.ReturnMessage = Response.Message(returnResult);
               
            }
            else
            {
                response._agreementModel = model;
            }
            return response;
        }
        public AgreementResponse SelectAgreements(AgreementModel model)
        {
            AgreementResponse response= new AgreementResponse();
            response._agreementModel = new AgreementModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            AgreementModel info= null;
            DataSet ds = null;
            int returnResult = 0;
            //if (model != null && !String.IsNullOrEmpty(model.EncryptedAgreementId))
            //    model.AgreementId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedAgreementId));

            if (model.Privacy == 1)
            {
                ds = agreementData.SelectAgreementFooter(model, out returnResult);
            }
            else
            {
                ds = agreementData.SelectAgreements(model, out returnResult);
            }
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                       response._agreementModel.Title = Convert.ToString(row["Title"]);
                        response._agreementModel.Description = Convert.ToString(row["Description"]);
                        response._agreementModel.EncryptedAgreementId= UtilityAccess.Encrypt(model.AgreementId.ToString());
                      
                    }
                }
               response.ReturnCode = returnResult;
               response.ReturnMessage = Response.Message(returnResult);
               
            }
            else
            {
                response._agreementModel = model;
            }
            return response;
        }
        public AgreementResponse AddOrEdit(AgreementModel model)
        {
            Int32 ReturnResult = 0;
            AgreementResponse response = new AgreementResponse();
            response._agreementModel = new AgreementModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(ReturnResult);
            try
            {

                if (!string.IsNullOrEmpty(model.EncryptedAgreementId))
                {
                    model.AgreementId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedAgreementId));
                }
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                DataSet ds = agreementData.AddOrEdit(model, out ReturnResult);

                response.ReturnCode = ReturnResult;
                response.ReturnMessage = Response.Message(ReturnResult);

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AgreementAccess", "AddOrEdit");
                return response;
            }
        }
    }
}
