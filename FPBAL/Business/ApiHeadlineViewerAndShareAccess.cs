﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class ApiHeadlineViewerAndShareAccess : IApiHeadlineViewerAndShare
    {
        public FPResponse HeadLineShareInsert(FP_HeadlineViewerAndShareRequest request)
        {
            string returnResult = "0";
            FPResponse response = new FPResponse();
            try
            {
                request.FollowerId = (UtilityAccess.Decrypt(request.FollowerId));
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                int returnvalues = ApiHeadlineViewerAndShareData.HeadLineShareInsert(request);
                if (returnvalues > 0)
                {
                    response.ReturnCode = returnvalues.ToString();
                    response.ReturnMessage = Response.Message(returnvalues);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ApiHeadlineViewerAndShareAccess", "HeadLineShareInsert");
                return response;
            }
        }

        public FPResponse HeadLineViewerInsert(FP_HeadlineViewerAndShareRequest request)
        {
            string returnResult = "0";
            FPResponse response = new FPResponse();
            try
            {
                request.FollowerId = (UtilityAccess.Decrypt(request.FollowerId));
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                int returnvalues = ApiHeadlineViewerAndShareData.HeadLineViewerInsert(request);
                if (returnvalues > 0)
                {
                    response.ReturnCode = returnvalues.ToString();
                    response.ReturnMessage = Response.Message(returnvalues);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ApiHeadlineViewerAndShareAccess", "HeadLineViewerInsert");
                return response;
            }
        }
    }
}
