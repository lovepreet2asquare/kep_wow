﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using FPModels.Models;
using FPDAL.Data;
using System.Web;
using System.IO;

namespace FPBAL.Business
{
    public class AppHeadlineAccess : IAppHeadlines
    {
        public FPHeadlineAPIResponse FollowerHeadline(FollowerHeadlinesRequest request)
        {
            List<HeadlineAPIModel> headlineList = new List<HeadlineAPIModel>();
            FPHeadlineAPIResponse serviceResponse = new FPHeadlineAPIResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "No data found.";
            serviceResponse.HeadlineList = headlineList;
            
            int returnResult = 0;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            DataSet ds = AppHeadlineData.FollowerHeadlineSelect(request, out returnResult);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                serviceResponse.HeadlineList = HeadlineList(ds, out returnResult);
                returnResult = 2;
            }
            else if (returnResult == 0)
                returnResult = -2;

            // response message
            serviceResponse.ReturnCode = returnResult == -2 ? "0" : returnResult.ToString();
            serviceResponse.ReturnMessage = Response.Message(returnResult);
            return serviceResponse;
        }
        
        public FPHeadlineDetailAPIResponse FollowerHeadlineDetail(FollowerHeadlineDetailRequest request)
        {
            HeadlineAPIModel headlineDetail = new HeadlineAPIModel();
            FPHeadlineDetailAPIResponse serviceResponse = new FPHeadlineDetailAPIResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "No data found.";
            serviceResponse.HeadlineDetail = headlineDetail;

            int returnResult = 0;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            DataSet ds = AppHeadlineData.FollowerHeadlineDetail(request, out returnResult);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                serviceResponse.HeadlineDetail = HeadlineDetail(ds, out returnResult);
                returnResult = 2;
            }
            serviceResponse.ReturnCode = returnResult.ToString();
            serviceResponse.ReturnMessage = Response.Message(returnResult);
            return serviceResponse;
        }

        private List<HeadlineAPIModel> HeadlineList(DataSet ds, out int returnResult)
        {
            List<HeadlineAPIModel> HeadlineList = new List<HeadlineAPIModel>();
            HeadlineAPIModel headlineAPIModel = null;
            returnResult = 0;

            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        
                        returnResult = Convert.ToInt32(row["FollowerId"]);

                        if (returnResult > 0)
                        {
                            headlineAPIModel = new HeadlineAPIModel();

                            headlineAPIModel.HeadlineId = Convert.ToString(row["HeadlineId"]);
                            headlineAPIModel.Title = Convert.ToString(row["Title"]);
                            headlineAPIModel.ImagePath = Convert.ToString(row["ImagePath"]);
                            headlineAPIModel.PublishDate = Convert.ToString(row["PublishDate"]);
                            headlineAPIModel.PublishTime = Convert.ToString(row["PublishTime"]);
                            headlineAPIModel.IsUpdated = Convert.ToBoolean(row["IsUpdated"]);
                            headlineAPIModel.Content = "";
                            HeadlineList.Add(headlineAPIModel);
                        }
                        
                    }
                }
                
            }

            return HeadlineList;
        }

        private HeadlineAPIModel HeadlineDetail(DataSet ds, out int returnResult)
        {
            HeadlineAPIModel headlineAPIModel = new HeadlineAPIModel();
 
            returnResult = 0;
            string DonateURL = string.Empty;
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {

                        returnResult = Convert.ToInt32(row["FollowerId"]);

                        if (returnResult > 0)
                        {
                            headlineAPIModel = new HeadlineAPIModel();

                            headlineAPIModel.HeadlineId = Convert.ToString(row["HeadlineId"]);
                            headlineAPIModel.Title = Convert.ToString(row["Title"]);
                            headlineAPIModel.Content = Convert.ToString(row["Content"]);
                            headlineAPIModel.ImagePath = Convert.ToString(row["ImagePath"]);
                            headlineAPIModel.PublishDate = Convert.ToString(row["PublishDate"]);
                            headlineAPIModel.PublishTime = Convert.ToString(row["PublishTime"]);
                            headlineAPIModel.ShareLink = Convert.ToString(row["ShareLink"])+headlineAPIModel.HeadlineId;
                            headlineAPIModel.IsUpdated = Convert.ToBoolean(row["IsUpdated"]);
                            headlineAPIModel.IsDonate = Convert.ToBoolean(row["IsDonate"]);
                            //headlineAPIModel.DonateURL = Convert.ToString(row["DonateURL"]);
                            DonateURL = Convert.ToString(row["DonateURL"]);
                            if (DonateURL.Contains("http"))
                                headlineAPIModel.DonateURL = DonateURL;
                            else if (headlineAPIModel.IsDonate && !string.IsNullOrEmpty(DonateURL))
                                headlineAPIModel.DonateURL = "http://" + DonateURL;



                        }

                    }
                }

            }

            return headlineAPIModel;
        }

    }
}
