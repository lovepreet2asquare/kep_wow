﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using FPModels.Models;
using FPDAL.Data;
using System.Web;
using System.IO;

namespace FPBAL.Business
{
    public class AppUserAccess : IAppUser
    {
        public SignUpRespone SignUp(SignUpAPIModel request)
        {
            UserAPIModel userDetail = new UserAPIModel();
            SignUpRespone serviceResponse = new SignUpRespone();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";
            serviceResponse.UserDetail = userDetail;

            int returnResult = 0;

            if (!String.IsNullOrEmpty(request.Password))
                request.Password = UtilityAccess.Encrypt(request.Password);


            DataSet ds = AppUserData.SignUp(request, out returnResult);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                userDetail = UserInfo(ds, out returnResult);
                if (returnResult > 0)
                {
                    serviceResponse.ReturnCode = "1";
                    serviceResponse.ReturnMessage = "Submitted successfuly.";
                    serviceResponse.UserDetail = userDetail;

                    if (!string.IsNullOrEmpty(request.Email))
                    {
                        //Send welcome email
                        SendWelcomeOnSignUp(request.FirstName, request.Email);
                        //Send Verify email
                        SendVarifyEmail(userDetail.EncryptedFollowerId, request.FirstName, request.Email);
                    }
                }
                else if (returnResult == -1)
                {
                    serviceResponse.ReturnCode = "-1";
                    serviceResponse.ReturnMessage = "Technical error.";

                }
                else if (returnResult == -3)
                {
                    serviceResponse.ReturnCode = "0";
                    serviceResponse.ReturnMessage = "Email already exists.";
                }
            }
            return serviceResponse;
        }


        public SignUpRespone Login(LoginRequest request)
        {
            UserAPIModel userDetail = new UserAPIModel();
            SignUpRespone serviceResponse = new SignUpRespone();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";
            serviceResponse.UserDetail = userDetail;

            int returnResult = 0;
            string OwnerEmail = string.Empty, CustomerId = string.Empty;
            DataSet ds = AppUserData.Login(request, out returnResult);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["FollowerId"]);
                if (returnResult > 0)
                {
                    //CustomerId = Convert.ToString(ds.Tables[0].Rows[0]["CustomerId"]);
                    //OwnerEmail = Convert.ToString(ds.Tables[0].Rows[0]["OwnerEmail"]);
                    string encryptedPassword = Convert.ToString(ds.Tables[0].Rows[0]["password"]);
                    string DecryptedPassword = UtilityAccess.Decrypt(encryptedPassword);
                    if (DecryptedPassword == request.Password)
                    {
                        userDetail = UserInfo(ds, out returnResult);
                        if (returnResult > 0)
                        {

                            returnResult = AppUserData.CreateAndSaveToken(request, returnResult, UtilityAccess.Decrypt(userDetail.EncryptedSessionToken));

                            serviceResponse.ReturnCode = "1";
                            serviceResponse.ReturnMessage = "Login successfuly.";
                            serviceResponse.UserDetail = userDetail;
                        }
                        else if (returnResult == -1)
                        {
                            serviceResponse.ReturnCode = "-1";
                            serviceResponse.ReturnMessage = "Technical error.";

                        }
                        else if (returnResult == -3)
                        {
                            serviceResponse.ReturnCode = "0";
                            serviceResponse.ReturnMessage = "Invalid Email or Password.";
                        }
                    }
                    else
                    {
                        serviceResponse.ReturnCode = "0";
                        serviceResponse.ReturnMessage = "Invalid Email or Password.";
                    }
                }
                else
                {
                    serviceResponse.ReturnCode = "0";
                    serviceResponse.ReturnMessage = "Invalid Email or Password.";
                }
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -3)
            {
                serviceResponse.ReturnCode = "0";
                serviceResponse.ReturnMessage = "Invalid Email or Password.";
            }
            return serviceResponse;
        }

        public FPResponse ForgotPassword(string Email)
        {
            FPResponse serviceResponse = new FPResponse();
            int returnResult = 0, followerId = 0;
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "This email is not registered with us.";
            Exception exc;
            try
            {
                DataTable dt = AppUserData.ForgotPassword(Email, out returnResult);
                if (dt != null && dt.Rows.Count > 0)
                {
                    string body = string.Empty;
                    foreach (DataRow row in dt.Rows)
                    {
                        followerId = Convert.ToInt32(row["FollowerId"]);

                        string followerIdStr = followerId.ToString();
                        string followerIdEncrypted = UtilityAccess.Encrypt(followerIdStr);

                        string strToken = UtilityAccess.CalculateMD5Hash(row["PWD"].ToString() + Email.ToLower() + followerIdStr);

                        String strLink = UtilityAccess.BaseUrl + "reset.aspx?q=" + followerIdEncrypted + "&token=" + strToken; //@"https://demosaavorapi.saavor.io/

                        ////

                        string strpath = "";
                        string strNewsletterPath = "~/Newsletters/Follower/ForgotPassword.html";

                        if (HttpContext.Current != null)
                        {
                            strpath = HttpContext.Current.Server.MapPath(strNewsletterPath);
                        }
                        else
                        {
                            strpath = System.Web.Hosting.HostingEnvironment.MapPath(strNewsletterPath);
                        }
                        using (StreamReader reader = new StreamReader(strpath))
                        {
                            body = reader.ReadToEnd();
                        }
                        body = body.Replace("{firstname}", row["FirstName"].ToString());
                        body = body.Replace("{link}", strLink);

                    }
                    string subject = "Reset your password";
                    if (EmailUtility.SendEmailNew(Email, "Josh Team", subject, body, out exc, true) == -1)
                    {
                        ApplicationLogger.LogError(exc, "EmailUtility", "SendEmail");
                        serviceResponse.ReturnCode = "-1";
                        serviceResponse.ReturnMessage = "Due to some technical problem action cannot be completed please try later.";
                    }
                    else
                    {
                        serviceResponse.ReturnCode = "1";
                        serviceResponse.ReturnMessage = "Reset password link sent to your registered email.";

                        returnResult = 1;
                    }
                    //return returnResult;
                }
                else
                {
                    serviceResponse.ReturnCode = "0";
                    serviceResponse.ReturnMessage = "This email is not registered with us.";
                }
            }
            catch (Exception ex)
            {
                exc = ex;
                ApplicationLogger.LogError(ex, "AppUserAccess", "ForgotPassword");
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";
            }
            finally
            {
            }
            return serviceResponse;
        }

        public ProfileUpdateAPIRespone ProfileUpdate(UserAPIModel request)
        {
            //UserAPIModel userDetail = new UserAPIModel();
            ProfileUpdateAPIRespone serviceResponse = new ProfileUpdateAPIRespone();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";
            serviceResponse.IsEmailVerified = true;

            int returnResult = 0;
            int followerId = 0;
            string sessionToken = string.Empty;
            Int32.TryParse(UtilityAccess.Decrypt(request.EncryptedFollowerId), out followerId);
            sessionToken = UtilityAccess.Decrypt(request.EncryptedSessionToken);

            //GeoAddress geoAddress = new GeoAddress
            //{
            //    AddressLine1 = request.AddressLine1,
            //    AddressLine2 = request.AddressLine2,
            //    StateName = request.StateName,
            //    ZipCode = request.Zipcode,
            //    CountryName = request.CountryName,
            //    CityName = request.CityName
            //};
            //GeoTimezone geoTimezone = Timezone.GeoTimezone(geoAddress);
            ////model.Timezone = Convert.ToInt32(geoTimezone.rawOffset);
            //request.Latitude = geoTimezone.latitude.ToString();
            //request.Longitude = geoTimezone.longitude.ToString();

            // string OwnerEmail = string.Empty, CustomerId = string.Empty;
            returnResult = AppUserData.ProfileUpdate(request, followerId, sessionToken);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Updated successfully.";

            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -2)
            {
                serviceResponse.ReturnCode = "0";
                serviceResponse.ReturnMessage = "Please verify your email address.";
                serviceResponse.IsEmailVerified = false;

                SendVarifyEmail(request.EncryptedFollowerId, request.FirstName, request.Email);

            }
            else if (returnResult == -3)
            {
                serviceResponse.ReturnCode = "0";
                serviceResponse.ReturnMessage = "Email already exists.";
            }
            return serviceResponse;
        }

        public ProfileUpdateAPIRespone UploadProfilePic(ProfilePicAPIModel request)
        {
            //UserAPIModel userDetail = new UserAPIModel();
            ProfileUpdateAPIRespone serviceResponse = new ProfileUpdateAPIRespone();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";
            serviceResponse.IsEmailVerified = true;

            int returnResult = 0;
            //request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            //request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);


            // string OwnerEmail = string.Empty, CustomerId = string.Empty;
            returnResult = AppUserData.ProfilePicUpdate(request);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Updated successfully.";

            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }

            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "0";
                serviceResponse.ReturnMessage = "Authentication failed.";
            }
            return serviceResponse;
        }

        public CommonDataRespone GetCommonData()
        {
            string DonateMessage1 = string.Empty, DonateMessage2 = string.Empty;
            List<APICountry> Countries = new List<APICountry>();
            List<APIState> States = new List<APIState>();
            List<APIDonationType> DonationTypes = new List<APIDonationType>();
            List<APIRecurringType> RecurringTypes = new List<APIRecurringType>();
            APICountry country = null;
            APIState state = null;
            APIDonationType donationType = null;
            APIRecurringType recurringType = null;
            CommonDataRespone serviceResponse = new CommonDataRespone();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";
            serviceResponse.Countries = Countries;
            //serviceResponse.States = States;
            serviceResponse.DonationTypes = DonationTypes;
            serviceResponse.RecurringTypes = RecurringTypes;

            int returnResult = 0;

            DataSet ds = AppUserData.GetCommonData(out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        returnResult = 1;
                        States = new List<APIState>();
                        country = new APICountry();
                        country.CountryId = Convert.ToInt32(row["CountryId"]);
                        country.CountryName = Convert.ToString(row["CountryName"]);
                        country.ISDCode = Convert.ToString(row["DialCode"]);
                        if (ds.Tables[1].Rows.Count > 0)
                        {

                            foreach (DataRow row1 in ds.Tables[1].Select("CountryId = " + country.CountryId))
                            {
                                state = new APIState();
                                state.StateId = Convert.ToInt32(row1["StateId"]);
                                state.StateName = Convert.ToString(row1["StateName"]);
                                States.Add(state);
                            }
                        }
                        country.States = States;
                        Countries.Add(country);
                    }
                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {
                        returnResult = 1;
                        donationType = new APIDonationType();
                        donationType.DonationTypeId = Convert.ToInt32(row["DonationTypeId"]);
                        donationType.DonationType = Convert.ToString(row["DonationType"]);
                        donationType.Amount = Convert.ToDecimal(row["Amount"]);
                        DonationTypes.Add(donationType);
                    }
                }
                if (ds.Tables[3].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[3].Rows)
                    {
                        returnResult = 1;
                        recurringType = new APIRecurringType();
                        recurringType.RecurringTypeId = Convert.ToInt32(row["RecurringTypeId"]);
                        recurringType.RecurringType = Convert.ToString(row["RecurringType"]);
                        RecurringTypes.Add(recurringType);
                    }
                }
                if (ds.Tables[4].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[4].Rows)
                    {
                        returnResult = 1;
                        DonateMessage1 = Convert.ToString(row["DonateMessage1"]);
                        DonateMessage2 = Convert.ToString(row["DonateMessage2"]);
                    }
                }
                serviceResponse.Countries = Countries;
                //serviceResponse.States = States;
                serviceResponse.DonationTypes = DonationTypes;
                serviceResponse.RecurringTypes = RecurringTypes;
                serviceResponse.DonateMessage1 = DonateMessage1;
                serviceResponse.DonateMessage2 = DonateMessage2;

                if (returnResult == 1)
                {
                    serviceResponse.ReturnCode = "1";
                    serviceResponse.ReturnMessage = "Retrieved successfully.";
                }
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }

            return serviceResponse;
        }

        public FPResponse DeleteAccount(AccountDeleteRequest request)
        {
            FPResponse serviceResponse = new FPResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";


            int returnResult = 0;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            // string OwnerEmail = string.Empty, CustomerId = string.Empty;
            returnResult = AppUserData.DeleteAccount(request);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "We have received your request to delete the account and will be permanently deleted within 7 days. If you login to your account within 7 days, you can continue to access your account.";

                string subject = "Account delete request";

                string body = string.Empty;
                string strpath = "";
                string strNewsletterPath = "~/Newsletters/Follower/accountdeleterequest.html";

                if (HttpContext.Current != null)
                {
                    strpath = HttpContext.Current.Server.MapPath(strNewsletterPath);
                }
                else
                {
                    strpath = System.Web.Hosting.HostingEnvironment.MapPath(strNewsletterPath);
                }
                using (StreamReader reader = new StreamReader(strpath))
                {
                    body = reader.ReadToEnd();
                }

                body = body.Replace("@FirstName", request.FirstName);
                //body = body.Replace("@body", userInfo);
                Exception ex;
                if (EmailUtility.SendEmailNew(request.Email, "Josh Team", subject, body, out ex, true) == -1)
                {
                    // ApplicationLogger.LogError(ex, "EmailUtility", "SendEmail");
                }

            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Invaid session.";

            }

            return serviceResponse;
        }

        public FPResponse Logout(LogoutRequest request)
        {
            FPResponse serviceResponse = new FPResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";


            int returnResult = 0;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            returnResult = AppUserData.Logout(request);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Logout successfully";
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }

            return serviceResponse;
        }

        public SignUpRespone GetFollowerInfo(LogoutRequest request)
        {
            UserAPIModel userDetail = new UserAPIModel();
            SignUpRespone serviceResponse = new SignUpRespone();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "No record found";
            serviceResponse.UserDetail = userDetail;

            int returnResult = 0;
            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            DataSet ds = AppUserData.GetFollowerInfo(request, out returnResult);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["FollowerId"]);
                if (returnResult > 0)
                {
                    userDetail = UserInfo(ds, out returnResult);
                    if (returnResult > 0)
                    {


                        serviceResponse.ReturnCode = "1";
                        serviceResponse.ReturnMessage = "Retrieved successfuly.";
                        serviceResponse.UserDetail = userDetail;
                    }
                    else if (returnResult == -1)
                    {
                        serviceResponse.ReturnCode = "-1";
                        serviceResponse.ReturnMessage = "Technical error.";

                    }
                    else if (returnResult == -5)
                    {
                        serviceResponse.ReturnCode = "-5";
                        serviceResponse.ReturnMessage = "Authentication failed.";

                    }

                }
                else
                {
                    serviceResponse.ReturnCode = "0";
                    serviceResponse.ReturnMessage = "Invalid Email or Password.";
                }
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }
            return serviceResponse;
        }

        public static FPResponse ValidateSession(FPRequests request)
        {
            FPResponse serviceResponse = new FPResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";


            int returnResult = 0;

            //  request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            //  request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            returnResult = AppUserData.ValidateSession(request);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Retrieved successfully";
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }

            return serviceResponse;
        }

        public ProfileUpdateAPIRespone ValidateEmail(FPRequests request)
        {
            bool IsEmailVerified = false;
            ProfileUpdateAPIRespone serviceResponse = new ProfileUpdateAPIRespone();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";
            serviceResponse.IsEmailVerified = IsEmailVerified;

            int returnResult = -3;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            returnResult = AppUserData.ValidateEmail(request);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Retrieved successfully";
                IsEmailVerified = true;
            }
            else if (returnResult == 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Retrieved successfully";
                IsEmailVerified = false;
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }
            serviceResponse.IsEmailVerified = IsEmailVerified;
            return serviceResponse;
        }

        public FPResponse SendFriendInvitations(FPInviteRequests request)
        {
            FPResponse serviceResponse = new FPResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";


            int returnResult = 0;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
            string messageBody = "You are invited to join Josh app by your friend";
            messageBody = System.Configuration.ConfigurationManager.AppSettings["invitefriendmessage"].ToString();

            BulkSMSInfo bulkSMSInfo;
            DataSet ds = AppUserData.SendFriendInvitations(request);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["IsValid"]);
                if (returnResult > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        bulkSMSInfo = new BulkSMSInfo();
                        bulkSMSInfo.body = messageBody;
                        bulkSMSInfo.msisdn = Convert.ToString(row["MobileNo"]);

                        BulkSMSAccress.SendBulkSMS(bulkSMSInfo);
                    }
                }
            }
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Invited successfully";
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }

            return serviceResponse;
        }













        public static int SendDonationEmail(string cardHolderName, string Email, string amount, string Type, string duration, string Address1, string TransactionDate, string TransactionTime, string contributionId, string transactionId, string status, string cardType, string cardNumber, string Mobile, string Address2, string Address3, string refundAmount)
        {
            ////
            int returnValue = 0;
            string strLink = System.Configuration.ConfigurationManager.AppSettings["baseurl"].ToString();
            string body = string.Empty;
            string strpath = "";
            string strNewsletterPath = "~/Newsletters/Follower/refundinvoice.html";
            decimal totalAmount = Convert.ToDecimal(amount) - Convert.ToDecimal(refundAmount);
            if (HttpContext.Current != null)
            {
                strpath = HttpContext.Current.Server.MapPath(strNewsletterPath);
            }
            else
            {
                strpath = System.Web.Hosting.HostingEnvironment.MapPath(strNewsletterPath);
            }
            using (StreamReader reader = new StreamReader(strpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Name}", cardHolderName);
            body = body.Replace("{amount}", amount);
            //body = body.Replace("{type}", Type);

            body = body.Replace("{Date}", TransactionDate);
            body = body.Replace("{Time}", TransactionTime);
            body = body.Replace("{Email}", Email);
            body = body.Replace("{Mobile}", Mobile);
            body = body.Replace("{Address1}", Address1);
            body = body.Replace("{Address2}", Address2);
            body = body.Replace("{Address3}", Address3);
            body = body.Replace("{ContributionID}", contributionId);
            body = body.Replace("{contributiontype}", Type);
            body = body.Replace("{TransactionID}", transactionId);
            body = body.Replace("{methodtype}", cardType);
            body = body.Replace("{cardnumber}", cardNumber);
            body = body.Replace("{STATUS}", status);
            body = body.Replace("{refundamount}", refundAmount);
            body = body.Replace("{totalamount}", totalAmount.ToString());
            
            //if (!string.IsNullOrEmpty(duration))
            //    body = body.Replace("{duration}", duration);
            //else
            //{
            //    body = body.Replace("{duration}", duration);
            //    body = body.Replace("Duration-", string.Empty);
            //}

            //body = body.Replace("{lockPin}", Convert.ToString(ScreenLockPin));

            Exception exc = null;
            string subject = "Refund email";
            if (EmailUtility.SendEmailNew(Email, "Josh Team", subject, body, out exc, true) == -1)
            {
                returnValue = -1;
                ApplicationLogger.LogError(exc, "EmailUtility", "SendEmailNewDonate");
                //serviceResponse.ReturnCode = "-1";
                //serviceResponse.ReturnMessage = "Technical error.";
            }
            else
                returnValue = 1;

            return returnValue;
        }

        public static int SendDonationEmail(string cardHolderName, string Email, string amount, string Type, string duration, string Address1, string TransactionDate, string TransactionTime, string contributionId, string transactionId, string status, string cardType, string cardNumber, string Mobile, string Address2, string Address3)
        {
            ////
            int returnValue = 0;
            string strLink = System.Configuration.ConfigurationManager.AppSettings["baseurl"].ToString();
            string body = string.Empty;
            string strpath = "";
            string strNewsletterPath = "~/Newsletters/Follower/invoice.html";

            if (HttpContext.Current != null)
            {
                strpath = HttpContext.Current.Server.MapPath(strNewsletterPath);
            }
            else
            {
                strpath = System.Web.Hosting.HostingEnvironment.MapPath(strNewsletterPath);
            }
            using (StreamReader reader = new StreamReader(strpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Name}", cardHolderName);
            body = body.Replace("{amount}", amount);
            //body = body.Replace("{type}", Type);

            body = body.Replace("{Date}", TransactionDate);
            body = body.Replace("{Time}", TransactionTime);
            body = body.Replace("{Email}", Email);
            body = body.Replace("{Mobile}", Mobile);
            body = body.Replace("{Address1}", Address1);
            body = body.Replace("{Address2}", Address2);
            body = body.Replace("{Address3}", Address3);
            body = body.Replace("{ContributionID}", contributionId);
            body = body.Replace("{contributiontype}", Type);
            body = body.Replace("{TransactionID}", transactionId);
            body = body.Replace("{methodtype}", cardType);
            body = body.Replace("{cardnumber}", cardNumber);
            body = body.Replace("{STATUS}", status);

            //if (!string.IsNullOrEmpty(duration))
            //    body = body.Replace("{duration}", duration);
            //else
            //{
            //    body = body.Replace("{duration}", duration);
            //    body = body.Replace("Duration-", string.Empty);
            //}

            //body = body.Replace("{lockPin}", Convert.ToString(ScreenLockPin));

            Exception exc = null;
            string subject = "Donation email";
            if (EmailUtility.SendEmailNew(Email, "Josh Team", subject, body, out exc, true) == -1)
            {
                returnValue = -1;
                ApplicationLogger.LogError(exc, "EmailUtility", "SendEmailNewDonate");
                //serviceResponse.ReturnCode = "-1";
                //serviceResponse.ReturnMessage = "Technical error.";
            }
            else
                returnValue = 1;

            return returnValue;
        }

        public static void SendDonationEmail(string cardHolderName, string Email, string amount, string Type, string duration)
        {
            ////
            string strLink = System.Configuration.ConfigurationManager.AppSettings["baseurl"].ToString();
            string body = string.Empty;
            string strpath = "";
            string strNewsletterPath = "~/Newsletters/Follower/DonateEmail.html";

            if (HttpContext.Current != null)
            {
                strpath = HttpContext.Current.Server.MapPath(strNewsletterPath);
            }
            else
            {
                strpath = System.Web.Hosting.HostingEnvironment.MapPath(strNewsletterPath);
            }
            using (StreamReader reader = new StreamReader(strpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{user}", cardHolderName);
            body = body.Replace("{amount}", amount);
            body = body.Replace("{type}", Type);
            if (!string.IsNullOrEmpty(duration))
                body = body.Replace("{duration}", duration);
            else
            {
                body = body.Replace("{duration}", duration);
                body = body.Replace("Duration-", string.Empty);
            }
            //body = body.Replace("{lockPin}", Convert.ToString(ScreenLockPin));

            Exception exc = null;
            string subject = "Donation email";
            if (EmailUtility.SendEmailNew(Email, "Josh Team", subject, body, out exc, true) == -1)
            {
                ApplicationLogger.LogError(exc, "EmailUtility", "SendEmailNewDonate");
                //serviceResponse.ReturnCode = "-1";
                //serviceResponse.ReturnMessage = "Technical error.";
            }
        }
        public static void SendRecurreingFailEmail(string cardHolderName, string Email, string amount, string Type, string duration, string errortext)
        {
            ////
            string strLink = System.Configuration.ConfigurationManager.AppSettings["baseurl"].ToString();
            string body = string.Empty;
            string strpath = "";
            string strNewsletterPath = "~/Newsletters/Follower/DonateFailedEmail.html";

            if (HttpContext.Current != null)
            {
                strpath = HttpContext.Current.Server.MapPath(strNewsletterPath);
            }
            else
            {
                strpath = System.Web.Hosting.HostingEnvironment.MapPath(strNewsletterPath);
            }
            using (StreamReader reader = new StreamReader(strpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{user}", cardHolderName);
            body = body.Replace("{amount}", amount);
            body = body.Replace("{type}", Type);
            body = body.Replace("{duration}", duration);
            body = body.Replace("{errortext}", errortext);
            //{ errortext}
            //body = body.Replace("{lockPin}", Convert.ToString(ScreenLockPin));

            Exception exc = null;
            string subject = "Donatation transaction failed email";
            if (EmailUtility.SendEmailNew(Email, "Josh Team", subject, body, out exc, true) == -1)
            {
                ApplicationLogger.LogError(exc, "EmailUtility", "SendEmailNewFailed");
                //serviceResponse.ReturnCode = "-1";
                //serviceResponse.ReturnMessage = "Technical error.";
            }
        }
        private void SendWelcomeOnSignUp(string FirstName, string Email)
        {
            ////
            string strLink = System.Configuration.ConfigurationManager.AppSettings["baseurl"].ToString();
            string body = string.Empty;
            string strpath = "";
            string strNewsletterPath = "~/Newsletters/Follower/Welcome.html";

            if (HttpContext.Current != null)
            {
                strpath = HttpContext.Current.Server.MapPath(strNewsletterPath);
            }
            else
            {
                strpath = System.Web.Hosting.HostingEnvironment.MapPath(strNewsletterPath);
            }
            using (StreamReader reader = new StreamReader(strpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{firstname}", FirstName);
            body = body.Replace("{link}", strLink);
            //body = body.Replace("{lockPin}", Convert.ToString(ScreenLockPin));

            Exception exc = null;
            string subject = "Welcome to Josh";
            if (EmailUtility.SendEmailNew(Email, "Josh Team", subject, body, out exc, true) == -1)
            {
                ApplicationLogger.LogError(exc, "EmailUtility", "SendEmailNewSignUp");
                //serviceResponse.ReturnCode = "-1";
                //serviceResponse.ReturnMessage = "Technical error.";
            }
        }

        private void SendVarifyEmail(string followerIdEncrypted, string FirstName, string Email)
        {
            ////
            string followerIdStr = UtilityAccess.Decrypt(followerIdEncrypted);
            //string followerIdEncrypted = UtilityAccess.Encrypt(followerIdStr);

            string strToken = UtilityAccess.CalculateMD5Hash(FirstName + Email.ToLower() + followerIdStr);

            String strLink = UtilityAccess.BaseUrl + "verify.aspx?q=" + followerIdEncrypted + "&token=" + strToken; //@"https://demosaavorapi.saavor.io/

            ////


            //string strLink = System.Configuration.ConfigurationManager.AppSettings["baseurl"].ToString();
            string body = string.Empty;
            string strpath = "";
            string strNewsletterPath = "~/Newsletters/Follower/VarifyEmail.html";

            if (HttpContext.Current != null)
            {
                strpath = HttpContext.Current.Server.MapPath(strNewsletterPath);
            }
            else
            {
                strpath = System.Web.Hosting.HostingEnvironment.MapPath(strNewsletterPath);
            }
            using (StreamReader reader = new StreamReader(strpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{firstname}", FirstName);
            body = body.Replace("{link}", strLink);
            //body = body.Replace("{lockPin}", Convert.ToString(ScreenLockPin));

            Exception exc = null;
            string subject = "Verify your email address";
            if (EmailUtility.SendEmailNew(Email, "Josh Team", subject, body, out exc, true) == -1)
            {
                ApplicationLogger.LogError(exc, "EmailUtility", "SendEmailNewVarifyE");
                //serviceResponse.ReturnCode = "-1";
                //serviceResponse.ReturnMessage = "Technical error.";
            }
        }

        public void SendEmailOnPasswordUpdate(string FirstName, string Email)
        {
            ////
            //string strLink = System.Configuration.ConfigurationManager.AppSettings["baseurl"].ToString();
            string body = string.Empty;
            string strpath = "";
            string strNewsletterPath = "~/Newsletters/Follower/PasswordUpdated.html";

            if (HttpContext.Current != null)
            {
                strpath = HttpContext.Current.Server.MapPath(strNewsletterPath);
            }
            else
            {
                strpath = System.Web.Hosting.HostingEnvironment.MapPath(strNewsletterPath);
            }
            using (StreamReader reader = new StreamReader(strpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{firstname}", FirstName);
            //body = body.Replace("{link}", strLink);

            Exception exc = null;
            string subject = "Password Updated successfully";
            if (EmailUtility.SendEmailNew(Email, "NJSPBA Team", subject, body, out exc, true) == -1)
            {
                ApplicationLogger.LogError(exc, "EmailUtility", "SendEmailNewPassword");

            }
        }

        private UserAPIModel UserInfo(DataSet ds, out int returnResult)
        {
            UserAPIModel userInfo = new UserAPIModel();
            returnResult = 0;

            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        returnResult = Convert.ToInt32(row["FollowerId"]);

                        if (returnResult > 0)
                        {
                            userInfo.EncryptedFollowerId = UtilityAccess.Encrypt(returnResult.ToString());

                            userInfo.AddressLine1 = Convert.ToString(row["AddressLine1"]);
                            userInfo.AddressLine2 = Convert.ToString(row["AddressLine2"]);
                            userInfo.Email = Convert.ToString(row["Email"]);
                            userInfo.FirstName = Convert.ToString(row["FirstName"]);
                            userInfo.LastName = Convert.ToString(row["LastName"]);
                            //userInfo.IsNotification = Convert.ToBoolean(row["IsNotification"]);
                            userInfo.MI = Convert.ToString(row["MI"]);
                            userInfo.MobileNumber = Convert.ToString(row["MobileNumber"]);
                            userInfo.CityName = Convert.ToString(row["CityName"]);
                            userInfo.CountryId = Convert.ToInt32(row["CountryId"]);
                            userInfo.Email = Convert.ToString(row["Email"]);
                            userInfo.StateId = Convert.ToInt32(row["StateId"]);
                            userInfo.Zipcode = Convert.ToString(row["Zipcode"]);
                            userInfo.IsHeadlineNotify = Convert.ToBoolean(row["IsHeadlineNotify"]);
                            userInfo.IsNewsNotify = Convert.ToBoolean(row["IsNewsNotify"]);
                            userInfo.IsEventNotify = Convert.ToBoolean(row["IsEventNotify"]);
                            userInfo.EncryptedSessionToken = UtilityAccess.Encrypt(Convert.ToString(row["TokenKey"]));
                            userInfo.ISDCode = Convert.ToString(row["ISDCode"]);
                            userInfo.IsSkip = Convert.ToBoolean(row["IsSkip"]);
                            userInfo.IsEmailVerified = Convert.ToBoolean(row["IsEmailVerified"]);
                            userInfo.BirthDayName = Convert.ToString(row["BirthDayName"]);
                            userInfo.BirthMonth = Convert.ToString(row["BirthMonth"]);
                            userInfo.Accupation = Convert.ToString(row["Accupation"]);
                            userInfo.Employer = Convert.ToString(row["Employer"]);
                            userInfo.AddressId = Convert.ToInt32(row["AddressId"]);
                            userInfo.ProfilePic = Convert.ToString(row["ProfilePic"]);
                            userInfo.Latitude = Convert.ToDecimal(row["Latitude"]).ToString();
                            userInfo.Longitude = Convert.ToDecimal(row["Longitude"]).ToString();
                            userInfo.MessageCount = Convert.ToInt32(row["MessageCount"]);
                            userInfo.StateName = Convert.ToString(row["StateName"]);
                            userInfo.CountryName = Convert.ToString(row["CountryName"]);

                        }
                    }
                }

            }

            return userInfo;
        }

        public int TimeZoneInsert(string Id, string StandardName, string DisplayName)
        {

            return AppUserData.TimezoneInsert(Id, StandardName, DisplayName);
        }

        public static void SendCardExpiryEmail(string firstName, string Email, string message)
        {
            ////
            string strLink = System.Configuration.ConfigurationManager.AppSettings["baseurl"].ToString();
            string body = string.Empty;
            string strpath = "";
            string strNewsletterPath = "~/Newsletters/Follower/CardExpiryEmail.html";

            if (HttpContext.Current != null)
            {
                strpath = HttpContext.Current.Server.MapPath(strNewsletterPath);
            }
            else
            {
                strpath = System.Web.Hosting.HostingEnvironment.MapPath(strNewsletterPath);
            }
            using (StreamReader reader = new StreamReader(strpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{user}", firstName);
            body = body.Replace("{message}", message);

            Exception exc = null;
            string subject = "Card expiry email";
            if (EmailUtility.SendEmailNew(Email, "Josh Team", subject, body, out exc, true) == -1)
            {
                ApplicationLogger.LogError(exc, "EmailUtility", "SendCardExpiryEmail");
                //serviceResponse.ReturnCode = "-1";
                //serviceResponse.ReturnMessage = "Technical error.";
            }
        }
        public static void GetCardExpiryFollowers()
        {
            string firstName = string.Empty, email = string.Empty, emailMessage = string.Empty;
            DataSet ds = AppUserData.GetCardExpiryFollowers();
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    firstName = Convert.ToString(row["FirstName"]);
                    email = Convert.ToString(row["Email"]);
                    emailMessage = Convert.ToString(row["EmailMessage"]);
                    SendCardExpiryEmail(firstName, email, emailMessage);
                }
            }
        }


        public FPResponse ApnsNotification(string deviceToken, string Message)
        {
            FPResponse serviceResponse = new FPResponse();
            serviceResponse = APNSNotificationAccess.ApnsNotification(deviceToken, Message);

            return serviceResponse;
        }

}
}
