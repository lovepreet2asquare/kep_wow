﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
namespace FPBAL.Business
{
    public class CampaignAccess : ICampaign
    {
        CampaignData campaignData = new CampaignData();
        public CampaignResponse AddOrEdit(CampaignModel model)
        {
            Int32 returnResult = 0;
            CampaignResponse response = new CampaignResponse();
            response.CampaignModel = new CampaignModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                //model.OfficeId = UtilityAccess.Decrypt(model.OfficeId);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                if (model.OfficeId != null)
                    model.OfficeId = UtilityAccess.Decrypt(model.OfficeId);
                else
                    model.OfficeId = "0";
                DataSet ds = null;
                if (model.OfficeId == "0")
                {
                     ds = campaignData.AddOrEdit(model, AddDay(model.DayList), out returnResult);
                }
                else
                {
                     ds = campaignData.AddOrEdit(model, AddDay(model._campaignTimelist), out returnResult);
                }
                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            int result = 0;
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                result = Convert.ToInt32(row["FollowerId"]);
                                if (result > 0)
                                {
                                    Thread T1 = new Thread(delegate ()
                                    {
                                        string deviceToken = Convert.ToString(row["DeviceToken"] ?? string.Empty);
                                        string deviceType = Convert.ToString(row["DeviceType"] ?? string.Empty);
                                        string Message = Convert.ToString(row["NotificationMessage"] ?? string.Empty);

                                        if (deviceType.ToUpper() == "IOS")
                                            APNSNotificationAccess.ApnsNotification(deviceToken, Message);
                                        else
                                             if (deviceType.ToUpper() == "ANDROID")
                                            FCMNotificationAccess.SendFCMNotifications(deviceToken, Message, deviceType);
                                    });
                                }
                                //if (deviceType.ToUpper() == "IOS")
                                //    APNSNotificationAccess.ApnsNotification(deviceToken, message);
                                //else
                                //     if (deviceType.ToUpper() == "ANDROID")
                                //    FCMNotificationAccess.SendFCMNotifications(deviceToken, message, deviceType);
                            }
                            //}
                        }
                    }
                }

                //if (UserId > 0)
                //{
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);
                //}

                return response;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignAccess", "AddOrEdit");
                return response;
            }
        }
        private DataTable AddDay(List<CampaignModel> DList)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("UserId", typeof(Int32));
            dt.Columns.Add("OfficeId", typeof(string));
            dt.Columns.Add("IsChecked", typeof(bool));
            dt.Columns.Add("ModifyDate", typeof(string));
            dt.Columns.Add("DayId", typeof(Int32));
            dt.Columns.Add("TimeFrom", typeof(string));
            dt.Columns.Add("TimeTo", typeof(string));

            DataRow row = null;
            if (DList != null && DList.Count > 0)
            {
                foreach (var item in DList)
                {
                    row = dt.NewRow();
                    row["UserId"] = item.UserId;
                    row["OfficeId"] = item.OfficeId;
                    row["IsChecked"] = item.IsChecked;
                    row["ModifyDate"] = item.ModifyDate;
                    row["DayId"] = item.DayId;
                    row["TimeFrom"] = item.TimeFrom;
                    row["TimeTo"] = item.TimeTo;
                    dt.Rows.Add(row);
                }
            }

            return dt;

        }
        public CampaignResponse Select(CampaignModel model)
        {
            Int32 returnResult = 0;
            CampaignResponse response = new CampaignResponse();

            response.CampaignModel = new CampaignModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);

            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                if (model.OfficeId != null)
                    model.OfficeId = UtilityAccess.Decrypt(model.OfficeId);

                //string DateFrom = string.Empty;
                //string DateTo = string.Empty;
                //UtilityAccess.GetFromToDate(model.DateFilterText, out DateFrom, out DateTo);
                //model.DateFrom = DateFrom;
                //model.DateTo = DateTo;
                DataSet ds = campaignData.Select(model, out returnResult);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                    if (returnResult > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                response.CampaignModel.Title = Convert.ToString(row["Title"]);
                                response.CampaignModel.AddressLine1 = Convert.ToString(row["AddressLine1"]);
                                response.CampaignModel.AddressLine2 = Convert.ToString(row["AddressLine2"]);
                                response.CampaignModel.Address = Convert.ToString(row["Address"]);
                                response.CampaignModel.ZIP = Convert.ToString(row["ZIP"]);
                                response.CampaignModel.ISDCode = Convert.ToString(row["ISDCode"]);
                                response.CampaignModel.Phone = Convert.ToString(row["PhoneNumber"]);
                                response.CampaignModel.Fax = Convert.ToString(row["FaxNumber"]);
                                //response.CampaignModel.TimeFrom = Convert.ToString(row["TimeFrom"]);
                                //response.CampaignModel.TimeTo = Convert.ToString(row["TimeTo"]);
                                response.CampaignModel.DayId = Convert.ToInt32(row["DayId"]);
                                response.CampaignModel.DayTitle = Convert.ToString(row["DayTitle"]);
                                response.CampaignModel.OfficeTimeId = Convert.ToInt32(row["OfficeTimeId"]);
                                response.CampaignModel.OfficeId = UtilityAccess.Encrypt(Convert.ToString(row["OfficeId"]));
                                response.CampaignModel.StateId = Convert.ToInt32(row["StateId"]);
                                response.CampaignModel.StateName = Convert.ToString(row["StateName"]);
                                response.CampaignModel.CountryId = Convert.ToInt32(row["CountryId"]);
                                response.CampaignModel.CountryName = Convert.ToString(row["CountryName"]);
                                response.CampaignModel.CityName = Convert.ToString(row["CityName"]);

                            }
                        }
                    }
                }
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    //foreach (DataRow row1 in ds.Tables[4].Rows)
                    //{
                    response.CampaignModel._campaignTimelist = CampaignTimeList(ds.Tables[1], returnResult);
                    //}
                }
                response.CampaignModel.DayList = dayList(ds.Tables[4], returnResult);
                response.CampaignModel.CountryList = UtilityAccess.RenderCountryList(ds.Tables[2]);
                response.CampaignModel._StateList = UtilityAccess.RenderStateList(ds.Tables[3]);
                response.CampaignModel._ISDCodeList = UtilityAccess.RenderISDCodeList(ds.Tables[0],1);
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignAccess", "SelectAll");
                return response;
            }
        }
        public CampaignResponse SelectAll(CampaignModel model)
        {
            Int32 returnResult = 0;
            CampaignResponse response = new CampaignResponse();
            response.CampaignModel = new CampaignModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);

            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                DataSet ds = campaignData.SelectAll(model, out returnResult);
                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        response.CampaignModel._campaignlist = CampaignList(ds.Tables[0], returnResult);
                        returnResult = 2;
                    }
                }
                else
                {
                    response.ReturnCode = 0;
                    response.ReturnMessage = "No Data Found";
                }
                // response.CampaignModel.StatusList = UtilityAccess.StatusList(1);
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                //response.InAppMessageModel._DateFilterList = UtilityAccess.DateFilterList;
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignAccess", "SelectAll");
                return response;
            }
        }
        private List<CampaignModel> CampaignList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<CampaignModel> data = new List<CampaignModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new CampaignModel
                            {
                                OfficeTimeId = Convert.ToInt32(row["OfficeTimeId"]),
                                OfficeId = UtilityAccess.Encrypt(Convert.ToString(row["OfficeId"])),
                                Title = Convert.ToString(row["Title"]),
                                //Address = Convert.ToString(row["Address"]),
                                AddressLine1 = Convert.ToString(row["AddressLine1"]),
                                AddressLine2 = Convert.ToString(row["AddressLine2"]),
                                StateName = Convert.ToString(row["StateName"]),
                                CityName = Convert.ToString(row["CityName"]),
                                ZIP = Convert.ToString(row["ZIPCode"]),
                                Phone = Convert.ToString(row["PhoneNumber"]),
                                ISDCode = Convert.ToString(row["ISDCode"]),
                                Fax = Convert.ToString(row["FaxNumber"]),
                                TimeTo = Convert.ToString(row["TimeTo"]),
                                TimeFrom = Convert.ToString(row["TimeFrom"]),

                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignModel", "CampaignList");
                ReturnResult = -1;
                return null;
            }
        }
        private List<CampaignModel> CampaignTimeList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<CampaignModel> data = new List<CampaignModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new CampaignModel
                            {
                                UserId = Convert.ToInt32(row["UserId"]),
                                DayId = Convert.ToInt32(row["DayId"]),
                                DayTitle = Convert.ToString(row["DayTitle"]),
                                IsChecked = Convert.ToBoolean(row["IsChecked"]),
                                OfficeTimeId = Convert.ToInt32(row["OfficeTimeId"]),
                                OfficeId = Convert.ToString(row["OfficeId"]),
                                TimeTo = Convert.ToString(row["TimeTo"]),
                                TimeFrom = Convert.ToString(row["TimeFrom"]),

                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignModel", "CampaignList");
                ReturnResult = -1;
                return null;
            }
        }

        private List<CampaignModel> dayList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<CampaignModel> data = new List<CampaignModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new CampaignModel
                            {
                                DayId = Convert.ToInt32(row["DayId"]),
                                DayTitle = Convert.ToString(row["DayTitle"]),
                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignModel", "CampaignList");
                ReturnResult = -1;
                return null;
            }
        }

        public void PublishNSendNotification()
        {
            try
            {
                string deviceToken = string.Empty, deviceType = string.Empty, Message = string.Empty;
                int returnResult = 0;
                DataSet ds = campaignData.PublishNSendNotification(out returnResult);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row1 in ds.Tables[0].Rows)
                    {
                        Message = Convert.ToString(row1["Title"]);

                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[1].Rows)
                            {
                                deviceToken = Convert.ToString(row["DeviceToken"]);
                                deviceType = Convert.ToString(row["DeviceType"]);
                                if (deviceType.ToUpper() == "IOS")
                                    APNSNotificationAccess.ApnsNotification(deviceToken, Message);
                                else
                                   if (deviceType.ToUpper() == "ANDROID")
                                    FCMNotificationAccess.SendFCMNotifications(deviceToken, Message, deviceType);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventAccess", "PublishNSendNotification");

            }
        }
        //public CampaignResponse Delete(CampaignModel model)
        //{
        //    Int32 returnResult = 0;
        //    CampaignResponse response = new CampaignResponse();
        //    response.CampaignModel = new CampaignModel();
        //    // default response message
        //    response.ReturnCode = 0;
        //    response.ReturnMessage = Response.Message(returnResult);
        //    try
        //    {
        //        model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
        //        model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
        //        model.OfficeId = UtilityAccess.Decrypt(model.OfficeId);
        //        Int32 res = campaignData.Delete(model, out returnResult);
        //        if (returnResult > 0)
        //        {
        //            // response message
        //            response.ReturnCode = returnResult;// 
        //            response.ReturnMessage = Response.Message(returnResult);
        //        }
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        ApplicationLogger.LogError(ex, "CampaignAccess", "Delete");
        //        return response;
        //    }
        //}

        public CampaignResponse Delete(CampaignModel model)
        {
            CampaignResponse serviceResponse = new CampaignResponse();
            NewsModel userInfo = new NewsModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            CampaignModel headlineInfo = null;
            int returnResult = 0;
            if (model != null && model.OfficeId != null)
                model.OfficeId = (UtilityAccess.Decrypt(model.OfficeId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = campaignData.Delete(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.CampaignModel = headlineInfo;
            }
            return serviceResponse;
        }
    }
}
