﻿using System;
using System.Data;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers.Bases;
using FPModels.Models;
using FPDAL.Data;

namespace FPBAL.Business
{
    public class ChargeCustomerProfile
    {
        
        public static ANetApiResponse Donate(String ApiLoginID, String ApiTransactionKey, DonateAPIModel donateAPIModel, out CustomerTransactinResponse serviceResponse)
        {

            //string refId;
            //DonateAPIModel donateAPIModel = new DonateAPIModel();
            serviceResponse = new CustomerTransactinResponse();
            //Console.WriteLine("Charge Customer Profile");
            serviceResponse.TransactionId = "0";
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;

            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey
            };

            //create a customer payment profile
            customerProfilePaymentType profileToCharge = new customerProfilePaymentType();
            profileToCharge.customerProfileId = donateAPIModel.CustomerProfileId;
            profileToCharge.paymentProfile = new paymentProfile { paymentProfileId = donateAPIModel.CustomerPaymentProfileId };

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),    // refund type
                amount = donateAPIModel.Amount,
                profile = profileToCharge
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the collector that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            //validate

            CustomerTransactionInfo info = new CustomerTransactionInfo();
            info.Amount = donateAPIModel.Amount;
            info.CustomerPaymentProfileId = donateAPIModel.CustomerPaymentProfileId;
            info.CustomerProfileId = donateAPIModel.CustomerProfileId;
            info.FollowerId = Convert.ToInt32(donateAPIModel.FollowerId);
            info.RefId = donateAPIModel.FollowerId;
            
            if (response != null)
            {
                if (response.messages.resultCode == messageTypeEnum.Ok)
                {
                    if (response.transactionResponse.messages != null)
                    {
                        serviceResponse.ReturnCode = "1";
                        serviceResponse.ReturnMessage = response.transactionResponse.messages[0].description;
                        serviceResponse.TransactionId = response.transactionResponse.transId;

                        info.ErrorCode = "";
                        info.ErrorText = "";
                        info.MessageCode = response.transactionResponse.messages[0].code;
                        info.MessageText = response.transactionResponse.messages[0].description;
                        info.ResponseCode = response.transactionResponse.responseCode;
                        info.ResponseDescription = "Successfully created transaction with Transaction ID: " + response.transactionResponse.transId;
                        info.ResultCode = response.messages.resultCode.ToString();
                        info.TransactionID = response.transactionResponse.transId;
                        donateAPIModel.TransactionId = response.transactionResponse.transId;
                        
                        string userName = string.Empty, email = string.Empty;
                        string Address1 = string.Empty, TransactionDate = string.Empty, TransactionTime = string.Empty
                            , contributionId = string.Empty, transactionId = string.Empty, status = string.Empty, 
                            cardType = string.Empty, cardNumber = string.Empty, mobile = string.Empty,
                            Address2 = string.Empty, Address3 = string.Empty;
                        transactionId = donateAPIModel.TransactionId;
                        //status = "Paid";
                        int returnResult = 0;
                        DataSet ds = PaymentMethodData.DonateTransactionInsert(donateAPIModel);
                        if(ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["PaymentMethodId"]);
                            if(returnResult > 0)
                            {
                               contributionId = Convert.ToString(Convert.ToInt32(ds.Tables[0].Rows[0]["PaymentId"]));
                                userName = Convert.ToString(ds.Tables[0].Rows[0]["CardHolderName"]);
                                TransactionDate = Convert.ToString(ds.Tables[0].Rows[0]["TransactionDate"]);
                                TransactionTime = Convert.ToString(ds.Tables[0].Rows[0]["TransactionTime"]);
                                cardType = Convert.ToString(ds.Tables[0].Rows[0]["CardType"]);
                                cardNumber = Convert.ToString(ds.Tables[0].Rows[0]["CardNumber"]);
                                email = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);
                                status = Convert.ToString(ds.Tables[0].Rows[0]["Status"]);
                                mobile = Convert.ToString(ds.Tables[0].Rows[0]["mobile"]);
                                Address1 = Convert.ToString(ds.Tables[0].Rows[0]["Address1"]);
                                Address2 = Convert.ToString(ds.Tables[0].Rows[0]["Address2"]);
                                Address3 = Convert.ToString(ds.Tables[0].Rows[0]["Address3"]);
                                // email = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);
                                string type = donateAPIModel.IsRecurring.ToLower() == "true" ? "Monthly" : "One-time";
                                string duration = donateAPIModel.IsRecurring.ToLower() == "true" ? Convert.ToString(ds.Tables[0].Rows[0]["RecurringType"]) : ""; 
                                AppUserAccess.SendDonationEmail(userName, email, donateAPIModel.Amount.ToString(), type, duration,Address1, TransactionDate, TransactionTime, contributionId, transactionId, status, cardType, cardNumber, mobile, Address2, Address3);


                            }
                        }
                        //                  Console.WriteLine("Successfully created transaction with Transaction ID: " + response.transactionResponse.transId);
                        //                  Console.WriteLine("Response Code: " + response.transactionResponse.responseCode);
                        //                  Console.WriteLine("Message Code: " + response.transactionResponse.messages[0].code);
                        //                  Console.WriteLine("Description: " + response.transactionResponse.messages[0].description);
                        //Console.WriteLine("Success, Auth Code : " + response.transactionResponse.authCode);
                    }
                    else
                    {
                        //add history log for Failed Transaction.
                        if (response.transactionResponse.errors != null)
                        {
                            serviceResponse.ReturnCode = "0";
                            serviceResponse.ReturnMessage = response.transactionResponse.errors[0].errorText;
                            serviceResponse.TransactionId = "0";

                            info.ErrorCode = response.transactionResponse.errors[0].errorCode;
                            info.ErrorText = response.transactionResponse.errors[0].errorText;
                            info.MessageCode = "";
                            info.MessageText = "";

                            info.ResponseCode = response.transactionResponse.responseCode;
                            info.ResponseDescription = response.transactionResponse.errors[0].errorText;
                            info.ResultCode = response.messages.resultCode.ToString();
                            info.TransactionID = "0";
                            //serviceResponse = AuthorizePayments.CustomerPaymentTransactionLog(info);
                            //Console.WriteLine("Error Code: " + response.transactionResponse.errors[0].errorCode);
                            //Console.WriteLine("Error message: " + response.transactionResponse.errors[0].errorText);
                        }
                    }
                }
                else
                {
                    //Console.WriteLine("Failed Transaction.");
                    if (response.transactionResponse != null && response.transactionResponse.errors != null)
                    {
                        serviceResponse.ReturnCode = "0";
                        serviceResponse.ReturnMessage = response.transactionResponse.errors[0].errorText;
                        serviceResponse.TransactionId = "0";


                        info.ErrorCode = response.transactionResponse.errors[0].errorCode;
                        info.ErrorText = response.transactionResponse.errors[0].errorText;
                        info.MessageCode = "";
                        info.MessageText = "";

                        info.ResponseCode = response.transactionResponse.responseCode;
                        info.ResponseDescription = "Failed Transaction. " + response.transactionResponse.errors[0].errorText;
                        info.ResultCode = response.messages.resultCode.ToString();
                        info.TransactionID = "0";

                        //Commented on 8th April 2019
                       /// serviceResponse = AuthorizePayments.CustomerPaymentTransactionLog(info);

                        //Console.WriteLine("Error Code: " + response.transactionResponse.errors[0].errorCode);
                        //Console.WriteLine("Error message: " + response.transactionResponse.errors[0].errorText);
                    }
                    else
                    {
                        //Console.WriteLine("Error Code: " + response.messages.message[0].code);
                        //Console.WriteLine("Error message: " + response.messages.message[0].text);
                        serviceResponse.ReturnCode = "0";
                        serviceResponse.ReturnMessage = response.transactionResponse.errors[0].errorText;
                        serviceResponse.TransactionId = "0";

                        info.ErrorCode = response.transactionResponse.errors[0].errorCode;
                        info.ErrorText = response.transactionResponse.errors[0].errorText;
                        info.MessageCode = response.messages.message[0].code;
                        info.MessageText = response.messages.message[0].text;

                        info.ResponseCode = response.transactionResponse.responseCode;
                        info.ResponseDescription = "Failed Transaction. " + response.transactionResponse.errors[0].errorText;
                        info.ResultCode = response.messages.resultCode.ToString();
                        info.TransactionID = "0";
                        //serviceResponse = AuthorizePayments.CustomerPaymentTransactionLog(info);
                    }
                }
            }
            else
            {
                serviceResponse.ReturnCode = "0";
                serviceResponse.ReturnMessage = "Transaction falied.";
                serviceResponse.TransactionId = "0";
                //Console.WriteLine("Null Response.");
            }

            return response;
        }


        public static ANetApiResponse RecurringPaymentsCronJob(String ApiLoginID, String ApiTransactionKey, TransactionCronjobModel donateAPIModel, out CronJobTransactinResponse serviceResponse)
        {
            //string refId;
            //DonateAPIModel donateAPIModel = new DonateAPIModel();
            serviceResponse = new CronJobTransactinResponse();
            //Console.WriteLine("Charge Customer Profile");
            serviceResponse.TransactionId = "0";
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;

            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey
            };

            //create a customer payment profile
            customerProfilePaymentType profileToCharge = new customerProfilePaymentType();
            profileToCharge.customerProfileId = donateAPIModel.CustomerProfileId;
            profileToCharge.paymentProfile = new paymentProfile { paymentProfileId = donateAPIModel.CustomerPaymentProfileId };

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),    // refund type
                amount = donateAPIModel.Amount,
                profile = profileToCharge
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the collector that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            //validate
            if (response != null)
            {
                if (response.messages.resultCode == messageTypeEnum.Ok)
                {
                    if (response.transactionResponse.messages != null)
                    {
                        //serviceResponse.ReturnCode = "1";
                        //serviceResponse.ReturnMessage = response.transactionResponse.messages[0].description;
                        serviceResponse.TransactionId = response.transactionResponse.transId;

                        serviceResponse.ErrorCode = "";
                        serviceResponse.ErrorText = "";
                        serviceResponse.Status = "Paid";

                        //info.MessageCode = response.transactionResponse.messages[0].code;
                        //info.MessageText = response.transactionResponse.messages[0].description;
                        //info.ResponseCode = response.transactionResponse.responseCode;
                        //info.ResponseDescription = "Successfully created transaction with Transaction ID: " + response.transactionResponse.transId;
                        //info.ResultCode = response.messages.resultCode.ToString();
                        //info.TransactionID = response.transactionResponse.transId;
                        //serviceResponse.TransactionId = response.transactionResponse.transId;


                        //string userName = string.Empty, email = string.Empty;
                        //int returnResult = 0;
                        //DataSet ds = PaymentMethodData.DonateTransactionInsert(donateAPIModel);
                        //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        //{
                        //    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["PaymentMethodId"]);
                        //    if (returnResult > 0)
                        //    {
                        //        userName = Convert.ToString(ds.Tables[0].Rows[0]["CardHolderName"]);
                        //        email = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);
                        //        string type = donateAPIModel.IsRecurring.ToLower() == "true" ? "Monthly" : "One-time";
                        //        string duration = donateAPIModel.IsRecurring.ToLower() == "true" ? Convert.ToString(ds.Tables[0].Rows[0]["RecurringType"]) : "";
                        //        AppUserAccess.SendDonationEmail(userName, email, donateAPIModel.Amount.ToString(), type, duration);

                        //    }
                        //}
                        
                    }
                    else
                    {
                        //add history log for Failed Transaction.
                        if (response.transactionResponse.errors != null)
                        {
                            //serviceResponse.ReturnCode = "0";
                            //serviceResponse.ReturnMessage = response.transactionResponse.errors[0].errorText;
                            serviceResponse.TransactionId = "0";
                            serviceResponse.ErrorCode = response.transactionResponse.errors[0].errorCode;
                            serviceResponse.ErrorText = response.transactionResponse.errors[0].errorText;
                            serviceResponse.Status = "Failed";
                            
                            //info.ResponseCode = response.transactionResponse.responseCode;
                            //info.ResponseDescription = response.transactionResponse.errors[0].errorText;
                            //info.ResultCode = response.messages.resultCode.ToString();
                            //info.TransactionID = "0";
                            //serviceResponse = AuthorizePayments.CustomerPaymentTransactionLog(info);
                            //Console.WriteLine("Error Code: " + response.transactionResponse.errors[0].errorCode);
                            //Console.WriteLine("Error message: " + response.transactionResponse.errors[0].errorText);
                        }
                    }
                }
                else
                {
                    //Console.WriteLine("Failed Transaction.");
                    if (response.transactionResponse != null && response.transactionResponse.errors != null)
                    {
                        serviceResponse.TransactionId = "0";
                        serviceResponse.ErrorCode = response.transactionResponse.errors[0].errorCode;
                        serviceResponse.ErrorText = response.transactionResponse.errors[0].errorText;
                        serviceResponse.Status = "Failed";
                        //Commented on 8th April 2019
                        //serviceResponse = AuthorizePayments.CustomerPaymentTransactionLog(info);
                    }
                    else
                    {
                        //Console.WriteLine("Error Code: " + response.messages.message[0].code);
                        //Console.WriteLine("Error message: " + response.messages.message[0].text);
                        serviceResponse.TransactionId = "0";
                        serviceResponse.ErrorCode = response.transactionResponse.errors[0].errorCode;
                        serviceResponse.ErrorText = response.transactionResponse.errors[0].errorText;
                        serviceResponse.Status = "Failed";
                        //serviceResponse = AuthorizePayments.CustomerPaymentTransactionLog(info);
                    }
                }
            }
            else
            {
                serviceResponse.TransactionId = "0";
                serviceResponse.ErrorCode = "";
                serviceResponse.ErrorText = "";
                serviceResponse.Status = "Failed";
                //Console.WriteLine("Null Response.");
            }

            return response;
        }

    }
}
