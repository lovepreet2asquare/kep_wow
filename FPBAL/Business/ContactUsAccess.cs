﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class ContactUsAccess : IContactUs
    {
        public FPInvolvedResponse AddContactUs(EPContactUsRequest request)
        {
            int returnResult = 0;
            FPInvolvedResponse response = new FPInvolvedResponse();
            try
            {
                request.FollowerId = (UtilityAccess.Decrypt(request.FollowerId));
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                //if (request.IsEmailRespnse == "1")
                //{
                //    UtilityAccess.SendEmailForContactUsApi(request);
                //}
                int returnvalues = ContactUsData.AddContactUs(request);
                if (returnvalues > 0)
                {
                    response.ReturnCode = Convert.ToString(returnvalues);
                    response.ReturnMessage = "Thanks! We will contact you soon."; //Response.Message(returnvalues);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ContactUsAccess", "AddContactUs");
                return response;
            }
        }

        public FPInvolvedResponse ChangePassword(ChangePasswordApiModel request)
        {
            FPInvolvedResponse serviceResponse = new FPInvolvedResponse();
            ChangePasswordApiModel model = new ChangePasswordApiModel();
            try
            {
                serviceResponse.ReturnCode = "0";
                serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";
                request.EncryptedSessionToken = UtilityAccess.Decrypt(request.EncryptedSessionToken);

                if (!String.IsNullOrEmpty(request.EncryptedFollowerId))
                {
                    request.EncryptedFollowerId = UtilityAccess.Decrypt(request.EncryptedFollowerId);
                    //request.FollowerId = Convert.ToInt32(request.EncryptedFollowerId);
                }

                if (!string.IsNullOrEmpty(request.Password))
                    request.Password = UtilityAccess.Encrypt(request.Password);


                int returnResult = 0;

                DataSet ds = ContactUsData.ChangePassword(request);
                if (ds != null && ds.Tables.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["ReturnValue"]);
                    string email = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);
                    string FirstName = Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]);

                    if (returnResult == 1)
                    {
                        UtilityAccess.SendEmailForChangepassword(email,FirstName);
                    }
                }
                if (returnResult > 0)
                {
                    serviceResponse.ReturnCode = "1";
                    serviceResponse.ReturnMessage = "Updated successfuly.";
                }
                else if (returnResult == -1)
                {
                    serviceResponse.ReturnCode = "-1";
                    serviceResponse.ReturnMessage = "Technical error.";
                }
                else if (returnResult == -5)
                {
                    serviceResponse.ReturnCode = "-5";
                    serviceResponse.ReturnMessage = "Invalid session token.";
                }

                return serviceResponse;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ContactUsAccess", "ChangePassword");
                return serviceResponse;
            }
        }
    }
}
