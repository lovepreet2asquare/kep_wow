﻿using System;
using System.Collections.Generic;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Contracts;
using AuthorizeNet.Api.Controllers.Bases;
using FPModels.Models;
using FPDAL.Data;
using FPBAL.Interface;
using System.Data;


namespace FPBAL.Business
{
    public class AuthCustomerProfileAccess : IAuthCustomerProfile
    {
        public PaymentMethodsResponse GetPaymentMethods(FPRequests request)
        {
            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            List<PaymentMethodInfo> PaymentMethods = new List<PaymentMethodInfo>();
            PaymentMethodsResponse serviceResponse = new PaymentMethodsResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "No record found.";
            serviceResponse.PaymentMethods = PaymentMethods;
            int returnResult = 0;
            DataSet ds = PaymentMethodData.GetPaymentMethods(request, out returnResult);
            PaymentMethods = LoadPaymentMethods(ds, out returnResult);
            if (PaymentMethods != null && PaymentMethods.Count > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Retrived successfully.";
                serviceResponse.PaymentMethods = PaymentMethods;
            }
            else if(returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";
            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";
            }
            return serviceResponse;
        }

        public PaymentMethodResponse PaymentMethodInsert(CustProfileInfo customerProfileInfo)
        {
           PaymentMethodResponse serviceResponse = new PaymentMethodResponse();
            try
            {
                customerProfileInfo.FollowerId = UtilityAccess.Decrypt(customerProfileInfo.FollowerId);
                customerProfileInfo.SessionToken = UtilityAccess.Decrypt(customerProfileInfo.SessionToken);
                customerProfileInfo.RefId = customerProfileInfo.FollowerId;

                //string apiLogin = ConfigurationManager.AppSettings["ApiLogin"];
                //string transactionKey = ConfigurationManager.AppSettings["TransactionKey"];
                string apiLogin = string.Empty; //ConfigurationManager.AppSettings["ApiLogin"];
                string transactionKey = string.Empty; //ConfigurationManager.AppSettings["TransactionKey"];

                PaymentMethodData.GetAuthorizeKeys(out apiLogin, out transactionKey);
                //apiLogin = "58js2F9kA2";
                //transactionKey = "32meS5MAd7xG258f";
                serviceResponse.ReturnCode = "0";
                serviceResponse.ReturnMessage = string.Empty;
                PaymentMethodInfo paymentMethodInfo = new PaymentMethodInfo();
                serviceResponse.PaymentMethodInfo = paymentMethodInfo;
                //CustProfileInfo customerProfileInfo = new CustProfileInfo();
                //customerProfileInfo.Address1 = request.Address1;
                //customerProfileInfo.Address2 = request.Address2;
                //customerProfileInfo.CardNumber = request.CardNumber.ToString();
                //customerProfileInfo.CardSecurity = request.CardSecurity;
                //customerProfileInfo.City = request.City;
                //customerProfileInfo.Country = request.Country;
                //customerProfileInfo.ExpiryMonth = request.ExpiryMonth;
                //customerProfileInfo.ExpiryYear = request.ExpiryYear;
                //customerProfileInfo.FirstName = request.FirstName;
                //customerProfileInfo.LastName = request.LastName;
                //customerProfileInfo.Mobile = request.Mobile;
                //customerProfileInfo.FollowerId = request.FollowerId;
                //customerProfileInfo.State = request.State;
                //customerProfileInfo.Zipcode = request.Zipcode;
                //customerProfileInfo.CardType = request.CardType;

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                CustomerProfileResponse fpResponse;
                AuthorizeNet.Api.Contracts.V1.ANetApiResponse response = CreateProfileAndPaymentMthod(apiLogin, transactionKey, customerProfileInfo.Email, customerProfileInfo, out fpResponse);

                if (fpResponse != null && fpResponse.ReturnCode == "1")
                {
                    paymentMethodInfo.PaymentMethodId = UtilityAccess.Encrypt(fpResponse.CustomerProfile.PaymentMethodId);
                    paymentMethodInfo.CardNumber = fpResponse.CustomerProfile.CardNumber;
                    paymentMethodInfo.ExpiryMonth = fpResponse.CustomerProfile.ExpiryMonth;
                    paymentMethodInfo.ExpiryYear = fpResponse.CustomerProfile.ExpiryYear;
                    paymentMethodInfo.CardType = customerProfileInfo.CardType;
                    paymentMethodInfo.CustomerProfileId = UtilityAccess.Encrypt(fpResponse.CustomerProfile.CustomerProfileId);
                    paymentMethodInfo.CustomerPaymentProfileId = UtilityAccess.Encrypt(fpResponse.CustomerProfile.CustomerPaymentProfileId);

                    serviceResponse.ReturnCode = fpResponse.ReturnCode;
                    serviceResponse.ReturnMessage = fpResponse.ReturnMessage;
                    serviceResponse.PaymentMethodInfo = paymentMethodInfo;
                }
                else if (fpResponse != null && fpResponse.ReturnCode != "1")
                {
                    if (fpResponse != null && fpResponse.ReturnCode != "-5")
                    {
                        serviceResponse.ReturnCode = fpResponse.ReturnCode;
                        serviceResponse.ReturnMessage = "Authentication failed";
                    }
                    else
                    {
                        serviceResponse.ReturnCode = fpResponse.ReturnCode;
                        serviceResponse.ReturnMessage = fpResponse.ReturnMessage;
                    }
                }
                else
                {
                    serviceResponse.ReturnCode = "0";
                    serviceResponse.ReturnMessage = response.messages.message[0].text;

                }
                if(serviceResponse.ReturnCode == "-5")
                {
                    
                    serviceResponse.ReturnMessage = "Authentication failed";
                    serviceResponse.PaymentMethodInfo = paymentMethodInfo;
                }
                

            }
            catch (Exception ex)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = ex.Message;
            }
            return serviceResponse;
        }

        public FPResponse PaymentMethodDelete(DeletePaymentMethodRequest request)
        {
            FPResponse serviceResponse = new FPResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "No record found to delete.";
            int ReturnResult = 0;
            try
            {
                //string apiLogin = ConfigurationManager.AppSettings["ApiLogin"];
                //string transactionKey = ConfigurationManager.AppSettings["TransactionKey"];
                string apiLogin = string.Empty; //ConfigurationManager.AppSettings["ApiLogin"];
                string transactionKey = string.Empty; //ConfigurationManager.AppSettings["TransactionKey"];

                PaymentMethodData.GetAuthorizeKeys(out apiLogin, out transactionKey);
                //apiLogin = "58js2F9kA2";
                //transactionKey = "32meS5MAd7xG258f";
                request.PaymentMethodId = UtilityAccess.Decrypt(request.PaymentMethodId);
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                request.CustomerProfileId = UtilityAccess.Decrypt(request.CustomerProfileId);
                request.CustomerPaymentProfileId = UtilityAccess.Decrypt(request.CustomerPaymentProfileId);


                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                AuthorizeNet.Api.Contracts.V1.ANetApiResponse response =   DeleteCustomerPaymentProfile.Run(apiLogin, transactionKey, request.CustomerProfileId, request.CustomerPaymentProfileId);
                if (response.messages.resultCode == AuthorizeNet.Api.Contracts.V1.messageTypeEnum.Ok)
                {
                    
                    ReturnResult = PaymentMethodData.PaymentMethodDelete(request);
                    if (ReturnResult == 1)
                    {
                        serviceResponse.ReturnCode = "1";
                        serviceResponse.ReturnMessage = "Payment method deleted successfully";
                    }
                    else
                    {
                        serviceResponse.ReturnCode = "1";
                        serviceResponse.ReturnMessage = "Deleted from Authorize Net but not from database";
                    }
                }
                else
                {
                    serviceResponse.ReturnCode = "0";
                    serviceResponse.ReturnMessage = response.messages.message[0].text;
                }

                
            }
            catch (Exception ex)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = ex.Message;
            }
            return serviceResponse;
        }

        public FPResponse PaymentMethodSetPrimary(SetPrimaryRequest request)
        {
            FPResponse serviceResponse = new FPResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";
            int ReturnResult = 0;
            try
            {
                request.PaymentMethodId = UtilityAccess.Decrypt(request.PaymentMethodId);
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                ReturnResult = PaymentMethodData.PaymentMethodSetPrimary(request);
                if (ReturnResult == 1)
                {
                    serviceResponse.ReturnCode = "1";
                    serviceResponse.ReturnMessage = "Updated successfully";
                }
                else if (ReturnResult == -1)
                {
                    serviceResponse.ReturnCode = "-1";
                    serviceResponse.ReturnMessage = "Technical error.";
                }
                else if (ReturnResult == -5)
                {
                    serviceResponse.ReturnCode = "-5";
                    serviceResponse.ReturnMessage = "Authentication failed.";
                }

            }
            catch (Exception ex)
            {
                serviceResponse.ReturnCode = "-1";
                //serviceResponse.ReturnMessage = ex.Message;
                serviceResponse.ReturnMessage = "Technical error.";
            }
            return serviceResponse;
        }

        public FPResponse Donate(DonateAPIModel request)
        {
            FPResponse serviceResponse = new FPResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";
            CustomerTransactinResponse transResponse = new CustomerTransactinResponse();
           
            try
            {
                request.PaymentMethodId = UtilityAccess.Decrypt(request.PaymentMethodId);
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                request.CustomerPaymentProfileId = UtilityAccess.Decrypt(request.CustomerPaymentProfileId);
                request.CustomerProfileId = UtilityAccess.Decrypt(request.CustomerProfileId);

                string apiLogin = string.Empty; //ConfigurationManager.AppSettings["ApiLogin"];
                string transactionKey = string.Empty; //ConfigurationManager.AppSettings["TransactionKey"];

                FPRequests request2 = new FPRequests();
                request2.FollowerId = request.FollowerId;
                request2.SessionToken = request.SessionToken;
                serviceResponse = AppUserAccess.ValidateSession(request2);
                if (serviceResponse.ReturnCode == "1")
                {
                    PaymentMethodData.GetAuthorizeKeys(out apiLogin, out transactionKey);

                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                    ChargeCustomerProfile.Donate(apiLogin, transactionKey, request, out transResponse);

                    if (transResponse.ReturnCode == "1")
                    {
                        serviceResponse.ReturnCode = "1";
                        serviceResponse.ReturnMessage = "Submitted successfully";


                    }
                    else if (transResponse.ReturnCode == "-1")
                    {
                        serviceResponse.ReturnCode = "-1";
                        serviceResponse.ReturnMessage = "Technical error.";
                    }
                    else if (transResponse.ReturnCode == "-5")
                    {
                        serviceResponse.ReturnCode = "-5";
                        serviceResponse.ReturnMessage = "Authentication failed.";
                    }
                }

            }
            catch (Exception ex)
            {
                serviceResponse.ReturnCode = "-1";
                //serviceResponse.ReturnMessage = ex.Message;
                serviceResponse.ReturnMessage = "Technical error.";
            }
            return serviceResponse;
        }

        //Recurring Payments Cron Job 
        public FPResponse RecurringPaymentsCronJob()
        {
            FPResponse serviceResponse = new FPResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";
            int returnResult = 0;
            string userName = string.Empty, email = string.Empty, status=string.Empty, ErrorText = string.Empty;
            decimal amount = 0;
            int FollowerId = 0, PaymentId = 0, PaymentMethodId = 0;
            CronJobTransactinResponse transResponse = new CronJobTransactinResponse();
            CronJobTransactinResponse transResponse2 = new CronJobTransactinResponse();
            try
            {
                string apiLogin = string.Empty; //ConfigurationManager.AppSettings["ApiLogin"];
                string transactionKey = string.Empty; //ConfigurationManager.AppSettings["TransactionKey"];
                PaymentMethodData.GetAuthorizeKeys(out apiLogin, out transactionKey);
                TransactionCronjobModel request = null;
                List<TransactionCronjobModel> transList = new List<TransactionCronjobModel>();

                //Call to Get Recurring Payments of the month
                DataSet ds = PaymentMethodData.GetRecurringPayments(out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        request = new TransactionCronjobModel();
                        request.Amount = Convert.ToDecimal(row["Amount"]);
                        request.CustomerPaymentProfileId = Convert.ToString(row["CustomerPaymentProfileId"]);
                        request.CustomerProfileId = Convert.ToString(row["CustomerProfileId"]);
                        request.FollowerId = Convert.ToInt32(row["FollowerId"]);
                        request.PaymentId = Convert.ToInt32(row["PaymentId"]);

                        //Send payment to Authorize net gateway
                        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                        ChargeCustomerProfile.RecurringPaymentsCronJob(apiLogin, transactionKey, request, out transResponse);
                        request.Status = transResponse.Status;
                        request.TransactionId = transResponse.TransactionId;
                        request.ErrorCode = transResponse.ErrorCode;
                        request.ErrorText = transResponse.ErrorText;
                        transList.Add(request);
                    }
                    if(transList != null && transList.Count > 0)
                    {
                        //Update Recurring Payment Transaction in our database
                        DataSet ds2 = PaymentMethodData.RecurringPaymentTransactionUpdate(TransData(transList));
                        if(ds2 != null && ds2.Tables.Count > 0 && ds2.Tables[0].Rows.Count > 0)
                        {
                            returnResult = Convert.ToInt32(ds2.Tables[0].Rows[0]["PaymentMethodId"]);

                            if (returnResult > 0)
                            {
                                //Send email to Follower's
                                foreach (DataRow row in ds2.Tables[0].Rows)
                                {
                                    userName = Convert.ToString(row["CardHolderName"]);
                                    email = Convert.ToString(row["Email"]);
                                    status = Convert.ToString(row["Status"]);
                                    ErrorText = Convert.ToString(row["ErrorText"]);
                                    amount = Convert.ToDecimal(row["Amount"]);
                                    FollowerId = Convert.ToInt32(row["FollowerId"]);
                                    PaymentId = Convert.ToInt32(row["PaymentId"]);
                                    PaymentMethodId = Convert.ToInt32(row["PaymentMethodId"]);
                                    string type = "Monthly";
                                    string duration = Convert.ToString(row["RecurringType"]);
                                    FailTransactionCronjobModel request2 = null;
                                    bool IsSuccess = false;
                                    if (status.ToLower() == "unsettled")
                                        AppUserAccess.SendDonationEmail(userName, email, amount.ToString(), type, duration);
                                    else
                                    {

                                        DataSet ds3 = PaymentMethodData.GetFailedPayments(FollowerId, PaymentId, PaymentMethodId, out returnResult);
                                        if(ds3 != null && ds3.Tables.Count > 0 && ds3.Tables[0].Rows.Count > 0)
                                        {
                                            foreach (DataRow row3 in ds3.Tables[0].Rows)
                                            {
                                                request2 = new FailTransactionCronjobModel();
                                                request = new TransactionCronjobModel();
                                                request.Amount = amount;
                                                request.CustomerPaymentProfileId = Convert.ToString(row3["CustomerPaymentProfileId"]);
                                                request.CustomerProfileId = Convert.ToString(row3["CustomerProfileId"]);
                                                request.FollowerId = FollowerId;
                                                request.PaymentId = PaymentId;
                                                request2.FollowerId = FollowerId;
                                                request2.PaymentId = PaymentId;
                                                
                                                //Send payment to Authorize net gateway
                                                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                                                ChargeCustomerProfile.RecurringPaymentsCronJob(apiLogin, transactionKey, request, out transResponse2);
                                                request.Status = transResponse2.Status;
                                                request.TransactionId = transResponse2.TransactionId;
                                                request.ErrorCode = transResponse2.ErrorCode;
                                                request.ErrorText = transResponse2.ErrorText;

                                                request2.Status = transResponse2.Status;
                                                request2.TransactionId = transResponse2.TransactionId;
                                                request2.ErrorCode = transResponse2.ErrorCode;
                                                request2.ErrorText = transResponse2.ErrorText;

                                                if (transResponse2.Status.ToLower() == "paid")
                                                {
                                                    
                                                    DataSet ds4 = PaymentMethodData.RecurringFailedPaymentTransactionUpdate(request2, out returnResult);
                                                    if (ds4 != null && ds4.Tables.Count > 0 && ds4.Tables[0].Rows.Count > 0)
                                                    {
                                                        returnResult = Convert.ToInt32(ds4.Tables[0].Rows[0]["PaymentMethodId"]);

                                                        if (returnResult > 0)
                                                        {
                                                            //Send email to Follower's
                                                            foreach (DataRow row4 in ds4.Tables[0].Rows)
                                                            {
                                                                userName = Convert.ToString(row["CardHolderName"]);
                                                                email = Convert.ToString(row["Email"]);
                                                                status = Convert.ToString(row["Status"]);
                                                                ErrorText = Convert.ToString(row["ErrorText"]);
                                                                amount = Convert.ToDecimal(row["Amount"]);
                                                                FollowerId = Convert.ToInt32(row["FollowerId"]);
                                                                PaymentId = Convert.ToInt32(row["PaymentId"]);
                                                                PaymentMethodId = Convert.ToInt32(row["PaymentMethodId"]);
                                                                type = "Monthly";
                                                                duration = Convert.ToString(row["RecurringType"]);

                                                                //if (status.ToLower() == "paid")
                                                                {
                                                                    AppUserAccess.SendDonationEmail(userName, email, amount.ToString(), type, duration);
                                                                    IsSuccess = true;
                                                                }

                                                                //else
                                                                //{

                                                                //}
                                                            }
                                                        }

                                                    }
                                                }
                                                if (IsSuccess)
                                                    break;
                                                
                                            }
                                            if(!IsSuccess)
                                                AppUserAccess.SendRecurreingFailEmail(userName, email, amount.ToString(), type, duration, ErrorText);
                                        }
                                        else
                                        if (status.ToLower() == "failed")
                                            AppUserAccess.SendRecurreingFailEmail(userName, email, amount.ToString(), type, duration, ErrorText);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                serviceResponse.ReturnCode = "-1";
                //serviceResponse.ReturnMessage = ex.Message;
                serviceResponse.ReturnMessage = "Technical error.";
            }
            return serviceResponse;
        }

        private DataTable TransData(List<TransactionCronjobModel> transData)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("PaymentId", typeof(Int32));
            dt.Columns.Add("TransactionId", typeof(String));
            dt.Columns.Add("Status", typeof(String));
            dt.Columns.Add("ErrorCode", typeof(String));
            dt.Columns.Add("ErrorText", typeof(String));
            dt.Columns.Add("Id", typeof(Int32));
            int Id = 1;
            DataRow row = null;
            if (transData != null && transData.Count > 0)
            {
                foreach (var item in transData)
                {
                    {
                        row = dt.NewRow();
                        row["PaymentId"] = item.PaymentId;
                        row["TransactionId"] = item.TransactionId ?? "";
                        row["Status"] = item.Status ?? "";
                        row["ErrorCode"] = item.ErrorCode ?? "";
                        row["ErrorText"] = item.ErrorText ?? "";
                        row["Id"] = Id;
                        Id = Id + 1;
                        dt.Rows.Add(row);
                    }
                }
            }
            return dt;
        }

        public static ANetApiResponse CreateProfileAndPaymentMthod(string ApiLoginID, string ApiTransactionKey, string emailId, CustProfileInfo parkeeeUserCreditCardObj, out CustomerProfileResponse parkeeeResponse)
        {
            int returnResult = 0;
            parkeeeResponse = new CustomerProfileResponse();
            //Console.WriteLine("CreateCustomerProfile Sample");

            //ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey,
            };
            string expiryMonth = parkeeeUserCreditCardObj.ExpiryMonth.ToString().Length == 1 ? "0" + parkeeeUserCreditCardObj.ExpiryMonth.ToString() : parkeeeUserCreditCardObj.ExpiryMonth.ToString();

            string expiryYear = parkeeeUserCreditCardObj.ExpiryYear.ToString().Length == 4 ? parkeeeUserCreditCardObj.ExpiryYear.ToString().Substring(parkeeeUserCreditCardObj.ExpiryYear.ToString().Length - 2) : parkeeeUserCreditCardObj.ExpiryYear.ToString().Substring(parkeeeUserCreditCardObj.ExpiryYear.ToString().Length - 2);

            var creditCard = new creditCardType
            {
                cardNumber = parkeeeUserCreditCardObj.CardNumber.ToString(),
                expirationDate = expiryMonth + expiryYear,
                cardCode = parkeeeUserCreditCardObj.CardSecurity
            };
            string address = parkeeeUserCreditCardObj.Address1 + " " + parkeeeUserCreditCardObj.Address2 ?? string.Empty;
            address = address.Trim();

            var billTo = new customerAddressType
            {
                firstName = parkeeeUserCreditCardObj.FirstName,
                lastName = parkeeeUserCreditCardObj.LastName,
                address = address,
                city = parkeeeUserCreditCardObj.City,
                state = parkeeeUserCreditCardObj.State,
                zip = parkeeeUserCreditCardObj.Zipcode,
                country = parkeeeUserCreditCardObj.Country,
            };



            //standard api call to retrieve response
            paymentType cc = new paymentType { Item = creditCard }; //or bankAccount
            //paymentType echeck = new paymentType { Item = bankAccount };

            List<customerPaymentProfileType> paymentProfileList = new List<customerPaymentProfileType>();
            customerPaymentProfileType ccPaymentProfile = new customerPaymentProfileType();
            ccPaymentProfile.payment = cc;
            ccPaymentProfile.billTo = billTo;
            //for bank account echeck
            //customerPaymentProfileType echeckPaymentProfile = new customerPaymentProfileType();
            //echeckPaymentProfile.payment = echeck;

            paymentProfileList.Add(ccPaymentProfile);
            //paymentProfileList.Add(echeckPaymentProfile);

            List<customerAddressType> addressInfoList = new List<customerAddressType>();
            addressInfoList.Add(billTo);


            customerProfileType customerProfile = new customerProfileType();
            customerProfile.merchantCustomerId = parkeeeUserCreditCardObj.FollowerId.ToString();
            customerProfile.email = emailId;
            customerProfile.paymentProfiles = paymentProfileList.ToArray();
            customerProfile.shipToList = addressInfoList.ToArray();

            var request = new createCustomerProfileRequest { refId = parkeeeUserCreditCardObj.FollowerId.ToString(), profile = customerProfile, validationMode = validationModeEnum.none };

            var controller = new createCustomerProfileController(request);          // instantiate the contoller that will call the service
            controller.Execute();

            createCustomerProfileResponse response = controller.GetApiResponse();   // get the response from the service (errors contained if any)

            //validate
            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                if (response != null && response.messages.message != null)
                {
                    var paymentProfileRequest = new getCustomerPaymentProfileRequest();
                    paymentProfileRequest.customerProfileId = response.customerProfileId;
                    paymentProfileRequest.customerPaymentProfileId = response.customerPaymentProfileIdList[0];
                    // instantiate the controller that will call the service
                    var controller2 = new getCustomerPaymentProfileController(paymentProfileRequest);
                    controller2.Execute();

                    // get the response from the service (errors contained if any)
                    var response2 = controller2.GetApiResponse();
                    if (response2 != null && response2.messages.resultCode == messageTypeEnum.Ok)
                    {
                        //Console.WriteLine(response.messages.message[0].text);
                        //Console.WriteLine("Customer Payment Profile Id: " + response.paymentProfile.customerPaymentProfileId);
                        if (response2.paymentProfile.payment.Item is creditCardMaskedType)
                        {
                            parkeeeUserCreditCardObj.CardNumber = (response2.paymentProfile.payment.Item as creditCardMaskedType).cardNumber;
                        }
                    }
                    else if (response != null)
                    {
                        //Console.WriteLine("Error: " + response.messages.message[0].code + "  " +
                        //                  response.messages.message[0].text);
                    }

                    parkeeeUserCreditCardObj.CustomerPaymentProfileId = response.customerPaymentProfileIdList[0];
                    parkeeeUserCreditCardObj.CustomerProfileId = response.customerProfileId;
                    parkeeeUserCreditCardObj.MessageCode = response.messages.message[0].code;
                    parkeeeUserCreditCardObj.MessageText = response.messages.message[0].text;
                    parkeeeUserCreditCardObj.ResultCode = response.messages.resultCode.ToString();

                    //CustomerProfileInfo customerInfo = new CustomerProfileInfo();
                    //customerInfo.Card_ID = payorUserCreditCardObj.Card_Id;
                    //customerInfo.PayorId = payorUserCreditCardObj.PayorId;
                    //customerInfo.Card_No = "";
                    //customerInfo.CustomerPaymentProfileId = response.customerPaymentProfileIdList[0];
                    //customerInfo.CustomerProfileId = response.customerProfileId;
                    //customerInfo.MerchantCustomerId = payorUserCreditCardObj.PayorId;
                    //customerInfo.MessageCode = response.messages.message[0].code;
                    //customerInfo.MessageText = response.messages.message[0].text;
                    //customerInfo.ResultCode = response.messages.resultCode.ToString();
                    //customerInfo.ValidationMode = validationModeEnum.none.ToString();
                    //PayorService service = new PayorService();

                    //Commented on 5th sept 2018
                    //Commented on 8th April 2019
                    returnResult = PaymentMethodData.CustomerProfileInsert(parkeeeUserCreditCardObj);
                    if(returnResult > 0)
                    {
                        parkeeeResponse.ReturnCode = "1";
                        parkeeeResponse.ReturnMessage = "Payment method added successfully.";
                        parkeeeUserCreditCardObj.PaymentMethodId = returnResult.ToString();
                        parkeeeResponse.CustomerProfile = parkeeeUserCreditCardObj;
                    }
                    else
                    {
                        parkeeeResponse.ReturnCode = returnResult.ToString();
                    }
                    //Console.WriteLine("Success, CustomerProfileID : " + response.customerProfileId);
                    //Console.WriteLine("Success, CustomerPaymentProfileID : " + response.customerPaymentProfileIdList[0]);
                    //Console.WriteLine("Success, CustomerShippingProfileID : " + response.customerShippingAddressIdList[0]);
                }
            }
            else if (response != null)
            {
                if (response.messages.message[0].code == "E00039")
                {
                    //A duplicate record with ID 1810355468 already exists.
                    string responseMsgText = response.messages.message[0].text;
                    responseMsgText = responseMsgText.Replace("A duplicate record with ID", "").Replace("already exists.", "");
                    string customerProfileId = responseMsgText.Trim();

                    //Commented on 8th April 2019
                    var paymentProfileResponse = CreateCustomerPaymentProfile.Run(ApiLoginID, ApiTransactionKey, customerProfileId, ref parkeeeUserCreditCardObj);
                    if (paymentProfileResponse.messages.message[0].code == "E00039")
                    {
                        parkeeeResponse.ReturnCode = "-2";
                        parkeeeResponse.ReturnMessage = "Payment method already exists.";
                    }
                    else
                    {
                        //Commented on 5th sept 2018
                        //Commented on 8th April 2019

                        returnResult = PaymentMethodData.CustomerProfileInsert(parkeeeUserCreditCardObj);
                        if (returnResult > 0)
                        {
                            parkeeeResponse.ReturnCode = "1";
                            parkeeeResponse.ReturnMessage = "Payment method added successfully.";
                            parkeeeUserCreditCardObj.PaymentMethodId = returnResult.ToString();
                            parkeeeResponse.CustomerProfile = parkeeeUserCreditCardObj;
                        }
                        else
                        {
                            parkeeeResponse.ReturnCode = returnResult.ToString();
                        }

                    }

                }
                else
                {
                    //payorResponse.ReturnMessage = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text;

                    //ADD history log id FAIL to create profile

                }
                //Console.WriteLine("Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text);
            }

            return response;
        }


        private List<PaymentMethodInfo> LoadPaymentMethods(DataSet ds, out int returnResult)
        {
            returnResult = 0;
            List<PaymentMethodInfo> PaymentMethods = new List<PaymentMethodInfo>();
            PaymentMethodInfo paymentMethod = null;
            try
            {
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        paymentMethod = new PaymentMethodInfo();
                        returnResult = Convert.ToInt32(row["PaymentMethodId"]);
                        if (returnResult > 0)
                        {
                            paymentMethod.PaymentMethodId = UtilityAccess.Encrypt(returnResult.ToString());
                            paymentMethod.CardNumber = Convert.ToString(row["CardNumber"]);
                            paymentMethod.CustomerProfileId = UtilityAccess.Encrypt(Convert.ToString(row["CustomerProfileId"]));
                            paymentMethod.CustomerPaymentProfileId = UtilityAccess.Encrypt(Convert.ToString(row["CustomerPaymentProfileId"]));
                            paymentMethod.CardType = Convert.ToString(row["CardType"]);
                            paymentMethod.ExpiryMonth = Convert.ToString(Convert.ToInt32(row["ExpiryMonth"]));
                            paymentMethod.ExpiryYear = Convert.ToString(Convert.ToInt32(row["ExpiryYear"]));
                            paymentMethod.IsPrimary = Convert.ToBoolean(row["IsPrimary"]);
                            PaymentMethods.Add(paymentMethod);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
            }
            return PaymentMethods;
        }

    }
}
