﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Threading;

namespace FPBAL.Business
{
    public class DirectoryAccess: IDirectory
    {
        DirectoryData directoryData = new DirectoryData();
        public DirectoryResponse SelectAll(DirectoryModel model)
        {
            DirectoryResponse directoryResponse = new DirectoryResponse();
            directoryResponse.ReturnCode = 0;
            directoryResponse.ReturnMessage = Response.Message(0);
            directoryResponse._directoryModel = new DirectoryModel();

            DirectoryModel directoryInfo = new DirectoryModel();
            Int32 returnResult = 0;

            try
            {
                
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

                DataSet ds = directoryData.SelectAll(model, out returnResult);

                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                           directoryResponse._directoryModel._DirectoryList = DirectoryList(ds.Tables[0], returnResult);
                           returnResult = 2;

                    }
                    if (directoryResponse._directoryModel._DirectoryList != null && directoryResponse._directoryModel._DirectoryList.Count > 0)
                    {

                        foreach (var row in directoryResponse._directoryModel._DirectoryList)
                        {
                            directoryResponse._directoryModel.PageCount = row.PageCount;
                        }

                    }
                }

                else
                {
                    directoryResponse.ReturnCode = 0;
                    directoryResponse.ReturnMessage = "No Data Found";
                }
                directoryResponse._directoryModel._StatusList = UtilityAccess.ActiveStatusList(2);
                directoryResponse.ReturnCode = returnResult;
                directoryResponse.ReturnMessage = Response.Message(returnResult);
                
                return directoryResponse;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryAccess", "SelectAll");
            }
            return directoryResponse;
        }

        public DirectoryResponse SelectAll1(DirectoryModel model)
        {
            DirectoryResponse directoryResponse = new DirectoryResponse();
            directoryResponse.ReturnCode = 0;
            directoryResponse.ReturnMessage = Response.Message(0);
            directoryResponse._directoryModel = new DirectoryModel();

            DirectoryModel directoryInfo = new DirectoryModel();
            Int32 returnResult = 0;

            try
            {

                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

                DataSet ds = directoryData.SelectAllForSendMail(model, out returnResult);

                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        directoryResponse._directoryModel._DirectoryList = DirectoryList1(ds.Tables[0], returnResult);
                        returnResult = 2;

                    }
                }

                else
                {
                    directoryResponse.ReturnCode = 0;
                    directoryResponse.ReturnMessage = "No Data Found";
                }
                directoryResponse._directoryModel._StatusList = UtilityAccess.ActiveStatusList(2);
                directoryResponse.ReturnCode = returnResult;
                directoryResponse.ReturnMessage = Response.Message(returnResult);

                return directoryResponse;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryAccess", "SelectAll");
            }
            return directoryResponse;
        }
        public DirectoryResponse LogSelectAll(DirectoryModel model)
        {
            DirectoryResponse response = new DirectoryResponse();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            response._directoryModel = new DirectoryModel();
            Int32 returnResult = 0;
            try
            {
                model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

                DataSet ds = directoryData.LogSelectAll(model, out returnResult);

                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        response._directoryModel._LogBookList = LogList(ds.Tables[0], returnResult);
                        returnResult = 2;

                    }
                }

                else
                {
                    response.ReturnCode = 0;
                    response.ReturnMessage = "No Data Found";
                }
                DocumentModel docinfo = new DocumentModel();
                //response._directoryModel._StatusList = UtilityAccess.ActiveStatusList(2);
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryAccess", "SelectAll");
            }
            return response;
        }

        private List<DirectoryModel> LogList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<DirectoryModel> data = new List<DirectoryModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new DirectoryModel
                            {

                                //EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"])),
                                FirstName = Convert.ToString(row["UploadBy"]),
                                CreateDate = Convert.ToString(row["CreateDate"]),
                                FileName = Convert.ToString(row["Filename"]),
                                NewAdded = Convert.ToString(row["NewAddedCount"]),
                                Updated = Convert.ToString(row["UpdatedCount"]),
                                Duplicate = Convert.ToString(row["DuplicateCount"]),

                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryModel", "DirectoryList");
                ReturnResult = -1;
                return null;
            }
        }
        private List<DirectoryModel> DirectoryList1(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<DirectoryModel> data = new List<DirectoryModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new DirectoryModel
                            {

                                EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"])),
                                FirstName = Convert.ToString(row["FirstName"] ?? 0),
                                Email = Convert.ToString(row["Email"] ?? string.Empty),
                               
                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryModel", "DirectoryList");
                ReturnResult = -1;
                return null;
            }
        }
        private List<DirectoryModel> DirectoryList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<DirectoryModel> data = new List<DirectoryModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new DirectoryModel
                            {
                               
                            EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"])),
                            Local = Convert.ToString(row["Local"]),
                            FirstName = Convert.ToString(row["FirstName"] ?? 0),
                            MI = Convert.ToString(row["MI"] ?? string.Empty),
                            LastName = Convert.ToString(row["LastName"] ?? string.Empty),
                            AddressLine1 = Convert.ToString(row["AddressLine1"] ?? string.Empty),
                            AddressLine2 = Convert.ToString(row["AddressLine2"] ?? string.Empty),
                            CityName = Convert.ToString(row["City"] ?? string.Empty),
                            StateId = Convert.ToInt32(row["StateId"] ?? 0),
                            District = Convert.ToString(row["District"]),
                            Zip = Convert.ToString(row["Zip"] ?? 0),
                            CountryId = Convert.ToInt32(row["CountryId"] ?? 0),
                            County = Convert.ToString(row["County"] ?? string.Empty),
                            Mobile = Convert.ToString(row["Mobile"] ?? string.Empty),
                            Telephone = Convert.ToString(row["Telephone"] ?? string.Empty),
                            PageCount = Convert.ToInt32(row["Total"] ?? 0),
                            Email = Convert.ToString(row["Email"] ?? string.Empty),
                            Title = Convert.ToString(row["Title"] ?? string.Empty),
                            MemberTypeId = Convert.ToInt32(row["MemberTypeId"] ?? string.Empty),
                            UniqueId = Convert.ToString(row["UniqueId"] ?? string.Empty),
                            ConDis = Convert.ToString(row["ConDis"] ?? string.Empty),
                            BirthDay = Convert.ToString(row["DOB"] ?? string.Empty),
                            DL = Convert.ToString(row["DL"] ?? string.Empty),
                            Status = Convert.ToString(row["Status"] ?? string.Empty),
                            LogoPath = Convert.ToString(row["ProfilePic"] ?? string.Empty),
                            ModifyDate = Convert.ToString(row["ModifyDate"] ?? string.Empty),
                            IsVerified = Convert.ToBoolean(row["IsVerified"] ?? string.Empty),

                        });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryModel", "DirectoryList");
                ReturnResult = -1;
                return null;
            }
        }
        public DirectoryResponse SelectById(DirectoryModel model)
        {
            DirectoryResponse directoryResponse = new DirectoryResponse();
            DirectoryModel directoryInfo = new DirectoryModel();
            directoryResponse._directoryModel = new DirectoryModel();
            directoryResponse.ReturnCode = 0;
            directoryResponse.ReturnMessage = Response.Message(0);
            int returnResult = 0;
            int userid = 0;
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.LoggedUserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedLoggedId));
            userid = model.LoggedUserId;
            if (model.EncryptedUserId == null)
            {
                model.UserId = 0;
            }
            else
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            }
            DataSet ds = directoryData.SelectById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {
                directoryInfo = new DirectoryModel();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        directoryResponse._directoryModel = new DirectoryModel();
                        directoryResponse._directoryModel.Local = Convert.ToString(row["Local"] ?? 0);
                        directoryResponse._directoryModel.FirstName = Convert.ToString(row["FirstName"] ?? 0);
                        directoryResponse._directoryModel.MI = Convert.ToString(row["MI"] ?? string.Empty);
                        directoryResponse._directoryModel.LastName = Convert.ToString(row["LastName"] ?? string.Empty);
                        directoryResponse._directoryModel.AddressLine1 = Convert.ToString(row["AddressLine1"] ?? string.Empty);
                        directoryResponse._directoryModel.AddressLine2 = Convert.ToString(row["AddressLine2"] ?? string.Empty);
                        directoryResponse._directoryModel.CityName = Convert.ToString(row["City"] ?? string.Empty);
                        directoryResponse._directoryModel.StateId = Convert.ToInt32(row["StateId"] ?? 0);
                        directoryResponse._directoryModel.District = Convert.ToString(row["District"]);
                        directoryResponse._directoryModel.Zip = Convert.ToString(row["Zip"] ?? 0);
                        directoryResponse._directoryModel.CountryId = Convert.ToInt32(row["CountryId"] ?? 0);
                        directoryResponse._directoryModel.County = Convert.ToString(row["County"] ?? 0);
                        directoryResponse._directoryModel.Mobile = Convert.ToString(row["Mobile"] ?? string.Empty);
                        directoryResponse._directoryModel.Telephone = Convert.ToString(row["Telephone"] ?? string.Empty);
                        directoryResponse._directoryModel.Email = Convert.ToString(row["Email"] ?? string.Empty);
                        directoryResponse._directoryModel.Title = Convert.ToString(row["Title"] ?? string.Empty);
                        directoryResponse._directoryModel.TitleId = Convert.ToInt32(row["TitleId"] ?? string.Empty);
                        directoryResponse._directoryModel.MemberTypeId = Convert.ToInt32(row["MemberTypeId"] ?? string.Empty);
                        directoryResponse._directoryModel.UniqueId = Convert.ToString(row["UniqueId"] ?? string.Empty);
                        directoryResponse._directoryModel.ConDis = Convert.ToString(row["ConDis"] ?? string.Empty);
                        directoryResponse._directoryModel.BirthDay = Convert.ToString(row["DOB"] ?? string.Empty);
                        directoryResponse._directoryModel.DL = Convert.ToString(row["DL"] ?? string.Empty);
                        directoryResponse._directoryModel.Status = Convert.ToString(row["Status"] ?? string.Empty);
                        directoryResponse._directoryModel.IsVerified = Convert.ToBoolean(row["IsVerified"] ?? string.Empty);
                        directoryResponse._directoryModel.LogoPath = Convert.ToString(row["ProfilePic"] ?? string.Empty);
                        directoryResponse._directoryModel.ISDCode = Convert.ToString(row["ISDCode"] ?? string.Empty);
                        directoryResponse._directoryModel.TeleISDCode = Convert.ToString(row["TeleIsdCode"] ?? string.Empty);
                        directoryResponse._directoryModel.EncryptedUserId =UtilityAccess.Encrypt(Convert.ToString(row["UserId"]) ?? string.Empty);
                        directoryResponse._directoryModel.UserId =Convert.ToInt32(row["UserId"]);
                        directoryResponse._directoryModel.LogoPath =Convert.ToString(row["ProfilePic"]) ?? string.Empty;
                        if(directoryResponse._directoryModel.LogoPath !="")
                        {
                            //directoryResponse._directoryModel.LogoPath = "https://njspba.keplerconnect.com/" + directoryResponse._directoryModel.LogoPath;
                        }

                        directoryResponse._directoryModel.ModifyDate =Convert.ToString(row["ModifyDate"]) ?? string.Empty;
                        directoryResponse._directoryModel.CreateDate =Convert.ToString(row["CreateDate"]) ?? string.Empty;
                        directoryResponse._directoryModel.LoggedUserId = userid;

                    }

                }
                
                directoryResponse._directoryModel._StateList = UtilityAccess.RenderList(ds.Tables[1],1);
                directoryResponse._directoryModel._MemberTypeList = UtilityAccess.RenderList(ds.Tables[2],1);
                directoryResponse._directoryModel._TitleList = UtilityAccess.RenderList(ds.Tables[3],1);
                directoryResponse._directoryModel._StatusList = UtilityAccess.ActiveStatusList(1);
                directoryResponse._directoryModel._ISDCodeList = UtilityAccess.RenderISDCodeList(ds.Tables[0], 1);
                directoryResponse.ReturnCode = returnResult;
                directoryResponse.ReturnMessage = Response.Message(returnResult);
                //directoryResponse._directoryModel = directoryInfo;
            }
            return directoryResponse;
        }
        public DirectoryResponse AddorEdit(DirectoryModel model)
        {
            Int32 returnResult = 0;
            DirectoryResponse response = new DirectoryResponse();
            response._directoryModel = new DirectoryModel();
            int User_Id = model.UserId;
            // default response message
             response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                
                if (!string.IsNullOrEmpty(model.EncryptedUserId))
                {
                    model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                }
                model.LoggedUserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedLoggedId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                string timeZoneid = "Eastern Standard Time";
                double timezoneSeconds = UtilityAccess.GetTimeZoneInSeconds(DateTime.UtcNow, timeZoneid);
                model.TimeOffSet = Convert.ToInt32(timezoneSeconds);


                DataSet ds = directoryData.AddorEdit(model,out returnResult);

                if (returnResult == -4)
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = "Email already exists";
                }
                else
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }
              if(User_Id==0)
                {
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        DirectoryModel directoryInfo = new DirectoryModel();
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                response._directoryModel.Email = Convert.ToString(row["Email"]);
                                response._directoryModel.UserId= Convert.ToInt32(row["Userid"]);
                                response._directoryModel.FirstName = Convert.ToString(row["FirstName"]);

                            }
                         SingleInvite(response._directoryModel.UserId, response._directoryModel.FirstName, response._directoryModel.Email);
                        }
                    }
                                
                            }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "HeadlineAccess", "AddorEdit");
                return response;
            }

        }
        public DirectoryResponse UploadHttpPostedFile(DirectoryModel model)
        {
            DirectoryAccess directoryaccess = new DirectoryAccess();
            DirectoryResponse response= new DirectoryResponse();
            model.LoggedUserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedLoggedId));

           Int32 ReturnValue = 0;
            response.ReturnCode = 0;
            string returnMessage = string.Empty;
            //response.ReturnMessage = "Failed";
            try
            {
                if (model.HttpPostedFile != null)
                {
                    // Insert data into database
                    response._directoryModel = new DirectoryModel();
                    response._inviteModel = new InviteModel();

                    string fileName = string.Empty;
                    DirectoryModel Model = new DirectoryModel();
                    InviteModel invite = new InviteModel();
                    String _FilePath = "";
                    //read excel file
                    Model.dtImportExcel = ReadExcelFile(model.HttpPostedFile, out _FilePath, out fileName, out returnMessage);
                    bool IsLength = true;
                    var regexItem = new Regex("^[a-zA-Z0-9 ]*$");
                    string format = string.Empty;
                    //add & get inserted data 
                    if (Model != null && Model.dtImportExcel != null)
                    {
                        foreach(DataRow row in Model.dtImportExcel.Rows)
                        {
                            format = string.Empty;
                            if (Convert.ToString(row["SSN"]).Length > 4)
                                IsLength = false;
                            if (Convert.ToString(row["SSN"]).Length <2)
                                IsLength = false;
                        }
                        if (IsLength)
                        {
                            Model.FilePath = _FilePath;
                            Model.LoggedUserId = model.LoggedUserId;
                            Model.InsertOnly = model.InsertOnly;
                            DataSet ds = directoryData.ExcellDataAdd(Model, fileName, out ReturnValue);

                            if (ReturnValue == -1 || !string.IsNullOrWhiteSpace(returnMessage))
                            {
                                if (ReturnValue == -1 && string.IsNullOrWhiteSpace(returnMessage))
                                    returnMessage = "Please check file format";
                                response.ReturnMessage = returnMessage;
                                response._directoryModel.FileNotAdded = 1;
                                response._directoryModel.ErrorMessage = returnMessage;
                            }
                            else
                            {
                                if (ds != null && ds.Tables.Count > 0)
                                {

                                    invite = new InviteModel();
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        string msg = "";
                                        foreach (DataRow row in ds.Tables[0].Rows)
                                        {

                                            response._directoryModel.FileNotAdded = 1;

                                            
                                            if (msg=="")
                                            {
                                                msg = Convert.ToString(row["Email"]);
                                            }
                                            else
                                            {
                                                msg = msg + "," + Convert.ToString(row["Email"]);
                                            }
                                            
                                           
                                        }
                                        response._directoryModel.ErrorMessage = "Email(s) already exist: "+msg ;
                                    }
                                    else {
                                        //if (ds != null && ds.Tables.Count == 3)
                                        //{

                                        //    invite = new InviteModel();

                                        //    if (ds.Tables[2].Rows.Count > 0)
                                        //    {
                                        //        foreach (DataRow row in ds.Tables[2].Rows)
                                        //        {


                                        //            response._inviteModel._InviteEmail = InviteEmail(ds.Tables[2], ReturnValue);
                                        //        }
                                        //    }
                                        //     response = directoryaccess.MultipleInvite(response._inviteModel);
                                        //}
                                        if (ds != null && ds.Tables.Count > 0)
                                        {

                                            model = new DirectoryModel();
                                            if (ds.Tables[1].Rows.Count > 0)
                                            {
                                                foreach (DataRow row in ds.Tables[1].Rows)
                                                {
                                                    response._directoryModel = InviteDataFile(ds);
                                                }
                                            }
                                        }

                                    }
                                }

                              
                            }
                          
                        }
                        else
                        {
                            response._directoryModel.FileNotAdded = 1;
                            response._directoryModel.ErrorMessage = "SSN length should be 4 digit!";
                        }
                    }
                    else
                    {
                        response._directoryModel.FileNotAdded = 1;
                        response._directoryModel.ErrorMessage = returnMessage;
                    }
                }

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryAccess", "UploadHttpPostedFile");

            }
            return response;

        }
        public DataTable ReadExcelFile(HttpPostedFileBase httpPostedFile, out String FilePath, out string filename2, out string errorMessage)
        {
            filename2 = string.Empty;
            errorMessage = string.Empty;
            DataSet ds = null;
            DataTable dtResult = null;
            FilePath = "";
            
            try
            {
                if (httpPostedFile != null)
                {
                    ds = new DataSet();
               
                    if (httpPostedFile.ContentLength > 0)
                    {
                        String fileExtension = System.IO.Path.GetExtension(httpPostedFile.FileName);
                        FilePath = Convert.ToString(DateTimeOffset.UtcNow.ToUnixTimeSeconds());

                        if (fileExtension == ".xls" || fileExtension == ".xlsx")
                        {
                            String fileLocation =
                                System.Web.HttpContext.Current.Server.MapPath("~/App_Data/DataFiles/") + FilePath;
                            

                            if (!System.IO.File.Exists(fileLocation))
                            {
                                System.IO.Directory.CreateDirectory(fileLocation);
                            }

                            filename2 = httpPostedFile.FileName;

                            fileLocation += "/" + Path.GetFileName(httpPostedFile.FileName);
                            // save file path into database
                            FilePath += Path.GetFileName(httpPostedFile.FileName);

                            httpPostedFile.SaveAs(fileLocation);
                            String excelConnectionString = String.Empty;

                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                            fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                            //connection String for xls file format.
                            if (fileExtension == ".xls")
                            {
                                excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                                fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                            }
                            //connection String for xlsx file format.
                            else if (fileExtension == ".xlsx")
                            {
                                excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                                fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                            }
                            //Create Connection to Excel work book and add oledb namespace
                            OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                            excelConnection.Open();
                            DataTable dt = new DataTable();

                            dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            if (dt == null)
                            {
                                return null;
                            }
              
                            String[] excelSheets = new String[dt.Rows.Count];
                            Int32 t = 0;
                            //excel data saves in temp file here.
                            foreach (DataRow row in dt.Rows)
                            {
                                excelSheets[t] = row["TABLE_NAME"].ToString();
                                t++;
                            }

                            OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);

                            String query = String.Format("Select * from [{0}]", excelSheets[0]);
                            using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                            {
                                dataAdapter.Fill(ds);
                            }

                            
                                excelConnection.Close();
                        }

                        if (fileExtension.ToString().ToLower().Equals(".xml"))
                        {
                            String fileLocation = System.Web.HttpContext.Current.Server.MapPath("~/Content/") + httpPostedFile.FileName; //System.Web.HttpContext.Current.Request.Files["FileUpload"].FileName;
                            if (System.IO.File.Exists(fileLocation))
                            {
                                System.IO.File.Delete(fileLocation);
                            }
                            httpPostedFile.SaveAs(fileLocation);

                            //System.Web.HttpContext.Current.Request.Files["FileUpload"].SaveAs(fileLocation);

                            XmlTextReader xmlreader = new XmlTextReader(fileLocation);
                            // DataSet ds = new DataSet();
                            ds.ReadXml(xmlreader);
                            xmlreader.Close();
                           
                        }


                    }
                }
               
                dtResult = dtImportExcel(ds, out errorMessage);
                return dtResult;
            }
            catch (Exception ex)
            {

                ApplicationLogger.LogError(ex, "DirectoryAccess", "ReadExcelFile");
                return null;
            }

        }
        private DataTable dtImportExcel(DataSet ds)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Local", typeof(String));
            dt.Columns.Add("FirstName", typeof(String));
            dt.Columns.Add("MiddleInitial", typeof(String));
            dt.Columns.Add("LastName", typeof(String));
            dt.Columns.Add("AddressLine1", typeof(String));
            dt.Columns.Add("AddressLine2", typeof(String));
            dt.Columns.Add("City", typeof(String));
            dt.Columns.Add("District", typeof(String));
            dt.Columns.Add("Zip", typeof(String));
            dt.Columns.Add("County", typeof(String));
            dt.Columns.Add("ISDCode", typeof(String));
            dt.Columns.Add("MobileNumber", typeof(String));
            dt.Columns.Add("TeleIsdCode", typeof(String));
            dt.Columns.Add("Telephone", typeof(String));
            dt.Columns.Add("Email", typeof(String));
            dt.Columns.Add("Title", typeof(String));
           // dt.Columns.Add("MemberType", typeof(String));
            dt.Columns.Add("UniqueId", typeof(String));
            dt.Columns.Add("ConDis", typeof(String));
            dt.Columns.Add("DOB", typeof(string));
            dt.Columns.Add("DL", typeof(String));
            DataRow row = null;

            try
            {

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    // foreach (DataRow dr in ds.Tables[0].Rows)
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (IsValidEmail(Convert.ToString(ds.Tables[0].Rows[i][14])))
                        {
                            
                                row = dt.NewRow();

                                row["Local"] = Convert.ToString(ds.Tables[0].Rows[i][0]);
                                row["FirstName"] = Convert.ToString(ds.Tables[0].Rows[i][1]);
                                row["MiddleInitial"] = Convert.ToString(ds.Tables[0].Rows[i][2]);
                                row["LastName"] = Convert.ToString(ds.Tables[0].Rows[i][3]);
                                row["AddressLine1"] = Convert.ToString(ds.Tables[0].Rows[i][4]);
                                row["AddressLine2"] = Convert.ToString(ds.Tables[0].Rows[i][5]);
                                row["City"] = Convert.ToString(ds.Tables[0].Rows[i][6]);
                                row["District"] = Convert.ToString(ds.Tables[0].Rows[i][7]);
                                row["Zip"] = Convert.ToString(ds.Tables[0].Rows[i][8]);
                                row["County"] = Convert.ToString(ds.Tables[0].Rows[i][9]);
                                row["ISDCode"] = Convert.ToString(ds.Tables[0].Rows[i][10]);
                                row["MobileNumber"] = Convert.ToString(ds.Tables[0].Rows[i][11]);
                                row["TeleIsdCode"] = Convert.ToString(ds.Tables[0].Rows[i][12]);
                                row["Telephone"] = Convert.ToString(ds.Tables[0].Rows[i][13]);
                                row["Email"] = Convert.ToString(ds.Tables[0].Rows[i][14]);
                                row["Title"] = Convert.ToString(ds.Tables[0].Rows[i][15]);
                                //row["MemberType"] = Convert.ToString(ds.Tables[0].Rows[i][17]);
                                row["UniqueId"] = Convert.ToString(ds.Tables[0].Rows[i][16]);
                                row["ConDis"] = Convert.ToString(ds.Tables[0].Rows[i][17]);
                                row["DOB"] = Convert.ToString(ds.Tables[0].Rows[i][18] ?? null);
                                row["DL"] = Convert.ToString(ds.Tables[0].Rows[i][19]);
                            if (row["Local"] == "" || row["FirstName"] == "" || row["LastName"] == "" || row["AddressLine1"] == "" || row["City"] == ""
                               || row["Zip"] == "" || row["ISDCode"] == "" || row["MobileNumber"] == "" || row["Email"] == "" || row["Title"] == "" || row["UniqueId"] == "")
                            {
                               
                                return null;
                            }
                            else
                            {
                                dt.Rows.Add(row);
                            }
                           
                               
                            
                            
                           
                        }
                        
                    }
                }
                
                    
                
                
                    return dt;
                
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return dt;
            }
        }
        private DataTable dtImportExcel(DataSet ds, out string errorMessage)
        {
            errorMessage = string.Empty;
            DataTable dt = new DataTable();
            Hashtable ht = new Hashtable();

            int officerid =directoryData.GetOfficerMaxID();
            string colErrorMessage = string.Empty;
                //duplicateUniqueIdMessage = string.Empty
            string duplicateEmailMessage = string.Empty, lastNameformat = string.Empty, firstNameformat = string.Empty, middleNameformat = string.Empty, emailformat = string.Empty, uniqueIdformat = string.Empty, localformat = string.Empty;

            if (ds.Tables[0].Columns.Count == 10)
            {
                dt.Columns.Add("SSN", typeof(String));
                dt.Columns.Add("PayrollID", typeof(String));
                //dt.Columns.Add("Email", typeof(String));
                dt.Columns.Add("FirstName", typeof(String));
                dt.Columns.Add("MiddleName", typeof(String));
                dt.Columns.Add("LastName", typeof(String));
               // dt.Columns.Add("LocalName", typeof(String));
                //dt.Columns.Add("LastName", typeof(String));
                //dt.Columns.Add("NickName", typeof(String));
               // dt.Columns.Add("County", typeof(String));
                //dt.Columns.Add("MemberType", typeof(String));
                dt.Columns.Add("AddressLine1", typeof(String));
                // dt.Columns.Add("AddressLine2", typeof(String));
                dt.Columns.Add("City", typeof(String));
                dt.Columns.Add("StateName", typeof(String));
                dt.Columns.Add("ZipCode", typeof(String));
                dt.Columns.Add("DOB", typeof(String));
                dt.Columns.Add("OfficerId", typeof(String));
                
                //dt.Columns.Add("District", typeof(String));

                //dt.Columns.Add("ISDCode", typeof(String));
                //dt.Columns.Add("MobileNumber", typeof(String));
                //dt.Columns.Add("TeleIsdCode", typeof(String));
                //dt.Columns.Add("Telephone", typeof(String));

                //dt.Columns.Add("Title", typeof(String));

                //dt.Columns.Add("ConDis", typeof(String));
                //dt.Columns.Add("DOB", typeof(string));
                //dt.Columns.Add("DL", typeof(String));
                DataRow row = null;
                
                try
                {
                    if (ds.Tables[0].Columns.Count == 10)
                    {
                        if (ds.Tables[0].Columns[0].ColumnName.ToLower().Trim() != "ssn")
                        {
                            if (string.IsNullOrEmpty(colErrorMessage))
                                colErrorMessage = "SSN";
                            else
                                colErrorMessage = colErrorMessage + ", " + "SSN";
                        }

                        if (ds.Tables[0].Columns[1].ColumnName.ToLower().Trim() != "parole")
                        {
                            if (string.IsNullOrEmpty(colErrorMessage))
                                colErrorMessage = "Parole";
                            else
                                colErrorMessage = colErrorMessage + ", " + "Parole";
                        }
                        

                        if (ds.Tables[0].Columns[2].ColumnName.ToLower().Trim() != "first name")
                        {
                            if (string.IsNullOrEmpty(colErrorMessage))
                                colErrorMessage = "First Name";
                            else
                                colErrorMessage = colErrorMessage + ", " + "First Name";
                        }

                        if (ds.Tables[0].Columns[3].ColumnName.ToLower().Trim() != "mi")
                        {
                            if (string.IsNullOrEmpty(colErrorMessage))
                                colErrorMessage = "MI";
                            else
                                colErrorMessage = colErrorMessage + ", " + "MI";
                        }

                        if (ds.Tables[0].Columns[4].ColumnName.ToLower().Trim() != "last name")
                        {
                            if (string.IsNullOrEmpty(colErrorMessage))
                                colErrorMessage = "Last Name";
                            else
                                colErrorMessage = colErrorMessage + ", " + "Last Name";

                        }
                      
                        if (ds.Tables[0].Columns[5].ColumnName.ToLower().Trim() != "address line 1")
                        {
                            if (string.IsNullOrEmpty(colErrorMessage))
                                colErrorMessage = "Address Line 1";
                            else
                                colErrorMessage = colErrorMessage + ", " + "Address Line 1";

                        }
                        //if (ds.Tables[0].Columns[10].ColumnName.ToLower().Trim() != "address line 2")
                        //{
                        //    if (string.IsNullOrEmpty(colErrorMessage))
                        //        colErrorMessage = "Address Line 2";
                        //    else
                        //        colErrorMessage = colErrorMessage + ", " + "Address Line 2";

                        //}
                        if (ds.Tables[0].Columns[6].ColumnName.ToLower().Trim() != "city")
                        {
                            if (string.IsNullOrEmpty(colErrorMessage))
                                colErrorMessage = "City";
                            else
                                colErrorMessage = colErrorMessage + ", " + "City";

                        }
                        if (ds.Tables[0].Columns[7].ColumnName.ToLower().Trim() != "state")
                        {
                            if (string.IsNullOrEmpty(colErrorMessage))
                                colErrorMessage = "State";
                            else
                                colErrorMessage = colErrorMessage + ", " + "State";

                        }
                        if (ds.Tables[0].Columns[8].ColumnName.ToLower().Trim() != "zip")
                        {
                            if (string.IsNullOrEmpty(colErrorMessage))
                                colErrorMessage = "Zip";
                            else
                                colErrorMessage = colErrorMessage + ", " + "Zip";

                        }
                        if (ds.Tables[0].Columns[9].ColumnName.ToLower().Trim() != "dob")
                        {
                            if (string.IsNullOrEmpty(colErrorMessage))
                                colErrorMessage = "DOB";
                            else
                                colErrorMessage = colErrorMessage + ", " + "DOB";

                        }

                        if (!string.IsNullOrEmpty(colErrorMessage))
                        {
                            colErrorMessage = colErrorMessage + " column(s) missing!";
                            errorMessage = colErrorMessage;
                            return null;
                        }
                    }
                    else
                    {
                        errorMessage = "Number of columns are not matching!";
                    }
                    var regexItem = new Regex("^[0-9]+$");
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        // foreach (DataRow dr in ds.Tables[0].Rows)
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (Convert.ToString(ds.Tables[0].Rows[i][0]) == "" && Convert.ToString(ds.Tables[0].Rows[i][1]) == ""
                                && Convert.ToString(ds.Tables[0].Rows[i][1]) == "" && Convert.ToString(ds.Tables[0].Rows[i][3]) == ""
                                && Convert.ToString(ds.Tables[0].Rows[i][4]) == "" && Convert.ToString(ds.Tables[0].Rows[i][5]) == ""
                                && Convert.ToString(ds.Tables[0].Rows[i][6]) == "" && Convert.ToString(ds.Tables[0].Rows[i][7]) == ""
                                && Convert.ToString(ds.Tables[0].Rows[i][7]) == "" && Convert.ToString(ds.Tables[0].Rows[i][9]) == ""
                                && Convert.ToString(ds.Tables[0].Rows[i][8]) == ""  && Convert.ToString(ds.Tables[0].Rows[i][9]) == "" )
                            { }
                            else
                            {

                                //if (ht.ContainsValue(Convert.ToString(ds.Tables[0].Rows[i][0])))
                                //{
                                //    if (string.IsNullOrEmpty(duplicateUniqueIdMessage))
                                //        duplicateUniqueIdMessage = Convert.ToString(ds.Tables[0].Rows[i][0]);
                                //    else
                                //        duplicateUniqueIdMessage = duplicateUniqueIdMessage + ", " + Convert.ToString(ds.Tables[0].Rows[i][0]);

                                //}
                                //else
                            ////   ht.Add(Convert.ToString(ds.Tables[0].Rows[i][0]), Convert.ToString(ds.Tables[0].Rows[i][0]));

                                //if (ht.ContainsValue(Convert.ToString(ds.Tables[0].Rows[i][2])))
                                //{
                                //    if (string.IsNullOrEmpty(duplicateEmailMessage))
                                //        duplicateEmailMessage = Convert.ToString(ds.Tables[0].Rows[i][2]);
                                //    else
                                //        duplicateEmailMessage = duplicateEmailMessage + ", " + Convert.ToString(ds.Tables[0].Rows[i][2]);

                                //}
                                //else
                           ////    ht.Add(Convert.ToString(ds.Tables[0].Rows[i][2]), Convert.ToString(ds.Tables[0].Rows[i][2]));


                                //colErrorMessage = "UniqueId";


                                if (Convert.ToString(ds.Tables[0].Rows[i][0]) == "")
                                {
                                    
                                    if (string.IsNullOrEmpty(colErrorMessage))
                                        colErrorMessage = "SSN";
                                    else
                                        colErrorMessage = colErrorMessage + ", " + "SSN";


                                }

                                //if (Convert.ToString(ds.Tables[0].Rows[i][1]) == "")
                                //    if (string.IsNullOrEmpty(colErrorMessage))
                                //        colErrorMessage = "Payroll ID";
                                //    else
                                //        colErrorMessage = colErrorMessage + ", " + "Payroll ID";

                                if (Convert.ToString(ds.Tables[0].Rows[i][2]) == "")
                                    if (string.IsNullOrEmpty(colErrorMessage))
                                        colErrorMessage = "FirstName";
                                    else
                                        colErrorMessage = colErrorMessage + ", " + "FirstName";


                                if (Convert.ToString(ds.Tables[0].Rows[i][4]) == "")
                                    if (string.IsNullOrEmpty(colErrorMessage))
                                        colErrorMessage = "LastName";
                                    else
                                        colErrorMessage = colErrorMessage + ", " + "LastName";


                                if (!string.IsNullOrEmpty(colErrorMessage))
                                {
                                    colErrorMessage = colErrorMessage + " required!";
                                    errorMessage = colErrorMessage;
                                    return null;
                                }
                               
                                //if (IsValidEmail(Convert.ToString(ds.Tables[0].Rows[i][2])))
                                //{
                                    if (regexItem.IsMatch(Convert.ToString(ds.Tables[0].Rows[i][0])))
                                    {
                                        if (IsValidFirstNameFromat(Convert.ToString(ds.Tables[0].Rows[i][2])))
                                        {
                                            if (IsValidFromat(Convert.ToString(ds.Tables[0].Rows[i][3])))
                                            {
                                                if (IsValidFirstNameFromat(Convert.ToString(ds.Tables[0].Rows[i][4])))
                                                {
                                                    row = dt.NewRow();
                                                    
                                                if (ds.Tables[0].Rows[i][0].ToString().Length < 4)
                                                {

                                                    row["SSN"] = "0" + ds.Tables[0].Rows[i][0].ToString();

                                                }
                                                else
                                                {

                                                    row["SSN"] = Convert.ToString(ds.Tables[0].Rows[i][0]);
                                                }
                                                    row["PayrollID"] = Convert.ToString(ds.Tables[0].Rows[i][1]);
                                                    row["FirstName"] = Convert.ToString(ds.Tables[0].Rows[i][2]);
                                                    row["MiddleName"] = Convert.ToString(ds.Tables[0].Rows[i][3]);
                                                    row["LastName"] = Convert.ToString(ds.Tables[0].Rows[i][4]);
                                                    row["AddressLine1"] = Convert.ToString(ds.Tables[0].Rows[i][5]);
                                                    row["City"] = Convert.ToString(ds.Tables[0].Rows[i][6]);
                                                    row["StateName"] = Convert.ToString(ds.Tables[0].Rows[i][7]);
                                                    row["ZipCode"] = Convert.ToString(ds.Tables[0].Rows[i][8]);
                                                    
                                                    row["OfficerId"] = officerid+1;
                                                officerid++;



                                                if (ds.Tables[0].Rows[i][9].ToString().Length < 6)
                                                {
                                                     DateTime date;
                                                    string Date = "0" + ds.Tables[0].Rows[i][9].ToString();
                                                    string[] formats = { "MMddyy", "MMddyyyy" };
                                                    DateTime.TryParseExact(Date, formats, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out date);
                                                    //Date = date.ToString("MM/dd/yyyy");
                                                    row["DOB"] = date.ToString("MM/dd/yyyy");
                                                }
                                                else
                                                {
                                                    DateTime date;
                                                    string Date =  ds.Tables[0].Rows[i][9].ToString();
                                                    string[] formats = { "MMddyy", "MMddyyyy" };
                                                    DateTime.TryParseExact(Date, formats, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out date);
                                                    row["DOB"] = date.ToString("MM/dd/yyyy");
                                                    //row["DOB"] = Convert.ToString(ds.Tables[0].Rows[i][9]);
                                                }


                                                //row["District"] = Convert.ToString(ds.Tables[0].Rows[i][7]);
                                                //row["ISDCode"] = Convert.ToString(ds.Tables[0].Rows[i][10]);
                                                //row["MobileNumber"] = Convert.ToString(ds.Tables[0].Rows[i][11]);
                                                //row["TeleIsdCode"] = Convert.ToString(ds.Tables[0].Rows[i][12]);
                                                //row["Telephone"] = Convert.ToString(ds.Tables[0].Rows[i][13]);

                                                //row["Title"] = Convert.ToString(ds.Tables[0].Rows[i][15]);

                                                //row["ConDis"] = Convert.ToString(ds.Tables[0].Rows[i][17]);
                                                //row["DOB"] = Convert.ToString(ds.Tables[0].Rows[i][18] ?? null);
                                                //row["DL"] = Convert.ToString(ds.Tables[0].Rows[i][19]);

                                                if (row["SSN"].ToString() == "")
                                                        if (string.IsNullOrEmpty(colErrorMessage))
                                                            colErrorMessage = "SSN";
                                                        else
                                                            colErrorMessage = colErrorMessage + ", " + "SSN";

                                                  
                                                    if (row["PayrollID"].ToString() == "")
                                                        if (string.IsNullOrEmpty(colErrorMessage))
                                                            colErrorMessage = "PayrollID";
                                                        else
                                                            colErrorMessage = colErrorMessage + ", " + "PayrollID";

                                                    if (row["FirstName"].ToString() == "")
                                                        if (string.IsNullOrEmpty(colErrorMessage))
                                                            colErrorMessage = "First Name";
                                                        else
                                                            colErrorMessage = colErrorMessage + ", " + "First Name";

                                                    if (row["LastName"].ToString() == "")
                                                        if (string.IsNullOrEmpty(colErrorMessage))
                                                            colErrorMessage = "Last Name";
                                                        else
                                                            colErrorMessage = colErrorMessage + ", " + "Last Name";

                                                    //else
                                                    {
                                                        dt.Rows.Add(row);
                                                    }
                                                }
                                                else
                                                {
                                                    if (string.IsNullOrEmpty(lastNameformat))
                                                        lastNameformat = Convert.ToString(ds.Tables[0].Rows[i][4]);
                                                    else
                                                        lastNameformat = lastNameformat + ", " + Convert.ToString(ds.Tables[0].Rows[i][4]);
                                                    
                                                    //errorMessage = Convert.ToString(ds.Tables[0].Rows[i][5]) + " invalid last name format!";
                                                    //return null;
                                                }
                                            }
                                            else
                                            {
                                                if (string.IsNullOrEmpty(middleNameformat))
                                                    middleNameformat = Convert.ToString(ds.Tables[0].Rows[i][3]);
                                                else
                                                    middleNameformat = middleNameformat + ", " + Convert.ToString(ds.Tables[0].Rows[i][3]);


                                                //errorMessage = Convert.ToString(ds.Tables[0].Rows[i][4]) + " invalid middle name format!";
                                                //return null;
                                            }
                                        }
                                        else
                                        {
                                            if (string.IsNullOrEmpty(firstNameformat))
                                                firstNameformat = Convert.ToString(ds.Tables[0].Rows[i][2]);
                                            else
                                                firstNameformat = firstNameformat + ", " + Convert.ToString(ds.Tables[0].Rows[i][2]);

                                            //errorMessage = Convert.ToString(ds.Tables[0].Rows[i][3]) + " invalid first name format!";
                                            //return null;
                                        }
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(uniqueIdformat))
                                            uniqueIdformat = Convert.ToString(ds.Tables[0].Rows[i][0]);
                                        else
                                            uniqueIdformat = uniqueIdformat + ", " + Convert.ToString(ds.Tables[0].Rows[i][0]);

                                        //errorMessage = Convert.ToString(ds.Tables[0].Rows[i][0]) + " invalid uniqueid format!";
                                        //return null;
                                    }
                                //}
                                //else
                                //{
                                //    if (string.IsNullOrEmpty(emailformat))
                                //        emailformat = Convert.ToString(ds.Tables[0].Rows[i][2]);
                                //    else
                                //        emailformat = emailformat + ", " + Convert.ToString(ds.Tables[0].Rows[i][2]);

                                //    //errorMessage = Convert.ToString(ds.Tables[0].Rows[i][2]) + " invalid email address!";
                                //    //return null;
                                //}
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                    return null;
                }
            }
            else
            {
                errorMessage = "Number of column(s) not matching!";
                return null;
            }

            if (!string.IsNullOrEmpty(colErrorMessage))
            {
                colErrorMessage = colErrorMessage + " required!";
                errorMessage = colErrorMessage;
                return null;
            }

            if (!string.IsNullOrEmpty(uniqueIdformat))
            {
                uniqueIdformat = uniqueIdformat + " invalid SSN(s) format!";
                errorMessage = uniqueIdformat;
                return null;
            }

            //if (!string.IsNullOrEmpty(duplicateUniqueIdMessage))
            //{
            //    duplicateUniqueIdMessage = "duplicate SSN(s) found: " +duplicateUniqueIdMessage ;
            //    errorMessage = duplicateUniqueIdMessage;
            //    return null;
            //}
            if (!string.IsNullOrEmpty(duplicateEmailMessage))
            {
                duplicateEmailMessage = "duplicate emailId(s) found : " + duplicateEmailMessage ;
                errorMessage = duplicateEmailMessage;
                return null;
            }

            if (!string.IsNullOrEmpty(emailformat))
            {
                emailformat = "Email id(s) format invalid: " + emailformat  ;
                errorMessage = emailformat;
                return null;
            }

            

            if (!string.IsNullOrEmpty(firstNameformat))
            {
                firstNameformat = firstNameformat + " invalid first name(s) format!";
                errorMessage = firstNameformat;
                return null;
            }
            if (!string.IsNullOrEmpty(middleNameformat))
            {
                middleNameformat = middleNameformat + " invalid M name(s) format!";
                errorMessage = middleNameformat;
                return null;
            }
            if (!string.IsNullOrEmpty(lastNameformat))
            {
                lastNameformat = lastNameformat + " invalid last name(s) format!";
                errorMessage = lastNameformat;
                return null;
            }
           

            
            return dt;
        }

        bool IsValidFirstNameFromat(string stringtoValdate)
        {
            bool isValid = false;
            try
            {
                //var regexItem = new Regex(@"^[a-zA-Z0-9\d - ]*$");
                var regexItem = new Regex(@"^[a-zA-Z0-9\-_.' ]*$");
                //var regexItem = new Regex(@"/^[\w]+([-\s]{1}[a-zA-Z0-9]+)*$/i");
                if (regexItem.IsMatch(stringtoValdate))
                    isValid = true;

                return isValid;
            }
            catch
            {
                return false;
            }




        }
        bool IsValidFromat(string stringtoValdate)
        {
            bool isValid = false;
            try
            {
                var regexItem = new Regex("^[a-zA-Z0-9 ]*$");
                if (regexItem.IsMatch(stringtoValdate))
                    isValid = true;

                return isValid;
            }
            catch
            {
                return false;
            }




        }
        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }




        }
        private DirectoryModel InviteDataFile(DataSet ds)
        {
            DirectoryModel Model = new DirectoryModel();
            try
            {
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    // foreach (DataRow dr in ds.Tables[0].Rows)
                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    {

                        foreach (DataRow row in ds.Tables[1].Rows)
                        {
                            Model.DuplicateCount = Convert.ToInt32(row["DuplicateCount"]);
                            if (Model.DuplicateCount < 0)
                            {
                                Model.DuplicateCount = 0;
                            }
                            Model.InsertedCount = Convert.ToInt32(row["InsertedCount"]);
                            if (Model.InsertedCount < 0)
                            {
                                Model.InsertedCount = 0;
                            }
                            Model.TotalCount = Convert.ToInt32(row["TotalCount"]);
                            if (Model.TotalCount < 0)
                            {
                                Model.TotalCount = 0;
                            }
                            Model.PendingCount = Convert.ToInt32(row["PendingCount"]);
                            if(Model.PendingCount <0)
                            {
                                Model.PendingCount = 0;
                            }
                            Model.UpdatedCount= Convert.ToInt32(row["UpdatedCount"]);
                            if (Model.UpdatedCount < 0)
                            {
                                Model.UpdatedCount = 0;
                            }

                        }
                    }

                  
                }

                return Model;
            }
            catch (Exception ex)
            {
                return Model;
            }
        }
        private List<InviteModel> InviteEmail(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            InviteModel Model = new InviteModel();
            try
            {
                List<InviteModel> data = new List<InviteModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new InviteModel
                            {
                                FirstName = Convert.ToString(row["FirstName"]),
                                Email = Convert.ToString(row["Email"])
                            });
                        }
                        
                    }

                  
                }

                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DirectoryResponse MultipleInvite(InviteModel model)
        {
            DirectoryResponse directoryResponse = new DirectoryResponse();
            //InviteModel Model = new InviteModel();
            Int32 ReturnValue = 0;
            directoryResponse.ReturnCode = 0;
            directoryResponse.ReturnMessage = "Failed";
            DataSet ds = null;
            int i = 0;
            
            //directoryResponse._inviteModel._InviteEmail = Model._InviteEmail;
            try
            {
                if(model != null) { 
                if (model._InviteEmail != null && model._InviteEmail.Count > 0)
                {

                    model.Email = model._InviteEmail[i].Email;
                    ReturnValue = SendEmail(model._InviteEmail);
                    //ds = directoryData.MultipleInvite(model, out ReturnValue);
                }
                   //directoryResponse._directoryModel = InviteDataFile(ds);
                
                if (ReturnValue > 0)
                {
                    directoryResponse.ReturnCode = 1;
                    directoryResponse.ReturnMessage = "Success";
                }
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryAccess", "SendInvitation");

            }
            return directoryResponse;
        }
        private Int32 SendEmail(List<InviteModel> _EmailList)
        {

            try
            {

                if (_EmailList != null && _EmailList.Count > 0)
                {
                    string recieptents = string.Empty;
                    String _Body = EmailUtility.ReadHtml("Invite.html");
                    String _EmailBody = "";

                    int returnVal = -1;
                    foreach (var item in _EmailList)
                    {
                        
                            try
                            {
                                _EmailBody = _Body
                                       .Replace("{link}", "invitaion")
                                       .Replace("{FirstName}", item.FirstName);

                               
                                returnVal = EmailUtilityNew.SendMessageSmtpNew(
                                    item.FirstName,
                                    item.Email,
                                    "Invitation from NJPBA Local 105",
                                    _EmailBody
                                    , true);
                            //returnVal = EmailUtility.SendEmail(
                            //        item.FirstName,
                            //        item.Email,
                            //        "Invitation from NJPBA Local 105",
                            //        _EmailBody
                            //        , true);
                              
                                //directoryData.InviteEmailSendStatusUpdate(item.UserId, returnVal > 0 ? "Sent" : "Failed");
                            }
                            catch (Exception ex)
                            {
                                return returnVal;
                            }
                        

                        return returnVal;
                    }

                }

                return 1;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryAccess", "SendInvitation");
                return -1;
            }

        }

        public DirectoryResponse MultiInvite(DirectoryModel directoryModel,CancellationToken cancellationToken = default(CancellationToken))
        {
            DirectoryModel directory = new DirectoryModel();
            DirectoryResponse response = new DirectoryResponse();
            try
            {
                response.ReturnCode = 0;
                response.ReturnMessage = "Failed";
                directory._DirectoryList = new List<DirectoryModel>();
                directory._DirectoryList = directoryModel._DirectoryList;
               
                    Int32 _UserId = 0;
               for (int i = 0; i < directory._DirectoryList.Count; i++)
               {
                    cancellationToken.ThrowIfCancellationRequested();
                     //file.WriteLine("Its Line number : " + i + "\n");
                 if (directory._DirectoryList[i].EncryptedUserId != null && directory._DirectoryList[i].EncryptedUserId != "")
                   {
                         _UserId = Convert.ToInt32(UtilityAccess.Decrypt(directory._DirectoryList[i].EncryptedUserId));
                   }
                        String _FirstName = directory._DirectoryList[i].FirstName;
                        String _Email = directory._DirectoryList[i].Email;
                         response = SendInviteEmail(_UserId, _FirstName, _Email);
                        if(response.ReturnCode>0)
                        Thread.Sleep(1000);
               }

            }
            catch (Exception ex)
            {
                ProcessCancellation();
                 response.ReturnCode = 0;
                response.ReturnMessage = "Failed";
               
            }
            return response;
        }
        private void ProcessCancellation()
        {
            Thread.Sleep(10000);
        }
        public DirectoryResponse SendInviteEmail(Int32 _UserId, String _FirstName, String _Email)
        {
            DirectoryResponse Response = new DirectoryResponse();
            DirectoryModel Model = new DirectoryModel();
            Int32 ReturnValue = 0;
            Response.ReturnCode = 0;
            Response.ReturnMessage = "Failed";

            try
            {
                Response._inviteModel= new InviteModel();
                Response._inviteModel._InviteData = new List<InviteModel>();
                Response._inviteModel._InviteData.Add(new InviteModel
                {
                    UserId= _UserId,
                    FirstName = _FirstName,
                    Email = _Email

                });
                ReturnValue = SendEmail(Response._inviteModel._InviteData);
                if (ReturnValue > 0)
                {
                    Model.Status = "Invited";
                    Int32 ds = directoryData.InviteEmailSendStatusUpdate(_UserId, Model.Status);
                    Response.ReturnCode = ReturnValue;
                    Response.ReturnMessage = "Success";
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryAccess", "SendInvitation");

            }
            return Response;
        }
        public DirectoryResponse SingleInvite(Int32 _UserId, String _FirstName, String _Email)
        {
            DirectoryResponse Response = new DirectoryResponse();
            DirectoryModel Model = new DirectoryModel();
            Int32 ReturnValue = 0;
            Response.ReturnCode = 0;
            Response.ReturnMessage = "Failed";

            try
            {
                Response._inviteModel = new InviteModel();
                Response._inviteModel._InviteData = new List<InviteModel>();
                Response._inviteModel._InviteData.Add(new InviteModel
                {
                    UserId = _UserId,
                    FirstName = _FirstName,
                    Email = _Email

                });
                ReturnValue = SendEmail(Response._inviteModel._InviteData);
                if (ReturnValue > 0)
                {
                    Model.Status = "Invited";
                    Int32 ds = directoryData.InviteEmailSendStatusUpdate(_UserId, Model.Status);
                    Response.ReturnCode = ReturnValue;
                    Response.ReturnMessage = "Success";
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryAccess", "SendInvitation");

            }
            return Response;
        }
        public DirectoryResponse Delete(DirectoryModel model)
        {
            Int32 returnResult = 0;
            DirectoryResponse response = new DirectoryResponse();

            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.LoggedUserId =Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedLoggedId));
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                returnResult = directoryData.Delete(model);
                if (returnResult > 0)
                {
                    response._directoryModel= new DirectoryModel();
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryAccess", "Delete");
                return response;
            }
        }

    }
}
