﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class DocumentAccess: IDocument
    {
        DocumentData documentData = new DocumentData();
        public DocumentResponse Add(DocumentModel model)
        {
            Int32 ReturnResult = 0;
            DocumentResponse response = new DocumentResponse();
            response._documentModel = new DocumentModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(ReturnResult);
            try
            {

                if (!string.IsNullOrEmpty(model.EncryptedDocumentId))
                {
                    model.DocumentId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedDocumentId));
                }
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
               
                DataSet ds = documentData.Add(model, out ReturnResult);

                string deviceToken = string.Empty, deviceType = string.Empty, Message = string.Empty, did = string.Empty;
                
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        did = Convert.ToInt32(row["DocumentId"]).ToString();
                        deviceToken = Convert.ToString(row["DeviceToken"]);
                        deviceType = Convert.ToString(row["DeviceType"]);
                        Message = Convert.ToString(row["NotifyMessage"]);
                        if (deviceType.ToUpper() == "IOS")
                            APNSNotificationAccess.ApnsNotification(deviceToken, Message, did, "d");
                        else
                           if (deviceType.ToUpper() == "ANDROID")
                            FCMNotificationAccess.SendFCMNotifications(deviceToken, Message, deviceType, did, "document");
                    }
                }
                response.ReturnCode = ReturnResult;
                response.ReturnMessage = Response.Message(ReturnResult);

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentAccess", "Add");
                return response;
            }
        }
        public DocumentResponse SelectAll(DocumentModel model)
        {
            DocumentResponse response = new DocumentResponse();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            response._documentModel = new DocumentModel();
            Int32 returnResult = 0;
            try
            {
                model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

                DataSet ds = documentData.SelectAll(model, out returnResult);

                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        response._documentModel._DocumentList = DocumentList(ds.Tables[0], returnResult);
                        returnResult = 2;

                    }
                }

                else
                {
                    response.ReturnCode = 0;
                    response.ReturnMessage = "No Data Found";
                }
                DocumentModel docinfo = new DocumentModel();
                response._documentModel._StatusList = UtilityAccess.ActiveStatusList(2);
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentAccess", "SelectAll");
            }
            return response;
        }
        public DocumentResponse LogSelectAll(DocumentModel model)
        {
            DocumentResponse response = new DocumentResponse();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            response._documentModel = new DocumentModel();
            Int32 returnResult = 0;
            try
            {
                model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

                DataSet ds = documentData.LogSelectAll(model, out returnResult);

                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        response._documentModel._LogBookList = LogList(ds.Tables[0], returnResult);
                        returnResult = 2;

                    }
                }

                else
                {
                    response.ReturnCode = 0;
                    response.ReturnMessage = "No Data Found";
                }
                DocumentModel docinfo = new DocumentModel();
                response._documentModel._StatusList = UtilityAccess.ActiveStatusList(2);
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentAccess", "SelectAll");
            }
            return response;
        }
        private List<DocumentModel> DocumentList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<DocumentModel> data = new List<DocumentModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new DocumentModel
                            {

                                EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"])),
                                EncryptedDocumentId = UtilityAccess.Encrypt(Convert.ToString(row["DocumentId"])),
                                DocumentName =Convert.ToString(row["DocumentName"]),
                                DocumentPath=Convert.ToString(row["DocumentPath"]),
                                UserId = Convert.ToInt32(row["UserId"]),

                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentModel", "DocumentList");
                ReturnResult = -1;
                return null;
            }
        }
        private List<DocumentModel> LogList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<DocumentModel> data = new List<DocumentModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new DocumentModel
                            {

                                EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"])),
                                EncryptedDocumentId = UtilityAccess.Encrypt(Convert.ToString(row["DocumentId"])),
                                UniqueId =Convert.ToString(row["UniqueId"]),
                                FirstName=Convert.ToString(row["FirstName"]),
                                LastName=Convert.ToString(row["LastName"]),
                                MI=Convert.ToString(row["MI"]),
                                CreateDate = Convert.ToString(row["CreateDate"]),
                                ISDCode=Convert.ToString(row["ISDCode"]),
                                Mobile=Convert.ToString(row["Mobile"]),
                                UserId = Convert.ToInt32(row["UserId"]),
                                FileName = Convert.ToString(row["FileName"]),
                                ThumbnailPath = Convert.ToString(row["ThumbnailPath"]),

                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentModel", "DocumentList");
                ReturnResult = -1;
                return null;
            }
        }
        public DocumentResponse Delete(DocumentModel model)
        {
            DocumentResponse response= new DocumentResponse();
            DocumentModel docinfo= new DocumentModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            int returnResult = 0;
            if (model != null && model.EncryptedDocumentId != null)
                model.DocumentId= Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedDocumentId));

            model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = documentData.Delete(model);
            if (returnResult > 0)
            {
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);
                response._documentModel= docinfo;
            }
            return response;
        }
    }
}
