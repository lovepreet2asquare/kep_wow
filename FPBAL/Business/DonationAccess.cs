﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FPBAL.Business
{
    public class DonationAccess : IDonation
    {
        DonationData donationData = new DonationData();
        public DonationModel DonationSelectAll(DonationModel model)
        {
            DonationModel data = new DonationModel();
            try
            {
                model.UserId = Convert.ToString(UtilityAccess.Decrypt(model.UserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                DataSet ds = donationData.DonationSelectAll(model);

                data._StatusAIList = UtilityAccess.DonationStatusList(1);
                if (ds != null && ds.Tables.Count > 0)
                {

                    data._DonationModel = new List<DonationModel>();
                    //foreach (DataRow row in ds.Tables[0].Rows)
                    //{
                    //    data._DonationModel.Add(new DonationModel
                    //    {
                    //        //InvolvementId = Convert.ToInt32(row["InvolvementId"]),
                    //        FirstName = Convert.ToString(row["FirstName"]),
                    //        MI= Convert.ToString(row["MI"]),
                    //        LastName = Convert.ToString(row["LastName"]),
                    //        AddressLine1 = Convert.ToString(row["AddressLine1"]),
                    //        AddressLine2= Convert.ToString(row["AddressLine2"]),
                    //        DonationType = Convert.ToString(row["DonationType"]),
                    //        TransctionDate= Convert.ToString(row["TransactionDate"]),
                    //        TransactionTime= Convert.ToString(row["TransactionTime"]),
                    //        Amount= Convert.ToDecimal(row["Amount"]),
                    //    });
                    //}
                    if (ds.Tables[0] != null && ds.Tables.Count > 0)
                        data._DonationTypeModel = DonationTypeList(ds.Tables[0]);
                    if (ds.Tables[1] != null && ds.Tables.Count > 0)
                        data.donationWeekMMYY = donationWeekMMYYDetail(ds.Tables[1]);
                    if (ds.Tables[2] != null && ds.Tables.Count > 0)
                        data.donationAmount = DonationAmountDeatil(ds.Tables[2]);
                    if (ds.Tables[3] != null && ds.Tables.Count > 0)
                        data._GraphData = GrapgDataList(ds.Tables[3]);
                    if (ds.Tables[7] != null && ds.Tables.Count > 0)
                        data._DonationModel = DonationList(ds.Tables[7]);

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "DonationSelectAll");
                return null;
            }
        }

        public List<GraphData> GrapgDataList(DataTable dt)
        {
            try
            {
                List<GraphData> data = new List<GraphData>();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new GraphData
                            {
                                Day = Convert.ToString(row["Day"]),
                                Involvement = Convert.ToInt32(row["Amount"]),
                            });
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EmployeeAccess", "DashBoardGraphDetail");
                return null;
            }
        }
        public DonationAmount DonationAmountDeatil(DataTable dt)
        {
            try
            {
                DonationAmount data = new DonationAmount();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.DonationAmountTotal =(UtilityAccess.CurrencyFormat(Convert.ToDecimal(row["TotalAmount"])));
                            data.RefundAmount = (UtilityAccess.CurrencyFormat(Convert.ToDecimal(row["RefundAmount"])));

                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InvolvementAccess", "GraphmodelCount");
                return null;
            }
        }
        public DonationWeekMMYY donationWeekMMYYDetail(DataTable dt)
        {
            try
            {
                DonationWeekMMYY data = new DonationWeekMMYY();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.DonationWeekMMYYName = Convert.ToString(row["ThisWeakMMYY"]);
                            data.FromDate = Convert.ToString(row["FromDate"]);
                            data.ToDate = Convert.ToString(row["ToDate"]);
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InvolvementAccess", "GraphmodelCount");
                return null;
            }
        }

        public List<DonationModel> DonationList(DataTable dt)
        {
            try
            {
                List<DonationModel> data = new List<DonationModel>();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new DonationModel
                            {
                                PaymentId = Convert.ToInt32(row["PaymentId"]),
                                EncryptedPaymentId = UtilityAccess.Encrypt(row["PaymentId"].ToString()),
                                FirstName = (row["FirstName"] as string ?? ""),
                                MI = (row["MI"] as string ?? ""),
                                LastName = (row["LastName"] as string ?? ""),
                                CardHolderName= (row["CardHolderName"] as string ?? ""),
                                AddressLine1 = Convert.ToString(row["AddressLine1"]),
                                AddressLine2 = Convert.ToString(row["AddressLine2"]),
                                StateName = Convert.ToString(row["StateName"]),
                                CountryName = Convert.ToString(row["CountryName"]),
                                zipcode = Convert.ToString(row["Zipcode"]),
                                cityName = Convert.ToString(row["Cityname"]),
                                DonationType = Convert.ToString(row["DonationType"]),
                                TransctionDate = Convert.ToString(row["TransactionDate"]),
                                TransactionTime = Convert.ToString(row["TransactionTime"]),
                                FollowerId = Convert.ToString(row["FollowerId"]),
                                EncryptedFollowerId = UtilityAccess.Encrypt(row["FollowerId"].ToString()),
                                Amount = Convert.ToDecimal(row["Amount"]),
                                Volunteer = Convert.ToString(row["IsVolunteer"]),
                                Hosting = Convert.ToString(row["IsHostingEvent"]),
                                DonationStatus= Convert.ToString(row["Status"]),
                            });
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "DonationList");
                return null;
            }
        }

        public List<DonationTypeModel> DonationTypeList(DataTable dt)
        {
            try
            {
                List<DonationTypeModel> data = new List<DonationTypeModel>();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new DonationTypeModel
                            {
                                DonationType = Convert.ToString(row["DonationType"]),
                                DonationPercentage = Convert.ToDecimal(row["percentage"]),

                            });
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "DonationList");
                return null;
            }
        }

        public GraphsDataModel DonationGraphData(string duration)
        {
            GraphsDataModel data = new GraphsDataModel();
            try
            {
                DataSet ds = donationData.DonationGraphData(duration);
                if (ds != null && ds.Tables.Count > 0)
                {

                    data._GraphData = new List<GraphsDataModel>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        data._GraphData.Add(new GraphsDataModel
                        {
                            //InvolvementId = Convert.ToInt32(row["InvolvementId"]),
                            Day = Convert.ToString(row["Day"]),
                            Amount = Convert.ToDecimal(row["Amount"]),
                        });
                    }
                    if (ds.Tables[1] != null && ds.Tables.Count > 0)
                        data.DonationWeekMMYY = AfterDropDownChangeFromToDates(ds.Tables[1]);
                    if (ds.Tables[2] != null && ds.Tables.Count > 0)
                        data.DonationAmount = GraphDataCountForChange(ds.Tables[2]);

                    if (ds.Tables[3] != null && ds.Tables.Count > 0)
                        data._DonationTypeModel = DonationTypeListOfDropDownChange(ds.Tables[3]);

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "DonationGraphData");
                return null;
            }
        }

        public List<DonationTypeModel> DonationTypeListOfDropDownChange(DataTable dt)
        {
            try
            {
                List<DonationTypeModel> data = new List<DonationTypeModel>();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new DonationTypeModel
                            {
                                DonationType = Convert.ToString(row["DonationType"]),
                                DonationPercentage = Convert.ToDecimal(row["percentage"]),

                            });
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "DonationList");
                return null;
            }
        }
        public DonationWeekMMYY AfterDropDownChangeFromToDates(DataTable dt)
        {
            try
            {
                DonationWeekMMYY data = new DonationWeekMMYY();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.DonationWeekMMYYName = Convert.ToString(row["ThisWeakMMYY"]);
                            data.FromDate = Convert.ToString(row["FromDate"]);
                            data.ToDate = Convert.ToString(row["ToDate"]);
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "AfterDropDownChangeFromToDates");
                return null;
            }
        }
        public DonationAmount GraphDataCountForChange(DataTable dt)
        {
            try
            {
                DonationAmount data = new DonationAmount();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.DonationAmountTotal = (UtilityAccess.CurrencyFormat(Convert.ToDecimal(row["TotalAmount"])));
                            data.RefundAmount = (UtilityAccess.CurrencyFormat(Convert.ToDecimal(row["RefundAmount"])));

                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "AfterDropDownChangeFromToDates");
                return null;
            }
        }

        public RecurringTypeModel RecurringTypeSelectAll(RecurringTypeModel model)
        {
            RecurringTypeModel data = new RecurringTypeModel();
            try
            {
                model.UserId = Convert.ToString(UtilityAccess.Decrypt(model.UserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                DataSet ds = donationData.RecurringTypeSelectAll(model);

                if (ds != null && ds.Tables.Count > 0)
                {

                    data._RecurringTypeModel = new List<RecurringTypeModel>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        data._RecurringTypeModel.Add(new RecurringTypeModel
                        {
                            //InvolvementId = Convert.ToInt32(row["InvolvementId"]),
                            RecurringTypeId = Convert.ToInt32(row["RecurringTypeId"]),
                            RecurringType = (row["RecurringType"] as string ?? ""),
                            RecurringDate = (row["RecurringDate"] as string ?? ""),
                            Status = (row["Status"] as string ?? ""),
                        });
                    }
                }

                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "DonationSelectAll");
                return null;
            }
        }

        public RecurringTypeModel RecurringTypeById(String Id)
        {
            try
            {
                return RecurringTypeDetial(donationData.RecurringTypeById(Id));
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "Select");
                return null;
            }
        }
        private RecurringTypeModel RecurringTypeDetial(DataSet ds)
        {
            RecurringTypeModel data = new RecurringTypeModel();
            data._StatusAIList = UtilityAccess.StatusListValText(1);
            data.ReturnValue = -1;
            try
            {
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        data.RecurringTypeId = Convert.ToInt32(row["RecurringTypeId"]);
                        data.RecurringType2 = Convert.ToString(row["RecurringType"]);
                        data.RecurringTypeDate = (row["RecurringDate"] as String) ?? "";
                        data.StatusId = Convert.ToInt32(row["Status"]);
                        data.NoEndDate = Convert.ToBoolean(row["IsNoEndDate"]);
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                data.ReturnValue = -1;
                ApplicationLogger.LogError(ex, "DonationAccess", "RecurringTypeDetial");
                return null;
            }
        }

        public RecurringTyperesponse RecurringTypeInsert(RecurringTypeModel model)
        {
            RecurringTyperesponse response = new RecurringTyperesponse();
            try
            {
                model.UserId = Convert.ToString(UtilityAccess.Decrypt(model.UserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                DataSet ds = donationData.RecurringTypeInsert(model);
                if (ds!=null)
                {
                    response.RecurringTypeModel = RecurringTypeInsertDetail(ds);
                }
                response.ReturnCode = response.RecurringTypeModel.ReturnValue;
                response.ReturnMessage = Response.Message(response.RecurringTypeModel.ReturnValue);
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "AddOrEdit");
                return null;
            }

        }
        private RecurringTypeModel RecurringTypeInsertDetail(DataSet ds)
        {
            RecurringTypeModel data = new RecurringTypeModel();
            data.ReturnValue = -1;
            try
            {
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        data.ReturnValue = Convert.ToInt32(row["ReturnValue"]);

                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                data.ReturnValue = -1;
                ApplicationLogger.LogError(ex, "DonationAccess", "RecurringTypeDetial");
                return null;
            }
        }
        public RecurringTyperesponse Delete(RecurringTypeModel model)
        {
            Int32 returnResult = 0;
            RecurringTyperesponse response = new RecurringTyperesponse();

            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                //model.RecurringTypeId =Convert.ToInt32(UtilityAccess.Decrypt(Convert.ToString(model.RecurringTypeId)));
                model.UserId = UtilityAccess.Decrypt(model.UserId);
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                returnResult = donationData.Delete(model);
                if (returnResult > 0)
                {
                    response.RecurringTypeModel = new RecurringTypeModel();
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "RecurringTypeAccess", "Delete");
                return response;
            }
        }

        public DonationResponse DetailSelectAll(DonationModel model)
        {
            Int32 returnResult = 0;
            DonationResponse result = new DonationResponse();
            result.ReturnCode = 0;
            result.ReturnMessage = Response.Message(returnResult);
            result.DonationModel = new DonationModel();
            DonationModel response = new DonationModel();
            try
            {

                model.UserId = Convert.ToString(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.FollowerId = UtilityAccess.Decrypt(model.EncryptedFollowerId);

                if (!String.IsNullOrEmpty(model.EncryptedPaymentId))
                    model.PaymentId =Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedPaymentId));
                //model.PaymentId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedPaymentId));
                DataSet ds = donationData.DetailSelectAll(model, out returnResult);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {

                            response = new DonationModel();
                            response.PaymentMethodId = Convert.ToInt32(row["PaymentMethodId"]);
                            response.CardHolderName = Convert.ToString(row["CardHolderName"]);
                            response.Email = Convert.ToString(row["Email"]);
                            response.PaymentId= Convert.ToInt32(row["PaymentId"]);
                            response.EncryptedPaymentId= UtilityAccess.Encrypt(Convert.ToString(row["PaymentId"]));
                            response.AddressLine1 = Convert.ToString(row["AddressLine1"]);
                            response.AddressLine2 = Convert.ToString(row["AddressLine2"]);
                            response.StateId = Convert.ToInt32(row["StateId"]);
                            response.StateName = Convert.ToString(row["StateName"]);
                            response.CountryName= Convert.ToString(row["CountryName"]);
                            response.cityName= Convert.ToString(row["cityName"]);
                            response.zipcode= Convert.ToString(row["zipcode"]);
                            response.Occupation = Convert.ToString(row["Accupation"]);
                            response.ISDCode= Convert.ToString(row["ISDCode"]);
                            response.MobileNumber= Convert.ToString(row["MobileNumber"]);
                            response.TransactionId= Convert.ToString(row["TransactionId"]);
                            response.UserName= Convert.ToString(row["UserName"]);
                            response.Employer = Convert.ToString(row["Employer"]);
                            response.PaymentId = Convert.ToInt32(row["PaymentId"]);
                            response.Amount1 = (UtilityAccess.CurrencyFormat(Convert.ToDecimal(row["Amount"])));
                            response.RefundAmount = (UtilityAccess.CurrencyFormat(Convert.ToDecimal(row["RefundAmount"])));
                            response.FollowerId = Convert.ToString(row["FollowerId"]);
                            response.Volunteer = Convert.ToString(row["IsVolunteer"]);
                            response.Hosting = Convert.ToString(row["IsHostingEvent"]);
                            response.IsRefund = Convert.ToBoolean(row["IsRefunded"]);
                            response.TransactionStatus = Convert.ToString(row["TransactionStatus"]);
                            response.EncryptedFollowerId= UtilityAccess.Encrypt(row["FollowerId"].ToString());
                            response.SpouseName= Convert.ToString(row["SpouseName"]);
                            
                        }
                    }
                }
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    response._DonorDetail = DonorDetailList(ds.Tables[1]);
                }
                response._StatusList = UtilityAccess.PaymentStatusList(2);
                result.ReturnCode = 1;
                result.ReturnMessage = Response.Message(1);
                result.DonationModel = response;
                return result;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "DetailSelectAll");
                return result;
            }
        }

        public List<DonationModel> DonorDetailList(DataTable dt)
        {
            try
            {
                List<DonationModel> data = new List<DonationModel>();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new DonationModel
                            {
                                PaymentMethodId = Convert.ToInt32(row["PaymentMethodId"]),
                                PaymentId = Convert.ToInt32(row["PaymentId"]),
                                EncryptedPaymentId =UtilityAccess.Encrypt(Convert.ToString(row["PaymentId"])),
                                TransactionId = Convert.ToString(row["TransactionId"]),
                                TransactionDate = Convert.ToString(row["TransactionDate"]),
                                Time = Convert.ToString(row["Time"]),
                                CardNumber = UtilityAccess.MaskCardNo(Convert.ToString(row["CardNumber"])),
                                ExpMonth = Convert.ToString(row["ExpMonth"]),
                                ExpYear = Convert.ToString(row["ExpYear"]),
                                IsSkip = Convert.ToBoolean(row["IsSkip"]),
                                Amount = Convert.ToDecimal(row["Amount"]),
                                Status = Convert.ToString(row["Status"]),
                                IsRefund = Convert.ToBoolean(row["IsRefunded"]),
                                TransactionStatus = Convert.ToString(row["TransactionStatus"]),
                                RecurringType = Convert.ToString(row["RecurringType"]),
                                FollowerId = UtilityAccess.Encrypt(Convert.ToString(row["FollowerId"])),
                            });
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "DonationList");
                return null;
            }
        }
        public DonationModel DonorDetail(DataTable dt)
        {
            try
            {
                DonationModel data = new DonationModel();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.TransactionId = Convert.ToString(row["TransactionId"]);
                            data.Amount = Convert.ToInt32(row["Amount"]);
                            data.CardNumber = Convert.ToString(row["CardNumber"]);
                            data.ExpMonth = Convert.ToString(row["ExpMonth"]);
                            data.ExpYear = Convert.ToString(row["ExpYear"]);
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "DonorDetail");
                return null;
            }
        }

        public DonationResponse DetailAddOrEdit(DonationModel model)
        {
            Int32 returnResult = 0;
            DonationResponse response = new DonationResponse();
            response.DonationModel = new DonationModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.UserId = UtilityAccess.Decrypt(model.EncryptedUserId);
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                Int32 UserId = donationData.DetailAddOrEdit(model, out returnResult);
                //if (UserId > 0)
                //{
                //response.ReturnCode = returnResult;
                // response.ReturnMessage = Response.Message(returnResult);
                //}

                return response;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "AddOrEdit");
                return response;
            }
        }


        public int DonorSkip(DonationModel model)
        {
            int returnResult = 0; 
            donationData.DonorSkip(model, out returnResult);
            return returnResult;
        }
        public int AmountChange(DonationModel model)
        {
            int returnResult = 0;
            model.PaymentId = Convert.ToInt32(model.EncryptedPaymentId);
            model.UserId = UtilityAccess.Decrypt(model.EncryptedUserId);
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            donationData.AmountChange(model, out returnResult);
            return returnResult;
        }


        public DonationResponse DonorRefund(DonationModel model)
        {
            Int32 returnResult = 0;
            DonationResponse response = new DonationResponse();
            response.DonationModel = new DonationModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.UserId = UtilityAccess.Decrypt(model.EncryptedUserId);
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                //DataSet ds = donationData.DonorRefund(model, out returnResult);

                //if (ds != null && ds.Tables.Count > 0)
                //{
                //    if (ds.Tables[0].Rows.Count > 0)
                //    {
                //        dModel = DonorDetail(ds.Tables[0]);
                //    }
                //}
                string refundamount = model.Amount1;
                string amount = Convert.ToString(model.Amount);

                model.Amount =Convert.ToDecimal(model.Amount1);
                string apiLogin = string.Empty; //ConfigurationManager.AppSettings["ApiLogin"];
                string transactionKey = string.Empty; //ConfigurationManager.AppSettings["TransactionKey"];
                PaymentMethodData.GetAuthorizeKeys(out apiLogin, out transactionKey);
                net.authorize.RefundTransaction.Refund(apiLogin, transactionKey, model.Amount, model.TransactionId.ToString(), model.CardNumber, model.ExpMonth, model.ExpYear, out response);
               // [FP_DonorPaymentRefundUpdate]
                if (response.ReturnCode == 27)
                {
                    response.ReturnCode = 27;
                    response.ReturnMessage = Response.Message(response.ReturnCode);
                    // model.ReturnCode = RefundEmail(model);
                }
               else if (response.ReturnCode > 0)
                {
                    Int32 myInt = donationData.DonorPaymentRefundUpdate(model,response.AuthTransactionId, out returnResult);
                    if (myInt > 0)
                    {
                        if (returnResult > 1)
                            model.PaymentId = returnResult;
                       response.ReturnCode = 26;
                        response.ReturnMessage = Response.Message(response.ReturnCode);

                        //model.ReturnCode= RefundEmail(model);

                        model.ReturnCode = AppUserAccess.SendDonationEmail(model.CardHolderName, model.Email, model.Amount.ToString(), model.RecurringType, string.Empty, model.AddressLine1, model.TransactionDate, model.TransactionTime, model.PaymentId.ToString(), response.AuthTransactionId, "Refunded", model.CardType, model.CardNumber, "", model.AddressLine2, model.cityName, refundamount);


                    }
                    
                }
                else if (response.ReturnCode == 0)
                {
                    response.ReturnCode = 25;
                    response.ReturnMessage = Response.Message(response.ReturnCode);
                   // model.ReturnCode = RefundEmail(model);
                }
                
                else
                {
                    response.DonationModel.PaymentId = model.PaymentId;

                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "DonorRefund");
                return response;
            }
        }
        public string PdfPrintShare(List<DonationModel> model, DonationModel modell, string type, string senderName, string emailTo, out int ReturnResult)
        {
            ReturnResult = 1;
            DonationModel data = new DonationModel();
            data.UserId = "2";
            //data.UserId = model.UserId;
            string UserIdd = "1";
            try
            {
                modell.UserId = UtilityAccess.Decrypt(modell.UserId);
                String _directory = HttpContext.Current.Server.MapPath(@"~/Invoices/");

                if (!Directory.Exists(_directory))
                    Directory.CreateDirectory(_directory);

                string reutnPath = "/donations_receipt_" + ".pdf";
                string filePath = _directory + reutnPath;
                String _logo = HttpContext.Current.Server.MapPath("/content/images/logo@2x.png");

                using (FileStream msReport = new FileStream(filePath, FileMode.OpenOrCreate))
                {
                    Document pdfDocument = new Document(PageSize.A4, 50f, 20f, 70f, 80f);

                    PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, msReport);
                    PdfPTable pdfPTable = null;
                    Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
                    Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
                    Phrase phrase = null;

                    // pdfWriter.PageEvent = new ITextEvents();
                    pdfDocument.Open();

                    pdfPTable = new PdfPTable(3);
                    pdfPTable.DefaultCell.Phrase = new Phrase() { Font = normalFont };
                    pdfPTable.TotalWidth = 500f;
                    pdfPTable.LockedWidth = true;
                    pdfPTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    pdfPTable.SetWidths(new float[] { 0.2f, 0.3f, 0.3f });
                    // header
                    Image logo = Image.GetInstance(_logo);
                    logo.ScaleAbsoluteWidth(80);
                    logo.ScaleAbsoluteHeight(30);
                    pdfPTable.AddCell(logo);
                    //pdfPTable.AddCell("");
                    // pdfPTable.AddCell("");

                    // row 1
                    var paragraph = new Paragraph();
                    paragraph.IndentationLeft = 150;

                    //paragraph.Add(data.FirstName ?? string.Empty ?? "" + data.MI ?? string.Empty ?? "" + "\n");
                    //paragraph.Add(data.LastName ?? string.Empty ?? "" + "\n");
                    //paragraph.Add(data.AddressLine1 ?? string.Empty ?? "" + "\n");
                    //paragraph.Add(data.DonationType ?? string.Empty ?? "" + " ");
                    //paragraph.Add(data.Duration ?? string.Empty ?? "" + " ");
                    // paragraph.Add(data.Amount + "\n");
                    //pdfPTable.AddCell(paragraph);

                    //paragraph = new Paragraph();
                    //phrase = new Phrase("Receipt Date\n", boldFont);
                    //paragraph.Add(phrase);
                    //paragraph.Add(DateTime.Now.ToString() + "\n");
                    //paragraph.Add("Donation Duration\n");
                    //pdfPTable.AddCell(paragraph);

                    //paragraph = new Paragraph();
                    //phrase = new Phrase("Receipt#\n", boldFont);
                    //paragraph.Add(phrase);
                    //paragraph.Add(data.TransactionId + "\n");
                    //phrase = new Phrase("Amount($)", boldFont);
                    //paragraph.Add(phrase);
                    //pdfPTable.AddCell(paragraph);

                    //pdfDocument.Add(pdfPTable);
                    // row 1 end

                    // row 2 start
                    // pdfPTable = new PdfPTable(3);
                    //  paragraph = new Paragraph(3);
                    phrase = new Phrase("Donor Detail\n", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.CardHolderName + "\n");
                    phrase = new Phrase("Email:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.Email + "\n");
                    phrase = new Phrase("Address:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.AddressLine1 + modell.AddressLine2);
                    pdfPTable.AddCell(paragraph);

                    paragraph = new Paragraph();
                    phrase = new Phrase("Employer:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.Employer + "\n");
                    phrase = new Phrase("Occupation:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.Occupation + "\n");
                    phrase = new Phrase("Mobile:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add("+1 (917) 069-1234" + "\n");
                    pdfPTable.AddCell(paragraph);

                    //paragraph = new Paragraph();
                    //pdfPTable.AddCell(paragraph);
                    // add table into document
                    pdfDocument.Add(pdfPTable);
                    try
                    {
                        // item list
                        pdfPTable = new PdfPTable(7);
                        pdfPTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        pdfPTable.DefaultCell.Phrase = new Phrase() { Font = normalFont };
                        pdfPTable.TotalWidth = 500f;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.SpacingBefore = 10f;
                        pdfPTable.SetWidths(new float[] { 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f });
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    //pdfPCell=new PdfPCell()                           
                    // phrase = new Phrase("Items", boldFont);
                    pdfPTable.AddCell(new PdfPCell(new Phrase("Contribution ID", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(150, 150, 150) });
                    pdfPTable.AddCell(new PdfPCell(new Phrase("Transaction ID", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(150, 150, 150) });
                    pdfPTable.AddCell(new PdfPCell(new Phrase("Date", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(150, 150, 150) });
                    pdfPTable.AddCell(new PdfPCell(new Phrase("Time", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(150, 150, 150) });
                    pdfPTable.AddCell(new PdfPCell(new Phrase("Card ", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(150, 150, 150) });
                    pdfPTable.AddCell(new PdfPCell(new Phrase("Amount", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(150, 150, 150) });
                    pdfPTable.AddCell(new PdfPCell(new Phrase("Status", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(150, 150, 150) });

                    for (int i = 0; i < model.Count; i++)
                    {
                        phrase = new Phrase(model[i].PaymentId.ToString(), boldFont);
                        pdfPTable.AddCell(phrase);

                        //pdfPTable.AddCell(phrase);
                        phrase = new Phrase(model[i].TransactionId.ToString(), boldFont);
                        pdfPTable.AddCell(phrase);
                        // phrase = new Phrase(model._BillingInvoices.Sum(x => x.PlanAmount).ToString("0.##"), boldFont);
                        //pdfPTable.AddCell(phrase);
                        phrase = new Phrase(model[i].TransactionDate, boldFont);
                        pdfPTable.AddCell(phrase);

                        //pdfPTable.AddCell(phrase);
                        phrase = new Phrase(model[i].Time, boldFont);
                        pdfPTable.AddCell(phrase);

                        //pdfPTable.AddCell(phrase);
                        phrase = new Phrase(model[i].CardNumber, boldFont);
                        pdfPTable.AddCell(phrase);

                        //pdfPTable.AddCell(phrase);
                        phrase = new Phrase(model[i].Amount.ToString(), boldFont);
                        pdfPTable.AddCell(phrase);

                        phrase = new Phrase(model[i].Status, boldFont);
                        pdfPTable.AddCell(phrase);
                    }
                    // sub total
                    //pdfPTable.AddCell(phrase);

                    // sub total
                    // pdfPTable.AddCell("");
                    // phrase = new Phrase("Amount($)", boldFont);
                    //pdfPTable.AddCell(phrase);
                    //phrase = new Phrase("250", boldFont);
                    // phrase = new Phrase(model._BillingInvoices[0].SaleTax.ToString("0.##"), boldFont);
                    // pdfPTable.AddCell(phrase);
                    // draw line
                    //pdfPTable.AddCell("");
                    //pdfPTable.AddCell(new PdfPCell(new Phrase("")) { Border = PdfPCell.BOTTOM_BORDER, Colspan = 2 });

                    // total
                    //pdfPTable.AddCell("");
                    //phrase = new Phrase("TOTAL", boldFont);
                    //pdfPTable.AddCell(phrase);
                    //phrase = new Phrase((model._BillingInvoices.Sum(x => x.PlanAmount) + model._BillingInvoices[0].SaleTax).ToString("0.##"), boldFont);
                    //phrase = new Phrase("250", boldFont);
                    //pdfPTable.AddCell(phrase);

                    // add table into document
                    pdfDocument.Add(pdfPTable);
                    pdfDocument.Close();
                }

                return reutnPath;
            }

            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PrintDiv", "PrintPdfInvoice");
                return "";
            }
        }

        public String DownloadFile(String CertificateId)
        {
            try
            {
                if (!String.IsNullOrEmpty(CertificateId))
                    CertificateId = UtilityAccess.Decrypt(CertificateId);

                Int32 _CertificateId = 0;
                if (Int32.TryParse(CertificateId, out _CertificateId))
                    return donationData.DownloadFile(_CertificateId);
                else
                    return "";
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "DownloadFile");
                return "";
            }
            finally
            {
            }
        }

        public string PdfPrintreceipt(DonationModel modell, string type, string senderName, string emailTo, out int ReturnResult)
        {
            ReturnResult = 1;
            DonationModel data = new DonationModel();
            data.UserId = "2";
            //data.UserId = model.UserId;
            string UserIdd = "1";
            try
            {
                modell.UserId = UtilityAccess.Decrypt(modell.UserId);
                String _directory = HttpContext.Current.Server.MapPath(@"~/Invoices/");

                if (!Directory.Exists(_directory))
                    Directory.CreateDirectory(_directory);

                string reutnPath = "/DonationReceipt/DonationPaymet_Recipt" + ".pdf";
                string filePath = _directory + reutnPath;
                String _logo = HttpContext.Current.Server.MapPath("/content/images/LogoDownload.png");

                using (FileStream msReport = new FileStream(filePath, FileMode.OpenOrCreate))
                {
                    Document pdfDocument = new Document(PageSize.A4, 50f, 20f, 70f, 80f);

                    PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, msReport);
                    PdfPTable pdfPTable = null;
                    Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
                    Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
                    Phrase phrase = null;

                    // pdfWriter.PageEvent = new ITextEvents();
                    pdfDocument.Open();

                    pdfPTable = new PdfPTable(4);
                    pdfPTable.DefaultCell.Phrase = new Phrase() { Font = normalFont };
                    pdfPTable.TotalWidth = 500f;
                    pdfPTable.LockedWidth = true;
                    pdfPTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    pdfPTable.SetWidths(new float[] { 0.3f, 0.3f, 0.3f,0.3f});
                    // header
                    Image logo = Image.GetInstance(_logo);
                    logo.ScaleAbsoluteWidth(80);
                    logo.ScaleAbsoluteHeight(30);
                    pdfPTable.AddCell(logo);
                    pdfPTable.AddCell("");
                    pdfPTable.AddCell("");

                    // row 1
                    var paragraph = new Paragraph();
                   

                    //paragraph = new Paragraph();
                    //phrase = new Phrase("Receipt Date\n", boldFont);
                    //paragraph.Add(phrase);
                    //paragraph.Add(DateTime.Now.ToString() + "\n");
                    //paragraph.Add("Donation Duration\n");
                    //pdfPTable.AddCell(paragraph);

                    //paragraph = new Paragraph();
                    //phrase = new Phrase("Receipt#\n", boldFont);
                    //paragraph.Add(phrase);
                    //paragraph.Add(data.TransactionId + "\n");
                    //phrase = new Phrase("Amount($)", boldFont);
                    //paragraph.Add(phrase);
                    //pdfPTable.AddCell(paragraph);

                    //pdfDocument.Add(pdfPTable);
                    // row 1 end

                    // row 2 start
                    // pdfPTable = new PdfPTable(3);
                    paragraph = new Paragraph(4);

                    phrase = new Phrase("Doner Name:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.CardHolderName +"\n");

                    phrase = new Phrase("Contribution ID:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.PaymentId +"\n");

                    phrase = new Phrase("Transaction Id:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.TransactionId +"\n");

                    phrase = new Phrase("Amount($):-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.Amount + "\n");

                    phrase = new Phrase("Date:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.TransactionDate + "\n");

                    phrase = new Phrase("Transaction Time:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.TransactionTime + "\n");

                    phrase = new Phrase("Card Number:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.CardNumber + "\n");

                    phrase = new Phrase("Status:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.Status + "\n");

                    pdfPTable.AddCell(paragraph);

                    pdfPTable.AddCell(paragraph);



                    paragraph = new Paragraph(4);
                    phrase = new Phrase("Date:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.TransactionDate + "\n");
                    phrase = new Phrase("Transaction Time:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.TransactionTime + "\n");
                    phrase = new Phrase("Card Number:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.CardNumber + "\n");
                    phrase = new Phrase("Status:-", boldFont);
                    paragraph.Add(phrase);
                    paragraph.Add(modell.Status + "\n");
                    pdfPTable.AddCell(paragraph);

                    paragraph = new Paragraph();
                    pdfPTable.AddCell(paragraph);
                    // add table into document
                    pdfDocument.Add(pdfPTable);
                    

                    pdfDocument.Close();
                }

                return reutnPath;
            }

            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PrintDiv", "PrintPdfInvoice");
                return "";
            }
        }
        public DonationResponse IssueRefund(DonationModel model)
        {
            Int32 returnResult = 0;
            DonationResponse result = new DonationResponse();
            result.ReturnCode = 0;
            result.ReturnMessage = Response.Message(returnResult);
            result.DonationModel = new DonationModel();
            DonationModel response = new DonationModel();
            try
            {

                model.UserId = Convert.ToString(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                //model.PaymentId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedPaymentId));
                DataSet ds = donationData.IssueRefund(model, out returnResult);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {

                            response = new DonationModel();
                            response.PaymentMethodId = Convert.ToInt32(row["PaymentMethodId"]);
                            response.PaymentId = Convert.ToInt32(row["PaymentId"]);
                            response.EncryptedPaymentId =UtilityAccess.Encrypt(Convert.ToString(row["PaymentId"]));
                            response.TransactionId = Convert.ToString(row["TransactionId"]);
                            response.Amount = Convert.ToDecimal(row["Amount"]);
                            response.ExpMonth = Convert.ToString(row["ExpMonth"]);
                            response.ExpYear = Convert.ToString(row["ExpYear"]);
                            response.CardNumber = Convert.ToString(row["CardNumber"]);
                            response.FollowerId = Convert.ToString(row["FollowerId"]);
                            response.EncryptedFollowerId = UtilityAccess.Encrypt(Convert.ToString(row["FollowerId"]));
                            response.TransactionDate = Convert.ToString(row["TransactionDate"]);
                            response.TransactionTime = Convert.ToString(row["TransactionTime"]);
                            response.Email = Convert.ToString(row["Email"]);
                            response.FirstName = Convert.ToString(row["FirstName"]);
                            response.AddressLine1 = Convert.ToString(row["AddressLine1"]);
                            response.AddressLine2 = Convert.ToString(row["AddressLine2"]);
                            response.cityName = Convert.ToString(row["Address3"]);
                            response.CardType = Convert.ToString(row["CardType"]);
                            response.RecurringType = Convert.ToString(row["RecurringType"]);

                        }
                    }
                    if (ds.Tables[1].Rows.Count > 0)
                    {

                        response._ReasonList = UtilityAccess.RenderList(ds.Tables[1], 1);
                        response._ReasonList.Add(new SelectListItem() { Text = "Add New", Value = "-2" });

                    }

                    result.ReturnCode = returnResult;
                    result.ReturnMessage = Response.Message(returnResult);
                    result.DonationModel = response;
                }
                return result;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "IssueRefund");
                return result;
            }
        }
        public DonationResponse ViewRefund(DonationModel model)
        {
            Int32 returnResult = 0;
            DonationResponse result = new DonationResponse();
            result.ReturnCode = 0;
            result.ReturnMessage = Response.Message(returnResult);
            result.DonationModel = new DonationModel();
            DonationModel response = new DonationModel();
            try
            {

                model.UserId = Convert.ToString(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                //model.PaymentId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedPaymentId));
                DataSet ds = donationData.ViewRefund(model, out returnResult);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {

                            response = new DonationModel();
                            response.PaymentMethodId = Convert.ToInt32(row["PaymentMethodId"]);
                            response.PaymentId = Convert.ToInt32(row["PaymentId"]);
                            response.EncryptedPaymentId =UtilityAccess.Encrypt(Convert.ToString(row["PaymentId"]));
                            response.TransactionId = Convert.ToString(row["TransactionId"]);
                            response.Amount1 = Convert.ToString(row["Amount"]);
                            response.CardNumber = Convert.ToString(row["CardNumber"]);
                            response.UserName = Convert.ToString(row["UserName"]);
                            response.FollowerId = Convert.ToString(row["FollowerId"]);
                            response.EncryptedFollowerId = UtilityAccess.Encrypt(Convert.ToString(row["FollowerId"]));
                            response.RefundReasonAdd = Convert.ToString(row["Reason"]);
                            response.RefundReasonId = Convert.ToInt32(row["ReasonId"]);
                            response.Comment = Convert.ToString(row["Comment"]);
                            response.FullRefund = Convert.ToString(row["RefundStatus"]);
                            response.CardHolderName = Convert.ToString(row["CardHolderName"]);
                            response.TransactionDate = Convert.ToString(row["TransactionDate"]);
                            response.TransactionTime = Convert.ToString(row["TransactionTime"]);
                            // response.UserName = Convert.ToString(row["UserName"]);
                        }
                    }
                    if (ds.Tables[1].Rows.Count > 0)
                    {

                        response._ReasonList = UtilityAccess.RenderList(ds.Tables[1], 1);
                        response._ReasonList.Add(new SelectListItem() { Text = "Add New", Value = "0" });

                    }

                    result.ReturnCode = returnResult;
                    result.ReturnMessage = Response.Message(returnResult);
                    result.DonationModel = response;
                }
                return result;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "IssueRefund");
                return result;
            }
        }
        public static Int32 RefundEmail(DonationModel data)
        {
            Int32 RetVal = -1;
            try
            {

                if (!String.IsNullOrEmpty(data.Email))
                {
                    String url = ConfigurationManager.AppSettings["webUrl"].ToString();
                    url += "/donation/refund?ur=" + UtilityAccess.Encrypt(Convert.ToString(data.UserId));
                    url += "&tm=" + UtilityAccess.Encrypt(Convert.ToString(UtilityAccess.DateTimeToUnixTimestamp(DateTime.Now)));

                    String _body = EmailUtility.ReadHtml("RefundEmail.html")
                         .Replace("{firstname}", data.FirstName)
                         .Replace("{amount}", data.Amount1)
                        .Replace("{email}", data.Email);

                    RetVal = EmailUtility.SendEmail("Josh Team", data.Email, "Refund Successfull", _body, true);
                }
                return RetVal;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "RefundEmail");
                return RetVal;
            }
        }

    }
}