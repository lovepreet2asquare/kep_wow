﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using FPDAL.Data;
using FPModels.Models;

namespace FPBAL.Business
{
    public class EmailUtility
    {
        public static int SendEmailNew(string strEmailTo, string fromName, string strEmailSubject, string strEmailBody, out Exception exObj, bool blnIsHtml = false)
        {
            if (string.IsNullOrEmpty(fromName))
                fromName = "NJPBA Team";
            exObj = null;
            if (!string.IsNullOrEmpty(strEmailTo))
            {
                try
                {
                    string smtpFromEmail = ConfigurationManager.AppSettings["smtpFromEmail"].ToString();
                    string smtpMailPassword = ConfigurationManager.AppSettings["smtpMailPassword"].ToString();
                    string smtpClient = ConfigurationManager.AppSettings["smtpClient"].ToString();

                    //string smtpFromEmail = "support@kepleredge.com";//ConfigurationManager.AppSettings["smtpfromemail"].ToString();
                    //string smtpMailPassword = "x9q7^4Oy";// ConfigurationManager.AppSettings["smtppassword"].ToString();
                    //string smtpClient = "kepleredge.com"; //ConfigurationManager.AppSettings["smtpclient"].ToString();
                    //string smtpFromEmail = ConfigurationManager.AppSettings["smtpFromEmail"].ToString();
                    //string smtpMailPassword = ConfigurationManager.AppSettings["smtpMailPassword"].ToString();
                    //string smtpClient = ConfigurationManager.AppSettings["smtpClient"].ToString();


                    MailMessage msg = new MailMessage();
                    msg.ReplyToList.Add(strEmailTo);
                    msg.To.Add(strEmailTo);
                    msg.From = new MailAddress(smtpFromEmail, fromName);
                    msg.Subject = strEmailSubject;
                    msg.Body = strEmailBody;

                    msg.BodyEncoding = System.Text.Encoding.UTF8;// System.Text.Encoding.GetEncoding("utf-8");
                    msg.SubjectEncoding = System.Text.Encoding.Default;
                    msg.IsBodyHtml = blnIsHtml;




                    /******** Using Gmail Domain ********/
                    SmtpClient client = new SmtpClient(smtpClient, 25);
                    client.Credentials = new System.Net.NetworkCredential(smtpFromEmail, smtpMailPassword);
                    //SmtpClient client = new SmtpClient("relay.secureserver.net", 25);
                    //client.UseDefaultCredentials = true;
                    client.EnableSsl = false;
                    /************************************/

                    /******** Using Yahoo Domain ********/
                    //SmtpClient client = new SmtpClient("smtp.mail.yahoo.com", 25);
                    /************************************/

                    client.Send(msg);         // Send our email.
                    msg = null;

                    return 1;
                }
                catch (Exception ex)
                {
                    exObj = ex;
                    return -1;
                }
            }
            return 1;
        }

        public static int SendEmailNew1(string strEmailTo, string fromName, string strEmailSubject, string strEmailBody, out Exception exObj, bool blnIsHtml = false)
        {
            if (string.IsNullOrEmpty(fromName))
                fromName = "NJPBA Team";
            exObj = null;
            if (!string.IsNullOrEmpty(strEmailTo))
            {
                try
                {
                    //string smtpFromEmail = "njspbarequest@keplerconnect.com";
                    //string smtpMailPassword = "Kepler@321";
                    //string smtpClient = "webmail.keplerconnect.com";
                    string smtpFromEmail = ConfigurationManager.AppSettings["smtpFromEmail"].ToString();
                    string smtpMailPassword = ConfigurationManager.AppSettings["smtpMailPassword"].ToString();
                    string smtpClient = ConfigurationManager.AppSettings["smtpClient"].ToString();


                    MailMessage msg = new MailMessage();
                    msg.ReplyToList.Add(strEmailTo);
                    msg.To.Add(strEmailTo);
                    msg.From = new MailAddress(smtpFromEmail, fromName);
                    msg.Subject = strEmailSubject;
                    msg.Body = strEmailBody;

                    msg.BodyEncoding = System.Text.Encoding.UTF8;// System.Text.Encoding.GetEncoding("utf-8");
                    msg.SubjectEncoding = System.Text.Encoding.Default;
                    msg.IsBodyHtml = blnIsHtml;




                    /******** Using Gmail Domain ********/
                    SmtpClient client = new SmtpClient(smtpClient, 25);
                  //  SmtpClient client = new SmtpClient(smtpClient, 587);
                    client.Credentials = new System.Net.NetworkCredential(smtpFromEmail, smtpMailPassword);

                    client.EnableSsl = false;
                    //SmtpClient client = new SmtpClient(smtpClient, 25);
                    //client.Credentials = new System.Net.NetworkCredential(smtpFromEmail, smtpMailPassword);
                    ////SmtpClient client = new SmtpClient("relay.secureserver.net", 25);
                    ////client.UseDefaultCredentials = true;
                    //client.EnableSsl = false;
                    /************************************/

                    /******** Using Yahoo Domain ********/
                    //SmtpClient client = new SmtpClient("smtp.mail.yahoo.com", 25);
                    /************************************/

                    client.Send(msg);         // Send our email.
                    msg = null;

                    return 1;
                }
                catch (Exception ex)
                {
                    exObj = ex;
                    return -1;
                }
            }
            return 1;
        }


        public static int SendEmailWithAttachment(string filePath, string strEmailTo, string fromName, string strEmailSubject, string strEmailBody, out Exception exObj, bool blnIsHtml = false)
        {
            if (string.IsNullOrEmpty(fromName))
                fromName = "NJPBA Team";
            exObj = null;
            if (!string.IsNullOrEmpty(strEmailTo))
            {
                try
                {
                    string smtpFromEmail = ConfigurationManager.AppSettings["smtpFromEmail"].ToString();
                    string smtpMailPassword = ConfigurationManager.AppSettings["smtpMailPassword"].ToString();
                    string smtpClient = ConfigurationManager.AppSettings["smtpClient"].ToString();

                  //  string smtpFromEmail = "support@kepleredge.com";//ConfigurationManager.AppSettings["smtpfromemail"].ToString();
                   // string smtpMailPassword = "x9q7^4Oy";// ConfigurationManager.AppSettings["smtppassword"].ToString();
                  //  string smtpClient = "kepleredge.com"; //ConfigurationManager.AppSettings["smtpclient"].ToString();
                                                             //string smtpFromEmail = ConfigurationManager.AppSettings["smtpFromEmail"].ToString();
                                                             //string smtpMailPassword = ConfigurationManager.AppSettings["smtpMailPassword"].ToString();
                                                             //string smtpClient = ConfigurationManager.AppSettings["smtpClient"].ToString();
                    using (MailMessage msg = new MailMessage())
                    {
                        msg.ReplyToList.Add(strEmailTo);
                        msg.To.Add(strEmailTo);
                        msg.From = new MailAddress(smtpFromEmail, fromName);
                        msg.Subject = strEmailSubject;
                        msg.Body = strEmailBody;

                        System.Net.Mail.Attachment attachment;
                        attachment = new System.Net.Mail.Attachment(filePath);
                        msg.Attachments.Add(attachment);


                        msg.BodyEncoding = System.Text.Encoding.UTF8;// System.Text.Encoding.GetEncoding("utf-8");
                        msg.SubjectEncoding = System.Text.Encoding.Default;
                        msg.IsBodyHtml = blnIsHtml;

                        /******** Using Gmail Domain ********/
                        SmtpClient client = new SmtpClient(smtpClient, 25);
                        client.Credentials = new System.Net.NetworkCredential(smtpFromEmail, smtpMailPassword);
                        //SmtpClient client = new SmtpClient("relay.secureserver.net", 25);
                        //client.UseDefaultCredentials = true;
                        client.EnableSsl = false;
                        /************************************/

                        /******** Using Yahoo Domain ********/
                        //SmtpClient client = new SmtpClient("smtp.mail.yahoo.com", 25);
                        /************************************/

                        client.Send(msg);         // Send our email.
                        //msg = null;
                    }
                    return 1;
                }
                catch (Exception ex)
                {
                    exObj = ex;
                    return -1;
                }
            }
            return 1;
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="SenderName"></param>
        /// <param name="strEmailTo"></param>
        /// <param name="strEmailSubject"></param>
        /// <param name="strEmailBody"></param>
        /// <param name="FileName">Optional parameter to read html file and pass into email's body</param>
        /// <param name="blnIsHtml"></param>
        /// <returns></returns>
        public static int SendEmail(String senderName, String emailTo, String subject, String emailBody, bool blnIsHtml = false)
        {
            try
            {
                //
                string smtpFromEmail =ConfigurationManager.AppSettings["smtpFromEmail"].ToString();
                string smtpMailPassword =ConfigurationManager.AppSettings["smtpMailPassword"].ToString();
                string smtpClient = ConfigurationManager.AppSettings["smtpClient"].ToString();

                MailMessage msg = new MailMessage();
                msg.To.Add(emailTo);
                //msg.From = new MailAddress("sukhvirs63@gmail.com", strEmailSubject);
                msg.From = new MailAddress(smtpFromEmail, "NJPBA");

                msg.Subject = subject;
                msg.Body = emailBody;
                msg.IsBodyHtml = blnIsHtml;

                /******** Using Gmail Domain ********/
                SmtpClient client = new SmtpClient(smtpClient, 25);
                client.Credentials = new System.Net.NetworkCredential(smtpFromEmail, smtpMailPassword);
                
                client.EnableSsl = false;
                //client.UseDefaultCredentials = true;
                /************************************/

                /******** Using Yahoo Domain ********/
                //SmtpClient client = new SmtpClient("smtp.mail.yahoo.com", 25);
                /************************************/

                client.Send(msg);         // Send our email.
                    msg = null;
                

                return 1;
            }
            catch (Exception exc)
            {
                ApplicationLogger.LogError(exc, "EmailUtility", "SendEmail");
                return -1;
            }
        }

        public static int SendBccEmail(String senderName, String emailTo, String subject, String emailBody, bool blnIsHtml = false)
        {
            try
            {
                string smtpFromEmail = ConfigurationManager.AppSettings["smtpFromEmail"].ToString();
                string smtpMailPassword = ConfigurationManager.AppSettings["smtpMailPassword"].ToString();
                string smtpClient = ConfigurationManager.AppSettings["smtpClient"].ToString();

               // string smtpFromEmail = "support@kepleredge.com";//ConfigurationManager.AppSettings["smtpfromemail"].ToString();
               // string smtpMailPassword = "x9q7^4Oy";// ConfigurationManager.AppSettings["smtppassword"].ToString();
              //  string smtpClient = "kepleredge.com"; //ConfigurationManager.AppSettings["smtpclient"].ToString();

                MailMessage msg = new MailMessage();
                msg.Bcc.Add(emailTo);
                //msg.From = new MailAddress("sukhvirs63@gmail.com", strEmailSubject);
                msg.From = new MailAddress(smtpFromEmail, senderName);

                msg.Subject = subject;
                msg.Body = emailBody;
                msg.IsBodyHtml = blnIsHtml;

                /******** Using Gmail Domain ********/
                SmtpClient client = new SmtpClient(smtpClient, 25);
                client.Credentials = new System.Net.NetworkCredential(smtpFromEmail, smtpMailPassword);
                //client.UseDefaultCredentials = false;
                client.EnableSsl = false;
                /************************************/

                /******** Using Yahoo Domain ********/
                //SmtpClient client = new SmtpClient("smtp.mail.yahoo.com", 25);
                /************************************/
                client.Send(msg);         // Send our email.
                msg = null;
                return 1;
            }
            catch (Exception exc)
            {
                ApplicationLogger.LogError(exc, "EmailUtility", "SendEmail");
                return -1;
            }
        }

        public static String ReadHtml(String FileName)
        {
            string body = string.Empty;
            try
            {
                string Url = "~/newsletters/user" + "/" + FileName;
                //string path = Path.Combine(HttpContext.Current.Server.MapPath("/newsletters/user/" + FileName));
                string path = "";
                if (HttpContext.Current != null)
                {
                    path = HttpContext.Current.Server.MapPath(Url);
                }
                else
                {
                    path = System.Web.Hosting.HostingEnvironment.MapPath(Url);
                }
                using (StreamReader reader = new StreamReader(path))
                {
                    body = reader.ReadToEnd();
                }
                return body;
            }
            catch (Exception exc)
            {
                ApplicationLogger.LogError(exc, "EmailUtility", "ReadHtml");
                return "";
            }
        }

        public static int SendBulkEmails(DataTable dt, String subject, String emailBody, bool blnIsHtml = false)
        {
            int msgg = 0;
            try
            {
                Parallel.ForEach(dt.AsEnumerable(), row =>
                {
                    msgg = EmailUtilityNew.SendMessageSmtpNew(row["FirstName"].ToString(), row["Email"].ToString(), subject, emailBody, blnIsHtml);
                  //  msgg = SendEmail(row["FirstName"].ToString(), row["Email"].ToString(), subject, emailBody, blnIsHtml);
                    Thread.Sleep(300);
                });



            }
            catch (Exception ex)
            {
                return -1;
            }
            return msgg;
        }

    }
}
