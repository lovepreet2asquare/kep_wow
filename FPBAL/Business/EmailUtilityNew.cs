﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using FPDAL.Data;
using MimeKit;
using MailKit;
using MailKit.Net.Smtp;

namespace FPBAL.Business
{
    public class EmailUtilityNew
    {
        public static String ReadHtml(String FileName)
        {
            string body = string.Empty;
            try
            {
                string Url = "~/newsletters/user" + "/" + FileName;
                //string path = Path.Combine(HttpContext.Current.Server.MapPath("/newsletters/user/" + FileName));
                string path = "";
                if (HttpContext.Current != null)
                {
                    path = HttpContext.Current.Server.MapPath(Url);
                }
                else
                {
                    path = System.Web.Hosting.HostingEnvironment.MapPath(Url);
                }
                using (StreamReader reader = new StreamReader(path))
                {
                    body = reader.ReadToEnd();
                }
                return body;
            }
            catch (Exception exc)
            {
                ApplicationLogger.LogError(exc, "EmailUtilityNew", "ReadHtml");
                return "";
            }
        }
     public static int SendMessageSmtpNew1(String senderName, String emailTo, String emailBody)
      {
            try
            {

                MimeMessage mail = new MimeMessage();
                //mail.From.Add(new MailboxAddress("Excited Admin", "foo@YOUR_DOMAIN_NAME"));
                mail.From.Add(new MailboxAddress("NJPBA", "njpba105@keplerconnect.com"));
                mail.To.Add(new MailboxAddress(senderName, emailTo));
                mail.Subject = "Invitation From NJPBA";
                //mail.Body = new TextPart("plain")
                //{
                //    Text = @emailBody,
                //};
                var bodyBuilder = new BodyBuilder();
                bodyBuilder.HtmlBody = emailBody;

                mail.Body = bodyBuilder.ToMessageBody();
                // Send it!
                using (var client = new SmtpClient())
                {
                    // XXX - Should this be a little different?
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect("smtp.mailgun.org", 587, false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate("njpba105@keplerconnect.com", "e2403f3b9c827206aeed86a88c402952-c322068c-760d8d2c");

                    client.Send(mail);
                    client.Disconnect(true);
                }
                return 1;
            }

            catch (Exception exc)
            {
                ApplicationLogger.LogError(exc, "EmailUtilityNew", "SendEmail");
                return -1;
            }

        }
        public static int SendEmail(String senderName, String emailTo, String subject, String emailBody, bool blnIsHtml = false)
        {
            try
            {
                //
                string smtpFromEmail = ConfigurationManager.AppSettings["smtpFromEmail"].ToString();
                string smtpMailPassword = ConfigurationManager.AppSettings["smtpMailPassword"].ToString();
                string smtpClient = ConfigurationManager.AppSettings["smtpClient"].ToString();

                MimeMessage msg = new MimeMessage();
                msg.To.Add(new MailboxAddress(senderName, emailTo));
                //msg.From = new MailAddress("sukhvirs63@gmail.com", strEmailSubject);
                msg.From.Add(new MailboxAddress("NJSPBA", "njspba@keplerconnect.com"));

                msg.Subject = subject;
                var bodyBuilder = new BodyBuilder();
                bodyBuilder.HtmlBody = emailBody;

                msg.Body = bodyBuilder.ToMessageBody();



                /******** Using Gmail Domain ********/
                using (var client = new SmtpClient())
                {
                    // XXX - Should this be a little different?
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect("smtp.mailgun.org", 587, false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate("njspba@keplerconnect.com", "a6c013770239c8a64bbaf137142fdc02-f8faf5ef-f3498d19");

                    client.Send(msg);
                    client.Disconnect(true);
                }
                return 1;

              
            }
            catch (Exception exc)
            {
                ApplicationLogger.LogError(exc, "EmailUtility", "SendEmail");
                return -1;
            }
        }
        public static int SendEmailNew1(string strEmailTo, string fromName, string strEmailSubject, string strEmailBody, out Exception exObj, bool blnIsHtml = false)
        {
            if (string.IsNullOrEmpty(fromName))
                fromName = "NJPBA Team";
            exObj = null;
            if (!string.IsNullOrEmpty(strEmailTo))
            {
                try
                {
                    //string smtpFromEmail = "njspbarequest@keplerconnect.com";
                    //string smtpMailPassword = "Kepler@321";
                    //string smtpClient = "webmail.keplerconnect.com";
                    string smtpFromEmail = ConfigurationManager.AppSettings["smtpFromEmail"].ToString();
                    string smtpMailPassword = ConfigurationManager.AppSettings["smtpMailPassword"].ToString();
                    string smtpClient = ConfigurationManager.AppSettings["smtpClient"].ToString();


                    MimeMessage mail = new MimeMessage();
                    //mail.From.Add(new MailboxAddress("Excited Admin", "foo@YOUR_DOMAIN_NAME"));
                    mail.From.Add(new MailboxAddress("NJPBA", "njpba105@keplerconnect.com"));
                    mail.To.Add(new MailboxAddress(fromName, strEmailTo));
                    mail.Subject = strEmailSubject;
                    //mail.Body = new TextPart("plain")
                    //{
                    //    Text = @emailBody,
                    //};
                    var bodyBuilder = new BodyBuilder();
                    bodyBuilder.HtmlBody = strEmailBody;

                    mail.Body = bodyBuilder.ToMessageBody();
                    // Send it!
                    using (var client = new SmtpClient())
                    {
                        // XXX - Should this be a little different?
                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                        client.Connect("smtp.mailgun.org", 587, false);
                        client.AuthenticationMechanisms.Remove("XOAUTH2");
                        client.Authenticate("njpba105@keplerconnect.com", "e2403f3b9c827206aeed86a88c402952-c322068c-760d8d2c");

                        client.Send(mail);
                        client.Disconnect(true);
                    }
                    return 1;
                }
                catch (Exception ex)
                {
                    exObj = ex;
                    return -1;
                }
            }
            return 1;
        }


        public static int SendMessageSmtpNew(String senderName, String emailTo, String subject, String emailBody, bool blnIsHtml = false)
        {
            try
            {

                MimeMessage mail = new MimeMessage();
                //mail.From.Add(new MailboxAddress("Excited Admin", "foo@YOUR_DOMAIN_NAME"));
                mail.From.Add(new MailboxAddress("NJPBA", "njpba105@keplerconnect.com"));
                mail.To.Add(new MailboxAddress(senderName, emailTo));
                mail.Subject = subject;
                //mail.Body = new TextPart("plain")
                //{
                //    Text = @emailBody,
                //};
                var bodyBuilder = new BodyBuilder();
                bodyBuilder.HtmlBody = emailBody;

                mail.Body = bodyBuilder.ToMessageBody();
                // Send it!
                using (var client = new SmtpClient())
                {
                    // XXX - Should this be a little different?
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect("smtp.mailgun.org", 587, false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate("njpba105@keplerconnect.com", "e2403f3b9c827206aeed86a88c402952-c322068c-760d8d2c");
                   
                    client.Send(mail);
                    client.Disconnect(true);
                }
                return 1;
            }
            
            catch (Exception exc)
            {
                ApplicationLogger.LogError(exc, "EmailUtilityNew", "SendEmailForInvitation");
                return -1;
            }

        }

        public static void SendMessageSmtp()
        {
            // Compose a message
            MimeMessage mail = new MimeMessage();
            //mail.From.Add(new MailboxAddress("Excited Admin", "foo@YOUR_DOMAIN_NAME"));
            mail.From.Add(new MailboxAddress("NJPBA", "njpba105@keplerconnect.com"));
            mail.To.Add(new MailboxAddress("Excited User", "lovepreet@2asquare.com"));
            mail.Subject = "Hello";
            mail.Body = new TextPart("plain")
            {
                Text = @"Testing some Mailgun awesomesauce!",
            };

            // Send it!
            using (var client = new SmtpClient())
            {
                // XXX - Should this be a little different?
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect("smtp.mailgun.org", 587, false);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate("njpba105@keplerconnect.com", "e2403f3b9c827206aeed86a88c402952-c322068c-760d8d2c");

                client.Send(mail);
                client.Disconnect(true);
            }

        }
   
    }
}
