﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace FPBAL.Business
{
    public class EventAccess : IEventAccess
    {
        EventData eventData = new EventData();
        public EventResponse EventSelectAll(EventModel model)
        {
            EventResponse eventResponse = new EventResponse();
            eventResponse.ReturnCode = 0;
            eventResponse.ReturnMessage = Response.Message(0);
            eventResponse._EventModel = new EventModel();

            List<EventModel> eventList = new List<EventModel>();

            EventModel eventInfo = new EventModel();
            try
            {
                if (!string.IsNullOrEmpty(model.DateFrom))
                    model.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                if (!string.IsNullOrEmpty(model.DateTo))
                    model.DateTo = UtilityAccess.ToDate(model.DateTo);
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                int returnResult = 0;
                DataSet ds = eventData.EventSelectAll(model, out returnResult);
                eventInfo._StatusList = UtilityAccess.StatusList(2);
                if (ds != null && ds.Tables.Count > 0)
                {

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            eventInfo = new EventModel();
                            eventInfo.EventId = Convert.ToInt32(row["EventId"] ?? 0);
                            eventInfo.EncryptedEventId = UtilityAccess.Encrypt(row["EventId"].ToString());
                            eventInfo.Title = Convert.ToString(row["Title"] ?? string.Empty);
                             eventInfo.Status = Convert.ToString(row["Status"] ?? string.Empty);
                            eventInfo.InvitationType = Convert.ToString(row["InvitationType"] ?? string.Empty);
                            eventInfo.AddressLine1= Convert.ToString(row["AddressLine1"] ?? string.Empty);
                            eventInfo.AddressLine2= Convert.ToString(row["AddressLine2"] ?? string.Empty);
                            eventInfo.StateName= Convert.ToString(row["StateName"] ?? string.Empty);
                            eventInfo.CountryName= Convert.ToString(row["CountryName"] ?? string.Empty);
                            eventInfo.Zip= Convert.ToString(row["Zip"] ?? string.Empty);
                            eventInfo.CityName= Convert.ToString(row["CityName"] ?? string.Empty);
                            eventInfo.EventDate = Convert.ToString(row["EventDate"] ?? string.Empty);
                            eventInfo.EventTime = Convert.ToString(row["EventTime"] ?? string.Empty);
                            eventInfo.AgendaId = Convert.ToString(row["AgendaId"] ?? string.Empty);
                            eventInfo.AgendaAdd = Convert.ToString(row["AgendaTitle"] ?? string.Empty);
                            eventInfo.IsAccepted = Convert.ToString(row["IsAccepted"] ?? "0");
                            eventInfo.AttendeeCount = Convert.ToString(row["AttendeeCount"] ?? "0");
                            eventInfo.Declined = Convert.ToString(row["Declined"] ??"0");
                            eventList.Add(eventInfo);
                        }
                    }

                    eventInfo._StatusList = UtilityAccess.StatusList(0);

                    eventResponse._EventModel = new EventModel();
                    eventResponse._EventModel._EventList = new List<EventModel>();
                    eventResponse._EventModel._EventList = eventList;
                    eventResponse._EventModel._StatusList = eventInfo._StatusList;

                    eventResponse.ReturnCode = 1;
                    eventResponse.ReturnMessage = Response.Message(1);

                }
                else
                {
                    eventResponse.ReturnCode = -2;
                    eventResponse.ReturnMessage = Response.Message(-2);
                }
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventAccess", "EventSelectAll");
            }
            return eventResponse;
        }
        
        public EventResponse EventCount(EventModel model)
        {
            EventResponse eventResponse = new EventResponse();
            eventResponse.ReturnCode = 0;
            eventResponse.ReturnMessage = Response.Message(0);
            eventResponse._EventModel = new EventModel();
            List<EventModel> eventList = new List<EventModel>();

            EventModel eventInfo = new EventModel();
            try
            {
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.EventId =Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedEventId));
                int returnResult = 0;
                DataSet ds = eventData.EventCountSelectAll(model, out returnResult);
                if (ds != null && ds.Tables.Count > 0)
                {

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            eventInfo = new EventModel();
                            eventInfo.EventId = Convert.ToInt32(row["EventId"] ?? 0);
                            eventInfo.EncryptedEventId = UtilityAccess.Encrypt(row["EventId"].ToString());
                            eventInfo.Title = Convert.ToString(row["Title"] ?? string.Empty);
                            eventInfo.EventName = Convert.ToString(row["EventName"] ?? string.Empty);
                            eventInfo.Email = Convert.ToString(row["Email"] ?? string.Empty);
                            eventInfo.FirstName = Convert.ToString(row["FirstName"] ?? string.Empty);
                            eventInfo.LastName = Convert.ToString(row["LastName"] ?? string.Empty);
                            eventInfo.MI = Convert.ToString(row["MI"] ?? string.Empty);
                            eventInfo.AttendeeCount = Convert.ToString(row["AttendeeCount"] ?? "0");
                            eventList.Add(eventInfo);
                        }
                    }

                    eventResponse._EventModel = new EventModel();
                    eventResponse._EventModel._EventList = new List<EventModel>();
                    eventResponse._EventModel._EventList = eventList;
                    eventResponse.ReturnCode = 1;
                    eventResponse.ReturnMessage = Response.Message(1);

                }
                else
                {
                    eventResponse.ReturnCode = -2;
                    eventResponse.ReturnMessage = Response.Message(-2);
                }
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventAccess", "EventSelectAll");
            }
            return eventResponse;
        }

        public EventResponse EventDetail(EventModel model)
        {
            EventResponse serviceResponse = new EventResponse();
            EventModel userInfo = new EventModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            EventModel eventInfo = null;
            int returnResult = 0;

            if (!String.IsNullOrEmpty(model.EncryptedEventId))
                model.EventId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedEventId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.UserId =Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            DataSet ds = eventData.SelectById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {
                eventInfo = new EventModel();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        eventInfo.EventId = Convert.ToInt32(row["EventId"] ?? 0);
                        eventInfo.EncryptedEventId = UtilityAccess.Encrypt(row["EventId"].ToString());
                        eventInfo.Title = Convert.ToString(row["Title"] ?? string.Empty);
                        eventInfo.Status = Convert.ToString(row["Status"] ?? string.Empty);
                        eventInfo.InvitationType = Convert.ToString(row["InvitationType"] ?? string.Empty);
                        eventInfo.EventDate = Convert.ToString(row["EventDate"] ?? string.Empty);
                        eventInfo.EventTime = Convert.ToString(row["EventTime"] ?? string.Empty);
                        eventInfo.PublishDate = Convert.ToString(row["PublishDate"] ?? string.Empty);
                        eventInfo.AgendaId = Convert.ToString(row["AgendaId"] ??string.Empty);
                        eventInfo.AgendaAdd = Convert.ToString(row["AgendaTitle"] ??string.Empty);
                        eventInfo.SelectAudience = Convert.ToString(row["AudienceType"] ?? string.Empty);
                        eventInfo.AddressLine1 = Convert.ToString(row["AddressLine1"] ?? string.Empty);
                        eventInfo.AddressLine2 = Convert.ToString(row["AddressLine2"] ?? string.Empty);
                        eventInfo.Summary = Convert.ToString(row["Summary"] ?? string.Empty);
                        eventInfo.URL = Convert.ToString(row["EventURL"] ?? string.Empty);
                        eventInfo.CityName = Convert.ToString(row["CityName"] ?? string.Empty);
                        eventInfo.StateId = Convert.ToInt32(row["StateId"]);
                        eventInfo.StateName = Convert.ToString(row["StateName"] ?? string.Empty);
                        eventInfo.CountryId = Convert.ToInt32(row["CountryId"]);
                        eventInfo.CountryName = Convert.ToString(row["CountryName"] ?? string.Empty);
                        eventInfo.Confirmation = Convert.ToString(row["ConfirmationMsgResponse"] ?? string.Empty);
                        eventInfo.ApproxAudience = Convert.ToString(row["AudienceCount"] ?? string.Empty);
                        eventInfo.LogoPath = Convert.ToString(row["EventImage"] ?? string.Empty);
                        eventInfo.SelectedCountyId = Convert.ToString(row["EventCounties"] ?? string.Empty);
                        eventInfo.SelectedLocalId= Convert.ToString(row["EventLocals"] ?? string.Empty);
                        eventInfo.SelectedIndividualId = Convert.ToString(row["IndividualIds"] ?? string.Empty);
                        eventInfo.EventDayLeft = Convert.ToInt32(row["EventDayLeft"] ?? 0);
                        eventInfo.Zip = Convert.ToString(row["Zip"]);
                        eventInfo.RejectReason = Convert.ToString(row["RejectReason"]??string.Empty);
                        //eventInfo._IndividualList = UtilityAccess.RenderIndividualList(ds.Tables[0], -1);
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    eventInfo._CountryList = UtilityAccess.RenderCountryList(ds.Tables[1]);
                    eventInfo._EventCountryList = UtilityAccess.RenderCountryList(ds.Tables[1]);

                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    //eventInfo._StateList = UtilityAccess.RenderStateList(ds.Tables[2]);
                    eventInfo._StateList = UtilityAccess.RenderList1(ds.Tables[2],1);
                    eventInfo._EventStateList = UtilityAccess.RenderStateList(ds.Tables[2]);
                }
                if (ds.Tables[3].Rows.Count > 0)
                {
                    eventInfo._AgendaList = UtilityAccess.RenderList(ds.Tables[3], 1);
                    eventInfo._AgendaList.Add(new SelectListItem() { Text = "Add New", Value = "-2" });
                }
                if (ds.Tables[4].Rows.Count >= 0)
                {
                    foreach (DataRow row in ds.Tables[4].Rows)
                    {
                        eventInfo.LocalIsChecked = Convert.ToBoolean(row["IsSelected"]);
                    }
                    eventInfo._LocalList = UtilityAccess.RenderIndividualList(ds.Tables[4], -1);
                }
                if (ds.Tables[5].Rows.Count >= 0)
                {
                    foreach (DataRow row in ds.Tables[5].Rows)
                    {
                        eventInfo.CountyIsChecked = Convert.ToBoolean(row["IsSelected"]);
                    }
                    eventInfo._CountyList = UtilityAccess.RenderIndividualList(ds.Tables[5], -1);
                }
                if (ds.Tables[6].Rows.Count >= 0)
                {

                    foreach (DataRow row in ds.Tables[6].Rows)
                    {
                        eventInfo.IndividualIsChecked = Convert.ToBoolean(row["IsSelected"]);
                    }
                    eventInfo._IndividualList = UtilityAccess.RenderIndividualList(ds.Tables[6], -1);
                    
                }
                if (ds.Tables[7].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[7].Rows)
                    {

                        eventInfo.UserTitle = Convert.ToInt32(row["TitleId"] ?? 0);
                    }
                }
                eventInfo._StatusListforAdmin = UtilityAccess.RenderStatusList(1);
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse._EventModel = eventInfo;
            }
            return serviceResponse;
        }

        public EventResponse AddorEdit(EventModel model)
        {
            Int32 returnResult = 0;
            EventResponse response = new EventResponse();
            response._EventModel = new EventModel();
            //default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                if (!string.IsNullOrEmpty(model.EncryptedEventId))
                {
                    model.EventId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedEventId));
                }

                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                
                if (model.Status == "Now")
                {
                    model.Status = "Published";
                }
                string timeZoneid = "India Standard Time";
                if (HttpContext.Current.Session["timezoneid"] != null)
                    timeZoneid = HttpContext.Current.Session["timezoneid"].ToString();
                double timezoneSeconds = UtilityAccess.GetTimeZoneInSeconds(!string.IsNullOrEmpty(model.PublishDate) ? Convert.ToDateTime(model.PublishDate) : DateTime.UtcNow, timeZoneid);
                model.TimeZone = Convert.ToInt32(timezoneSeconds);
                // get rawoffset by station address -- end
                string deviceToken = string.Empty, deviceType = string.Empty, Message = string.Empty, eid = string.Empty;
                Message = model.Title;
                DataSet ds = eventData.AddorEdit(model, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        eid = Convert.ToInt32(row["EventId"]).ToString();
                        deviceToken = Convert.ToString(row["DeviceToken"]);
                        deviceType = Convert.ToString(row["DeviceType"]);
                        //Message = Convert.ToString(row["Title"]);
                        if (deviceType.ToUpper() == "IOS")
                            APNSNotificationAccess.ApnsNotification(deviceToken, Message, eid, "e");
                        else
                           if (deviceType.ToUpper() == "ANDROID")
                            FCMNotificationAccess.SendFCMNotifications(deviceToken, Message, deviceType, eid, "events");
                    }
                }

                response.ReturnCode = returnResult;// 
               //response.ReturnMessage = Response.Message(returnResult);
                if (returnResult == 12)
                {
                    response.ReturnMessage = "Event Updated Successfully.";
                }
                else
                {
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventAccess", "AddorEdit");
                return response;
            }
        }
        public EventResponse AddAgenda(EventModel model)
        {
            Int32 returnResult = 0;
            EventResponse response = new EventResponse();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            response._EventModel = new EventModel();
            //response._EventModel.EncryptedEventId= model.EncryptedEventId;
            try
            {
                
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                eventData.AddAgenda(model,out returnResult);
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);
                return response;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CommunityAccess", "LanguagesUpdate");
                response.ReturnCode = -1;
                return response;
            }
        }

        public void PublishNSendNotification()
        {
            try
            {
                string deviceToken = string.Empty, deviceType = string.Empty, Message = string.Empty, eid = string.Empty;
                int returnResult = 0;
                DataSet ds = eventData.PublishNSendNotification(out returnResult);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                       

                        //if (ds.Tables[1].Rows.Count > 0)
                        //{
                        //    foreach (DataRow row in ds.Tables[1].Rows)
                        //    {
                                Message = Convert.ToString(row["Title"]);
                                eid = Convert.ToInt32(row["EventId"]).ToString();
                                deviceToken = Convert.ToString(row["DeviceToken"]);
                                deviceType = Convert.ToString(row["DeviceType"]);
                                
                                if (deviceType.ToUpper() == "IOS")
                                    APNSNotificationAccess.ApnsNotification(deviceToken, Message, eid, "e");
                                else
                                   if (deviceType.ToUpper() == "ANDROID")
                                    FCMNotificationAccess.SendFCMNotifications(deviceToken, Message, deviceType, eid, "events");
                     //       }
                     //   }
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventAccess", "PublishNSendNotification");

            }
        }

        public EventResponse Delete(EventModel model)
        {
            EventResponse serviceResponse = new EventResponse();
            EventModel userInfo = new EventModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            int returnResult = 0;

            if (model != null && model.EncryptedEventId != null)
                model.EventId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedEventId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = eventData.Delete(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
            }

            return serviceResponse;
        }

        public EventResponse Archive(EventModel model)
        {
            EventResponse serviceResponse = new EventResponse();
            EventModel userInfo = new EventModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            int returnResult = 0;
            if (model != null && model.EncryptedEventId!= null)
                model.EventId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedEventId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = eventData.Archive(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
            }
            return serviceResponse;
        }
        private static void addCellnoborder2(PdfPTable table, string text, float top, float bottom, float left, float right, BaseColor bckcolor, BaseColor forecolor)
        {//FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1257, false);
            Font times = new Font(bfTimes, 14, Font.BOLD, forecolor);

            PdfPCell cell = new PdfPCell(new Phrase(text, times));
            cell.BackgroundColor = bckcolor;
            cell.Colspan = 1;
            cell.BorderWidthTop = top;
            cell.BorderWidthBottom = bottom;
            cell.BorderWidthRight = right;
            cell.BorderWidthLeft = left;
            table.DefaultCell.FixedHeight = 80f;
            cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            cell.VerticalAlignment = PdfPCell.ALIGN_LEFT;

            table.SpacingAfter = 1;
            table.AddCell(cell);
        }
        public string PdfPrintShare(List<EventModel> model, EventModel modell, string type, string senderName, string emailTo, out int ReturnResult)
        {
            ReturnResult = 1;
            EventModel data = new EventModel();
            data.UserId = 2;
            string UserIdd = "1";
            string reutnPath = "";

            if (model != null && model.Count > 0)
            {
                try
                {
                    string EventName = model[1].EventName.ToString();

                    //modell.UserId =Convert.ToInt32(UtilityAccess.Decrypt(modell.EncryptedUserId));
                    String _directory = HttpContext.Current.Server.MapPath(@"~/upload/Event/");

                    if (!Directory.Exists(_directory))
                        Directory.CreateDirectory(_directory);

                    reutnPath = "/Event_Count" + ".pdf";
                    string filePath = _directory + reutnPath;
                    String _logo = HttpContext.Current.Server.MapPath("/content/images/logo_NJ.jpg");

                    using (FileStream msReport = new FileStream(filePath, FileMode.OpenOrCreate))
                    {
                        Document pdfDocument = new Document(PageSize.A4, 50f, 20f, 70f, 80f);

                        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, msReport);
                        PdfPTable pdfPTable = null;
                        Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
                        Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
                        Phrase phrase = null;

                        // pdfWriter.PageEvent = new ITextEvents();

                        pdfDocument.Open();

                        pdfPTable = new PdfPTable(3);
                        pdfPTable.DefaultCell.Phrase = new Phrase() { Font = normalFont };
                        pdfPTable.TotalWidth = 500f;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        pdfPTable.SetWidths(new float[] { 0.2f, 0.3f, 0.3f });
                        // header
                        Image logo = Image.GetInstance(_logo);
                        logo.ScaleAbsoluteWidth(180);
                        logo.ScaleAbsoluteHeight(130);
                        addCellnoborder2(pdfPTable, "", 0f, 0f, 0f, 0f, BaseColor.WHITE, BaseColor.BLACK);
                        pdfPTable.AddCell(logo);
                        addCellnoborder2(pdfPTable, "", 0f, 0f, 0f, 0f, BaseColor.WHITE, BaseColor.BLACK);
                        //pdfDocument.Add(pdfPTable);

                        // row 1
                        var paragraph = new Paragraph();
                        paragraph.IndentationLeft = 150;

                        pdfPTable = new PdfPTable(1);
                        pdfPTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        pdfPTable.DefaultCell.Phrase = new Phrase() { Font = normalFont };
                        pdfPTable.TotalWidth = 500f;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.SpacingBefore = 10f;
                        addCellnoborder2(pdfPTable, EventName, 0f, 0f, 0f, 0f, BaseColor.WHITE, BaseColor.BLACK);
                        pdfPTable.AddCell(new PdfPCell(new Phrase("", boldFont)));
                        pdfDocument.Add(pdfPTable);
                        try
                        {
                            // item list
                            pdfPTable = new PdfPTable(4);
                            pdfPTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            pdfPTable.DefaultCell.Phrase = new Phrase() { Font = normalFont };
                            pdfPTable.TotalWidth = 500f;
                            pdfPTable.LockedWidth = true;
                            pdfPTable.SpacingBefore = 10f;
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        pdfPTable.AddCell(new PdfPCell(new Phrase("Name", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(150, 150, 150) });
                        pdfPTable.AddCell(new PdfPCell(new Phrase("Title", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(150, 150, 150) });
                        pdfPTable.AddCell(new PdfPCell(new Phrase("Email", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(150, 150, 150) });
                        pdfPTable.AddCell(new PdfPCell(new Phrase("Count", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(150, 150, 150) });

                        for (int i = 0; i < model.Count; i++)
                        {
                            phrase = new Phrase(model[i].FirstName.ToString() + " " + model[i].LastName.ToString(), boldFont);
                            pdfPTable.AddCell(phrase);

                            phrase = new Phrase(model[i].Title.ToString(), boldFont);
                            pdfPTable.AddCell(phrase);

                            phrase = new Phrase(model[i].Email, boldFont);
                            pdfPTable.AddCell(phrase);

                            phrase = new Phrase(model[i].AttendeeCount, boldFont);
                            pdfPTable.AddCell(phrase);
                        }

                        pdfDocument.Add(pdfPTable);
                        pdfDocument.Close();
                    }

                    return reutnPath;

                }

                catch (Exception ex)
                {
                    ApplicationLogger.LogError(ex, "PrintDiv", "PrintPdfInvoice");
                    return "";
                }
            }
            return reutnPath;
        }

    }
}
