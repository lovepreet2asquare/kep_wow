﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class EventApiAccess : IEvent
    {
        public EPAboutResponse aboutSelect(EpAboutRequest request)
        {
            Int32 returnResult = 0;
            EPAboutResponse response = new EPAboutResponse();
            response.aboutApiModel = new AboutApiModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                request.FollowerId = request.FollowerId;
                request.SessionToken = request.SessionToken;
                DataSet ds = EventApiData.AboutSelect(request, returnResult);
                if (ds != null && ds.Tables.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                    if (returnResult > 0)
                    {
                        response.aboutApiModel = AboutList(ds, returnResult);
                        returnResult = 2;
                    }
                    else if (returnResult == 0)
                        returnResult = -2;
                    // response message
                    response.ReturnCode = returnResult == -2 ? 0 : returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventApiAccess", "aboutSelect");
                return response;
            }
        }
        private AboutApiModel AboutList(DataSet ds, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                AboutApiModel data = new AboutApiModel();
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                data.ImagePath = Convert.ToString(row2["ImagePath"]);
                                data.Content = Convert.ToString(row2["Content"]);
                                data.UserId = Convert.ToInt32(row2["UserId"]);
                            }
                        }
                    }
                }
                return data;
            }

            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventApiAccess", "AboutList");
                ReturnResult = -1;
                return null;
            }
        }
        public FPEventResponse EventSelect(FPEventRequest request)
        {
            Int32 returnResult = 0;
            FPEventResponse response = new FPEventResponse();
            response.eventApiModel = new List<EventApiModel>();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                request.FollowerId = request.FollowerId;
                request.SessionToken = request.SessionToken;
                DataSet ds = EventApiData.EventSelect(request, returnResult);
                if (ds != null && ds.Tables.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["EventId"]);
                    if (returnResult > 0)
                    {
                        response.eventApiModel = EventList(ds, returnResult);
                        returnResult = 2;
                    }

                    // response message
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventApiAccess", "EventSelect");
                return response;
            }
        }

        private List<EventApiModel> EventList(DataSet ds, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<EventApiModel> data = new List<EventApiModel>();
                EventApiModel eventModel = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                eventModel = new EventApiModel();
                                eventModel.EventId = Convert.ToInt32(row2["EventId"]);
                                eventModel.Title = Convert.ToString(row2["Title"]);
                                eventModel.Content = Convert.ToString(row2["Content"]);
                                eventModel.Location = Convert.ToString(row2["Location"]);
                                eventModel.EventDate = (row2["EventDate"] as string) ?? "";
                                eventModel.EventTime = Convert.ToString(row2["EventTime"]);
                                eventModel.InvitationType = Convert.ToString(row2["InvitationType"]);
                                eventModel.EventDayLeft= Convert.ToString(row2["EventDayLeft"]);
                                data.Add(eventModel);
                            }
                        }
                    }

                }
                return data;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventApiAccess", "EventList");
                ReturnResult = -1;
                return null;
            }
        }

        public FPOfficeLocationResponse OfficeLocationSelect(EpAboutRequest request)
        {
            Int32 returnResult = 0;
            FPOfficeLocationResponse response = new FPOfficeLocationResponse();
            response.officeLocationModel = new List<OfficeLocationModel>();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                request.FollowerId = request.FollowerId;
                request.SessionToken = request.SessionToken;
                DataSet ds = EventApiData.OfficeLocationSelect(request, returnResult);
                if (ds != null && ds.Tables.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["OfficeId"]);
                    if (returnResult > 0)
                    {
                        response.officeLocationModel = OfficeLocationList(ds, returnResult);
                        returnResult = 2;
                    }

                    // response message
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventApiAccess", "OfficeLocationSelect");
                return response;
            }
        }

        private List<OfficeLocationModel> OfficeLocationList(DataSet ds, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<OfficeLocationModel> data = new List<OfficeLocationModel>();
                List<OfficeLocationDayModel> dayTitle = null;
                OfficeLocationModel officeDeatilModel = null;
                OfficeLocationDayModel DayTitleModel = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            dayTitle = new List<OfficeLocationDayModel>();
                            officeDeatilModel = new OfficeLocationModel();

                            officeDeatilModel.OfficeId = Convert.ToInt32(row["OfficeId"]);
                            officeDeatilModel.Title = (row["Title"] as string) ?? "";
                            officeDeatilModel.AddressLine1 = (row["AddressLine1"] as string) ?? "";
                            officeDeatilModel.AddressLine2 = (row["AddressLine2"] as string) ?? "";
                            officeDeatilModel.ZipCode = Convert.ToInt32(row["Zip"]);
                            officeDeatilModel.PhoneNumber = (row["PhoneNumber"] as string) ?? "";
                            officeDeatilModel.FaxNumber = (row["FaxNumber"] as string) ?? "";
                            officeDeatilModel.CreateDate = (row["CreateDate"] as string) ?? "";
                            officeDeatilModel.StateName= (row["StateName"] as string) ?? "";
                            officeDeatilModel.CityName= (row["CityName"] as string) ?? "";
                            if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                            {
                                foreach (DataRow row2 in ds.Tables[1].Select("OfficeId=" + officeDeatilModel.OfficeId))
                                {
                                    DayTitleModel = new OfficeLocationDayModel();
                                    DayTitleModel.DayTitle = (row2["DayTitle"] as string) ?? "";
                                    DayTitleModel.TimeFrom = (row2["TimeFrom"] as string) ?? "";
                                    DayTitleModel.TimeTo = (row2["TimeTo"] as string) ?? "";
                                    dayTitle.Add(DayTitleModel);
                                }
                            }
                            officeDeatilModel.officeLocationDayList = dayTitle;
                            data.Add(officeDeatilModel);
                        }
                    }

                }
                return data;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventApiAccess", "OfficeLocationList");
                ReturnResult = -1;
                return null;
            }
        }
    }
}
