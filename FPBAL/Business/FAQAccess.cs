﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class FAQAccess: IFAQ
    {
        FAQData faqData = new FAQData();
        public FAQResponse SelectAll(FAQModel model)
        {
            Int32 returnResult = 0;
            FAQResponse response = new FAQResponse();
            response._FAQModel = new FAQModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);

            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                DataSet ds = faqData.SelectAll(model, out returnResult);
                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        response._FAQModel._FAQList = FAQList(ds.Tables[0], returnResult);
                        returnResult = 2;
                    }
                }
                else
                {
                    response.ReturnCode = 0;
                    response.ReturnMessage = "No Data Found";
                }

                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "FAQAccess", "SelectAll");
                return response;
            }
        }
        private List<FAQModel> FAQList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<FAQModel> data = new List<FAQModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new FAQModel
                            {
                                UserId = Convert.ToInt32(row["UserId"]),
                                FaqId = Convert.ToInt32(row["FaqId"]),
                                EncryptedFaqId = UtilityAccess.Encrypt(Convert.ToString(row["FaqId"])),
                                Title = Convert.ToString(row["Title"]),
                                Question = Convert.ToString(row["Question"]),
                                Answer = Convert.ToString(row["Answer"]),
                                
                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "FAQModel", "FAQList");
                ReturnResult = -1;
                return null;
            }
        }
        public FAQResponse AddFaq(FAQModel model)
        {
            Int32 returnResult = 0;
            FAQResponse response = new FAQResponse();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            response._FAQModel = new FAQModel();
            try
            {

                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                faqData.AddFAQ(model, out returnResult);
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);
                return response;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "FAQAccess", "AddFaq");
                response.ReturnCode = -1;
                return response;
            }
        }
        public FAQResponse AddRes(FAQModel model)
        {
            Int32 returnResult = 0;
            FAQResponse response = new FAQResponse();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            response._FAQModel = new FAQModel();
            try
            {

                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.FaqId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedFaqId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                faqData.AddRes(model, out returnResult);
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);
                return response;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "FAQAccess", "AddRes");
                response.ReturnCode = -1;
                return response;
            }
        }
    }
}
