﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading;
using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
namespace FPBAL.Business
{
    public class HeadlineAccess : IHeadline
    {
        HeadlineData headlineData = new HeadlineData();

        public HeadlineResponse SelectAll(HeadlineModel model)
        {
            HeadlineResponse newsResponse = new HeadlineResponse();
            newsResponse.ReturnCode = 0;
            newsResponse.ReturnMessage = Response.Message(0);
            newsResponse._HeadlineModel = new HeadlineModel();

            List<HeadlineModel> headlineList = new List<HeadlineModel>();

            HeadlineModel headlineInfo = new HeadlineModel();
            try
            {
                model.DateFrom = !string.IsNullOrEmpty(model.DateFrom) ? UtilityAccess.FromDate(model.DateFrom) : null;
                model.DateTo = !string.IsNullOrEmpty(model.DateTo) ? UtilityAccess.FromDate(model.DateTo) : null;


                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                headlineInfo._StatusList = UtilityAccess.StatusList(2);
                newsResponse._HeadlineModel._StatusList = headlineInfo._StatusList;

                DataSet ds = headlineData.SelectAll(model);

                if (ds != null && ds.Tables.Count > 0)
                {

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            headlineInfo = new HeadlineModel();
                            headlineInfo.HeadLineId = Convert.ToInt32(row["HeadLineId"] ?? 0);
                            headlineInfo.EncryptedHeadLineId = UtilityAccess.Encrypt(row["HeadLineId"].ToString());
                            headlineInfo.FirstName = Convert.ToString(row["Firstname"] ?? string.Empty);
                            headlineInfo.MI = Convert.ToString(row["MI"] ?? string.Empty);
                            headlineInfo.LastName = Convert.ToString(row["LastName"] ?? string.Empty);
                            headlineInfo.Title = Convert.ToString(row["Title"] ?? string.Empty);
                            headlineInfo.Status = Convert.ToString(row["Status"] ?? string.Empty);
                            headlineInfo.ViewCount = Convert.ToInt32(row["ViewCount"] ?? 0);
                            headlineInfo.ImagePath = Convert.ToString(row["ImagePath"]);
                            headlineInfo.ImagePath1 = headlineInfo.ImagePath;
                            headlineInfo.PublishDate = Convert.ToString(row["PublishDate"] ?? string.Empty);
                            headlineInfo.PublishTime = Convert.ToString(row["PublishTime"] ?? string.Empty);
                            headlineList.Add(headlineInfo);
                        }
                    }
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        headlineInfo._UserList = UtilityAccess.RenderList(ds.Tables[1], 0);
                    }
                    newsResponse._HeadlineModel._HeadlineList = new List<HeadlineModel>();
                    newsResponse._HeadlineModel._HeadlineList = headlineList;
                    newsResponse._HeadlineModel._UserList = headlineInfo._UserList;
                    newsResponse.ReturnCode = 1;
                    newsResponse.ReturnMessage = Response.Message(1);

                }
                else
                {
                    newsResponse._HeadlineModel._StatusList = headlineInfo._StatusList;
                    newsResponse.ReturnCode = -2;
                    newsResponse.ReturnMessage = Response.Message(-2);
                }
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "HeadlineAccess", "SelectAll");
            }
            return newsResponse;
        }

        public HeadlineResponse Select(HeadlineModel model)
        {
            HeadlineResponse serviceResponse = new HeadlineResponse();
            NewsModel userInfo = new NewsModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            HeadlineModel headlineInfo = null;
            int returnResult = 0;
            if (model != null && !String.IsNullOrEmpty(model.EncryptedHeadLineId))
                model.HeadLineId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedHeadLineId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.UserId =Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            DataSet ds = headlineData.SelectById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {
                headlineInfo = new HeadlineModel();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        headlineInfo.HeadLineId = Convert.ToInt32(row["HeadLineId"] ?? 0);
                        headlineInfo.EncryptedHeadLineId = UtilityAccess.Encrypt(headlineInfo.HeadLineId.ToString());
                        headlineInfo.FirstName = Convert.ToString(row["Firstname"] ?? string.Empty);
                        headlineInfo.MI = Convert.ToString(row["MI"] ?? string.Empty);
                        headlineInfo.LastName = Convert.ToString(row["LastName"] ?? string.Empty);
                        headlineInfo.Title = Convert.ToString(row["Title"] ?? string.Empty);
                        headlineInfo.Status = Convert.ToString(row["Status"] ?? string.Empty);
                        headlineInfo.ViewCount = Convert.ToInt32(row["ViewCount"] ?? 0);
                        headlineInfo.ShareCount = Convert.ToInt32(row["ShareCount"] ?? 0);
                        headlineInfo.PublishDate = Convert.ToString(row["PublishDate"] ?? string.Empty);
                        headlineInfo.PublishTime = Convert.ToString(row["PublishTime"] ?? string.Empty);
                        headlineInfo.PublishCalDate = Convert.ToString(row["PublishCalDate"] ?? string.Empty);
                        headlineInfo.Content = Convert.ToString(row["Content"] ?? string.Empty);
                        headlineInfo.County = Convert.ToString(row["County"] ?? string.Empty);
                        headlineInfo.Location = Convert.ToString(row["Location"] ?? string.Empty);
                        headlineInfo.RejectReason = Convert.ToString(row["RejectReason"] ?? string.Empty);
                           }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    headlineInfo._HImagePathList = new List<ImageListModel>();
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        headlineInfo._HImagePathList.Add(new ImageListModel
                        {
                            ImageId = Convert.ToInt32(row["ImageId"] ?? 0),
                            ImagePath = Convert.ToString(row["ImagePath"])
                        });
                    }
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {

                        headlineInfo.UserTitle = Convert.ToInt32(row["TitleId"] ?? 0);
                    }
                }
                headlineInfo._StatusListforAdmin = UtilityAccess.RenderStatusList(1);
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse._HeadlineModel = headlineInfo;
            }
            return serviceResponse;
        }
        public HeadlineResponse DetailById(HeadlineModel model)
        {
            HeadlineResponse serviceResponse = new HeadlineResponse();
            NewsModel userInfo = new NewsModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            HeadlineModel headlineInfo = null;
            int returnResult = 0;
            if (model != null && !String.IsNullOrEmpty(model.EncryptedHeadLineId))
                model.HeadLineId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedHeadLineId));
            if (model != null && !String.IsNullOrEmpty(model.EncryptedSessionToken))
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            if (model != null && !String.IsNullOrEmpty(model.EncryptedUserId))
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

            DataSet ds = headlineData.DetailById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {
                headlineInfo = new HeadlineModel();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        headlineInfo.HeadLineId = Convert.ToInt32(row["HeadLineId"] ?? 0);
                        headlineInfo.EncryptedHeadLineId = UtilityAccess.Encrypt(headlineInfo.HeadLineId.ToString());
                        headlineInfo.FirstName = Convert.ToString(row["Firstname"] ?? string.Empty);
                        headlineInfo.MI = Convert.ToString(row["MI"] ?? string.Empty);
                        headlineInfo.LastName = Convert.ToString(row["LastName"] ?? string.Empty);
                        headlineInfo.Title = Convert.ToString(row["Title"] ?? string.Empty);
                        headlineInfo.Status = Convert.ToString(row["Status"] ?? string.Empty);
                        headlineInfo.ViewCount = Convert.ToInt32(row["ViewCount"] ?? 0);
                        headlineInfo.ShareCount = Convert.ToInt32(row["ShareCount"] ?? 0);
                        headlineInfo.PublishDate = Convert.ToString(row["PublishDate"] ?? string.Empty);
                        headlineInfo.PublishTime = Convert.ToString(row["PublishTime"] ?? string.Empty);
                        headlineInfo.PublishCalDate = Convert.ToString(row["PublishCalDate"] ?? string.Empty);
                        headlineInfo.Content = Convert.ToString(row["Content"] ?? string.Empty);
                        headlineInfo.County = Convert.ToString(row["County"] ?? string.Empty);
                        headlineInfo.Location = Convert.ToString(row["Location"] ?? string.Empty);
                        headlineInfo.RejectReason = Convert.ToString(row["RejectReason"] ?? string.Empty);
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    headlineInfo._HImagePathList = new List<ImageListModel>();
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        headlineInfo._HImagePathList.Add(new ImageListModel
                        {
                            ImageId = Convert.ToInt32(row["ImageId"] ?? 0),
                            ImagePath = Convert.ToString(row["ImagePath"])
                        });
                    }
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {

                        headlineInfo.UserTitle = Convert.ToInt32(row["TitleId"] ?? 0);
                    }
                }
                headlineInfo._StatusListforAdmin = UtilityAccess.RenderStatusList(1);
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse._HeadlineModel = headlineInfo;
            }
            return serviceResponse;
        }
        private DataTable AddHeadlineImages(List<ImageListModel> _ImageList)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ImageId", typeof(Int32));
            dt.Columns.Add("ImagePath", typeof(string));

            DataRow row = null;
            if (_ImageList != null && _ImageList.Count > 0)
            {
                foreach (var item in _ImageList)
                {
                    row = dt.NewRow();
                    row["ImageId"] = item.ImageId;
                    row["ImagePath"] = Convert.ToString(item.ImagePath);
                    dt.Rows.Add(row);
                }
            }
            return dt;
        }

        public HeadlineResponse AddorEdit(HeadlineModel model)
        {
            Int32 returnResult = 0;
            HeadlineResponse response = new HeadlineResponse();
            response._HeadlineModel = new HeadlineModel();
            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                if (!string.IsNullOrEmpty(model.EncryptedHeadLineId))
                {
                    model.HeadLineId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedHeadLineId));
                }
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                string deviceToken = string.Empty, deviceType = string.Empty;
                DataSet ds = headlineData.AddorEdit(model, AddHeadlineImages(model._HImagePathList), out returnResult);

                string title = model.Title;
                string content = model.Content;
                string hid = "";
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    int result = 0; //Convert.ToString(ds.Tables[0].Rows[0]["FollowerId"]);
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        result = Convert.ToInt32(row["UserId"]);
                        if (result > 0)
                        {
                            hid = "";
                            Thread T1 = new Thread(delegate ()
                            {
                                deviceToken = Convert.ToString(row["DeviceToken"]);
                                deviceType = Convert.ToString(row["DeviceType"]);
                                hid = Convert.ToInt32(row["HeadlineId"]).ToString();
                                if (deviceType.ToUpper() == "IOS")
                                    APNSNotificationAccess.ApnsNotification(deviceToken, title, hid, "h", content);
                                else
                                   if (deviceType.ToUpper() == "ANDROID")
                                    FCMNotificationAccess.SendFCMNotifications(deviceToken, title, deviceType, hid, "headlines", content);
                            });
                            T1.IsBackground = true;
                            T1.Start();
                        }
                    }
                }
                response.ReturnCode = returnResult;
                //response.ReturnMessage = Response.Message(returnResult);
                if(returnResult==12)
                {
                    response.ReturnMessage = "News Updated Successfully.";
                }
                else
                {
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "HeadlineAccess", "AddorEdit");
                return response;
            }

        }

        public HeadlineResponse Archive(HeadlineModel model)
        {
            HeadlineResponse serviceResponse = new HeadlineResponse();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            HeadlineModel headlineInfo = null;
            int returnResult = 0;
            if (model != null && model.EncryptedHeadLineId != null)
                model.HeadLineId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedHeadLineId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = headlineData.Archive(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse._HeadlineModel = headlineInfo;
            }
            return serviceResponse;
        }

        
        public HeadlineResponse Delete(HeadlineModel model)
        {
            HeadlineResponse serviceResponse = new HeadlineResponse();
            NewsModel userInfo = new NewsModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            HeadlineModel headlineInfo = null;
            int returnResult = 0;
            if (model != null && model.EncryptedHeadLineId != null)
                model.HeadLineId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedHeadLineId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = headlineData.Delete(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse._HeadlineModel = headlineInfo;
            }
            return serviceResponse;
        }

        public void PublishNSendNotification()
        {
            try
            {
                string deviceToken = string.Empty, deviceType = string.Empty, Message = string.Empty, hid = string.Empty;
                int returnResult = 0;
                DataSet ds = headlineData.PublishNSendNotification(out returnResult);
                string desc = string.Empty;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row1 in ds.Tables[0].Rows)
                    {
                        Message = Convert.ToString(row1["Title"]);
                        hid = Convert.ToInt32(row1["HeadlineId"]).ToString();
                        desc = Convert.ToString(row1["content"]);

                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[1].Rows)
                            {
                                deviceToken = Convert.ToString(row["DeviceToken"]);
                                deviceType = Convert.ToString(row["DeviceType"]);

                                if (deviceType.ToUpper() == "IOS")
                                    APNSNotificationAccess.ApnsNotification(deviceToken, Message, hid, "h", desc);
                                else
                                   if (deviceType.ToUpper() == "ANDROID")
                                    FCMNotificationAccess.SendFCMNotifications(deviceToken, Message, deviceType, hid, "headlines", desc);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "HeadlineAccess", "PublishNSendNotification");

            }
        }
    }
}
