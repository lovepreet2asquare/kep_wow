﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class InboxApiAccess : IInbox
    {
        public InboxApiResponse InboxMessageSelect(FPInboxApiRequest request)
        {
            Int32 returnResult = 0;
            InboxApiResponse response = new InboxApiResponse();
            response.InboxApiModel = new List<InboxApiModel>();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            InboxApiModel model = new InboxApiModel();
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
              
                DataSet ds = InboxApiData.InboxMessageSelect(request, returnResult);
                if (ds != null && ds.Tables.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["MessageId"]);
                    if (returnResult > 0)
                    {
                        response.InboxApiModel = InboxDetail(ds, returnResult);
                        returnResult = 2;
                    }

                    // response message
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InboxApiAccess", "InboxMessageSelect");
                return response;
            }
        }

        public InboxDetailApiResponse InAppMessageDetail(InAppMessageDetailApiRequest request)
        {
            Int32 returnResult = 0;
            string DonateURL = string.Empty;
            InboxDetailApiResponse response = new InboxDetailApiResponse();
            InboxApiModel MessageDetail = new InboxApiModel();
            response.MessageDetail = MessageDetail;
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            //InboxApiModel model = new InboxApiModel();
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = InboxApiData.InAppMessageDetail(request, returnResult);
                if (ds != null && ds.Tables.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["MessageId"]);
                    if (returnResult > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[0].Rows)
                        {
                            
                            MessageDetail.MessageId = Convert.ToInt32(row2["MessageId"]);
                            MessageDetail.Title = Convert.ToString(row2["Title"]);
                            MessageDetail.Content = Convert.ToString(row2["Content"]);
                            MessageDetail.PublishedDate = Convert.ToString(row2["PublishDate"]);
                            MessageDetail.PublishedTime = Convert.ToString(row2["PublishedTime"]);
                            MessageDetail.IsDonate = Convert.ToBoolean(row2["IsDonate"]);
                            MessageDetail.IsOpen = Convert.ToBoolean(row2["IsOpen"]);
                            MessageDetail.IsUpdated = Convert.ToBoolean(row2["IsUpdated"]);
                            //MessageDetail.DonateURL = Convert.ToString(row2["DonateURL"]);
                            DonateURL = Convert.ToString(row2["DonateURL"]);

                            if (DonateURL.Contains("http"))
                                MessageDetail.DonateURL = DonateURL;
                            else if (MessageDetail.IsDonate && !string.IsNullOrEmpty(DonateURL))
                                MessageDetail.DonateURL = "http://" + DonateURL;

                        }

                        response.MessageDetail = MessageDetail;


                        returnResult = 2;
                    }

                    // response message
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InboxApiAccess", "InAppMessageDetail");
                return response;
            }
        }

        private List<InboxApiModel> InboxDetail(DataSet ds, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<InboxApiModel> data = new List<InboxApiModel>();
                InboxApiModel inboxApiModel = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                inboxApiModel = new InboxApiModel();
                                inboxApiModel.MessageId = Convert.ToInt32(row2["MessageId"]);
                                inboxApiModel.Title = Convert.ToString(row2["Title"]);
                                inboxApiModel.Content = Convert.ToString(row2["Content"]);
                                inboxApiModel.PublishedDate = Convert.ToString(row2["PublishDate"]);
                                inboxApiModel.PublishedTime = Convert.ToString(row2["PublishedTime"]);
                                inboxApiModel.IsDonate= Convert.ToBoolean(row2["IsDonate"]);
                                inboxApiModel.IsOpen= Convert.ToBoolean(row2["IsOpen"]);
                                inboxApiModel.IsUpdated = Convert.ToBoolean(row2["IsUpdated"]);
                                
                                data.Add(inboxApiModel);
                            }
                        }
                    }
                }
                return data;
            }

            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InboxApiAccess", "InboxDetail");
                ReturnResult = -1;
                return null;
            }
        }
    }
}
