﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class InvolvementAccess : IInvolvement
    {
        InvolvementData involvementData = new InvolvementData();

        public InvolvementModel InvolvementGraphsData(InvolvementModel model)
        {
            model.UserId = Convert.ToString(UtilityAccess.Decrypt(model.UserId));
            model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
            DataSet ds = involvementData.InvolvementGraphsData(model);
            InvolvementModel data = new InvolvementModel();
            try
            {
                data._StatusAIList = UtilityAccess.StatusListValText(1);
                if (ds != null && ds.Tables.Count > 0)
                {

                    data._lnvolvementList = new List<InvolvementModel>();
                    //foreach (DataRow row in ds.Tables[0].Rows)
                    //{
                    //    data._CityNameModel.Add(new CityNameModel
                    //    {
                    //        //InvolvementId = Convert.ToInt32(row["InvolvementId"]),
                    //        CityName = Convert.ToString(row["CityName"]),
                    //        CountCity = Convert.ToInt32(row["count"]),
                    //    });
                    //}
                    if (ds.Tables[0] != null && ds.Tables.Count > 0)
                        data._CityNameModel = CityNameModelDetail(ds.Tables[0]);
                    if (ds.Tables[1] != null && ds.Tables.Count > 0)
                        data.graphsModel = GraphmodelCount(ds.Tables[1]);
                    if (ds.Tables[2] != null && ds.Tables.Count > 0)
                        data.thisMonthYearMonth = ThisMonthYearMonthDetail(ds.Tables[2]);
                    //if (ds.Tables[4] != null && ds.Tables.Count > 0)
                    //    data._GraphData = GrapgDataList(ds.Tables[4]);
                    if (ds.Tables[3] != null && ds.Tables.Count > 0)
                        data._lnvolvementList = InvolvementDetails(ds.Tables[3]);
                }

                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InvolvementAccess", "InvolvementSelectAll");
                return null;
            }
        }

       
        public GraphsModel GraphmodelCount(DataTable dt)
        {
            try
            {
                GraphsModel data = new GraphsModel();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Totalcount = Convert.ToInt32(row["TotalCount"]);
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InvolvementAccess", "GraphmodelCount");
                return null;
            }
        }

        public List<InvolvementModel> InvolvementDetails(DataTable dt)
        {
            try
            {
                List<InvolvementModel> data = new List<InvolvementModel>();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new InvolvementModel
                            {
                                SrNO = Convert.ToInt32(row["SrNo"]),
                                Email = Convert.ToString(row["Email"]),
                                FirstName = Convert.ToString(row["FirstName"]),
                                LastName = Convert.ToString(row["LastName"]),
                                MI = Convert.ToString(row["MI"]),
                                ISDCode = Convert.ToString(row["ISDCode"]),
                                Mobile = Convert.ToString(row["Mobile"]),
                                StdMobile= Convert.ToString(row["ISDCode"]),
                                CreateDate = Convert.ToString(row["CreateDate"]),
                                CountryName = Convert.ToString(row["CountryName"]),
                                AddressLine1 = Convert.ToString(row["AddressLine1"]),
                                AddressLine2 = Convert.ToString(row["AddressLine2"]),
                                CityName = Convert.ToString(row["CityName"]),
                                StateName = Convert.ToString(row["StateName"]),
                                ZipCode = Convert.ToString(row["ZipCode"]),
                                Status = Convert.ToString(row["Status"]),
                                Time = Convert.ToString(row["Time"]),
                            });
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EmployeeAccess", "DashBoardGraphDetail");
                return null;
            }
        }

        public List<GraphData> GrapgDataList(DataTable dt)
        {
            try
            {
                List<GraphData> data = new List<GraphData>();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new GraphData
                            {
                                Day = Convert.ToString(row["Day"]),
                                Involvement = Convert.ToInt32(row["Involvement"]),
                            });
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EmployeeAccess", "DashBoardGraphDetail");
                return null;
            }
        }
        public List<CityNameModel> CityNameModelDetail(DataTable dt)
        {
            try
            {
                List<CityNameModel> data = new List<CityNameModel>();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new CityNameModel
                            {
                                CityName = Convert.ToString(row["CityName"]),
                                ClassName = Convert.ToString(row["ClassName"]),
                                ProgressbarClass = Convert.ToDecimal(row["Percentage"]) < 15 ? "progress-bar bg-warning" : Convert.ToDecimal(row["Percentage"]) < 30 ? "progress-bar bg-danger" : "progress-bar bg-success",
                                CountCity = Convert.ToInt32(row["count"]),
                                CityCountPrercentage = Convert.ToDecimal(row["Percentage"]),
                            });
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EmployeeAccess", "DashBoardGraphDetail");
                return null;
            }
        }
        public ThisMonthYearMonth ThisMonthYearMonthDetail(DataTable dt)
        {
            try
            {
                ThisMonthYearMonth data = new ThisMonthYearMonth();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {

                            data.ThisWeakMMYY = Convert.ToString(row["ThisWeakMMYY"]);
                            data.FromDate = Convert.ToString(row["FromDate"]);
                            data.ToDate = Convert.ToString(row["ToDate"]);

                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EmployeeAccess", "DashBoardGraphDetail");
                return null;
            }
        }
        public InvolvementModel InvolvementSelectAll(InvolvementModel model)
        {
            model.UserId = Convert.ToString(UtilityAccess.Decrypt(model.UserId));
            model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
            DataSet ds = involvementData.InvolvementSelectAll(model);
            InvolvementModel data = new InvolvementModel();
            try
            {
                data._StatusAIList = UtilityAccess.StatusListValText(0);
                if (ds != null && ds.Tables.Count > 0)
                {

                    data._lnvolvementList = new List<InvolvementModel>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        data._lnvolvementList.Add(new InvolvementModel
                        {

                            //InvolvementId = Convert.ToInt32(row["InvolvementId"]),
                            SrNO = Convert.ToInt32(row["SrNo"]),
                            Email = Convert.ToString(row["Email"]),
                            FirstName = Convert.ToString(row["FirstName"]),
                            LastName = Convert.ToString(row["LastName"]),
                            MI = Convert.ToString(row["MI"]),
                            ISDCode = Convert.ToString(row["ISDCode"]),
                            Mobile = Convert.ToString(row["Mobile"]),
                            CreateDate = Convert.ToString(row["CreateDate"]),
                            CountryName = Convert.ToString(row["CountryName"]),
                            AddressLine1 = Convert.ToString(row["AddressLine1"]),
                            AddressLine2 = Convert.ToString(row["AddressLine2"]),
                            CityName = Convert.ToString(row["CityName"]),
                            StateName = Convert.ToString(row["StateName"]),
                            ZipCode = Convert.ToString(row["ZipCode"]),
                            Status = Convert.ToString(row["Status"]),
                            Time = Convert.ToString(row["Time"]),
                        });
                    }
                }

                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InvolvementAccess", "InvolvementSelectAll");
                return null;
            }
        }

        public GraphData GraphData(string duration)
        {

            GraphData data = new GraphData();
            
            try
            {
                DataSet ds = involvementData.GraphData(duration);
                if (ds != null && ds.Tables.Count > 0)
                {

                    data._GraphData = new List<GraphData>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        data._GraphData.Add(new GraphData
                        {
                            //InvolvementId = Convert.ToInt32(row["InvolvementId"]),
                            Day = Convert.ToString(row["Day"]),
                            Involvement = Convert.ToInt32(row["Involvement"]),
                        });
                    }
                    if (ds.Tables[1] != null && ds.Tables.Count > 0)
                        data.AfterDropDownChangeModel = AfterDropDownChange(ds.Tables[1]);
                    if (ds.Tables[2] != null && ds.Tables.Count > 0)
                        data.GraphsModel = GraphTotalCountForDateChange(ds.Tables[2]);
                    if (ds.Tables[3] != null && ds.Tables.Count > 0)
                        data._CityNameModel = GraphChangeCityNameModelDetail(ds.Tables[3]);

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InvolvementAccess", "InvolvementSelectAll");
                return null;
            }
        }
        public List<CityNameModel> GraphChangeCityNameModelDetail(DataTable dt)
        {
            try
            {
                List<CityNameModel> data = new List<CityNameModel>();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new CityNameModel
                            {
                                CityName = Convert.ToString(row["CityName"]),
                                ClassName = Convert.ToString(row["ClassName"]),
                                CountCity = Convert.ToInt32(row["count"]),
                                CityCountPrercentage = Convert.ToDecimal(row["Percentage"]),
                            });
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EmployeeAccess", "DashBoardGraphDetail");
                return null;
            }
        }
        public GraphsModel GraphTotalCountForDateChange(DataTable dt)
        {
            try
            {
                GraphsModel data = new GraphsModel();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Totalcount = Convert.ToDecimal(row["TotalVolunteers"]);
                            
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InvolvementAccess", "GraphmodelCount");
                return null;
            }
        }
        public AfterDropDownChange AfterDropDownChange(DataTable dt)
        {
            try
            {
                AfterDropDownChange data = new AfterDropDownChange();
                if (dt != null)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Name = Convert.ToString(row["ThisWeakMMYY"]);
                            data.FromDate = Convert.ToString(row["FromDate"]);
                            data.ToDate = Convert.ToString(row["ToDate"]);
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InvolvementAccess", "GraphmodelCount");
                return null;
            }
        }
    }
}
