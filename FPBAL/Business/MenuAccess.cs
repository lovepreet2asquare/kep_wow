﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPDAL.Data;
using System.Data;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using FamousPerson;

namespace FPBAL.Business
{
    public class MenuAccess
    {
        public MenuModel MenusByUser(String url, Int32 LoggedId, String MenuId)
        {
            MenuModel data = new MenuModel();
            DataSet ds = MenuData.MenusByUser(url, LoggedId, MenuId);
            try
            {
                if (ds != null && ds.Tables.Count > 0)
                {
                    // Top menus
                    data._TopMenus = _ListMenus(ds.Tables[0], ds.Tables[6]);
                    // Main menus
                    data._LeftMenus = _ListMenus(ds.Tables[1], ds.Tables[6]);

                    if (ds.Tables.Count > 2 && ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[2].Rows)
                        {
                            if (Convert.ToInt32(row["RoleId"]) > 0)
                            {
                                //HttpContext.Current.Session["FP_PageThead"] = Convert.ToBoolean(row["Thead"]);
                                HttpContext.Current.Session["FP_PageAccess"] = Convert.ToBoolean(row["Access"]);
                                HttpContext.Current.Session["FP_PageCreate"] = Convert.ToBoolean(row["Create"]);
                                HttpContext.Current.Session["FP_PageEdit"] = Convert.ToBoolean(row["Edit"]);
                                HttpContext.Current.Session["FP_PageDelete"] = Convert.ToBoolean(row["Delete"]);
                            }
                        }
                    }
                    if (ds.Tables.Count > 3 && ds.Tables[3].Rows.Count > 0)
                    {
                        HttpContext.Current.Session["Email"] = ds.Tables[3].Rows[0]["Email"];
                        HttpContext.Current.Session["UserName"] = ds.Tables[3].Rows[0]["UserName"];
                        HttpContext.Current.Session["ProfilePic"] = ds.Tables[3].Rows[0]["ProfilePic"];
                        HttpContext.Current.Session["UserStatus"] = ds.Tables[3].Rows[0]["Status"];
                        
                        string currUrl = MvcApplication.getControllerNameFromUrl(); ;
                    }
                    if (ds.Tables.Count > 4 && ds.Tables[4].Rows.Count > 0)
                    {
                        data._NotificationList = NotificationList(ds.Tables[4]);
                    }
                    if (ds.Tables.Count > 5 && ds.Tables[5].Rows.Count > 0)
                    {
                        //data.CountNotification = NotificationList(ds.Tables[5]);
                        data.CountNotification = notificationCount(ds.Tables[5]);
                    }
                    //if (ds.Tables.Count > 5 && ds.Tables[5].Rows.Count > 0)
                    //{
                    //    HttpContext.Current.Session["NotificationCount"] = ds.Tables[5].Rows[0]["NotificationCount"];
                    //}
                    if (ds.Tables.Count > 6 && ds.Tables[6].Rows.Count > 0)
                    {
                        data._menuCountList = menuCountDetails(ds.Tables[6]);
                    }
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return null;
            }
            return data;
        }

        //public List<MenuModel> _ListMenus(DataTable dt, DataTable dt1)
        //{
        //    try
        //    {
        //        List<MenuModel> data = new List<MenuModel>();
        //        List<MenuCountModel> _data1 = null;
        //        MenuModel model = null;
        //        MenuCountModel data1 = null;
        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            foreach (DataRow row in dt.Rows)
        //            {
        //                data.Add(new MenuModel
        //                {
        //                    MenuId = Convert.ToInt32(row["MenuId"]),
        //                    ParentId = Convert.ToInt32(row["ParentId"]),
        //                    Text = Convert.ToString(row["Text"]),
        //                    Url = Convert.ToString(row["Url"]),
        //                    Level = Convert.ToInt32(row["Level"]),
        //                    SortOrder = Convert.ToInt32(row["SortOrder"]),
        //                    NewWindow = Convert.ToBoolean(row["NewWindow"]),
        //                    Active = Convert.ToBoolean(row["Active"]),
        //                    PrefixIcon = Convert.ToString(row["PrefixIcon"]),
        //                    IsTop = Convert.ToBoolean(row["IsTop"]),
        //                });
        //            }
        //        }
        //        if (dt1 != null && dt1.Rows.Count > 0)
        //        {
        //            foreach (DataRow row1 in dt1.Rows) 
        //            {
        //                data1 = new MenuCountModel();
        //                data1.HeadlineCount = Convert.ToInt32(row1["HeadlineCount"]);
        //                data1.NewsCount = Convert.ToInt32(row1["NewsCount"]);
        //                data1.EventCount = Convert.ToInt32(row1["EventCount"]);
        //                data1.MessageCount = Convert.ToInt32(row1["MessageCount"]);
        //                data1.officeCount = Convert.ToInt32(row1["officeCount"]);
        //                //data.Add(data1);
        //            }
        //            _data1. = data1;
        //        }
        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        string err = ex.Message;
        //        return null;
        //    }
        //}
        public List<MenuModel> _ListMenus(DataTable dt, DataTable dt1)
        {
            try
            {
                List<MenuModel> data = new List<MenuModel>();
                List<MenuCountModel> _data1 = null;
                MenuModel model = null;
                MenuCountModel countModel = null;
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        model = new MenuModel();
                        _data1 = new List<MenuCountModel>();
                        model.MenuId = Convert.ToInt32(row["MenuId"]);
                        model.ParentId = Convert.ToInt32(row["ParentId"]);
                        model.Text = Convert.ToString(row["Text"]);
                        model.Url = Convert.ToString(row["Url"]);
                        model.Level = Convert.ToInt32(row["Level"]);
                        model.SortOrder = Convert.ToInt32(row["SortOrder"]);
                        model.NewWindow = Convert.ToBoolean(row["NewWindow"]);
                        model.Active = Convert.ToBoolean(row["Active"]);
                        model.PrefixIcon = Convert.ToString(row["PrefixIcon"]);
                        model.IsTop = Convert.ToBoolean(row["IsTop"]);
                        if (dt1 != null && dt1.Rows.Count > 0)
                        {
                            foreach (DataRow row1 in dt1.Rows)
                            {
                                countModel = new MenuCountModel();
                                countModel.HeadlineCount = Convert.ToInt32(row1["HeadlineCount"]);
                                countModel.NewsCount = Convert.ToInt32(row1["NewsCount"]);
                                //countModel.EventCount = Convert.ToInt32(row1["EventCount"]);
                                //countModel.MessageCount = Convert.ToInt32(row1["MessageCount"]);
                                countModel.officeCount = Convert.ToInt32(row1["officeCount"]);
                                _data1.Add(countModel);
                            }

                        }
                        model._menuCountList = _data1;
                        data.Add(model);
                    }
                }

                return data;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return null;
            }
        }

        private List<NotificationModel> NotificationList(DataTable dt)
        {
            //ReturnResult = 1;
            try
            {
                List<NotificationModel> data = new List<NotificationModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new NotificationModel
                            {
                                NotificationId = Convert.ToInt32(row["NotificationId"]),
                                Message = (row["Message"] as String ?? ""),
                                CreateDate = (row["CreateDate"] as String ?? ""),
                                CreateTime = (row["CreateTime"] as String ?? ""),
                                IsViewForDisplay = (row["Isview"] as String ?? ""),
                                //IsView = Convert.ToBoolean(row["IsView"]),
                                //NotifyDay = Convert.ToString(row["NotifyDay"])
                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NotificationData", "NotificationList");
                //ReturnResult = -1;
                return null;
            }
        }

        private NotificatiuonCount notificationCount(DataTable dt)
        {
            //ReturnResult = 1;
            try
            {
                NotificatiuonCount data = new NotificatiuonCount();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {

                            data.NotificationCount = Convert.ToInt32(row["NotificationCount"]);
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NotificationData", "notificationCount");
                //ReturnResult = -1;
                return null;
            }
        }

        private List<MenuCountModel> menuCountDetails(DataTable dt)
        {
            //ReturnResult = 1;
            List<MenuCountModel> data = new List<MenuCountModel>();
            try
            {
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new MenuCountModel
                            {
                                HeadlineCount = Convert.ToInt32(row["HeadlineCount"]),
                                NewsCount = Convert.ToInt32(row["NewsCount"]),
                                //EventCount = Convert.ToInt32(row["EventCount"]),
                                //MessageCount = Convert.ToInt32(row["MessageCount"]),
                                //DocCount = Convert.ToInt32(row["DocCount"]),
                                //ResDocCount = Convert.ToInt32(row["ResDocCount"]),
                                //officeCount = Convert.ToInt32(row["officeCount"]),
                                //IsView = Convert.ToBoolean(row["IsView"]),
                                //NotifyDay = Convert.ToString(row["NotifyDay"])
                            });
                        }

                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NotificationData", "NotificationList");
                //ReturnResult = -1;
                return null;
            }
        }

        public static Boolean Allow(String Action)
        {
            try
            {
                if (String.IsNullOrEmpty(Action))
                    return false;
                else if (HttpContext.Current.Session["FP_Page" + Action.Trim()] == null)
                    return false;
                else
                    return Convert.ToBoolean(HttpContext.Current.Session["FP_Page" + Action.Trim()]);

            }
            catch
            {
                return false;
            }
        }
    }
}
