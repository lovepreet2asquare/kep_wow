﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class NewsAPIAccess : INewsAPI
    {
        public FPFollowerNewsDetailResponse FollowerNewsDetail(FolloerNewsDetailRequest request)
        {
            Int32 returnResult = 0;
            FPFollowerNewsDetailResponse response = new FPFollowerNewsDetailResponse();
            response.newsApiDetailModel = new NewsApiDetailModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                request.FollowerId = request.FollowerId;
                request.SessionToken = request.SessionToken;
                DataSet ds = NewsAPIData.FollowerNewsDetail(request, returnResult);
                if (ds != null && ds.Tables.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["Newsid"]);
                    if (returnResult > 0)
                    {
                        response.newsApiDetailModel = NewsDetail(ds, returnResult);
                        returnResult = 2;
                    }

                    // response message
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsApiAccess", "FollowerNewsDetail");
                return response;
            }
    }

        private NewsApiDetailModel NewsDetail(DataSet ds, Int32 ReturnResult)
        {
            ReturnResult = 1;
            string DonateURL = string.Empty;
            try
            {
                NewsApiDetailModel data = new NewsApiDetailModel();
                List<NewsImage> newsImage = null;
                NewsApiDetailModel newsApiDeatilModel = null;
                NewsImage newsImageModel = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            newsImage = new List<NewsImage>();
                            newsApiDeatilModel = new NewsApiDetailModel();
                           
                            data.NewsId = Convert.ToInt32(row["NewsId"]);
                            data.Title= (row["Title"] as string) ?? "";
                            //newsApiDeatilModel.FollwerId = (row["FollwerId"] as string) ?? "";
                            data.Content = (row["Content"] as string) ?? "";
                            data.PublishDate= (row["PublishDate"] as string) ?? "";
                            data.PublishTime = (row["PublishedTime"] as string) ?? "";
                            data.ImagePath = (row["ImagePath"] as string) ?? "";
                            data.IsUpdated = Convert.ToBoolean(row["IsUpdated"]);
                            data.IsDonate = Convert.ToBoolean(row["IsDonate"]);
                            data.ShareLink = (row["ShareLink"] as string)+data.NewsId;
                            DonateURL = Convert.ToString(row["DonateURL"]);
                            if (DonateURL.Contains("http"))
                                data.DonateURL = DonateURL;
                            else if (data.IsDonate && !string.IsNullOrEmpty(DonateURL))
                                data.DonateURL = "http://"+ DonateURL;



                            if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                            {
                                foreach (DataRow row2 in ds.Tables[1].Rows)
                                {
                                    newsImageModel = new NewsImage();
                                    newsImageModel.ImagePath = (row2["Imagepath"] as string) ?? "";

                                    newsImage.Add(newsImageModel);
                                }
                            }
                            data.newsImageList = newsImage;
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsApiAccess", "FollowerNewsDetail");
                ReturnResult = -1;
                return null;
            }
        }

        public FPFollowerNewsResponse FollowerNewsList(FollowerNewsRequest request)
        {
            Int32 returnResult = 0;
            FPFollowerNewsResponse response = new FPFollowerNewsResponse();
            response.newsApiModel = new List<NewsApiModel>();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            NewsApiModel model = new NewsApiModel();
            try
            {
                //request.FollwerId = UtilityAccess.Encrypt(request.FollwerId);
                //request.SessionToken = UtilityAccess.Encrypt(request.SessionToken);
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
            //    model.FollwerId = model.FollwerId;
            //    model.SessionToken = model.SessionToken;
                DataSet ds = NewsAPIData.FollowerNewsList(request, returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["Newsid"]);
                    if (returnResult > 0)
                    {
                        response.newsApiModel = NewsList(ds, returnResult);
                        returnResult = 2;
                    }
                    else if (returnResult == 0)
                        returnResult = -2;

                    // response message
                    response.ReturnCode = returnResult == -2 ? 0 : returnResult;
                    response.ReturnMessage = Response.Message(returnResult);


                }

                return response;
            }
            catch(Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsApiAccess", "FollowerNewsList");
                return response;
            }
        }

        private List<NewsApiModel> NewsList(DataSet ds, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<NewsApiModel> data = new List<NewsApiModel>();
                NewsApiModel newsApiModel = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow row2 in ds.Tables[0].Rows)
                                {
                                newsApiModel = new NewsApiModel();
                                newsApiModel.NewsId = Convert.ToInt32(row2["NewsId"]);
                                newsApiModel.Title = Convert.ToString(row2["Title"]);
                                newsApiModel.Content = Convert.ToString(row2["Content"]);
                                newsApiModel.PublishDate = Convert.ToString(row2["PublishDate"]);
                                newsApiModel.ImagePath = (row2["ImagePath"] as string) ?? "";
                                newsApiModel.PublishTime = Convert.ToString(row2["PublishedTime"]);
                                //newsApiModel.ShareLink = "";
                                    data.Add(newsApiModel);
                                }
                            }
                    }

                }
                return data;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsApiAccess", "NewsList");
                ReturnResult = -1;
                return null;
            }
        }

    }
}
