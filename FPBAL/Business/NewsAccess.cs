﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Web;
namespace FPBAL.Business
{
    public class NewsAccess : INews
    {
        NewsData newsData = new NewsData();
        public NewsResponse NewsSelectAll(NewsModel model)
        {
            NewsResponse newsResponse = new NewsResponse();
            newsResponse.ReturnCode = 0;
            newsResponse.ReturnMessage = Response.Message(0);
            newsResponse._newsModel = new NewsModel();
            
            List<NewsModel> newsList = new List<NewsModel>();

            NewsModel newsInfo = new NewsModel();
            try
            {
                //model.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                //model.DateTo= UtilityAccess.ToDate(model.DateTo);
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                
               DataSet ds = newsData.NewsSelectAll(model);

                if (ds != null && ds.Tables.Count > 0)
                {
                    
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            newsInfo = new NewsModel();
                            //newsInfo.UserId = Convert.ToInt32(row["UserId"]);
                            //newsInfo.EncryptedUserId = UtilityAccess.Encrypt(row["UserId"].ToString());
                            newsInfo.NewsId = Convert.ToInt32(row["NewsId"] ?? 0);
                            newsInfo.EncryptedNewsId = UtilityAccess.Encrypt(row["NewsId"].ToString());
                            newsInfo.FirstName = Convert.ToString(row["Firstname"] ?? string.Empty);
                            newsInfo.MI = Convert.ToString(row["MI"] ?? string.Empty);
                            newsInfo.LastName = Convert.ToString(row["LastName"] ?? string.Empty);
                            newsInfo.Title = Convert.ToString(row["Title"] ?? string.Empty);
                            newsInfo.Status = Convert.ToString(row["Status"] ?? string.Empty);
                            newsInfo.ViewCount = Convert.ToInt32(row["ViewCount"] ?? 0);
                            //newsInfo.PublishDate = Convert.ToDateTime(row["PublishDate"] ?? DateTime.Now);
                            newsInfo.PublishDate = Convert.ToString(row["PublishDate"] ?? string.Empty);
                            newsInfo.PublishTime = Convert.ToString(row["PublishTime"] ?? string.Empty);

                            //userInfo.EncryptedSessionToken = UtilityAccess.Encrypt(Convert.ToString(row["TokenKey"]));
                            //userInfo.EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"]));
                            newsList.Add(newsInfo);
                        }
                    }
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        newsInfo._UserList = UtilityAccess.RenderList(ds.Tables[1], 0);
                    }

                    newsInfo._StatusList = UtilityAccess.StatusList(0);

                    newsResponse._newsModel = new NewsModel();
                    newsResponse._newsModel._NewsList = new List<NewsModel>();
                    newsResponse._newsModel._NewsList = newsList;
                    newsResponse._newsModel._UserList = newsInfo._UserList;
                    newsResponse._newsModel._StatusList = newsInfo._StatusList;

                    newsResponse.ReturnCode = 1;
                    newsResponse.ReturnMessage = Response.Message(1);

                }
                else
                {
                    newsResponse.ReturnCode = -2;
                    newsResponse.ReturnMessage = Response.Message(-2);
                }
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsAccess", "NewsSelectAll");
            }
            return newsResponse;
        }


        public NewsResponse NewsDetail(NewsModel model)
        {
            NewsResponse serviceResponse = new NewsResponse();
            NewsModel userInfo = new NewsModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            NewsModel newsInfo = null;
            int returnResult = 0;

            if(!String.IsNullOrEmpty(model.EncryptedNewsId))
                model.NewsId=Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedNewsId));
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.UserId =Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            DataSet ds = newsData.SelectById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {
                newsInfo = new NewsModel();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        newsInfo.NewsId = Convert.ToInt32(row["NewsId"]);
                        newsInfo.EncryptedNewsId = UtilityAccess.Encrypt(row["NewsId"].ToString());
                        newsInfo.FirstName = Convert.ToString(row["Firstname"] ?? string.Empty);
                        newsInfo.MI = Convert.ToString(row["MI"] ?? string.Empty);
                        newsInfo.LastName = Convert.ToString(row["LastName"] ?? string.Empty);
                        newsInfo.Title = Convert.ToString(row["Title"] ?? string.Empty);
                        newsInfo.Status = Convert.ToString(row["Status"] ?? string.Empty);
                        newsInfo.ViewCount = Convert.ToInt32(row["ViewCount"] ?? 0);
                        newsInfo.ShareCount = Convert.ToInt32(row["ShareCount"] ?? 0);
                        //newsInfo.PublishDate = Convert.ToDateTime(row["PublishDate"]?? DateTime.Now);
                        newsInfo.PublishDate = Convert.ToString(row["PublishDate"] ?? string.Empty);
                        newsInfo.PublishCalDate = Convert.ToString(row["PublishCalDate"] ?? string.Empty);
                        newsInfo.PublishTime = Convert.ToString(row["PublishTime"] ?? string.Empty);
                        newsInfo.Content = Convert.ToString(row["Content"] ?? string.Empty);
                        newsInfo.County = Convert.ToString(row["County"] ?? string.Empty);
                        newsInfo.Location = Convert.ToString(row["Location"] ?? string.Empty);
                        newsInfo.RejectReason = Convert.ToString(row["RejectReason"] ?? string.Empty);
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    newsInfo._ImagePathList = new List<ImageListModel>();
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        newsInfo._ImagePathList.Add(new ImageListModel
                        {
                            ImageId = Convert.ToInt32(row["ImageId"] ?? 0),
                            ImagePath = Convert.ToString(row["ImagePath"])
                        });
                    }
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {

                        newsInfo.UserTitle = Convert.ToInt32(row["TitleId"] ?? 0);
                    }
                }
                newsInfo._StatusListforAdmin = UtilityAccess.RenderStatusList(1);
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse._newsModel = newsInfo;
            }
            return serviceResponse;
        }
        public NewsResponse NewsDetailById(NewsModel model)
        {
            NewsResponse serviceResponse = new NewsResponse();
            NewsModel userInfo = new NewsModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            NewsModel newsInfo = null;
            int returnResult = 0;

            if (!String.IsNullOrEmpty(model.EncryptedNewsId))
                model.NewsId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedNewsId));
            if (!String.IsNullOrEmpty(model.EncryptedSessionToken))
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

            if (!String.IsNullOrEmpty(model.EncryptedUserId))
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            DataSet ds = newsData.DetailById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {
                newsInfo = new NewsModel();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        newsInfo.NewsId = Convert.ToInt32(row["NewsId"]);
                        newsInfo.EncryptedNewsId = UtilityAccess.Encrypt(row["NewsId"].ToString());
                        newsInfo.FirstName = Convert.ToString(row["Firstname"] ?? string.Empty);
                        newsInfo.MI = Convert.ToString(row["MI"] ?? string.Empty);
                        newsInfo.LastName = Convert.ToString(row["LastName"] ?? string.Empty);
                        newsInfo.Title = Convert.ToString(row["Title"] ?? string.Empty);
                        newsInfo.Status = Convert.ToString(row["Status"] ?? string.Empty);
                        newsInfo.ViewCount = Convert.ToInt32(row["ViewCount"] ?? 0);
                        newsInfo.ShareCount = Convert.ToInt32(row["ShareCount"] ?? 0);
                        //newsInfo.PublishDate = Convert.ToDateTime(row["PublishDate"]?? DateTime.Now);
                        newsInfo.PublishDate = Convert.ToString(row["PublishDate"] ?? string.Empty);
                        newsInfo.PublishCalDate = Convert.ToString(row["PublishCalDate"] ?? string.Empty);
                        newsInfo.PublishTime = Convert.ToString(row["PublishTime"] ?? string.Empty);
                        newsInfo.Content = Convert.ToString(row["Content"] ?? string.Empty);
                        newsInfo.County = Convert.ToString(row["County"] ?? string.Empty);
                        newsInfo.Location = Convert.ToString(row["Location"] ?? string.Empty);
                        newsInfo.RejectReason = Convert.ToString(row["RejectReason"] ?? string.Empty);
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    newsInfo._ImagePathList = new List<ImageListModel>();
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        newsInfo._ImagePathList.Add(new ImageListModel
                        {
                            ImageId = Convert.ToInt32(row["ImageId"] ?? 0),
                            ImagePath = Convert.ToString(row["ImagePath"])
                        });
                    }
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {

                        newsInfo.UserTitle = Convert.ToInt32(row["TitleId"] ?? 0);
                    }
                }
                newsInfo._StatusListforAdmin = UtilityAccess.RenderStatusList(1);
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse._newsModel = newsInfo;
            }
            return serviceResponse;
        }

        public Int32 DeleteNewsImage(ImageListModel imgModel, NewsModel model)
        {
            int returnResult = 0;
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            
            returnResult = newsData.DeleteNewsImage(imgModel, model.SessionToken);
            return returnResult;
        }

        public NewsResponse DeleteNews(NewsModel model)
        {
            NewsResponse serviceResponse = new NewsResponse();
            NewsModel userInfo = new NewsModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            
            int returnResult = 0;

            if (model != null && model.EncryptedNewsId!= null)
                model.NewsId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedNewsId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = newsData.DeleteNews(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
            }

            return serviceResponse;
        }

        public NewsResponse Archive(NewsModel model)
        {
            NewsResponse serviceResponse = new NewsResponse();
            NewsModel userInfo = new NewsModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            
            int returnResult = 0;
            if (model != null && model.EncryptedNewsId!= null)
                model.NewsId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedNewsId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = newsData.Archive(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
            }
            return serviceResponse;
        }
                
        public NewsResponse AddorEdit(NewsModel model)
        {
            Int32 returnResult = 0;
            NewsResponse response = new NewsResponse();
            response._newsModel = new NewsModel();
            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                if (!string.IsNullOrEmpty(model.EncryptedNewsId))
                {
                    model.NewsId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedNewsId));
                }
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                string timeZoneid = "India Standard Time";
                if (HttpContext.Current.Session["timezoneid"] != null)
                    timeZoneid = HttpContext.Current.Session["timezoneid"].ToString();
                double timezoneSeconds = UtilityAccess.GetTimeZoneInSeconds(!string.IsNullOrEmpty(model.PublishDate) ? Convert.ToDateTime(model.PublishDate) : DateTime.UtcNow, timeZoneid);
                model.TimeZone = Convert.ToInt32(timezoneSeconds);
                DataSet ds = newsData.AddorEdit(model, AddNewsImages(model._ImagePathList), out returnResult);
                string title = model.Title;
                string content = model.Content;
                string nid = "";
                string deviceToken = string.Empty, deviceType = string.Empty;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    int result = 0; //Convert.ToString(ds.Tables[0].Rows[0]["FollowerId"]);
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        result = Convert.ToInt32(row["UserId"]);
                        if (result > 0)
                        {
                            nid = "";
                            Thread T1 = new Thread(delegate ()
                            {
                                deviceToken = Convert.ToString(row["DeviceToken"]);
                                deviceType = Convert.ToString(row["DeviceType"]);
                                nid = Convert.ToInt32(row["NewsId"]).ToString();
                                if (deviceType.ToUpper() == "IOS")
                                    APNSNotificationAccess.ApnsNotification(deviceToken, title, nid, "n", content);
                                else
                                   if (deviceType.ToUpper() == "ANDROID")
                                    FCMNotificationAccess.SendFCMNotifications(deviceToken, title, deviceType, nid, "news", content);
                            });
                            T1.IsBackground = true;
                            T1.Start();
                        }
                    }
                }
                response.ReturnCode = returnResult;
                //response.ReturnMessage = Response.Message(returnResult);
                if (returnResult == 12)
                {
                    response.ReturnMessage = "Private News Updated Successfully.";
                }
                else
                {
                    response.ReturnMessage = Response.Message(returnResult);
                }


                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsAccess", "AddorEdit");
                return response;
            }

        }

        private DataTable AddNewsImages(List<ImageListModel> _ImageList)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ImageId", typeof(Int32));
            dt.Columns.Add("ImagePath", typeof(string));

            DataRow row = null;
            if (_ImageList != null && _ImageList.Count > 0)
            {
                foreach (var item in _ImageList)
                {
                    row = dt.NewRow();
                    row["ImageId"] = item.ImageId;
                    row["ImagePath"] = Convert.ToString(item.ImagePath);
                    dt.Rows.Add(row);
                }
            }
            return dt;
        }

        public NewsModel SelectDetail(DataSet ds, out Int32 ReturnResult)
        {
            NewsModel data = null;
            ReturnResult = -1;
            //data._ImagePathList = new List<ImageListModel>();
            try
            {
                if (ds != null && ds.Tables.Count > 0)
                {
                    data = new NewsModel();
                    data._ImagePathList = new List<ImageListModel>();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            data.NewsId = Convert.ToInt32(row["NewsId"]);
                            data.Title = Convert.ToString(row["Title"]);
                            data.Content = (row["Content"] as String) ?? "";
                            //data.PublishDate = Convert.ToDateTime(row["PublishDate"]);
                            data.PublishDate = Convert.ToString(row["PublishDate"]);
                            data.Status=Convert.ToString(row["Status"]);
                        }
                    }
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[1].Rows)
                        {
                            data._ImagePathList.Add(new ImageListModel
                            {
                                ImageId = Convert.ToInt32(row["ImageId"]),
                                ImagePath = Convert.ToString(row["ImagePath"])
                            });
                        }
                    }

                    // country list
                    //if (ds.Tables.Count > 1)
                    //    data._CertificateTypeList = UtilityAccess.RenderList(ds.Tables[1], 1);

                    ReturnResult = 1;
                }

                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsAccess", "SelectDetail");
                ReturnResult = -1;
                return data;
            }
        }
        public NewsResponse ReadMore(NewsModel model)
        {
            Int32 returnResult = 0;
            NewsResponse response = new NewsResponse();

            response._newsModel = new NewsModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);

            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                //string DateFrom = string.Empty;
                //string DateTo = string.Empty;
                //UtilityAccess.GetFromToDate(model.DateFilterText, out DateFrom, out DateTo);
                //model.DateFrom = DateFrom;
                //model.DateTo = DateTo;
                DataSet ds = newsData.ReadMore(model, out returnResult);

                if (ds != null && ds.Tables.Count > 0)
                {
                    model = new NewsModel();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            model.Title = Convert.ToString(row["Title"]);
                            model.Content = Convert.ToString(row["Content"]);
                            model.Status = Convert.ToString(row["Status"]);
                            model.PublishDate = Convert.ToString(row["PublishDate"]);
                            model.ShareCount = Convert.ToInt32(row["ShareCount"]);
                            model.UserName = Convert.ToString(row["UserName"]);

                        }
                    }

                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                    response._newsModel = model;
                }
                else
                {
                    response.ReturnCode = -2;
                    response.ReturnMessage = Response.Message(-2);

                }
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsAccess", "NewsSelectAll");
            }
            return response;
        }
        public void PublishNSendNotification()
        {
            try
            {
                string deviceToken = string.Empty, deviceType = string.Empty, Message = string.Empty, nid = string.Empty;
                int returnResult = 0;
                DataSet ds = newsData.PublishNSendNotification(out returnResult);
                string desc = string.Empty;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row1 in ds.Tables[0].Rows)
                    {
                        Message = Convert.ToString(row1["Title"]);
                        nid = Convert.ToInt32(row1["NewsId"]).ToString();
                        desc = Convert.ToString(row1["content"]);
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[1].Rows)
                            {
                                deviceToken = Convert.ToString(row["DeviceToken"]);
                                deviceType = Convert.ToString(row["DeviceType"]);
                                
                                if (deviceType.ToUpper() == "IOS")
                                    APNSNotificationAccess.ApnsNotification(deviceToken, Message, nid, "n", desc);
                                else
                                   if (deviceType.ToUpper() == "ANDROID")
                                    FCMNotificationAccess.SendFCMNotifications(deviceToken, Message, deviceType, nid, "news", desc);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsAccess", "PublishNSendNotification");

            }
        }

        public Int32 Updatenewnotification(string EncryptedUserId)
        {
            try
            {
               string EncryptedNewsId = UtilityAccess.Decrypt(EncryptedUserId);
                return newsData.Updatenewnotification((EncryptedUserId));
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChefAccess", "AccountStatusChange");
                return -1;
            }
        }
    }
}
