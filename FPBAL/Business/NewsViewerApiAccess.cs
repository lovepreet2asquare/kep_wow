﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class NewsViewerApiAccess : INewsViewer
    {
        public FPResponse NewsViewerInsert(FPNewsViewerRequest request)
        {
            string returnResult = "0";
            FPResponse response = new FPResponse();
            try
            {
                request.FollowerId = (UtilityAccess.Decrypt(request.FollowerId));
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                int returnvalues = NewsViewerApiData.NewsViewerInsert(request);
                if (returnvalues > 0)
                {
                    response.ReturnCode = returnvalues.ToString();
                    response.ReturnMessage = Response.Message(returnvalues);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsViewerApiAccess", "NewsViewerInsert");
                return response;
            }
        }

        public FPResponse NewsShareInsert(FPNewsViewerRequest request)
        {
            string returnResult = "0";
            FPResponse response = new FPResponse();
            try
            {
                request.FollowerId = (UtilityAccess.Decrypt(request.FollowerId));
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                int returnvalues = NewsViewerApiData.NewsShareInsert(request);
                if (returnvalues > 0)
                {
                    response.ReturnCode = returnvalues.ToString();
                    response.ReturnMessage = Response.Message(returnvalues);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsViewerApiAccess", "NewsShareInsert");
                return response;
            }
        }

       
    }
}
