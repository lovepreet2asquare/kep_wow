﻿using FamousPerson.Models;
using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class NotificationAccess
    {
        NotificationData notificationData = new NotificationData();
        public NotificationModel SelectAll(string UserId, string SessionToken)
        {
            NotificationModel model = new NotificationModel();
            UserId = UtilityAccess.Decrypt(Function.ReadCookie("EncryptedUserId"));
            SessionToken = UtilityAccess.Decrypt(Function.ReadCookie("EncryptedSessionToken"));
            DataSet ds = NotificationData.SelectAll(UserId, SessionToken);

            try
            {
                if (ds != null && ds.Tables.Count > 0)
                {

                    model._NotificationList = NotificationList(ds.Tables[0]);
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NotificationAccess", "SelectAll");
                return null;
            }
            return model;
        }

        public NotificationResponse NotificationRead(NotificationModel model)
        {
            Int32 returnResult = 0;
            NotificationResponse response = new NotificationResponse();
            response.NotificationModel = new NotificationModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                //model.StationId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptStationId));

                returnResult = notificationData.NotificationRead(model);
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NotificationAccess", "NotificationRead");
                return response;
            }
        }


        private List<NotificationModel> NotificationList(DataTable dt   )
        {
            //ReturnResult = 1;
            try
            {
                List<NotificationModel> data = new List<NotificationModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new NotificationModel
                            {
                                NotificationId = Convert.ToInt32(row["NotificationId"]),
                                Message = (row["Message"] as String ?? ""),
                                CreateDate = (row["CreateDate"] as String ?? ""),
                                //IsView = Convert.ToBoolean(row["IsView"]),
                                //NotifyDay = Convert.ToString(row["NotifyDay"])

                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NotificationData", "NotificationList");
                //ReturnResult = -1;
                return null;
            }
        }

    }
}
