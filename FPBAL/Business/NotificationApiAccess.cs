﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class NotificationApiAccess : INotificationAPI
    {
        public FPResponse UpdateNotification(UpdateSettingRequest request)
        {
            int returnResult = 0;
            FPResponse response = new FPResponse();
            try
            {
                request.FollowerId = (UtilityAccess.Decrypt(request.FollowerId));
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                int returnvalues = NotificationApiData.UpdateNotification(request);
                if (returnvalues > 0)
                {
                    response.ReturnCode = Convert.ToString(returnvalues);
                    response.ReturnMessage = Response.Message(returnvalues);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NotificationApiAccess", "UpdateNotification");
                return response;
            }
        }
    }
}
