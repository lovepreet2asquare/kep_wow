﻿using System;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers.Bases;
using FPModels.Models;

namespace net.authorize
{
    public class RefundTransaction
    {
        public static ANetApiResponse Refund(String ApiLoginID, String ApiTransactionKey, decimal TransactionAmount, string TransactionID, string CardNumber, string expiryMonth, string expiryYear, out DonationResponse refundResponse)
        {
            refundResponse = new DonationResponse();
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;

            expiryMonth = expiryMonth.Length == 1 ? "0" + expiryMonth : expiryMonth;
            expiryYear = expiryYear.Length == 4 ? expiryYear.Substring(expiryYear.Length - 2) : expiryYear.Substring(expiryYear.Length - 2);
            CardNumber = CardNumber.Length > 4 ? CardNumber.Substring(CardNumber.Length - 4) : CardNumber.Substring(CardNumber.Length - 4);
            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey
            };

            var creditCard = new creditCardType
            {
                cardNumber = CardNumber, ///"1111",
                expirationDate = expiryMonth + expiryYear
            };

            //standard api call to retrieve response
            var paymentType = new paymentType { Item = creditCard };

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.refundTransaction.ToString(),    // refund type
                payment = paymentType,
                amount = TransactionAmount,
                refTransId      = TransactionID
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the contoller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();
            refundResponse.AuthTransactionId = "0";
            //validate
            if (response != null)
            {
                if (response.messages.resultCode == messageTypeEnum.Ok)
                {
                    if(response.transactionResponse.messages != null)
                    {
                        refundResponse.ReturnCode = 1;
                        refundResponse.ReturnMessage = "Refunded successfully";
                        refundResponse.AuthTransactionId = response.transactionResponse.transId;
                    }
                    else
                    {
                        if (response.transactionResponse.errors != null)
                        {
                            refundResponse.ReturnCode = 0;
                            refundResponse.ReturnMessage = "Error: " + response.transactionResponse.errors[0].errorText;
                        }
                    }
                }
                else
                {
                    if (response.transactionResponse != null && response.transactionResponse.errors != null)
                    {
                        refundResponse.ReturnCode = 27;
                        refundResponse.ReturnMessage = "Error: "+ response.transactionResponse.errors[0].errorText;
                    }
                    else
                    {
                        refundResponse.ReturnCode = 0;
                        refundResponse.ReturnMessage = "Error: " + response.messages.message[0].text;
                    }
                }
            }
            else
            {
                refundResponse.ReturnCode = 0;
                refundResponse.ReturnMessage = "Refund failed";
            }

            return response;
        }      
    }
}
