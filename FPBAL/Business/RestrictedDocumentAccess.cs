﻿using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class RestrictedDocumentAccess
    {
        RestrictedDocumentData restrictedDocumentData = new RestrictedDocumentData();
        public RestrictedDocumentResponse Add(RestrictedDocumentModel model)
        {
            Int32 ReturnResult = 0;
            RestrictedDocumentResponse response = new RestrictedDocumentResponse();
            response._restrictedDocumentModel = new RestrictedDocumentModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(ReturnResult);
            try
            {

                if (!string.IsNullOrEmpty(model.EncryptedDocumentId))
                {
                    model.RDocumentId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedDocumentId));
                }
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                DataSet ds = restrictedDocumentData.Add(model, out ReturnResult);
                string deviceToken = string.Empty, deviceType = string.Empty, Message = string.Empty, did = string.Empty;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        did = Convert.ToInt32(row["DocumentId"]).ToString();
                        deviceToken = Convert.ToString(row["DeviceToken"]);
                        deviceType = Convert.ToString(row["DeviceType"]);
                        Message = Convert.ToString(row["NotifyMessage"]);
                        if (deviceType.ToUpper() == "IOS")
                            APNSNotificationAccess.ApnsNotification(deviceToken, Message, did, "r");
                        else
                           if (deviceType.ToUpper() == "ANDROID")
                            FCMNotificationAccess.SendFCMNotifications(deviceToken, Message, deviceType, did, "resdocument");
                    }
                }
                response.ReturnCode = ReturnResult;
                response.ReturnMessage = Response.Message(ReturnResult);

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "RestrictedDocumentAccess", "Add");
                return response;
            }
        }
        public RestrictedDocumentResponse SelectAll(RestrictedDocumentModel model)
        {
            RestrictedDocumentResponse response = new RestrictedDocumentResponse();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            response._restrictedDocumentModel = new RestrictedDocumentModel();


            Int32 returnResult = 0;

            try
            {

                model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

                DataSet ds = restrictedDocumentData.SelectAll(model, out returnResult);

                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        response._restrictedDocumentModel._RDocumentList = RDocumentList(ds.Tables[0], returnResult);
                        returnResult = 2;

                    }
                }

                else
                {
                    response.ReturnCode = 0;
                    response.ReturnMessage = "No Data Found";
                }
                RestrictedDocumentModel docinfo = new RestrictedDocumentModel();
                response._restrictedDocumentModel._StatusList = UtilityAccess.ActiveStatusList(2);
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "RestrictedDocumentAccess", "SelectAll");
            }
            return response;
        }
        private List<RestrictedDocumentModel> RDocumentList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<RestrictedDocumentModel> data = new List<RestrictedDocumentModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new RestrictedDocumentModel
                            {

                                EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"])),
                                EncryptedDocumentId = UtilityAccess.Encrypt(Convert.ToString(row["DocumentId"])),
                                RDocumentName = Convert.ToString(row["DocumentName"]),
                                RDocumentPath = Convert.ToString(row["DocumentPath"]),
                                UserId = Convert.ToInt32(row["UserId"]),
                                ModifiedBy = Convert.ToString(row["ModifiedBy"]),
                                ModifiedByDate = Convert.ToString(row["ModifiedByDate"]),
                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "RestrictedDocumentModel", "RDocumentList");
                ReturnResult = -1;
                return null;
            }
        }
        public RestrictedDocumentResponse Delete(RestrictedDocumentModel model)
        {
            RestrictedDocumentResponse response = new RestrictedDocumentResponse();
            RestrictedDocumentModel docinfo = new RestrictedDocumentModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            int returnResult = 0;
            if (model != null && model.EncryptedDocumentId != null)
                model.RDocumentId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedDocumentId));

            model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = restrictedDocumentData.Delete(model);
            if (returnResult > 0)
            {
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);
                response._restrictedDocumentModel = docinfo;
            }
            return response;
        }
        public RestrictedDocumentResponse LogSelectAll(RestrictedDocumentModel model)
        {
            RestrictedDocumentResponse response = new RestrictedDocumentResponse();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            response._restrictedDocumentModel= new RestrictedDocumentModel();
            Int32 returnResult = 0;
            try
            {
                model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

                DataSet ds = restrictedDocumentData.LogSelectAll(model, out returnResult);

                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        response._restrictedDocumentModel._LogBookList = LogList(ds.Tables[0], returnResult);
                        returnResult = 2;

                    }
                }

                else
                {
                    response.ReturnCode = 0;
                    response.ReturnMessage = "No Data Found";
                }
                RestrictedDocumentModel docinfo = new RestrictedDocumentModel();
                response._restrictedDocumentModel._StatusList = UtilityAccess.ActiveStatusList(2);
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "RestrictedDocumentAccess", "LogSelectAll");
            }
            return response;
        }
        private List<RestrictedDocumentModel> LogList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<RestrictedDocumentModel> data = new List<RestrictedDocumentModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new RestrictedDocumentModel
                            {

                                EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"])),
                                EncryptedDocumentId = UtilityAccess.Encrypt(Convert.ToString(row["DocumentId"])),
                                UniqueId = Convert.ToString(row["UniqueId"]),
                                FirstName = Convert.ToString(row["FirstName"]),
                                LastName = Convert.ToString(row["LastName"]),
                                MI = Convert.ToString(row["MI"]),
                                CreateDate = Convert.ToString(row["CreateDate"]),
                                ISDCode = Convert.ToString(row["ISDCode"]),
                                Mobile = Convert.ToString(row["Mobile"]),
                                UserId = Convert.ToInt32(row["UserId"]),
                                RDocumentName = Convert.ToString(row["DocumentName"]),
                                ThumbnailPath = Convert.ToString(row["ThumbnailPath"]),

                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "RestrictedDocumentModel", "DocumentList");
                ReturnResult = -1;
                return null;
            }
        }
    }
}
