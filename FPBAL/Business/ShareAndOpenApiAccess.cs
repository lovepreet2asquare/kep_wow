﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class ShareAndOpenApiAccess : IShareAndOpenApi
    {
        public FP_ShareAndOpenApiResponse OpenMessage(FP_ShareAndOpenApiRequest request)
        {
            int returnResult = 0;
            FP_ShareAndOpenApiResponse response = new FP_ShareAndOpenApiResponse();
            try
            {
                request.FollowerId = (UtilityAccess.Decrypt(request.FollowerId));
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                int returnvalues = ShareAndOpenApiData.OpenMessage(request);
                if (returnvalues > 0)
                {
                    response.ReturnCode = returnvalues;
                    response.ReturnMessage = Response.Message(returnvalues);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ShareAndOpenApiAccess", "OpenMessage");
                return response;
            }
        }

        public FP_ShareAndOpenApiResponse ShareCount(FP_ShareAndOpenApiRequest request)
        {
            int returnResult = 0;
            FP_ShareAndOpenApiResponse response = new FP_ShareAndOpenApiResponse();
            try
            {
                request.FollowerId = (UtilityAccess.Decrypt(request.FollowerId));
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                int returnvalues = ShareAndOpenApiData.ShareCount(request);
                if (returnvalues > 0)
                {
                    response.ReturnCode = returnvalues;
                    response.ReturnMessage = Response.Message(returnvalues);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ShareAndOpenApiAccess", "ShareCount");
                return response;
            }
        }
    }
}
