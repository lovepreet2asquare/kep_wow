﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static FPBAL.Business.UtilityAccess;

namespace FPBAL.Business
{
    public class TicketAccess : ITicket
    {
        TicketData ticketData = new TicketData();
        #region Ticket
        /// <summary>
        /// To get all Tickets data.
        /// </summary>
        /// <param name="model">Pass TicketModel class as search fields.</param>
        /// <returns>
        /// It returns TicketModel class properties and related array lists.
        /// </returns>        
        public TicketResponse SelectAll(TicketModel model)
        {
            Int32 returnResult = 0;
            TicketResponse response = new TicketResponse();
            response.TicketModel = new TicketModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                if (!String.IsNullOrEmpty(model.EncryptedTicketId))
                    model.TicketId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedTicketId));

                //data call
                DataSet ds = ticketData.SelectAll(model, out returnResult);

                if (ds != null && ds.Tables.Count > 0)
                {
                    response.TicketModel._StatusList = UtilityAccess.TicketStatusList(0);
                    response.TicketModel._PriorityList = UtilityAccess.PriorityList(0);
                    response.TicketModel._UserList = UtilityAccess.RenderList(ds.Tables[1], 0);

                    response.TicketModel._Tickets = TicketsList(ds.Tables[0]);

                    if (ds.Tables.Count > 1)
                        response.TicketModel._TicketReasonList = UtilityAccess.RenderList(ds.Tables[1], 0);

                    if (ds.Tables.Count > 2)
                        response.TicketModel._UserList = UtilityAccess.RenderList(ds.Tables[2], 0);

                    //if (ds.Tables.Count > 3)
                    //{
                    //    foreach (DataRow row in ds.Tables[3].Rows)
                    //    {
                    //        response.TicketModel.EncryptedTicketId = UtilityAccess.Encrypt(Convert.ToString(row["TicketId"]));
                    //        response.TicketModel.CommunityName = Convert.ToString(row["CommunityName"]);
                    //        response.TicketModel.AccountNumber = Convert.ToString(row["AccountNumber"]);
                    //    }
                    //}
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketkAccess", "SelectAll");
                return null;
            }
        }
        public TicketResponse UnassignSelectAll(TicketModel model)
        {
            Int32 returnResult = 0;
            TicketResponse response = new TicketResponse();
            response.TicketModel = new TicketModel();
            response.ReturnCode = 0;
            model.DateFrom = "2019/01/01";
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                DataSet ds = ticketData.UnassignSelectAll(model, out returnResult);
                if (ds != null && ds.Tables.Count > 0)
                {
                    response.TicketModel._StatusList = UtilityAccess.TicketStatusList(0);
                    response.TicketModel._PriorityList = UtilityAccess.PriorityList(0);
                    response.TicketModel._UserList = UtilityAccess.RenderList(ds.Tables[1], 0);

                    response.TicketModel._Tickets = TicketsList(ds.Tables[0]);

                    if (ds.Tables.Count > 1)
                        response.TicketModel._TicketReasonList = UtilityAccess.RenderList(ds.Tables[1], 0);

                    if (ds.Tables.Count > 2)
                        response.TicketModel._UserList = UtilityAccess.RenderList(ds.Tables[2], 1);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketkAccess", "SelectAll");
                return null;
            }
        }

        private List<TicketModel> TicketsList(DataTable dt)
        {
            try
            {
                List<TicketModel> data = new List<TicketModel>();
                TicketModel _item = null;
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        _item = new TicketModel();
                        _item.SrNo = Convert.ToInt32(row["SrNo"]);
                        _item.TicketId = Convert.ToInt64(row["TicketId"]);
                        _item.TicketNumber = Convert.ToInt64(row["TicketNumber"]);
                        _item.EncryptedTicketId = UtilityAccess.Encrypt(Convert.ToString(row["TicketId"]));
                        _item.TicketReasonId = Convert.ToInt32(row["TicketReasonId"]);
                        _item.ReasonName = Convert.ToString(row["ReasonName"]);
                        _item.TicketDate = Convert.ToString(row["TicketDate"]);
                        _item.TicketTime = Convert.ToString(row["TicketTime"]);
                        _item.Priority = Convert.ToString(row["Priority"]);
                        _item.Status = Convert.ToString(row["Status"]);
                        _item.FollowUpDate = Convert.ToString(row["FollowUpDate"]);
                        _item.FollowUpText = Convert.ToString(row["FollowUpText"]);
                        _item.CreatedBy = (row["CreatedBy"] as Int32?) ?? 0;
                        _item.AssignedTo = (row["AssignedTo"] as Int32?) ?? 0;
                        _item.TaggedTo = (row["TaggedTo"] as Int32?) ?? 0;
                        _item.CreatedByName = Convert.ToString(row["AssignByName"]);
                        _item.AssignToName = Convert.ToString(row["AssignToName"]);
                        _item.TaggedToName = Convert.ToString(row["TaggedToName"]);
                        _item.Description = Convert.ToString(row["Description"]);

                        data.Add(_item);
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketAccess", "TicketsList");
                return null;
            }
        }
        public TicketResponse TicketSearch(TicketModel model)
        {
            Int32 returnResult = 0;
            TicketResponse response = new TicketResponse();
            response.TicketModel = new TicketModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                DataSet ds = ticketData.TicketSearch(model);
                if (ds != null && ds.Tables.Count > 0)
                {
                    response.TicketModel._StatusList = UtilityAccess.TicketStatusList(0);
                    response.TicketModel._PriorityList = UtilityAccess.PriorityList(0);
                    response.TicketModel._UserList = UtilityAccess.RenderList(ds.Tables[1], 2);

                    response.TicketModel._Tickets = TicketsList(ds.Tables[0]);

                    if (ds.Tables.Count > 1)
                        response.TicketModel._TicketReasonList = UtilityAccess.RenderList(ds.Tables[1], 0);

                    if (ds.Tables.Count > 2)
                        response.TicketModel._UserList = UtilityAccess.RenderList(ds.Tables[2], 2);

                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketkAccess", "TicketSearch");
                return null;
            }
        }
        public TicketResponse AddOrEdit(TicketModel model)
        {
            Int32 returnResult = 0;
            TicketResponse response = new TicketResponse();
            response.TicketModel = new TicketModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                string strpath = "";
                string body = string.Empty;
                Exception exc;
                string strNewsletterPath = "~/Newsletters/Follower/Ticket.html";
                string Email = "megha.2asquare@gmail.com";
                // Email = "lovepreet@2asquare.com";
                if (!String.IsNullOrEmpty(model.EncryptedUserId))
                    model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

                if (!String.IsNullOrEmpty(model.SessionToken))
                    model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                if (!String.IsNullOrEmpty(model.EncryptedTicketId))
                    model.TicketId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedTicketId));

                DataSet ds = ticketData.AddOrEdit(model, DtImages(model._Images), out returnResult);
                if (returnResult > 0)
                {

                    if (HttpContext.Current != null)
                    {
                        strpath = HttpContext.Current.Server.MapPath(strNewsletterPath);
                    }
                    else
                    {
                        strpath = System.Web.Hosting.HostingEnvironment.MapPath(strNewsletterPath);
                    }
                    using (StreamReader reader = new StreamReader(strpath))
                    {
                        body = reader.ReadToEnd();
                    }
                    //body = body.Replace("{TicketId}", Convert.ToString(model.TicketId));
                    body = body.Replace("{Reason}", Convert.ToString(model.ReasonName));
                    body = body.Replace("{Subject}", Convert.ToString(model.Subject));
                    body = body.Replace("{Priority}", Convert.ToString(model.Priority));
                    body = body.Replace("{ContactPerson}", Convert.ToString(model.ContactName));
                    body = body.Replace("{ContactNumber}", Convert.ToString(model.ContactNumber));
                    body = body.Replace("{Description}", Convert.ToString(model.Description));


                    string subject = "Received New Ticket";
                    if (model.TicketId == 0)
                    {
                        if (EmailUtilityNew.SendEmailNew1(Email, "NJSPBA Team", subject, body, out exc, true) == -1)
                        {
                            ApplicationLogger.LogError(exc, "EmailUtility", "SendEmail");
                        }
                    }

                    response.TicketModel = SelectDetail(ds);
                }
                    

                //return message
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketsAccess", "AddOrEdit");
                return null;
            }
        }
        private DataTable DtImages(List<ImageModel> _ImageList)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ImageId", typeof(Int32));
            dt.Columns.Add("ImagePath", typeof(string));

            DataRow row = null;
            if (_ImageList != null && _ImageList.Count > 0)
            {
                foreach (var item in _ImageList)
                {
                    row = dt.NewRow();
                    row["ImageId"] = 0;
                    row["ImagePath"] = Convert.ToString(item.ImagePath);
                    dt.Rows.Add(row);
                }
            }
            return dt;
        }
        public TicketResponse SelectById(TicketModel model)
        {
            Int32 returnResult = 0;
            TicketResponse response = new TicketResponse();
            response.TicketModel = new TicketModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                if (!String.IsNullOrEmpty(model.EncryptedUserId))
                    model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                if (!String.IsNullOrEmpty(model.SessionToken))
                    model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                if (!String.IsNullOrEmpty(model.EncryptedTicketId))
                    model.TicketId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedTicketId));

                // call data
                DataSet ds = ticketData.SelectById(model, out returnResult);

                if (ds != null)
                {
                    response.TicketModel = SelectDetail(ds);

                    // community detail
                    if (ds.Tables.Count > 6)
                    {
                        foreach (DataRow row in ds.Tables[6].Rows)
                        {
                            response.TicketModel.EncryptedTicketId  = UtilityAccess.Encrypt(Convert.ToString(row["CommunityId"]));
                            response.TicketModel.CommunityName = Convert.ToString(row["CommunityName"]);
                            response.TicketModel.AccountNumber = Convert.ToString(row["AccountNumber"]);
                        }
                    }
                }
                //return message
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }
            catch (Exception ex)
            {

                ApplicationLogger.LogError(ex, "TicketsAccess", "Select");
                return response;
            }
        }

        public TicketModel SelectDetail(DataSet ds)
        {
            TicketModel data = new TicketModel();
            data.TicketId = 0;
            try
            {

                data._PriorityList = UtilityAccess.PriorityList(1);
                data._StatusList = UtilityAccess.TicketStatusList(1);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        // bool status = false;
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            data.TicketId = Convert.ToInt64(row["TicketId"]);
                            data.TicketNumber = Convert.ToInt64(row["TicketNumber"]);
                            data.TicketDate = Convert.ToString(row["SysDate"]);
                            data.TicketReasonId = Convert.ToInt32(row["TicketReasonId"]);
                            data.Priority = Convert.ToString(row["Priority"]);
                            data.Status = Convert.ToString(row["Status"]);
                            data.FollowUpDate = Convert.ToString(row["FollowUpDate"]);
                            data.FollowUpText = Convert.ToString(row["FollowUpText"]);
                            data.DReasonName = Convert.ToString(row["ReasonName"]);
                            data.CreatedBy = (row["CreatedBy"] as Int32?) ?? 0;
                            data.TitleId = (row["TitleId"] as Int32?) ?? 0;
                            data.AssignedTo = (row["AssignedTo"] as Int32?) ?? 0;
                            data.TaggedTo = (row["TaggedTo"] as Int32?) ?? 0;
                            data.AssignToName = Convert.ToString(row["AssignToName"]);
                            data.TaggedToName = Convert.ToString(row["TaggedToName"]);
                            data.CreatedByName = Convert.ToString(row["AssignByName"]);
                            data.TaggedToIDs = Convert.ToString(row["TaggedToIDs"]);
                            data.Subject = Convert.ToString(row["Subject"]);
                            data.ContactNumber = Convert.ToString(row["ContactNumber"]);
                            data.ISDCode = Convert.ToString(row["IsdCode"]);
                            data.ContactName = Convert.ToString(row["ContactName"]);
                        }
                    }

                    if (ds.Tables.Count > 1)// ticket detail and related images
                        data._TicketDetail = TicketsDetailList(ds.Tables[1], ds.Tables[2]);

                    if (ds.Tables.Count > 3)
                    {
                        data._TicketReasonList = UtilityAccess.RenderList(ds.Tables[3], 1);
                        data._TicketReasonList.Add(new SelectListItem
                        {
                            Text = "Other",
                            Value = "0"
                        });
                    }

                    if (ds.Tables.Count > 4)
                    {
                        data._UserList = UtilityAccess.RenderList(ds.Tables[4], 2);
                        data._TagToList = UtilityAccess.RenderList(ds.Tables[4], 3);
                    }
                    if (ds.Tables.Count > 5)
                        data._UserTitleList = UtilityAccess.RenderList(ds.Tables[5], 2);

                    if (data.TaggedToIDs != null)
                    {
                        data.SelectedTaggedToIDs = data.TaggedToIDs.Split(',').ToArray();
                        List<SelectListItem> selectedItems = data._UserList.Where(p => data.SelectedTaggedToIDs.Contains(p.Value)).ToList();
                        foreach (var selectedItem in selectedItems)
                        {
                            selectedItem.Selected = true;
                        }
                    }
                }

                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketsAccess", "SelectDetail");
                return null;
            }
        }

        private List<TicketDetail> TicketsDetailList(DataTable dt, DataTable dtImage)
        {
            try
            {
                List<TicketDetail> data = new List<TicketDetail>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        TicketDetail ticketDetail = null;
                        foreach (DataRow row in dt.Rows)
                        {
                            ticketDetail = new TicketDetail();
                            ticketDetail.SrNo = Convert.ToInt32(row["SrNo"]);
                            ticketDetail.UserName = (row["UserName"] as String) ?? "";
                            ticketDetail.Description = (row["Description"] as String) ?? "";
                            ticketDetail.SysDate = (row["SysDate"] as String) ?? "";
                            ticketDetail.TicketDetailStatus = Convert.ToString(row["TicketDetailStatus"]);
                            ticketDetail.ContactName = (row["ContactName"] as String) ?? "";
                            ticketDetail.ContactNumber = (row["ContactNumber"] as String) ?? "";
                            data.Add(ticketDetail);

                            if (dtImage != null && dtImage.Rows.Count > 0)
                            {
                                DataRow[] drImgs = dtImage.Select("TicketDetailId = " + row["TicketDetailId"].ToString());
                                if (drImgs.Count() > 0)
                                {
                                    ticketDetail._Images = new List<ImageModel>();
                                    foreach (DataRow drImg in drImgs)
                                    {
                                        ticketDetail._Images.Add(new ImageModel
                                        {
                                            TicketDetailId = Convert.ToInt64(drImg["TicketDetailId"]),
                                            ImagePath = Convert.ToString(drImg["ImagePath"])
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketAccess", "TicketsDetailList");
                return null;
            }
        }

        public List<SelectListItem> UserListByTitle(Int32 TitleId)
        {
            try
            {
                DataSet ds = ticketData.UserListByTitle(TitleId);
                return UtilityAccess.RenderList(ds.Tables[0], 1);

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketAccess", "ValidateReasonName");
                return null;
            }
        }
        public List<SelectListItem> CommunityList(String search)
        {
            try
            {
                DataSet ds = ticketData.CommunityList(search);
                return UtilityAccess.RenderList(ds.Tables[0], 3);

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketAccess", "CommunityList");
                return null;
            }
        }
        public TicketResponse AssignTicket(TicketModel model)
        {
            Int32 returnResult = 0;
            TicketResponse response = new TicketResponse();
            response.TicketModel = new TicketModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                if (!String.IsNullOrEmpty(model.EncryptedUserId))
                    model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                if (!String.IsNullOrEmpty(model.SessionToken))
                    model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                ticketData.AssignTicket(model, out returnResult);

                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }
            catch (Exception ex)
            {

                ApplicationLogger.LogError(ex, "TicketsAccess", "AssignTicket");
                return response;
            }
        }
        #endregion Ticket
        #region Community

    
        #endregion Community

        #region Community_TicketReport
        public TicketResponse Search(TicketModel model)
        {
            Int32 returnResult = 0;
            TicketResponse response = new TicketResponse();
            response.TicketModel = new TicketModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                DataSet ds = ticketData.Search(model);
                if (ds != null && ds.Tables.Count > 0)
                {
                    response.TicketModel._StatusList = UtilityAccess.TicketStatusList(0);
                    response.TicketModel._PriorityList = UtilityAccess.PriorityList(0);
                    response.TicketModel._UserList = UtilityAccess.RenderList(ds.Tables[1], 0);

                    response.TicketModel._Tickets = TicketsList(ds.Tables[0]);

                    if (ds.Tables.Count > 1)
                        response.TicketModel._TicketReasonList = UtilityAccess.RenderList(ds.Tables[1], 0);

                    if (ds.Tables.Count > 2)
                        response.TicketModel._UserList = UtilityAccess.RenderList(ds.Tables[2], 0);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketkAccess", "Search");
                return null;
            }
        }
        #endregion


        #region
        public DataTable ValidateReasonName(String ReasonName)
        {
            try
            {
                var data = ticketData.ValidateReasonName(ReasonName);
                return data.Tables[0];
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketAccess", "ValidateReasonName");
                return null;
            }
        }


        #endregion
    }
}
