﻿using FPDAL.Data;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.Xml;

namespace FPBAL.Business
{
    //public class TimezoneModel
    //{
    //    public double? Latitude { get; set; }
    //    public double? Longitude { get; set; }
    //    public Int32 RawOffset { get; set; }
    //    public DateTime LocalDateTime { get; set; }
    //}

    public class GeoTimezone
    {
        public double dstOffset { get; set; }
        public double rawOffset { get; set; }
        public string status { get; set; }
        public string timeZoneId { get; set; }
        public string timeZoneName { get; set; }

        public double? latitude { get; set; }
        public double? longitude { get; set; }
        public DateTime localDateTime { get; set; }
    }

    public class GeoAddress
    {
        public String AddressLine1 { get; set; }
        public String AddressLine2 { get; set; }
        public String StateName { get; set; }
        public String ZipCode { get; set; }
        public String CityName { get; set; }
        public String CountryName { get; set; }
    }


    public static class Timezone
    {
        public static GeoTimezone GeoTimezone(GeoAddress model)
        {
            GeoTimezone geoTimezone = new GeoTimezone();
            //to Read the Stream

            StreamReader sr = null;
            //model.AddressLine1 = "1600 Amphitheatre Parkway";
            //model.AddressLine2 = "Mountain View";
            //model.StateName = "California ";
            //model.ZipCode = "94043";
            //model.CountryName= "United States of America";
            //string address = "1600 Amphitheatre Parkway, Mountain View, CA 94043, USA";//model.AddressLine1 + ' ' + model.AddressLine2 + model.CityName + ' ' + model.StateName + ' ' + model.Zipcode + ' ' + model.CountryName;

            if (model != null && String.IsNullOrEmpty(model.ZipCode) == false)
            {
                StringBuilder sbUrl = new StringBuilder();
                sbUrl.Append(ConfigurationManager.AppSettings["googleMapGeocode"]);
                sbUrl.Append("&address=");
                sbUrl.Append(model.AddressLine1);
                sbUrl.Append(", " + model.AddressLine2);
                sbUrl.Append(", " + model.StateName);
                sbUrl.Append(" " + model.ZipCode);
                sbUrl.Append(", " + model.CountryName);
                sbUrl.Append("&sensor=false");
                //to Send the request to Web Client 
                WebClient wc = new WebClient();
                try
                {
                    sr = new StreamReader(wc.OpenRead(sbUrl.ToString()));
                }
                catch (Exception ex)
                {
                    ApplicationLogger.LogError(ex, "Timezone", "GeoCode1");

                    geoTimezone.latitude = null;
                    geoTimezone.longitude = null;
                    // throw new Exception("The Error Occured" + ex.Message);
                }

                try
                {
                    XmlTextReader xmlReader = new XmlTextReader(sr);
                    bool latread = false;
                    bool longread = false;

                    while (xmlReader.Read())
                    {
                        //  stringBuilder.Append("Name: " + xmlReader.Name + ":: Value" + xmlReader.Value.ToString());
                        xmlReader.MoveToElement();
                        switch (xmlReader.Name)
                        {
                            case "lat":

                                if (!latread)
                                {
                                    xmlReader.Read();
                                    geoTimezone.latitude = String.IsNullOrEmpty(xmlReader.Value.ToString()) ? 0 : Convert.ToDouble(xmlReader.Value.ToString());
                                    latread = true;

                                }
                                break;
                            case "lng":
                                if (!longread)
                                {
                                    xmlReader.Read();
                                    geoTimezone.longitude = String.IsNullOrEmpty(xmlReader.Value.ToString()) ? 0 : Convert.ToDouble(xmlReader.Value.ToString());// xmlReader.Value.ToString();
                                    longread = true;
                                }

                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //  throw new Exception("An Error Occured" + ex.Message);
                    ApplicationLogger.LogError(ex, "Timezone", "GeoCode2");
                    geoTimezone.latitude = null;
                    geoTimezone.longitude = null;
                }

                if (geoTimezone.latitude != null && geoTimezone.longitude != null)
                {
                    // get rawoffset
                    geoTimezone = RawOffset(geoTimezone.latitude, geoTimezone.longitude);
                    // get local datetime
                    geoTimezone.localDateTime = GetLocalDateTime(geoTimezone.latitude, geoTimezone.longitude, DateTime.UtcNow);
                }
            }
            return geoTimezone;
        }

        public static DateTime GetLocalDateTime(double? latitude, double? longitude, DateTime utcDate)
        {
            try
            {
                if (latitude != null && longitude != null)
                {
                    StringBuilder sbUrl = new StringBuilder();
                    sbUrl.Append(ConfigurationManager.AppSettings["googleMapTimezone"]);
                    sbUrl.Append("&location=" + Convert.ToString(latitude) + "," + Convert.ToString(longitude));
                    sbUrl.Append("&timestamp=1331161200");
                    WebRequest request = WebRequest.Create(sbUrl.ToString());
                    GeoTimezone geoTimezone = null;
                    using (WebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        geoTimezone = new GeoTimezone();
                        using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                        {
                            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            geoTimezone = javaScriptSerializer.Deserialize<GeoTimezone>(reader.ReadToEnd().ToString());
                        }
                    }

                    if (geoTimezone != null)
                        utcDate = utcDate.AddSeconds(geoTimezone.rawOffset + geoTimezone.dstOffset);
                }

                return utcDate;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "Timezone", "GetLocalDateTime");
                return utcDate;
            }
        }

        public static GeoTimezone RawOffset(double? latitude, double? longitude)
        {
            GeoTimezone geoTimezone = new GeoTimezone();
            geoTimezone.rawOffset = 0;

            try
            {
                if (latitude != null && longitude != null)
                {
                    StringBuilder sbUrl = new StringBuilder();
                    sbUrl.Append(ConfigurationManager.AppSettings["googleMapTimezone"]);
                    sbUrl.Append("&location=" + Convert.ToString(latitude) + "," + Convert.ToString(longitude));
                    sbUrl.Append("&timestamp=1331161200");
                    WebRequest request = WebRequest.Create(sbUrl.ToString());
                    using (WebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        geoTimezone = new GeoTimezone();
                        using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                        {
                            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            geoTimezone = javaScriptSerializer.Deserialize<GeoTimezone>(reader.ReadToEnd().ToString());
                            // _RawOffset = Convert.ToInt32(geoTimezone.rawOffset);
                        }
                    }

                    // keep lat,lng
                    geoTimezone.latitude = latitude;
                    geoTimezone.longitude = longitude;
                }

                return geoTimezone;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "Timezone", "RawOffset");
                return geoTimezone;
            }
        }

        public static double ToTimestamp(this DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalSeconds);
        }
    }
}

