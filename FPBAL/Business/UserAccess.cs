﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;

namespace FPBAL.Business
{
    public class UserAccess : IUser
    {
        UserData userData = new UserData();
        public LoginRespone Login(LoginModel request)
        {
            LoginRespone serviceResponse = new LoginRespone();
            UserModel userInfo = new UserModel();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "Login failed";
            serviceResponse.UserInfo = userInfo;
            string encryptedPasdssword = Convert.ToString("oiR5J3NDD13R4F0g08q5CMKhJXVJpIzlQSYtOxYsO3DhskhGOnWxTCScSsEY1zbb");
            string DecryptedPasfdsword = UtilityAccess.Decrypt(encryptedPasdssword);
            int returnResult = 0, returnResult1 = 0, permissionCount = 0;
            string Email = string.Empty, UserId = string.Empty, status = string.Empty;
            DataSet ds = userData.Login(request, out returnResult);
            returnResult1 = returnResult;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                if (returnResult > 0)
                {
                    permissionCount = Convert.ToInt32(ds.Tables[0].Rows[0]["PermisionCount"]);
                    UserId = Convert.ToString(ds.Tables[0].Rows[0]["UserId"]);
                    Email = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);
                    status = Convert.ToString(ds.Tables[0].Rows[0]["status"]);
                    if (permissionCount == 0)
                        returnResult = -4;

                    if(status.ToLower() == "inactive")
                        returnResult = -5;

                    string encryptedPassword = Convert.ToString(ds.Tables[0].Rows[0]["WebPassword"]);
                     string DecryptedPassword = UtilityAccess.Decrypt(encryptedPassword);
                    if (DecryptedPassword == request.Password)
                    {
                        userInfo = UserInfo(ds, out returnResult1);
                        if (returnResult > 0 && returnResult1 > 0)
                        {
                            returnResult = userData.CreateAndSaveToken(request, userInfo.UserId, UtilityAccess.Decrypt(userInfo.EncryptedSessionToken));
                            serviceResponse.ReturnCode = "1";
                            serviceResponse.ReturnMessage = "Login successfuly.";
                            serviceResponse.UserInfo = userInfo;
                        }

                        else if (returnResult == -1)
                        {
                            serviceResponse.ReturnCode = "-1";
                            serviceResponse.ReturnMessage = "Technical error.";

                        }
                        else if (returnResult == -3)
                        {
                            serviceResponse.ReturnCode = "0";
                            serviceResponse.ReturnMessage = "Invalid Email or Password.";
                        }
                        else if (returnResult == -4)
                        {
                            serviceResponse.ReturnCode = "0";
                            serviceResponse.ReturnMessage = "You don't have permission to access.";
                        }
                        else if (returnResult == -5)
                        {
                            serviceResponse.ReturnCode = "0";
                            serviceResponse.ReturnMessage = "Your account is inactive, please contact administator.";
                        }
                    }
                    else
                    {
                        serviceResponse.ReturnCode = "0";
                        serviceResponse.ReturnMessage = "Invalid Email or Password.";
                    }
                }
                else if (returnResult == -3)
                {
                    serviceResponse.ReturnCode = "0";
                    serviceResponse.ReturnMessage = "This user does not exist.";
                }
                else
                {
                    serviceResponse.ReturnCode = "0";
                    serviceResponse.ReturnMessage = "Invalid Email or Password";
                }
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -3)
            {
                serviceResponse.ReturnCode = "0";
                serviceResponse.ReturnMessage = "Invalid Email or Password.";
            }
            return serviceResponse;
        }
        public int sendotpmail()
        {
                string body = string.Empty; ;
            ForgotPasswordResponse response = new ForgotPasswordResponse();
            string strpath = "";
                string strNewsletterPath = "~/Newsletters/Otp.html";

                if (HttpContext.Current != null)
                {
                    strpath = HttpContext.Current.Server.MapPath(strNewsletterPath);
                }
                else
                {
                    strpath = System.Web.Hosting.HostingEnvironment.MapPath(strNewsletterPath);
                }
                using (StreamReader reader = new StreamReader(strpath))
                {
                    body = reader.ReadToEnd();
                }

                body = body.Replace("{OTP}", Convert.ToString(HttpContext.Current.Session["otp"]));



          int res= EmailUtilityNew.SendMessageSmtpNew("NJPBA Local 105", Convert.ToString(HttpContext.Current.Session["Email"]), "OTP", body, true);
         // int res= EmailUtility.SendEmail("NJPBA Local 105", Convert.ToString(HttpContext.Current.Session["Email"]), "OTP", body, true);
            
            return res;
           
        }
        private UserModel UserInfo(DataSet ds, out int returnResult1)
        {
            UserModel userInfo = new UserModel();
            returnResult1 = 1;
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        userInfo.UserId = Convert.ToInt32(row["UserId"]);
                        if (userInfo.UserId > 0)
                        {
                            returnResult1 = userInfo.UserId;
                            userInfo.FirstName = Convert.ToString(row["FirstName"]);
                            userInfo.LastName = Convert.ToString(row["LastName"]);
                            userInfo.MI = Convert.ToString(row["MI"]);
                            userInfo.Mobile = Convert.ToString(row["MobileNumber"]);
                            userInfo.Email = Convert.ToString(row["Email"]);
                            userInfo.EncryptedSessionToken = Convert.ToString(row["TokenKey"]);
                            userInfo.EncryptedSessionToken = UtilityAccess.Encrypt(Convert.ToString(row["TokenKey"]));
                            userInfo.EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"]));
                            userInfo.Password = UtilityAccess.Encrypt(Convert.ToString(row["WebPassword"]));
                            //userInfo.Password = Convert.ToString(row["Password"]);
                            userInfo.ISDCode = Convert.ToString(row["ISDCode"]);
                            userInfo.TitleId = Convert.ToInt32(row["TitleId"]);
                            userInfo.IsEnabled = Convert.ToBoolean(row["IsEnabled"]);
                            //userInfo.TimeZone = Convert.ToString(row["TimeZoneId"]);
                            //userInfo.IsProfileComplete = Convert.ToBoolean(row["IsProfileComplete"]);
                        }
                        else
                        {
                            returnResult1 = userInfo.UserId;
                        }

                    }

                }
            }

            return userInfo;
        }
        public SignupRespone Signup(SignupModel request)
        {
            SignupRespone response = new SignupRespone();
            SignupModel userInfo = new SignupModel();
            response.ReturnCode = 0;
            response.ReturnMessage = "Failed";


            int returnResult = 0;
            request.Password = UtilityAccess.Encrypt(request.Password);
            DataSet ds = userData.Signup(request, out returnResult);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                if (returnResult > 0)
                {
                    String _url = UtilityAccess.BaseUrl;
                    String _email = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);

                    _url += "account/verifyaccount?usr=" + UtilityAccess.Encrypt(Convert.ToString(ds.Tables[0].Rows[0]["UserId"]));

                    String _body = EmailUtility.ReadHtml("signup.html")
                        .Replace("{firstname}", _email.Substring(0, _email.IndexOf("@")))
                        .Replace("{link}", _url);

                    returnResult = EmailUtilityNew.SendMessageSmtpNew("NJPBA Local 105 Team", request.Email, "Welcome", _body, true);
                    //returnResult = EmailUtility.SendEmail("NJPBA Local 105 Team", request.Email, "Welcome", _body, true);
                }
            }
            response.ReturnCode = returnResult;
            response.ReturnMessage = Response.Message(returnResult);
            return response;
        }

        public ForgotPasswordResponse ForgotPassword(string Email)
        {
            ForgotPasswordResponse serviceResponse = new ForgotPasswordResponse();
            int returnResult = 0, userId = 0;
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "This email is not registered with us.";
            Exception exc;
            try
            {
                DataTable dt = new DataTable(); //userData.ForgotPassword(Email, out returnResult);
                if (dt != null && dt.Rows.Count > 0)
                {
                    string body = string.Empty;
                    foreach (DataRow row in dt.Rows)
                    {
                        userId = Convert.ToInt32(row["UserId"]);

                        string userIdStr = userId.ToString();
                        string userIdEncrypted = UtilityAccess.Encrypt(userIdStr);

                        string strToken = UtilityAccess.CalculateMD5Hash(row["PWD"].ToString() + Email.ToLower() + userIdStr);

                        String strLink = UtilityAccess.BaseUrl + "reset.aspx?q=" + userIdEncrypted + "&token=" + strToken; //@"https://demosaavorapi.saavor.io/

                        string strpath = "";
                        string strNewsletterPath = "~/Newsletters/UserForgotPassword.html";

                        if (HttpContext.Current != null)
                        {
                            strpath = HttpContext.Current.Server.MapPath(strNewsletterPath);
                        }
                        else
                        {
                            strpath = System.Web.Hosting.HostingEnvironment.MapPath(strNewsletterPath);
                        }
                        using (StreamReader reader = new StreamReader(strpath))
                        {
                            body = reader.ReadToEnd();
                        }
                        body = body.Replace("{firstname}", row["FirstName"].ToString());
                        body = body.Replace("{link}", strLink);

                    }
                    string subject = "Reset your password";
                    if (EmailUtility.SendEmailNew(Email, "NJPBA Local 105 Team", subject, body, out exc, true) == -1)
                    {
                        ApplicationLogger.LogError(exc, "EmailUtility", "SendEmail");
                        serviceResponse.ReturnCode = "-1";
                        serviceResponse.ReturnMessage = "Technical error.";
                    }
                    else
                    {
                        serviceResponse.ReturnCode = "1";
                        serviceResponse.ReturnMessage = "Reset password link sent to your registered email.";

                        returnResult = 1;
                    }
                    //return returnResult;
                }
                else
                {
                    serviceResponse.ReturnCode = "0";
                    serviceResponse.ReturnMessage = "This email is not registered with us.";
                }
            }
            catch (Exception ex)
            {
                exc = ex;
                ApplicationLogger.LogError(ex, "UserAccessLayer", "ForgotPassword");
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";
            }
            finally
            {
            }
            return serviceResponse;
        }
        public ForgotPasswordModel IsEmailExist(ForgotPasswordModel model)
        {
            ForgotPasswordModel _data = new ForgotPasswordModel();
            _data.ReturnValue = 0;
            try
            {
                DataTable dt = userData.IsEmailExist(model.Email);

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        _data.UserId = Convert.ToInt32(row["UserId"]);
                        _data.Username = Convert.ToString(row["Username"]);
                        _data.Email = Convert.ToString(row["Email"]);
                    }

                    _data.ReturnValue = SendResetPasswrordLink(_data);
                }
                return _data;
            }
            catch (Exception ex)
            {
                _data.ReturnValue = -1;
                ApplicationLogger.LogError(ex, "UserAccess", "IsEmailExist");
                return _data;
            }
        }
        public static Int32 SendResetPasswrordLink(ForgotPasswordModel data)
        {
            Int32 RetVal = -1;
            try
            {

                if (!String.IsNullOrEmpty(data.Email))
                {
                    String url = ConfigurationManager.AppSettings["webUrl"].ToString();
                    url += "/account/resetpassword?ur=" + UtilityAccess.Encrypt(Convert.ToString(data.UserId));
                    url += "&tm=" + UtilityAccess.Encrypt(Convert.ToString(UtilityAccess.DateTimeToUnixTimestamp(DateTime.Now)));

                    String _body = EmailUtility.ReadHtml("ForgotPassword.html")
                         .Replace("{firstname}", data.Username)
                        .Replace("{link}", url);

                    RetVal = EmailUtilityNew.SendMessageSmtpNew("NJPBA Local 105 Team", data.Email, "Reset Password", _body, true);
                   // RetVal = EmailUtility.SendEmail("NJPBA Local 105 Team", data.Email, "Reset Password", _body, true);
                }
                return RetVal;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "SendResetPasswrordLink");
                return RetVal;
            }
        }
        public static String IsAuthorize()
        {
            try
            {
                if (UtilityAccess.SessionGet("EncryptedUserId") == "")
                    return "/account/login";
                else if (UtilityAccess.SessionGet("FP_PageAccess") == "")
                    return "/home/unauthorize";
                else if (UtilityAccess.SessionGet("FP_PageAccess").ToLower() == "false")
                    return "/home/unauthorize";
                else
                    return "";
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "ResetPassword");
                return null;
            }
        }
        public static Int32 ResetPassword(ResetPasswordModel model)
        {
            try
            {
                model.Password = UtilityAccess.Encrypt(model.Password);
                return UserData.ResetPassword(model);
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "ResetPassword");
                return -1;
            }
        }
        public UserResponse UserSelectAll(UserModel model)
        {
            UserResponse userResponse = new UserResponse();
            userResponse.ReturnCode = 0;
            userResponse.ReturnMessage = Response.Message(0);
            DataSet ds = null;
            List<UserModel> userList = new List<UserModel>();
            UserModel userInfo = null;
            try
            {
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                if (model.New == 1)
                {
                    ds = UserData.UserMenuPermission(model);
                }
                else
                {
                    ds = UserData.UserSelectAll(model);
                }

                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            userInfo = new UserModel();
                            userInfo.UserId = Convert.ToInt32(row["UserId"]);
                            userInfo.EncryptedUserId = UtilityAccess.Encrypt(row["UserId"].ToString());
                            userInfo.FirstName = Convert.ToString(row["FirstName"]);
                            userInfo.MI = Convert.ToString(row["MI"]);
                            userInfo.LastName = Convert.ToString(row["LastName"]);
                            userInfo.Title = Convert.ToString(row["UserTitle"]);
                            userInfo.Email = Convert.ToString(row["Email"]);
                            userInfo.Mobile = Convert.ToString(row["Mobile"]);
                            userInfo.UniqueId = Convert.ToString(row["UniqueId"]);
                            userInfo.Status = Convert.ToString(row["Status"]);// == DBNull.Value ? "InActive" : row["Status"]);
                            //userInfo.UserCode = Convert.ToString(row["UserCode"]);
                            //userInfo.EncryptedSessionToken = UtilityAccess.Encrypt(Convert.ToString(row["TokenKey"]));
                            //userInfo.EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"]));
                            userList.Add(userInfo);
                        }
                        userResponse.UserModel = new UserModel();
                        userResponse.UserModel._UserList = new List<UserModel>();
                        userResponse.UserModel._UserList = userList;
                        userResponse.ReturnCode = 1;
                        userResponse.ReturnMessage = Response.Message(1);

                    }
                    else
                    {
                        userResponse.ReturnCode = -2;
                        userResponse.ReturnMessage = Response.Message(-2);
                    }
                }
                else
                {
                    userResponse.ReturnCode = -2;
                    userResponse.ReturnMessage = Response.Message(-2);
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "GetUserList");
            }
            return userResponse;
        }
        public UserResponse UserSelectNewAll(UserModel model)
        {
            UserResponse userResponse = new UserResponse();
            userResponse.ReturnCode = 0;
            userResponse.ReturnMessage = Response.Message(0);
            DataSet ds = null;
            List<UserModel> userList = new List<UserModel>();
            UserModel userInfo = null;
            try
            {
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                if (model.New == 1)
                {
                    ds = UserData.UserMenuPermission(model);
                }
                else
                {
                    ds = UserData.UserSelectNewAll(model);
                }

                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            userInfo = new UserModel();
                            userInfo.UserId = Convert.ToInt32(row["UserId"]);
                            userInfo.EncryptedUserId = UtilityAccess.Encrypt(row["UserId"].ToString());
                            userInfo.FirstName = Convert.ToString(row["FirstName"]);
                            userInfo.MI = Convert.ToString(row["MI"]);
                            userInfo.LastName = Convert.ToString(row["LastName"]);
                            userInfo.Title = Convert.ToString(row["UserTitle"]);
                            userInfo.Email = Convert.ToString(row["Email"]);
                            userInfo.Mobile = Convert.ToString(row["Mobile"]);
                            userInfo.UniqueId = Convert.ToString(row["UniqueId"]);
                            userInfo.Status = Convert.ToString(row["Status"]);// == DBNull.Value ? "InActive" : row["Status"]);
                            //userInfo.UserCode = Convert.ToString(row["UserCode"]);
                            //userInfo.EncryptedSessionToken = UtilityAccess.Encrypt(Convert.ToString(row["TokenKey"]));
                            //userInfo.EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"]));
                            userList.Add(userInfo);
                        }
                        userResponse.UserModel = new UserModel();
                        userResponse.UserModel._UserList = new List<UserModel>();
                        userResponse.UserModel._UserList = userList;
                        userResponse.ReturnCode = 1;
                        userResponse.ReturnMessage = Response.Message(1);

                    }
                    else
                    {
                        userResponse.ReturnCode = -2;
                        userResponse.ReturnMessage = Response.Message(-2);
                    }
                }
                else
                {
                    userResponse.ReturnCode = -2;
                    userResponse.ReturnMessage = Response.Message(-2);
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "GetUserList");
            }
            return userResponse;
        }
        public UserVerificationResponse UserVerificationSelect(UserModel model)
        {
            UserVerificationResponse userResponse = new UserVerificationResponse();
            userResponse.ReturnCode = 0;
            userResponse.ReturnMessage = Response.Message(0);
            DataSet ds = null;
            List<UserModel> userList = new List<UserModel>();
            userResponse.UserModel = new UserModel();
            UserModel userInfo = new UserModel();
           // UserModel userInfo = null;
            try
            {
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.EncryptedUserId= UtilityAccess.Decrypt(model.EncryptedUserId);
                 ds = UserData.UserVerificationSelect(model);
    
                userResponse.UserModel = new UserModel();

                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            
                            
                                userInfo.Mobile = Convert.ToString(row["MobileNumber"]);
                                userInfo.Email = Convert.ToString(row["Email"]);
                                userInfo.IsEnabled = Convert.ToBoolean(row["IsEnabled"] ?? string.Empty);
                             
                        }
                    }
                }
                    userResponse.UserModel = userInfo;


                userResponse.ReturnCode = 1;
                userResponse.ReturnMessage = Response.Message(1);


            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "GetUserList");
            }
            return userResponse;

        }
          public UserVerificationResponse UserVerificationUpsert(UserModel model)
        {
            UserVerificationResponse userResponse = new UserVerificationResponse();
            userResponse.ReturnCode = 0;
            userResponse.ReturnMessage = Response.Message(0);
            DataSet ds = null;
            List<UserModel> userList = new List<UserModel>();
            userResponse.UserModel = new UserModel();
            UserModel userInfo = null;
            try
            {
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.EncryptedUserId= UtilityAccess.Decrypt(model.EncryptedUserId);

                int result = UserData.UserVerificationupsert(model);
               
               // userResponse.UserModel.IsEnabledId = result;
                //userResponse.UserModel.IsEnabledId = result;


                userResponse.ReturnCode = result;
                userResponse.ReturnMessage = Response.Message(1);


            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "GetUserList");
            }
            return userResponse;

        }

        public UserResponse GetUser(UserModel model)
        {
            UserResponse serviceResponse = new UserResponse();
            UserModel userInfo = new UserModel();
            userInfo.ChangePassworMMOdel  = new ChangePasswordModell();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            int returnResult = 0;
            try
            {

                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                //model.UserId =Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                DataSet ds = userData.SelectById(model, out returnResult);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            userInfo.UserId = Convert.ToInt32(row["UserId"]);
                            if (userInfo.UserId > 0)
                            {
                                returnResult = userInfo.UserId;
                                userInfo.FirstName = Convert.ToString(row["FirstName"]);
                                userInfo.LastName = Convert.ToString(row["LastName"]);
                                userInfo.MI = Convert.ToString(row["MI"] ?? string.Empty);
                                userInfo.ProfilePic= Convert.ToString(row["ProfilePic"]);
                                if (userInfo.ProfilePic != "")
                                {
                                   // userInfo.ProfilePic = "https://njspba.keplerconnect.com/" + userInfo.ProfilePic;
                                }
                                userInfo.Email = Convert.ToString(row["Email"]);
                                userInfo.OldEmail = Convert.ToString(row["Email"]);
                                //userInfo.TimeZone= Convert.ToString(row["TimeZone"]);
                                userInfo.Mobile = Convert.ToString(row["Mobile"] ?? string.Empty);
                                userInfo.ISDCode = Convert.ToString(row["ISDCode"] ?? string.Empty);
                                userInfo.TitleId = Convert.ToInt32(row["TitleId"] ?? -1);
                                userInfo.Title = Convert.ToString(row["Title"] ?? string.Empty);
                                userInfo.UserTitle = Convert.ToString(row["UserTitle"] ?? string.Empty);
                                userInfo.Status = Convert.ToString(row["Status"] ?? string.Empty);
                                userInfo.MemberType = Convert.ToString(row["MemberType"] ?? string.Empty);
                                userInfo.MemberTypeId = Convert.ToInt32(row["MemberTypeId"] ?? string.Empty);
                                userInfo.UniqueId = Convert.ToString(row["UniqueId"] ?? string.Empty);
                                //userInfo.Status = Convert.ToBoolean(row["Status"])== DBNull.Value ? "false" : row["Status"]);
                                //userInfo.UserCode = Convert.ToString(row["UserCode"] ?? string.Empty);
                                userInfo.Password= userInfo.ConfirmPassword = userInfo.CurrentPassword=UtilityAccess.Decrypt(Convert.ToString(row["Password"]) ?? string.Empty);
                                HttpContext.Current.Session["UserPassword"] = Convert.ToString(row["Password"]??string.Empty);
                            }
                        }
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            userInfo.ChangePassworMMOdel.UserId = Convert.ToString(row["UserId"]);
                            userInfo.ChangePassworMMOdel.Password = Convert.ToString(row["Password"] ?? string.Empty);
                        }
                    }
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        userInfo._TitleList = UtilityAccess.RenderList(ds.Tables[1], 1);
                    }
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        userInfo._MenuList = new List<MenuModel>();

                        foreach (DataRow row in ds.Tables[2].Rows)
                        {
                            userInfo._MenuList.Add(new MenuModel
                            {
                                MenuId = Convert.ToInt32(row["menuid"] ?? 0),
                                Text = Convert.ToString(row["Text"]),
                                UserId = Convert.ToInt32(row["userid"] == DBNull.Value ? 0 : row["userid"]),
                                Access = Convert.ToBoolean(row["Access"] == DBNull.Value ? 0 : row["Access"]),
                                Create = Convert.ToBoolean(row["Create"] == DBNull.Value ? 0 : row["Create"]),
                                Edit = Convert.ToBoolean(row["Edit"] == DBNull.Value ? 0 : row["Edit"]),
                                Delete = Convert.ToBoolean(row["Delete"] == DBNull.Value ? 0 : row["Delete"])
                            });
                        }
                    }
                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        //userInfo._CountryList = UtilityAccess.RenderList(ds.Tables[3], 1);
                        userInfo._CountryList = UtilityAccess.RenderCountryList(ds.Tables[3]);

                    }
                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[4].Rows)
                        {
                            userInfo.AddressLine1 = Convert.ToString(row["AddressLine1"] ?? string.Empty);
                            userInfo.AddressLine2 = Convert.ToString(row["AddressLine2"] ?? string.Empty);
                            userInfo.CityName = Convert.ToString(row["City"] ?? string.Empty);
                            userInfo.StateId = Convert.ToInt32(row["StateId"] ?? 0);
                            userInfo.CountryId = Convert.ToInt32(row["CountryId"] ?? 0);
                            userInfo.Zip = Convert.ToString(row["Zip"] ?? string.Empty);
                            userInfo.ProfilePic = Convert.ToString(row["ProfilePic"] ?? string.Empty);
                        }
                    }
                    if (ds.Tables[5].Rows.Count > 0)
                    {
                        //userInfo._StateList = UtilityAccess.RenderList(ds.Tables[5], 1);
                        userInfo._StateList = UtilityAccess.RenderStateList(ds.Tables[5]);

                    }
                    if (ds.Tables[6].Rows.Count > 0)
                    {
                        userInfo._TimeZoneList = UtilityAccess.RenderZoneList(ds.Tables[6]);
                    }
                    //userInfo._ISDCodeList = UtilityAccess.RenderISDCodeList(ds.Tables[0], 1);
                    serviceResponse.ReturnCode = returnResult;
                    serviceResponse.ReturnMessage = Response.Message(returnResult);
                    serviceResponse.UserModel = userInfo;
                }
                return serviceResponse;
            }
            catch(Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "GetUser");
                return serviceResponse;

            }
        }

        public UserResponse PermissionByRole(Int32 TitleId)
        {
            UserResponse userResponse = new UserResponse();
            userResponse.UserModel  = new UserModel();
            userResponse.UserModel._PermissionList = new List<PermissionModel>();

            userResponse.ReturnCode = 0;
            userResponse.ReturnMessage = Response.Message(0);
                        
            try
            {                
                DataSet ds = userData.PermissionByTitle(TitleId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach(DataRow row in ds.Tables[0].Rows)
                    {
                        userResponse.UserModel._PermissionList.Add(new PermissionModel
                        {
                            MenuId = Convert.ToInt32(row["MenuId"]),
                            Access =Convert.ToBoolean(row["Access"]),
                            Create = Convert.ToBoolean(row["Create"]),
                            Edit = Convert.ToBoolean(row["Edit"]),
                            Delete = Convert.ToBoolean(row["Delete"]),
                        });
                    }

                    userResponse.ReturnCode = 1;
                    userResponse.ReturnMessage = Response.Message(1);
                }
                return userResponse;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "PermissionByRole");
                return userResponse;
            }
        }
        private UserModel GetUserById(DataSet ds, out int returnResult)
        {
            UserModel userInfo = new UserModel();
            returnResult = 1;
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        userInfo.UserId = Convert.ToInt32(row["UserId"]);
                        if (userInfo.UserId > 0)
                        {
                            returnResult = userInfo.UserId;
                            userInfo.FirstName = Convert.ToString(row["FirstName"]);
                            userInfo.LastName = Convert.ToString(row["LastName"]);
                            userInfo.MI = Convert.ToString(row["MI"]);
                            userInfo.Mobile = Convert.ToString(row["MobileNumber"]);
                            userInfo.EncryptedSessionToken = Convert.ToString(row["TokenKey"]);
                            userInfo.EncryptedSessionToken = UtilityAccess.Encrypt(Convert.ToString(row["TokenKey"]));
                            userInfo.EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"]));
                            userInfo.ISDCode = Convert.ToString(row["ISDCode"]);
                            userInfo.TitleId = Convert.ToInt32(row["TitleId"] == DBNull.Value ? -1 : row["TitleId"]);
                        }
                        else
                        {
                            returnResult = userInfo.UserId;
                        }

                    }

                }
            }

            return userInfo;
        }

        public UserResponse AddOrEditUser(UserModel model)
        {
            Int32 returnResult = 0;
            UserResponse response = new UserResponse();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                //for create userid =0 n edit userid will hold some value in it

                model.EncryptedUserId = Convert.ToString(UtilityAccess.Decrypt(model.EncryptedUserId));
                //model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                DataTable dtbl = userData.IsEmailExist(model.Email);
                if (dtbl != null & dtbl.Rows.Count > 0 & model.UserId == 0)
                {
                    response.ReturnCode = -3;
                    response.ReturnMessage = Response.Message(-3); //email already exists
                }
                else
                {
                    string timeZoneid = "Eastern Standard Time";
                    double timezoneSeconds = UtilityAccess.GetTimeZoneInSeconds(DateTime.UtcNow, timeZoneid);
                    model.TimeOffSet = Convert.ToInt32(timezoneSeconds);

                    string IsEmailSend = "";
                    string OldPassword= UtilityAccess.Decrypt(Convert.ToString(HttpContext.Current.Session["UserPassword"]) ?? string.Empty);

                    if(OldPassword==model.Password)
                    {
                        IsEmailSend = "False";
                    }
                    else
                    {
                        IsEmailSend = "True";
                    }
                    model.Password = UtilityAccess.Encrypt(model.Password);
                    Int32 returnId = userData.AddOrEditUser(model, AddUserPermissions(model._MenuList), out returnResult);

                    if (returnResult > 0)
                    {
                        response.ReturnCode = returnResult;
                        response.ReturnMessage = Response.Message(returnResult);
                    }
                    //in case of create new user email will be sent
                    if (model.UserId > 0)
                    {
                        DataTable dt = userData.IsEmailExist(model.Email);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                model.UserId = Convert.ToInt32(row["UserId"]);
                                model.Username = Convert.ToString(row["Username"]);
                                model.Email = Convert.ToString(row["Email"]);
                                model.RoleType = Convert.ToString(row["RoleType"]);
                            }

                            // send login welcome email
                            if (IsEmailSend == "True")
                            {
                                SendWelcomeEmail(model);
                            }
                            if (response.ReturnCode == -1)
                                response.ReturnMessage = Response.Message(-1);
                            else if (response.ReturnCode == 0)
                                response.ReturnMessage = Response.Message(-2);
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "AddOrEditUser");
                return response;
            }
        }


        public static void UserTimeZoneCronJob()
        {
            try
            {
                //Get time zone offset via time zone id
                string timeZoneid = "Eastern Standard Time";
                double timezoneSeconds = UtilityAccess.GetTimeZoneInSeconds(DateTime.UtcNow, timeZoneid);
                int timeZoneOffSet = Convert.ToInt32(timezoneSeconds);
                
                int returnResult = UserData.TimeZoneCronJobUpdate(timeZoneOffSet);
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "AddOrEditUser");
            }
        }

        private DataTable AddUserPermissions(List<MenuModel> _MenuList)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("MenuId", typeof(Int32));
            dt.Columns.Add("UserId", typeof(Int32));
            dt.Columns.Add("Access", typeof(bool));
            dt.Columns.Add("Create", typeof(bool));
            dt.Columns.Add("Edit", typeof(bool));
            dt.Columns.Add("Delete", typeof(bool));

            DataRow row = null;
            if (_MenuList != null && _MenuList.Count > 0)
            {
                foreach (var item in _MenuList)
                {
                    row = dt.NewRow();
                    row["MenuId"] = item.MenuId;
                    row["UserId"] = item.UserId;
                    row["Access"] = Convert.ToBoolean(item.Access);
                    row["Create"] = Convert.ToBoolean(item.Create);
                    row["Edit"] = Convert.ToBoolean(item.Edit);
                    row["Delete"] = Convert.ToBoolean(item.Delete);
                    dt.Rows.Add(row);
                }
            }
            return dt;
        }
        public Int32 SendWelcomeEmail(UserModel model)
        {
            Int32 RetVal = -1;
            try
            {
                if (!String.IsNullOrEmpty(model.Email))
                {
                    DateTime date = DateTime.Now;
                    String _webUrl = ConfigurationManager.AppSettings["webUrl"].ToString();

                    String _Body = EmailUtility.ReadHtml("welcome.html")
                                       .Replace("{link}", _webUrl)
                                       .Replace("{EmailId}", model.Email)
                                       .Replace("{Password}", model.ConfirmPassword)
                                       .Replace("{RoleType}", model.RoleType);

                   
                   

                    RetVal = EmailUtilityNew.SendMessageSmtpNew("NJPBA Team", model.Email, "Welcome to NJPBA Local 105", _Body, true);
                //    RetVal = EmailUtility.SendEmail("NJPBA Team", model.Email, "Welcome to NJPBA Local 105", _Body, true);
                }
                return RetVal;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "SendResetPasswrordLink");
                return RetVal;
            }
        }

        public UserResponse EditUserProfile(UserModel model)
        {
            Int32 returnResult = 0;
            
            UserResponse response = new UserResponse();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                
                Int32 returnId = userData.EditUserProfile(model, out returnResult);

                if (returnResult > 0)
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }

                //if (dt != null && dt.Rows.Count > 0)
                //{
                //    foreach (DataRow row in dt.Rows)
                //    {
                //        model.UserId = Convert.ToInt32(row["UserId"]);
                //        model.Username = Convert.ToString(row["Username"]);
                //        model.Email = Convert.ToString(row["Email"]);
                //    }

                //    response.ReturnCode = SendCredentialLink(model);
                //    if (response.ReturnCode == -1)
                //        response.ReturnMessage = Response.Message(-1);
                //    else if (response.ReturnCode == 0)
                //        response.ReturnMessage = Response.Message(-2);
                //    else
                //    {
                //        response.ReturnCode = 1;
                //        response.ReturnMessage = Response.Message(1);
                //    }
                //}
                return response;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "EditUserProfile");
                return response;
            }
        }

        public UserResponse NewProfileEmail(UserModel model)
        {
            Int32 returnResult = 0;
            UserResponse response = new UserResponse();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                DataTable dt = userData.IsEmailExist(model.Email);

                if (dt == null && dt.Rows.Count < 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        model.UserId = Convert.ToInt32(row["UserId"]);
                        model.Email = Convert.ToString(row["Email"]);
                    }
                    Int32 returnId = userData.NewProfileEmail(model, out returnResult);
                    response.ReturnCode = SendVerificationLink(model);

                    if (response.ReturnCode == -1)
                        response.ReturnMessage = Response.Message(-1);
                    else if (response.ReturnCode == 0)
                        response.ReturnMessage = Response.Message(-2);
                    else
                    {
                        response.ReturnCode = 1;
                        response.ReturnMessage = Response.Message(1);
                    }

                }
                else
                {
                    response.ReturnCode = -3;
                    response.ReturnMessage = Response.Message(-3);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "EditUserProfile");
                return response;
            }
        }



        public Int32 SendVerificationLink(UserModel model)
        {
            Int32 RetVal = -1;
            try
            {
                if (!String.IsNullOrEmpty(model.Email))
                {
                    DateTime date = DateTime.Now;
                    String _Body = String.Empty;
                    String url = ConfigurationManager.AppSettings["webUrl"].ToString();
                    url += "/user/UpdateNewEmail?ur=" + UtilityAccess.Encrypt(Convert.ToString(model.UserId));
                    url += "&tm=" + UtilityAccess.Encrypt(Convert.ToString(UtilityAccess.DateTimeToUnixTimestamp(date)));

                    _Body = "Hi " + model.Username + ",<br/>";
                    _Body += "To update your requested email id with FP. Kindly click on the link given below:<br/>";
                    _Body += "Link: " + url + "<br /> ";
                    _Body += "If you are still having problems accessing your account, you can contact us for help.<br /> ";
                    _Body += "Thanks for choosing FP.<br /> ";
                    _Body += "Sincerely,<br /> ";
                    _Body += "The NJPBA Support Team<br /> ";

                    //String _SenderName = Convert.ToString(HttpContext.Current.Session["FirstName"]);

                    RetVal = EmailUtilityNew.SendMessageSmtpNew("NJPBA Team", model.Email, "Welcome to NJPBA", _Body, true);
                   // RetVal = EmailUtility.SendEmail("NJPBA Team", model.Email, "Welcome to NJPBA", _Body, true);
                }
                return RetVal;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "SendResetPasswrordLink");
                return RetVal;
            }
        }

        public UserResponse Delete(UserModel model)
        {
            Int32 returnResult = 0;
            UserResponse response = new UserResponse();
            response.UserModel = new UserModel();
            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                Int32 res = userData.Delete(model, out returnResult);
                if (returnResult > 0)
                {
                    // response message
                    response.ReturnCode = returnResult;// 
                    response.ReturnMessage = Response.Message(returnResult);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "Delete");
                return response;
            }
        }

        public UserResponse Disable(UserModel model)
        {
            Int32 returnResult = 0;
            UserResponse response = new UserResponse();
            response.UserModel = new UserModel();
            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                Int32 res = userData.Disable(model, out returnResult);
                if (returnResult > 0)
                {
                    // response message
                    response.ReturnCode = returnResult;// 
                    response.ReturnMessage = Response.Message(returnResult);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserAccess", "Delete");
                return response;
            }
        }
        public Int32 ProfilePicUpdate(Int32 UserId, String SessionToken, String ProfilePicPath)
        {
            int returnResult = 0;
            if (!String.IsNullOrEmpty(SessionToken))
                SessionToken = UtilityAccess.Decrypt(SessionToken);

            returnResult = UserData.ProfilePicUpdate(UserId, SessionToken, ProfilePicPath);
            return returnResult;
        }

        public UserResponse ChangePassword(UserModel model)
        {
            UserResponse response = new UserResponse();
            int ReturnValue = -16;
            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                //string EncryptedUserIdd = model.EncryptedUserId;
                model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.Password = UtilityAccess.Encrypt(model.ChangePassworMMOdel.Password);
                DataSet dss = UserData.selectUserById(model.UserId);
                string encryptedPassword = Convert.ToString(dss.Tables[0].Rows[0]["Password"]);
                string OldPasswordCompare1 = Convert.ToString(UtilityAccess.Decrypt(encryptedPassword));
                if (OldPasswordCompare1 == model.ChangePassworMMOdel.OldPassWord)
                {
                    DataSet ds = UserData.ChangePassword(model);
                    if (ds != null)
                    {
                        response.UserModel = SelectDetailUser(ds);
                    }
                    response.ReturnCode = response.UserModel.ChangePassworMMOdel.ReturnValue;
                    response.ReturnMessage = Response.Message(response.UserModel.ChangePassworMMOdel.ReturnValue);
                }
                else
                {
                    response.ReturnCode = ReturnValue;
                    response.ReturnMessage = Response.Message(ReturnValue);
                }
               
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChangePasswordAccess", "ChangePassword");
                return null;
            }
        }
        private UserModel SelectDetailUser(DataSet ds)
        {
            UserModel data = new UserModel();
            string returnValues;
            data.ChangePassworMMOdel = new ChangePasswordModell();
            data.ChangePassworMMOdel.ReturnValue = -1;
            try
            {
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            data.ChangePassworMMOdel.Email = Convert.ToString(row["Email"]);
                            data.ChangePassworMMOdel.UserId = (row["UserId"] as string ?? "");
                            data.ChangePassworMMOdel.FirstName = Convert.ToString(row["FirstName"]);
                            data.ChangePassworMMOdel.MobileNumber = Convert.ToString(row["MobileNumber"]);
                            data.ChangePassworMMOdel.ReturnValue = Convert.ToInt32(row["ReturnValue"]);
                        }
                    
                }
                
                return data;
            }
            catch (Exception ex)
            {
                data.ChangePassworMMOdel.ReturnValue = -1;
                ApplicationLogger.LogError(ex, "ChangePasswordAccess", "SelectDetail");
                return null;
            }
        }
    }
}
