﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IAboutUs
    {
        AboutUsResponse AddOrEdit(AboutUsModel model);
        AboutUsResponse Select(AboutUsModel model);
        AboutUsResponse Delete(AboutUsModel model);
        //AboutUsResponse SelectAll(AboutUsModel model);
        //Int32 ProfilePicUpdate(Int32 UserId, String SessionToken, String ProfilePicPath);
    }
}
