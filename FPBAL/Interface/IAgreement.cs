﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IAgreement
    {
        AgreementResponse SelectAll(AgreementModel model);
        AgreementResponse Select(AgreementModel model);
        AgreementResponse SelectAgreements(AgreementModel model);
        AgreementResponse AddOrEdit(AgreementModel model);


    }
}
