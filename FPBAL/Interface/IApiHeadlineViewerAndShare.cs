﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IApiHeadlineViewerAndShare
    {
        FPResponse HeadLineViewerInsert(FP_HeadlineViewerAndShareRequest request);
        FPResponse HeadLineShareInsert(FP_HeadlineViewerAndShareRequest request);
    }
}
