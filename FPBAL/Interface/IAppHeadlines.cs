﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;

namespace FPBAL.Business
{
    public interface IAppHeadlines
    {
        FPHeadlineAPIResponse FollowerHeadline(FollowerHeadlinesRequest request);
        FPHeadlineDetailAPIResponse FollowerHeadlineDetail(FollowerHeadlineDetailRequest request);

    }
}
