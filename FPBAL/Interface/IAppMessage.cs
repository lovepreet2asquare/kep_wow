﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IAppMessage
    {
        InAppMessageResponse AddOrEdit(InAppMessageModel request);
        InAppMessageResponse Detail(InAppMessageModel request); 
        InAppMessageResponse DetailView(InAppMessageModel request); 
        InAppMessageResponse Delete(InAppMessageModel request); 
        InAppMessageResponse Archive(InAppMessageModel request); 


    }
}
