﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;

namespace FPBAL.Business
{
    public interface IAppUser
    {
        SignUpRespone SignUp(SignUpAPIModel request);
        SignUpRespone Login(LoginRequest request);
        FPResponse ForgotPassword(string Email);
        CommonDataRespone GetCommonData();
        ProfileUpdateAPIRespone ProfileUpdate(UserAPIModel request);
        FPResponse DeleteAccount(AccountDeleteRequest request);
        FPResponse Logout(LogoutRequest request);

        SignUpRespone GetFollowerInfo(LogoutRequest request);

        FPResponse SendFriendInvitations(FPInviteRequests request);

        ProfileUpdateAPIRespone UploadProfilePic(ProfilePicAPIModel request);

        ProfileUpdateAPIRespone ValidateEmail(FPRequests request);


        FPResponse ApnsNotification(string deviceToken, string Message);
    }
}
