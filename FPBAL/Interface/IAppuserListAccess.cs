﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FPBAL.Interface
{
    public interface IAppuserListAccess
    {
        AppUserListResponse AppUserSelectAll(AppUserListModel model);
       // AppUserListResponse SendBulkEmail(AppUserListModel model);

        InviteResponse UploadHttpPostedFile(InviteDataFileModel model);

        InviteResponse MultiInvite(String InviteDataFileId);
        InviteResponse SingleInvite(Int32 InviteDataId, String FirstName, String Email);
        InviteResponse ExcellDataSelectByFileId(String InviteDataFileId);
    }
}
