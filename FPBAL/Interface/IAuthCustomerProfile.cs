﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;

namespace FPBAL.Interface
{
    public interface IAuthCustomerProfile
    {
        PaymentMethodResponse PaymentMethodInsert(CustProfileInfo customerProfileInfo);
        PaymentMethodsResponse GetPaymentMethods(FPRequests request);
        FPResponse PaymentMethodDelete(DeletePaymentMethodRequest request);
        FPResponse PaymentMethodSetPrimary(SetPrimaryRequest request);

        FPResponse Donate(DonateAPIModel request);
    }
}
