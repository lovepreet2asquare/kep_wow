﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IBatch
    {

        BatchResponse BatchSelectAll(BatchModel model);
        BatchResponse Select(BatchModel model);
        BatchResponse SelectBatchTransactions(BatchModel model);
        BatchResponse BatchAdd(BatchModel model);
        BatchResponse FileStatusUpdate(BatchModel model);
        
    }
}
