﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface ICampaign
    {
        CampaignResponse AddOrEdit(CampaignModel model);
        CampaignResponse Select(CampaignModel model);
        CampaignResponse SelectAll(CampaignModel model);
        CampaignResponse Delete(CampaignModel model);
    }
}
