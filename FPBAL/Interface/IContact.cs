﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
     public interface IContact
    {
        ContactResponse SelectAll(ContactModel model);
        ContactResponse AddOrEdit(ContactModel model);
        ContactResponse Select(ContactModel model);
    }
}
