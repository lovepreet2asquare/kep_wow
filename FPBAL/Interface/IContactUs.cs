﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
  public  interface IContactUs
    {
        FPInvolvedResponse AddContactUs(EPContactUsRequest request);
        FPInvolvedResponse ChangePassword(ChangePasswordApiModel request);
    }
}
