﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IDashboard
    {
        DashboardResponse LoadData(DashboardSearch model);
      string  PdfPrintArticle(List<DbTrendArticleModel> _List, out int ReturnResult);
    }
}
