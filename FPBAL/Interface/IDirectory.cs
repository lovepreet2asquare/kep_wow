﻿using FPBAL.Business;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IDirectory
    {
        DirectoryResponse SelectAll(DirectoryModel model);
        DirectoryResponse SelectAll1(DirectoryModel model);
        DirectoryResponse  SelectById(DirectoryModel model);
        DirectoryResponse AddorEdit(DirectoryModel model);
        DirectoryResponse Delete(DirectoryModel model);
        DirectoryResponse SingleInvite(Int32 InviteDataId, String FirstName, String Email);
        DirectoryResponse MultiInvite(DirectoryModel directoryModel, CancellationToken cancellationToken = default(CancellationToken));
        DirectoryResponse UploadHttpPostedFile(DirectoryModel model);
        DirectoryResponse LogSelectAll(DirectoryModel model);

    }
}
