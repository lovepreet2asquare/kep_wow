﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IDonation
    {
        DonationModel DonationSelectAll(DonationModel model);
        DonationResponse IssueRefund(DonationModel model);
        DonationResponse ViewRefund(DonationModel model);
        int AmountChange(DonationModel model);
        GraphsDataModel DonationGraphData(string duration);

        RecurringTypeModel RecurringTypeSelectAll(RecurringTypeModel model);
        RecurringTypeModel RecurringTypeById(String Id);
        RecurringTyperesponse RecurringTypeInsert(RecurringTypeModel model);
        RecurringTyperesponse Delete(RecurringTypeModel model);
        DonationResponse DetailSelectAll(DonationModel model);
        DonationResponse DetailAddOrEdit(DonationModel model);
        DonationResponse DonorRefund(DonationModel model);
        int DonorSkip(DonationModel model);
        string PdfPrintShare(List<DonationModel> model, DonationModel modell, string type, string senderName, string emailTo, out int ReturnResult);
        string PdfPrintreceipt(DonationModel modell, string type, string senderName, string emailTo, out int ReturnResult);


    }
}
