﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IEvent
    {
        FPEventResponse EventSelect(FPEventRequest request);
        EPAboutResponse aboutSelect(EpAboutRequest request);
        FPOfficeLocationResponse OfficeLocationSelect(EpAboutRequest request);
    }
}
