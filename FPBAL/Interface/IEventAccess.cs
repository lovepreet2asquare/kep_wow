﻿using FPModels.Models;
using System;
using System.Collections.Generic;

namespace FPBAL.Interface
{
    public interface IEventAccess
    {
        EventResponse EventSelectAll(EventModel model);
        EventResponse EventDetail(EventModel model);
        EventResponse AddorEdit(EventModel model);
        EventResponse Archive(EventModel model);
        EventResponse Delete(EventModel model);
        EventResponse AddAgenda(EventModel model);
        EventResponse EventCount(EventModel model);
        string PdfPrintShare(List<EventModel> model, EventModel modell, string type, string senderName, string emailTo, out int ReturnResult);

    }
}
