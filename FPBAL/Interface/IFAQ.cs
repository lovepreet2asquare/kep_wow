﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IFAQ
    {
        FAQResponse SelectAll(FAQModel model);
        FAQResponse AddFaq(FAQModel model);
        FAQResponse AddRes(FAQModel model);
    }
}
