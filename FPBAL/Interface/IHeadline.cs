﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;

namespace FPBAL.Interface
{
    public interface IHeadline
    {
        HeadlineResponse AddorEdit(HeadlineModel model);
        HeadlineResponse Select(HeadlineModel model);
        HeadlineResponse SelectAll(HeadlineModel model);

        HeadlineResponse Delete(HeadlineModel model);
        HeadlineResponse Archive(HeadlineModel model);
        HeadlineResponse DetailById(HeadlineModel model);
    }
}
