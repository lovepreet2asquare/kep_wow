﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;
namespace FPBAL.Interface
{
    public interface IMemberBenefit
    {
        MemberBenefitResponse AddorEdit(MemberBenefitModel model);
        MemberBenefitResponse SelectAll(MemberBenefitModel model);
        MemberBenefitResponse Select(MemberBenefitModel model);
        MemberBenefitResponse DetailById(MemberBenefitModel model);
        MemberBenefitResponse Delete(MemberBenefitModel model);
    }
}
