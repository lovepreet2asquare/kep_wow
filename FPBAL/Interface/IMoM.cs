﻿using FPModels.Models;
namespace FPBAL.Interface
{
    public interface IMoM
    {
        MoMResponse AddorEdit(MoMModel model);
        MoMResponse SelectAll(MoMModel model);
        MoMResponse Select(MoMModel model);
        MoMResponse Delete(MoMModel model);

        MoMValidateResponse ValidateMoM(string month, string year);

        MoMResponse LogSelectAll(MoMModel model);

        FPResponse ApnsNotification(string deviceToken, string message);
    }
}
