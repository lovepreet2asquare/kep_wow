﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface INewsViewer
    {
        FPResponse NewsViewerInsert(FPNewsViewerRequest request);
        FPResponse NewsShareInsert(FPNewsViewerRequest request);
    }
}
