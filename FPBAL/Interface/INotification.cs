﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface INotification
    {
        NotificationResponse SelectAll(NotificationModel model);
        NotificationResponse NotificationRead(NotificationModel model);
    }
}
