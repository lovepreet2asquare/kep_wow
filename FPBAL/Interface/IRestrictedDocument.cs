﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IRestrictedDocument
    {
        RestrictedDocumentResponse Add(RestrictedDocumentModel model);
        RestrictedDocumentResponse SelectAll(RestrictedDocumentModel model);
    }
}
