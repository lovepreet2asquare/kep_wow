﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPBAL.Interface
{
    public interface ITicket
    {
        TicketResponse SelectAll(TicketModel model);
        TicketResponse SelectById(TicketModel model);
        TicketResponse TicketSearch(TicketModel model);
        TicketResponse AddOrEdit(TicketModel model);
        TicketResponse AssignTicket(TicketModel model);
        TicketResponse UnassignSelectAll(TicketModel model);
        List<SelectListItem> CommunityList(String search);
        List<SelectListItem> UserListByTitle(Int32 TitleId);
        DataTable ValidateReasonName(String ReasonName);
    }
}
