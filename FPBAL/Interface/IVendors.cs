﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;

namespace FPBAL.Interface
{
    public interface IVendors
    {
        VendorsResponse SelectAll(VendorsModel model);
        VendorsResponse Delete(VendorsModel model);
        VendorsResponse SelectById(VendorsModel model);
        VendorsResponse AddorEdit(VendorsModel model);
    }
}
