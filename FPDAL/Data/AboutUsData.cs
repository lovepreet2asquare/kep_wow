﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class AboutUsData:ConnectionObject
    {
        public Int32 AddOrEdit(AboutUsModel model, out Int32 ReturnResult)
        {
            int myInt = -1;

            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@Content",model.Content),
                                            new SqlParameter("@ImagePath",model.ImagePath),
                                            new SqlParameter("@Status",model.Status),
                                            new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),

        };
            try
            {
                myInt = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_AboutUsUpsert]", parameters));
                ReturnResult = Convert.ToInt32(parameters[5].Value);
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AboutUsData", "Create");
                ReturnResult = -1;
                return myInt;

            }

        }
        public DataSet Select(AboutUsModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            //model.AboutId = 75;

            SqlParameter[] parameters ={
                                           new SqlParameter("@UserId",model.UserId) ,
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AboutUsDetail", parameters);
                ReturnResult = Convert.ToInt32(parameters[2].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AboutUsData", "Detail");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }
        //public DataSet SelectAll(AboutUsModel model, out Int32 ReturnResult)
        //{
        //    DataSet myDataSet = null;
        //    ReturnResult = 0;
        //    SqlParameter[] parameters ={
        //                                   new SqlParameter("@UserId",model.UserId) ,
        //                                   new SqlParameter("@SessionToken",model.SessionToken) ,
        //                                   //new SqlParameter("@search",model.Search??string.Empty),
        //                                   new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
        //                               };
        //    try
        //    {
        //        myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_CampaignDetail]", parameters);
        //        ReturnResult = Convert.ToInt32(parameters[3].Value);
        //        return myDataSet;
        //    }
        //    catch (Exception ex)
        //    {
        //        ApplicationLogger.LogError(ex, "AboutUsData", "SelectAll");
        //        ReturnResult = -1;
        //        return null;
        //    }
        //    finally

        //    {
        //    }
        //}
        public static int ProfilePicUpdate(int userId, string sessionToken, string FilePath)
        {

            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",userId),
                                            new SqlParameter("@SessionToken",sessionToken),
                                            new SqlParameter("@ImagePath",FilePath)
                                        };
                object obj = SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_AboutPicUpdate]", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "AboutUsData", "ProfilePicUpdate");
            }

            return returnResult;
        }

        public int Delete(AboutUsModel model)
        {

            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@AboutId",model.AboutId),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                           
                                        };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_AboutDelete]", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "AboutUsData", "Delete");
            }

            return returnResult;
        }

    }
}
