﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class AgreementData: ConnectionObject
    {
        public DataSet SelectAll(AgreementModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("@Search",model.Search??string.Empty),
                                           new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_AgreementSelectAll]", parameters);
                ReturnResult = Convert.ToInt32(parameters[2].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AgreementData", "SelectAll");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }

        public DataSet Select(AgreementModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("@AgreementId",model.AgreementId),
                                           new SqlParameter("@UserId",model.UserId),
                                           new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_AgreementDetailById]", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AgreementData", "Detail");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
        public DataSet SelectAgreements(AgreementModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@AgreementId",model.AgreementId),
                                           new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_AgreementsDetailById]", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AgreementData", "Detail");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
        public DataSet SelectFooter(AgreementModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("@Title",model.Title),
                                           new SqlParameter("@UserId",model.UserId),
                                           new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_AgreementFooterById]", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AgreementData", "Detail");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
        public DataSet SelectAgreementFooter(AgreementModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                          
                                           new SqlParameter("@Title",model.Title),
                                           new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_AgreementsFooterById]", parameters);
                ReturnResult = 1;
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AgreementData", "Detail");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
        public DataSet AddOrEdit(AgreementModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.EncryptedSessionToken),
                                            new SqlParameter("@DocumentPath",model.DocumentPath),
                                            new SqlParameter("@DocumentName",model.DocumentName),
                                            new SqlParameter("@AgreementId",model.AgreementId),
                                            new SqlParameter("@Title",model.Title),
                                            new SqlParameter("@Description",model.Description),
                                            new SqlParameter("@ModifyDate",model.DateTime),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AgreementUpsert", parameters);
                ReturnResult = Convert.ToInt32(parameters[8].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AgreementData", "Add");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }
    }
}
