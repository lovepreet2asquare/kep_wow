﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Infotech.ClassLibrary;
using System.Data;
using FPModels.Models;



namespace FPDAL.Data
{
    public class AppHeadlineData : ConnectionObject
    {
        public static DataSet FollowerHeadlineSelect(FollowerHeadlinesRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                           
                                            new SqlParameter("@SessionToken", request.SessionToken),
                                            new SqlParameter("@FollowerId",request.FollowerId),
                                            new SqlParameter("@StartIndex",request.StartIndex),
                                            new SqlParameter("@EndIndex",request.EndIndex),
                                           
                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerHeadlineSelect", parameters);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "HeadlineAPIData", "FollowerHeadlineSelect");
            }

            return ds;
        }


        public static DataSet FollowerHeadlineDetail(FollowerHeadlineDetailRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={


                                            new SqlParameter("@SessionToken", request.SessionToken),
                                            new SqlParameter("@FollowerId",request.FollowerId),
                                            new SqlParameter("@HeadlineId",request.HeadlineId),
                                           

                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerHeadlineDetail", parameters);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "HeadlineAPIData", "FP_FollowerHeadlineDetail");
            }

            return ds;
        }
    }
}
