﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class AppUserListData: ConnectionObject
    {
        public DataSet AppUserListSelectAll(AppUserListModel model)
        {
            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken),
                                            new SqlParameter("@userid", model.UserId),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_AppUserSelectAll", parameters);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AppUserListData", "AppUserListSelectAll");
                return ds;
            }
            finally
            {
            }
        }


        public DataSet FollowerExcellDataAdd(InviteDataFileModel model,out Int32 ReturnValue)
        {
            ReturnValue = -1;
               DataSet myDataSet = null;

            SqlParameter[] parameters ={
                                            new SqlParameter("@InviteDataFileId", model.InviteDataFileId),
                                            new SqlParameter("@FilePath", model.FilePath),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@FollowerExcellDataType",model.dtInviteData),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerExcellDataAdd", parameters);
                ReturnValue = Convert.ToInt32(parameters[4].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ReturnValue = -1;
                ApplicationLogger.LogError(ex, "AppUserListData", "FollowerExcellDataAdd");
                return null;
            }
            finally
            {
            }
        }
        public Int32 InviteEmailSendStatusUpdate(Int64 InviteDataId,String Status)
        {  

            SqlParameter[] parameters ={
                                            new SqlParameter("@InviteDataId", InviteDataId),
                                            new SqlParameter("@Status", Status),
                                            new SqlParameter("ReturnValue", SqlDbType.Int, 4, ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {
                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerInviteSendStatusUpdate", parameters);
                
                return 1;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AppUserListData", "InviteEmailSendStatusUpdate");
                return -1;
            }
            finally
            {
            }
        }

        public DataSet FollowerExcellDataSelectByFileId(String InviteDataFileId, out Int32 ReturnValue)
    {
        ReturnValue = -1;
        DataSet myDataSet = null;

        SqlParameter[] parameters ={
                                            new SqlParameter("@InviteDataFileId", InviteDataFileId),                                           
                                            new SqlParameter("ReturnValue", SqlDbType.Int, 4, ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
        try
        {

            myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerExcellDataSelectByFileId", parameters);
            ReturnValue = Convert.ToInt32(parameters[1].Value);
            return myDataSet;
        }
        catch (Exception ex)
        {
            ReturnValue = -1;
            ApplicationLogger.LogError(ex, "AppUserListData", "FollowerExcellDataSelectByFileId");
            return null;
        }
        finally
        {
        }
    }
}
}
