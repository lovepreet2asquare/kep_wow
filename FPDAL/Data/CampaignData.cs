﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class CampaignData:ConnectionObject
    {
        public DataSet AddOrEdit(CampaignModel model, DataTable dt, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@OfficeId",model.OfficeId??string.Empty),
                                            new SqlParameter("@Title", model.Title),
                                            new SqlParameter("@AddressLine1", model.AddressLine1),
                                            new SqlParameter("@AddressLine2", model.AddressLine2),
                                            new SqlParameter("@ZIP", model.ZIP),
                                            new SqlParameter("@ISDCode", model.ISDCode),
                                            new SqlParameter("@PhoneNumber", model.Phone),
                                            new SqlParameter("@FaxNumber", model.Fax),
                                            new SqlParameter("@CountryId",model.CountryId),
                                            new SqlParameter("@StateId",model.StateId),
                                            new SqlParameter("@CityName",model.CityName),
                                            new SqlParameter("@CreateDate",model.CreateDate),
                                            new SqlParameter("@Status",model.Status),
                                            new SqlParameter("@OfficeTimeId",model.OfficeTimeId),
                                            new SqlParameter("@DayId",model.DayId),
                                            //new SqlParameter("@TimeFrom",model.TimeFrom??DateTime.Now.ToShortTimeString()),
                                            //new SqlParameter("@TimeTo",model.TimeTo??DateTime.Now.ToShortTimeString()),
                                             new SqlParameter("@FP_CampaignLocationDay",dt),
                                            new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
        
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_CampaignUpsert", parameters);
                ReturnResult = Convert.ToInt32(parameters[18].Value);
                return myDataSet;
               
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignData", "Create");
                ReturnResult = -1;
                return null;

            }

        }
        public DataSet Select(CampaignModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@UserId",model.UserId) ,
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("@OfficeId",model.OfficeId??string.Empty),
                                           new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_CampaignDetailById]", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignData", "Detail");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }
        public DataSet SelectAll(CampaignModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@UserId",model.UserId) ,
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("@search",model.Search??string.Empty),
                                           new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_CampaignDetail]", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignData", "SelectAll");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }
        public DataSet PublishNSendNotification(out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@CurrentDate",null),

                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_CampaignCronJob]", parameters);
                ReturnResult = 1;
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignData", "PublishNSendNotification");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }
        public int Delete(CampaignModel model)
        {
            int ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@OfficeId",model.OfficeId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_CampaignDelete", parameters);
                ReturnResult = Convert.ToInt32(obj);
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignData", "Delete");
                ReturnResult = -1;
                return ReturnResult;
            }
            finally

            {
            }
        }



    }
}
