﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class ContactUsData:ConnectionObject
    {
        public static int AddContactUs(EPContactUsRequest model)
        {
            int ReturnMessage = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@FollowerId",model.FollowerId),
                                            new SqlParameter("@FirstName",model.FirstName),
                                            new SqlParameter("@MI",model.MI ?? ""),
                                            new SqlParameter("@LastName",model.LastName),
                                            new SqlParameter("@Email",model.Email),
                                            new SqlParameter("@ISDCode",model.IsdCode),
                                            new SqlParameter("@Mobile",model.Mobile),
                                            new SqlParameter("@MessageContent",model.Message),
                                            new SqlParameter("@IsEmailREsponse",model.IsEmailRespnse)
                                            };

            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_ContactUsInsert", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ContactUsData", "AddContactUs");
                return ReturnMessage;
            }
        }

        public static DataSet ChangePassword(ChangePasswordApiModel model)
        {
            DataSet myDataSet = null;
            Int32 ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@SessionToken",model.EncryptedSessionToken),
                                            new SqlParameter("@FollowerId",model.EncryptedFollowerId),
                                            new SqlParameter("@PWD",model.Password)
                                            };

            try
            {
                myDataSet= SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerChangePassword", parameters);
                return myDataSet;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ContactUsData", "ChangePassword");
                 ReturnResult = -1;
                return null; ;
            }
        }
    }
}
