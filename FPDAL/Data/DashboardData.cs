﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FPDAL.Data
{

    public class DashboardData : ConnectionObject
    {
        public DataSet LoadData(DashboardSearch model, out Int32 returnResult)
        {
            returnResult =0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                                new SqlParameter("@UserId",model.UserId),
                                                new SqlParameter("@TokenKey",model.TokenKey),
                                                new SqlParameter("@Duration",model.Duration??"0"),
                                                new SqlParameter("@CallType",model.CallType??"Load"),
                                        };
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_UserDashboard", parameters);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "DashboardData", "LoadData");
            }

            return ds;
        }

    }
}
