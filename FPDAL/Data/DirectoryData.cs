﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class DirectoryData:ConnectionObject
    {
        public DataSet SelectAll(DirectoryModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken ),
                                            new SqlParameter("@Status",model.Status=="0"||string.IsNullOrEmpty(model.Status)?string.Empty:model.Status),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            new SqlParameter("@StartIndex",model.StartIndex??"1"),
                                            new SqlParameter("@EndIndex",model.EndIndex??"5"),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_DirectorySelectAll", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryData", "DirectorySelectAll");
                return myDataSet;
            }
            finally
            {
            }
        }

        public DataSet SelectAllForSendMail(DirectoryModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken ),
                                            new SqlParameter("@Status",model.Status=="0"||string.IsNullOrEmpty(model.Status)?string.Empty:model.Status),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_DirectorySelectAllMail", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryData", "DirectorySelectAll");
                return myDataSet;
            }
            finally
            {
            }
        }
        public DataSet LogSelectAll(DirectoryModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.EncryptedSessionToken ),
                                            new SqlParameter("@Status",model.Status=="0"||string.IsNullOrEmpty(model.Status)?string.Empty:model.Status),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_DirectoryLogSelectAll", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentData", "LogSelectAll");
                return myDataSet;
            }
            finally
            {
            }
        }

        public DataSet SelectById(DirectoryModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@loggeduserid",model.LoggedUserId) ,
                                            new SqlParameter("@UserId",model.UserId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_DirectorySelectById", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryData", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }
        public DataSet AddorEdit(DirectoryModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@FirstName",model.FirstName),
                                            new SqlParameter("@MI",model.MI??string.Empty),
                                            new SqlParameter("@LastName",model.LastName??string.Empty),
                                            new SqlParameter("@TitleId",model.TitleId),
                                            new SqlParameter("@Title",model.Title),
                                            new SqlParameter("@Email",model.Email),
                                            new SqlParameter("@ProfilePic",model.LogoPath),
                                            new SqlParameter("@ISDCode",model.ISDCode),
                                            new SqlParameter("@MobileNumber",model.Mobile),
                                            new SqlParameter("@UniqueId",model.UniqueId),
                                            new SqlParameter("@Local",model.Local),
                                            new SqlParameter("@AddressLine1",model.AddressLine1??string.Empty),
                                            new SqlParameter("@AddressLine2",model.AddressLine2??string.Empty),
                                            new SqlParameter("@City",model.CityName??string.Empty),
                                            new SqlParameter("@StateId",model.StateId),
                                            new SqlParameter("@District",model.District),
                                            new SqlParameter("@ZipCode",model.Zip),
                                            new SqlParameter("@County",model.County),
                                            new SqlParameter("@Telephone",model.Telephone),
                                            new SqlParameter("@TeleIsdCode",model.TeleISDCode),                                           
                                            new SqlParameter("@MemberTypeId",model.MemberTypeId),
                                            new SqlParameter("@ConDis",model.ConDis),
                                            new SqlParameter("@DOB",model.BirthDay),
                                            new SqlParameter("@DL",model.DL),
                                            new SqlParameter("@CreatedBy",model.LoggedUserId),
                                            new SqlParameter("@Status",model.StatusId),
                                            new SqlParameter("@TimeZoneOffSet",model.TimeOffSet),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_DirectoryUpsert", parameters);
                ReturnResult = Convert.ToInt32(parameters[29].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirevtoryData", "AddorEdit");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }
        public DataSet ExcellDataAdd(DirectoryModel model, string fileName, out Int32 ReturnValue)
        {
            ReturnValue = -1;
            DataSet myDataSet = null;

            SqlParameter[] parameters ={
                                            //new SqlParameter("@FilePath", model.FilePath),
                                            new SqlParameter("@NJ_ExcellDataTypeNew",model.dtImportExcel),
                                            new SqlParameter("@InsertOnly",model.InsertOnly == true ? 1 : 0),
                                            new SqlParameter("@UserId",model.LoggedUserId),
                                            new SqlParameter("@Filename",fileName),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_ExcellDataAddUpdate]", parameters);
                ReturnValue = Convert.ToInt32(parameters[2].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ReturnValue = -1;
                ApplicationLogger.LogError(ex, "DirectoryData", "ExcellDataAdd");
                return null;
            }
            finally
            {
            }
        }
        public DataSet ExcellDataAdd(InviteDataFileModel model, out Int32 ReturnValue)
        {
            ReturnValue = -1;
            DataSet myDataSet = null;

            SqlParameter[] parameters ={
                                            new SqlParameter("@InviteDataFileId", model.InviteDataFileId),
                                            new SqlParameter("@FilePath", model.FilePath),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@FollowerExcellDataType",model.dtInviteData),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerExcellDataAdd", parameters);
                ReturnValue = Convert.ToInt32(parameters[4].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ReturnValue = -1;
                ApplicationLogger.LogError(ex, "DirectoryData", "ExcellDataAdd");
                return null;
            }
            finally
            {
            }
        }
         public DataSet MultipleInvite(InviteModel model, out Int32 ReturnValue)
        {
            ReturnValue = -1;
            DataSet myDataSet = null;

            SqlParameter[] parameters ={
                                            new SqlParameter("@Email",model.Email),
                                            new SqlParameter("ReturnValue", SqlDbType.Int, 4, ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerExcellDataSelectByFileId", parameters);
                ReturnValue = Convert.ToInt32(parameters[1].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ReturnValue = -1;
                ApplicationLogger.LogError(ex, "DirectoryData", "MultipleInvite");
                return null;
            }
            finally
            {
            }
        }
        public Int32 InviteEmailSendStatusUpdate(int UserId, String Status)
        {

            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId", UserId),
                                            new SqlParameter("@Status", Status),
                                            new SqlParameter("ReturnValue", SqlDbType.Int, 4, ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {
                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_InviteStatusUpdate", parameters);

                return 1;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryData", "InviteEmailSendStatusUpdate");
                return -1;
            }
            finally
            {
            }
        }
        public int GetOfficerMaxID()
        {

            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_GetOfficerID]", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "DirectoryData", "Delete");
            }

            return returnResult;
        }
        public int Delete(DirectoryModel model)
        {

            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@LoggedUserId",model.LoggedUserId),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),

                                        };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_DirectoryDelete]", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "DirectoryData", "Delete");
            }

            return returnResult;
        }
    }
}
