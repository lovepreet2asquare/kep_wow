﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class DonationData : ConnectionObject
    {
        public DataSet DonationSelectAll(DonationModel model)
        {
            DataSet myDataSet = null;

            SqlParameter[] parameters ={
                                            new SqlParameter("@Field",model.Field??"") ,
                                            new SqlParameter("@UserId",model.UserId??"") ,
                                            new SqlParameter("@SessionToken",model.SessionToken??"") ,
                                            new SqlParameter("@Duration",model.Duration??"7"),
                                            new SqlParameter("@FromDate",model.FromDate??"") ,
                                            new SqlParameter("@ToDate",model.ToDate??"") ,
                                            new SqlParameter("@Status",model.Status??""),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null)
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_DonationSelectData", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationData", "DonationSelectAll");
                return null;
            }
            finally
            {
            }
        }

        public DataSet DonationGraphData(string duration)
        {
            DataSet myDataSet = null;

            SqlParameter[] parameters ={
                                           new SqlParameter("@duration",duration??"7") ,
                                           new SqlParameter("@DurationFromAmonut",duration??"7") ,
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null)
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_DonationGraphsData", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationData", "DonationGraphData");
                return null;
            }
            finally
            {
            }
        }

        public DataSet RecurringTypeSelectAll(RecurringTypeModel model)
        {
            DataSet myDataSet = null;

            SqlParameter[] parameters ={
                                            new SqlParameter("@Fields",model.Fields??"") ,
                                            new SqlParameter("@UserId",model.UserId??"") ,
                                            new SqlParameter("@SessionToken",model.SessionToken??"") ,
                                            //new SqlParameter("@Status",model.Status),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null)
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_RecurringTypeSelectAll", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationData", "RecurringTypeSelectAll");
                return null;
            }
            finally
            {
            }
        }

        public DataSet RecurringTypeById(String id)
        {
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@RecurringTypeId",id) ,
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_RecurringTypeById", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationData", "RecurringTypeById");
                return null;
            }
            finally
            {
            }
        }
        public DataSet RecurringTypeInsert(RecurringTypeModel model)
        {
            DataSet myDataSet = null;

            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken??"") ,
                                            new SqlParameter("@RecurringTypeId",model.RecurringTypeId) ,
                                            new SqlParameter("@RecurringType",model.RecurringType2),
                                            new SqlParameter("@RecurringDate",model.RecurringTypeDate??""),
                                            new SqlParameter("@Status",model.StatusId),
                                            new SqlParameter("@IsNoEndDate",model.NoEndDate),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null)
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_RecurringTypeInsert", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationData", "RecurringTypeInsert");
                return null;
            }
            finally
            {
            }
        }
        public int Delete(RecurringTypeModel model)
        {

            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@RecurringTypeId",model.RecurringTypeId),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),

                                        };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_RecurringTypeDelete]", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "RecurringTypeData", "Delete");
            }

            return returnResult;
        }

        public DataSet DetailSelectAll(DonationModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            //new SqlParameter("@Field",model.Field??"") ,
                                            new SqlParameter("@UserId",model.UserId??"") ,
                                            new SqlParameter("@SessionToken",model.SessionToken??"") ,
                                            new SqlParameter("@FollowerId",Convert.ToInt32(String.IsNullOrEmpty(model.FollowerId)?"0":model.FollowerId)),
                                            new SqlParameter("@PaymentId",Convert.ToInt32(model.PaymentId)),
                                            //new SqlParameter("@PaymentId",model.PaymentId),
                                            new SqlParameter("@FromDate",model.FromDate??"") ,
                                            new SqlParameter("@ToDate",model.ToDate??"") ,
                                            new SqlParameter("@Status",model.MultiStatus??""),
                                            new SqlParameter("@Search",model.Search??""),
                                            new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_DonorDetailSelectAll]", parameters);
                ReturnResult = Convert.ToInt32(parameters[8].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationData", "DonationSelectAll");
                return null;
            }
            finally
            {
            }
        }

        public Int32 DetailAddOrEdit(DonationModel model, out Int32 ReturnResult)
        {
            Int32 myInt = -1;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@PaymentMethodId",model.PaymentMethodId),
                                            new SqlParameter("@PaymentId", model.PaymentId),
                                            new SqlParameter("@IsSkip", model.IsSkip),
                                            new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };

            try
            {
                myInt = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_DonorDetailUpsert", parameters));

                return myInt;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationData", "addoredit");
                myInt = -1;
                return myInt;

            }

        }

        public Int32 DonorSkip(DonationModel model, out Int32 ReturnResult)
        {
            Int32 myInt = -1;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@PaymentId", model.PaymentId),
                                            new SqlParameter("@status", model.Status),
                                            new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };

            try
            {
                myInt = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_DonorSkip", parameters));
                ReturnResult = Convert.ToInt32(parameters[4].Value);
                return myInt;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationData", "DonorSkip");
                myInt = -1;
                return myInt;

            }
        }
        public Int32 AmountChange(DonationModel model, out Int32 ReturnResult)
        {
            Int32 myInt = -1;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@PaymentId", model.PaymentId),
                                            new SqlParameter("@Amount", model.Amount),
                                            new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };

            try
            {
                myInt = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_DonorAmountUpsert]", parameters));
                ReturnResult = Convert.ToInt32(parameters[4].Value);
                return myInt;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationData", "AmountChange");
                myInt = -1;
                return myInt;

            }
        }
        //public DataSet DonorRefund(DonationModel model, out Int32 ReturnResult)
        //{
        //    DataSet myDataSet = null;
        //    ReturnResult = 0;
        //    SqlParameter[] parameters ={
        //                                    new SqlParameter("@UserId",model.UserId),
        //                                    new SqlParameter("@SessionToken",model.SessionToken),
        //                                    new SqlParameter("@PaymentId", model.PaymentId),
        //                                    new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
        //                               };
        //    try
        //    {
        //        myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_DonorPaymentRefundSelect", parameters);
        //        ReturnResult = Convert.ToInt32(parameters[3].Value);
        //        return myDataSet;

        //    }
        //    catch (Exception ex)
        //    {
        //        ApplicationLogger.LogError(ex, "DonationData", "DonorPaymentRefundDetails");
        //        return null;

        //    }
        //}

        public Int32 DonorPaymentRefundUpdate(DonationModel model, string AuthTransactionId,out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",Convert.ToInt32(model.UserId)),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@PaymentId", model.PaymentId.ToString()),
                                            new SqlParameter("@refundTransactionId", AuthTransactionId),
                                            new SqlParameter("@RefundReasonAdd", model.RefundReasonAdd),
                                            new SqlParameter("@RefundStatus", model.FullRefund),
                                            new SqlParameter("@ReasonId", model.RefundReasonId),
                                            new SqlParameter("@RefundAmount", model.Amount1),
                                            new SqlParameter("@Comment",model.Comment),
                                            new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_DonorPaymentRefundUpdate", parameters);
                ReturnResult = Convert.ToInt32(parameters[9].Value);
                return ReturnResult;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationData", "DonorPaymentRefundDetails");
                return -1;

            }
        }
        public String DownloadFile(Int32 CertificateId)
        {
            Object myObject = "";
            SqlParameter[] parameters ={
                                        new SqlParameter("@CertificateId",CertificateId) ,
                                      };
            try
            {
                myObject = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.GS_CertificateFileById", parameters);
                return Convert.ToString(myObject);
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationData", "DownloadFile");
                return "";
            }
            finally
            {
            }
        }
        public DataSet IssueRefund(DonationModel model, out Int32 returnResult)
        {
            DataSet myDataSet = null;
            returnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",model.UserId??"") ,
                                            new SqlParameter("@SessionToken",model.SessionToken??"") ,
                                            new SqlParameter("@PaymentId",model.PaymentId),
                                            new SqlParameter("@ReturnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_IssueRefundSelectAll]", parameters);
                returnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationData", "IssueRefund");
                return myDataSet;
            }
            finally
            {
            }
        }
        public DataSet ViewRefund(DonationModel model, out Int32 returnResult)
        {
            DataSet myDataSet = null;
            returnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",model.UserId??"") ,
                                            new SqlParameter("@SessionToken",model.SessionToken??"") ,
                                            new SqlParameter("@PaymentId",model.PaymentId),
                                            //new SqlParameter("@PaymentMethodId",model.PaymentMethodId),
                                            new SqlParameter("@ReturnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_ViewRefundSelectAll]", parameters);
                returnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationData", "IssueRefund");
                return myDataSet;
            }
            finally
            {
            }
        }
    }
}
