﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class EventApiData: ConnectionObject
    {
        public static DataSet EventSelect(FPEventRequest model, Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                        new SqlParameter("@SessionToken",model.SessionToken) ,
                                         new SqlParameter("@FollowerId",model.FollowerId) ,
                                        new SqlParameter("@CurrentDate",model.RequestDate),
                                        new SqlParameter("@Latitude",model.Latitude),
                                        new SqlParameter("@Longitude",model.Longitude),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_EventSelect", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventApiData", "EventSelect");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
        public static DataSet AboutSelect(EpAboutRequest model, Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                        new SqlParameter("@SessionToken",model.SessionToken) ,
                                         new SqlParameter("@FollowerId",model.FollowerId)
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_AboutSelect", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventApiData", "EventSelect");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }

        public static DataSet OfficeLocationSelect(EpAboutRequest model, Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                        new SqlParameter("@SessionToken",model.SessionToken) ,
                                        new SqlParameter("@FollowerId",model.FollowerId)
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_OfficeLocationList", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventApiData", "EventSelect");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
    }
}
