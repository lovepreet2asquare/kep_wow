﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class FAQData : ConnectionObject
    {
        public DataSet SelectAll(FAQModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@UserId",model.UserId) ,
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("@Search",model.Search) ,
                                           new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_FaqSelectAll]", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "FAQData", "SelectAll");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }
        public void AddFAQ(FAQModel model, out Int32 ReturnResult)
        {
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@FaqId",model.FaqId),
                                           new SqlParameter("@Question",model.Title),
                                           new SqlParameter("@UserId",model.UserId) ,
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {
                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_FaqAdd]", parameters);
                ReturnResult = Convert.ToInt32(parameters[4].Value);

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "FaqData", "AddFaq");
                ReturnResult = -1;
            }
            finally
            {
            }
        }
        public void AddRes(FAQModel model, out Int32 ReturnResult)
        {
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@FaqId",model.FaqId),
                                           new SqlParameter("@Response",model.Response),
                                           new SqlParameter("@UserId",model.UserId) ,
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {
                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_FaqAddResponse]", parameters);
                ReturnResult = Convert.ToInt32(parameters[4].Value);

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "FaqData", "AddRes");
                ReturnResult = -1;
            }
            finally
            {
            }
        }

    }
}
