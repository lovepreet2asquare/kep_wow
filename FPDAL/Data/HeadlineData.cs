﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Infotech.ClassLibrary;
using FPModels.Models;
namespace FPDAL.Data
{
    public class HeadlineData : ConnectionObject
    {
        public DataSet SelectAll(HeadlineModel model)
        {
            Int32 myInt = 0;
            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken ),
                                            new SqlParameter("@IsView",model.isView??"1"),
                                            new SqlParameter("@UserId",model.SelectedUserId),
                                            new SqlParameter("@LoginUserId",model.UserId),
                                            new SqlParameter("@Status",model.Status=="0"||string.IsNullOrEmpty(model.Status)?string.Empty:model.Status),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_HeadlineSelectAll", parameters);
                myInt = Convert.ToInt32(parameters[6].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "HeadlineData", "HeadlineSelectAll");
                return ds;
            }
            finally
            {
            }
        }

        public DataSet SelectById(HeadlineModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@HeadlineId",model.HeadLineId) ,
                                            new SqlParameter("@UserId",model.UserId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_HeadlineSelectById", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "HeadlineData", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }
        public DataSet DetailById(HeadlineModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@HeadlineId",model.HeadLineId) ,
                                            new SqlParameter("@UserId",model.UserId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken??""),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_HeadlineDetailById", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "HeadlineData", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }
        public DataSet AddorEdit(HeadlineModel model, DataTable dt, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@HeadlineId",model.HeadLineId),
                                            new SqlParameter("@Title",model.Title??string.Empty),
                                            new SqlParameter("@Content",model.Content??string.Empty),
                                            new SqlParameter("@NJ_HeadlineImageType",dt),
                                            new SqlParameter("@Status",model.Status),
                                            new SqlParameter("@PublishDate",model.PublishDate),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@Timezone",model.TimeZone),
                                            new SqlParameter("@County",model.County),
                                            new SqlParameter("@Location",model.Location),
                                            new SqlParameter("@RejectReason",model.RejectReason),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_HeadlineUpsert", parameters);
                ReturnResult = Convert.ToInt32(parameters[12].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "HeadlineData", "AddorEdit");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }

        public int Archive(HeadlineModel model)
        {
            
            int ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@HeadlineId",model.HeadLineId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_HeadlineArchive", parameters);
                ReturnResult = Convert.ToInt32(obj);
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "HeadlineData", "Archive");
                ReturnResult = -1;
                return ReturnResult;
            }
            finally

            {
            }
        }

 
        public int Delete(HeadlineModel model)
        {
            int ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@HeadlineId",model.HeadLineId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_HeadlineDelete]", parameters);
                ReturnResult = Convert.ToInt32(obj);
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "HeadlineData", "Delete");
                ReturnResult = -1;
                return ReturnResult;
            }
            finally

            {
            }
        }

        public DataSet PublishNSendNotification(out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@CurrentDate",null),

                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_HeadlineCronJob", parameters);
                ReturnResult = 1;
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "HeadlineData", "PublishNSendNotification");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }
    }
}
