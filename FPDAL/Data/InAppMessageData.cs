﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class InAppMessageData: ConnectionObject
    {
        public DataSet AddOrEdit(InAppMessageModel model, out Int32 ReturnResult)
        {

            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@MessageId",model.MessageId),
                                            new SqlParameter("@Title", model.Title),
                                            new SqlParameter("@Content",model.Content),
                                            new SqlParameter("@Status",model.Status),
                                            new SqlParameter("@publishDate",model.PublishDate),
                                            new SqlParameter("@Timezone",model.TimeZone),
                                            new SqlParameter("@SelectedIndividualId",model.SelectedIndividualId),
                                            new SqlParameter("@SelectedCountyId",model.SelectedCountyId??string.Empty),
                                            new SqlParameter("@SelectedLocalId",model.SelectedLocalId??string.Empty),
                                            new SqlParameter("@AudienceCount",model.ApproxAudience??0),
                                            new SqlParameter("@AudienceType",model.SelectAudience),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_InAppMessageUpsert", parameters);
                ReturnResult = Convert.ToInt32(parameters[13].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InAppMessageData", "Create");
                ReturnResult = -1;
                return null;

            }

        }
       
        public DataSet SelectAll(InAppMessageModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@UserId",model.UserId) ,
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("@FromDate",model.DateFrom),
                                           new SqlParameter("@ToDate",model.DateTo),
                                           new SqlParameter("@IsView",model.isview??"1"),
                                           new SqlParameter("@Status",model.SearchStatus??"-1"),
                                           new SqlParameter("@Search",model.Search??string.Empty),
                                           new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_InAppMessageDetail", parameters);
                ReturnResult = Convert.ToInt32(parameters[7].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InAppMessageData", "Detail");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }

        public DataSet Select(InAppMessageModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("@MessageId",model.MessageId??"0"),
                                           new SqlParameter("@UserId",model.UserId),
                                           new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_InAppMessageDetailById", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InAppMessageData", "Detail");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }
        public DataSet PublishNSendNotification(out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@CurrentDate",null),

                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_InAppMessageCronJob]", parameters);
                ReturnResult = 1;
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InAppMessageData", "PublishNSendNotification");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }

        //public int Delete(InAppMessageModel model, out Int32 ReturnValue)
        //{

        //    ReturnValue = -1;
        //    Object myObject = -1;
        //    SqlParameter[] parameters ={
        //                                    new SqlParameter("@UserId",model.UserId),
        //                                    new SqlParameter("@SessionToken",model.SessionToken),
        //                                    new SqlParameter("@MessageId",model.MessageId),
        //                                    new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
        //                              };
        //    try
        //    {
        //        myObject = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_InAppMessageDelete", parameters);
        //        ReturnValue = Convert.ToInt32(parameters[3].Value);
        //        return Convert.ToInt32(myObject);
        //    }
        //    catch (Exception ex)
        //    {
        //        ReturnValue = -1;
        //        ApplicationLogger.LogError(ex, "InappmessageData", "Delete");
        //        return -1;

        //    }

        //}
        public int Delete(InAppMessageModel model)
        {
            int ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@MessageId",model.MessageId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_InAppMessageDelete]", parameters);
                ReturnResult = Convert.ToInt32(obj);
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InAppMessageData", "Delete");
                ReturnResult = -1;
                return ReturnResult;
            }
            finally

            {
            }
        }
        public int Archive(InAppMessageModel model)
        {

            int ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@MessageId",model.MessageId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_InAppMessageArchive]", parameters);
                ReturnResult = Convert.ToInt32(obj);
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InAppMessageData", "Archive");
                ReturnResult = -1;
                return ReturnResult;
            }
            finally

            {
            }
        }

    }
}
