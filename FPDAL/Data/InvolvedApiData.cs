﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
   public class InvolvedApiData: ConnectionObject
    {
        public static int AddInvolved(GetInvolvedRequest model)
        {
            int ReturnMessage = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@FollowerId",model.FollowerId),
                                            new SqlParameter("@FirstName",model.FirstName),
                                            new SqlParameter("@MI",model.MI),
                                            new SqlParameter("@LastName",model.LastName),
                                            new SqlParameter("@Email",model.Email),
                                            new SqlParameter("@ISDCode",model.ISDCode),
                                            new SqlParameter("@Mobile",model.Mobile),
                                            new SqlParameter("@Countryid",model.Countryid),
                                            new SqlParameter("@AddressLine1",model.AddressLine1),
                                            new SqlParameter("@AddressLine2",model.AddressLine2),
                                            new SqlParameter("@CityName",model.CityName),
                                            new SqlParameter("@Stateid",model.Stateid),
                                            new SqlParameter("@ZipCode",model.ZipCode),
                                            };

            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerInvolveInsert", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InvolvedApiData", "AddInvolved");
                return ReturnMessage;
            }
        }

        public static DataSet GetInvolveByEmail(FP_InvolvedApiRequest request,  int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@FollowerId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                            new SqlParameter("@Email",request.Email)
                                        };
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerApiInvolveSelectByEmail", parameters);
                returnResult = 1;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "InvolvedApiData", "AddInvolved");
            }

            return ds;
        }
    }
}
