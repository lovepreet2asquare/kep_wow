﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
   public class InvolvementData: ConnectionObject
    {
        public DataSet InvolvementSelectAll(InvolvementModel model)
        {
            DataSet myDataSet = null;

            SqlParameter[] parameters ={
                                            new SqlParameter("@Field",model.Field??"") ,
                                            new SqlParameter("@UserId",model.UserId??"") ,
                                            new SqlParameter("@SessionToken",model.SessionToken??""),
                                            new SqlParameter("@Duration",model.Duration??"7"),
                                            new SqlParameter("@FromDate",model.FromDate??"") ,
                                            new SqlParameter("@ToDate",model.Todate??"") ,
                                            new SqlParameter("@Status",model.Status),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null)
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_InvolvementGraphsDataSelect", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InvolvementData", "InvolvementSelectAll");
                return null;
            }
            finally
            {
            }
        }

        public DataSet InvolvementGraphsData(InvolvementModel model)
        {
            DataSet myDataSet = null;

            SqlParameter[] parameters ={
                                           new SqlParameter("@Field",model.Field??"") ,
                                            new SqlParameter("@UserId",model.UserId??"") ,
                                            new SqlParameter("@SessionToken",model.SessionToken??"") ,
                                            new SqlParameter("@Duration",model.Duration??"7"),
                                            new SqlParameter("@FromDate",model.FromDate??"") ,
                                            new SqlParameter("@ToDate",model.Todate??"") ,
                                            new SqlParameter("@Status",model.Status??""),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null)
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_InvolvementGraphsDataSelect", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InvolvementData", "InvolvementGraphsData");
                return null;
            }
            finally
            {
            }
        }

        public DataSet GraphData(string duration)
        {
            DataSet myDataSet = null;

            SqlParameter[] parameters ={
                                           new SqlParameter("@duration",duration??"7") ,
                                           new SqlParameter("@DurationFromAmonut",duration??"7") ,
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null)
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_DashboardInvolvementOverview", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InvolvementData", "GraphData");
                return null;
            }
            finally
            {
            }
        }
    }
}
