﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
  public class NewsAPIData: ConnectionObject
    {
        public static DataSet FollowerNewsList(FollowerNewsRequest model, Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                       
                                        new SqlParameter("@SessionToken",model.SessionToken) ,
                                         new SqlParameter("@FollowerId",model.FollowerId) ,
                                        new SqlParameter("@StartIndex",model.StartIndex) ,
                                        new SqlParameter("@EndIndex",model.EndIndex),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerNewsList", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsAPIData", "FollowerNewsList");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
        public static DataSet FollowerNewsDetail(FolloerNewsDetailRequest request, Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                        new SqlParameter("@SessionToken",request.SessionToken) ,
                                         new SqlParameter("@FollowerId",request.FollowerId) ,
                                          new SqlParameter("@NewsId",request.NewsId) ,
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerNewsDetail", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsAPIData", "FollowerNewsDetail");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }
    }
}

