﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class NotificationData:ConnectionObject
    {
        //public DataSet SelectAll(NotificationModel model, out Int32 ReturnResult)
        //{
        //    ReturnResult = -1;
        //    DataSet myDataSet = null;
        //    SqlParameter[] parameters ={
        //                                    new SqlParameter("@UserId",model.UserId) ,
        //                                    new SqlParameter("@SessionToken",model.SessionToken) ,
        //                                    new SqlParameter("@CurrentDate",DateTime.Now) ,
        //                                    new SqlParameter("@deviceType",model.DeviceType) ,
        //                                    new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
        //                                };
        //    try
        //    {
        //        myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_NotificationSelect]", parameters);
        //        ReturnResult = Convert.ToInt32(parameters[4].Value);
        //        return myDataSet;
        //    }
        //    catch (Exception ex)
        //    {
        //        ApplicationLogger.LogError(ex, "NotificationData", "SelectAll");
        //        return null;
        //    }
        //    finally
        //    {
        //    }
        //}
        public static DataSet SelectAll(string UserId, string SessionToken)
        {
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@EncryptedUserId",Convert.ToInt32(UserId)),
                                            new SqlParameter("@SessionToken",SessionToken),
                                            new SqlParameter("ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_NotificationSelect", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NotificationData", "SelectAll");
                return null;
            }
            finally
            {
            }
        }
        public Int32 NotificationRead(NotificationModel model)
        {
            Int32 returnResult = 0;
            //model.StationId = 6;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken) ,
                                            new SqlParameter("@NotificationIds",model.NotificationIds) ,
                                            new SqlParameter("@IsView",model.IsView)
                                        };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_NotificationRead]", parameters);
                if (obj != null)
                    returnResult = Convert.ToInt32(obj);
                return returnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NotificationData", "NotificationRead");
                return -1;
            }
            finally
            {
            }
        }
    }
}
