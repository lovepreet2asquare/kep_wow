﻿using System;
using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace FPDAL.Data
{
    public class PaymentMethodData : ConnectionObject
    {
        public static int CustomerProfileInsert(CustProfileInfo request)
        {
            string cardNumber = request.CardNumber.Replace("X", "");
            int ReturnMessage = 0;
            SqlParameter[] parameters ={
                
                new SqlParameter("@FollowerId",request.FollowerId),
                new SqlParameter("@SessionToken",request.SessionToken),
                new SqlParameter("@CustomerProfileId",request.CustomerProfileId),
                new SqlParameter("@CustomerPaymentProfileId",request.CustomerPaymentProfileId),
                new SqlParameter("@ExpMonth",request.ExpiryMonth),
                new SqlParameter("@ExpYear",request.ExpiryYear),
                new SqlParameter("@CardNumber",cardNumber),
                new SqlParameter("@CardType",request.CardType),
                new SqlParameter("@CountryId",request.CountryId),
                new SqlParameter("@StateId",request.StateId),
                new SqlParameter("@Email",request.Email),
                new SqlParameter("@CityName",request.City),
                new SqlParameter("@AddressLine1",request.Address1),
                new SqlParameter("@AddressLine2",request.Address2),
                new SqlParameter("@Zipcode",request.Zipcode),
                new SqlParameter("@CardHolderName",request.CardHolderName),
                new SqlParameter("@IsPrimary",0)
                };

            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_PaymentMethodInsert", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PaymentMethodData", "CustomerProfileInsert");
                return ReturnMessage;
            }
        }

        public static DataSet GetPaymentMethods(FPRequests request, out int returnResult)
        {
            DataSet myDataSet = null;
            returnResult = 0;
            SqlParameter[] parameters ={

                new SqlParameter("@FollowerId",request.FollowerId),
                new SqlParameter("@SessionToken",request.SessionToken)
                
                };

            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_PaymentMethodSelect", parameters);
                return myDataSet;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PaymentMethodData", "GetPaymentMethods");
                returnResult = -1;
                return myDataSet;
            }
        }
        public static int PaymentMethodDelete(DeletePaymentMethodRequest request)
        {
            int ReturnMessage = 0;
            SqlParameter[] parameters ={

                new SqlParameter("@FollowerId",request.FollowerId),
                new SqlParameter("@SessionToken",request.SessionToken),
                new SqlParameter("@PaymentMethodId",request.PaymentMethodId)
                };

            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_PaymentMethodDelete", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PaymentMethodData", "CustomerProfileDelete");
                return ReturnMessage;
            }
        }

        public static DataSet DonateTransactionInsert(DonateAPIModel request)
        {
            //int ReturnMessage = 0;
            DataSet myDataSet = null;
            SqlParameter[] parameters ={

                new SqlParameter("@FollowerId",request.FollowerId),
                new SqlParameter("@SessionToken",request.SessionToken),
                new SqlParameter("@PaymentMethodId",request.PaymentMethodId),
                new SqlParameter("@TransactionId",request.TransactionId),
                new SqlParameter("@Amount",request.Amount),
                new SqlParameter("@DonationTypeId",request.DonationTypeId),
                new SqlParameter("@RecurringTypeId",request.RecurringTypeId),
                new SqlParameter("@ErrorCode",request.ErrorCode),
                new SqlParameter("@ErrorText",request.ErrorText),
                new SqlParameter("@IsRecurring",request.IsRecurring),
                new SqlParameter("@Accupation",request.Accupation),
                new SqlParameter("@Employer",request.Employer),
                new SqlParameter("@IsHostingEvent",request.IsHostingEvent),
                new SqlParameter("@IsVolunteer",request.IsVolunteer),
                new SqlParameter("@TransactionDate",request.CurrentDateTime??null),
                new SqlParameter("@SpouseName",request.SpouseName??"")
                
                };

            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_PaymentTransactionInsert", parameters);
                
                return myDataSet;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PaymentMethodData", "DonateTransactionInsert");
                return myDataSet;
            }
        }

        public static DataSet GetRecurringPayments(out int returnResult)
        {
            DataSet myDataSet = null;
            returnResult = 0;
            SqlParameter[] parameters ={

                new SqlParameter("@FollowerId",1)
                //new SqlParameter("@SessionToken",request.SessionToken)

                };

            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_PaymentTransactionCronJob", parameters);
                returnResult = 1;
                return myDataSet;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PaymentMethodData", "GetRecurringPayments");
                returnResult = -1;
                return myDataSet;
            }
        }

        public static DataSet RecurringPaymentTransactionUpdate(DataTable dt)
        {
            //int ReturnMessage = 0;
            DataSet myDataSet = null;
            SqlParameter[] parameters ={

                new SqlParameter("@FP_PaymentTransType",dt)
                };

            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_PaymentTransactionUpdate", parameters);

                return myDataSet;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PaymentMethodData", "RecurringPaymentTransactionUpdate");
                return myDataSet;
            }
        }

        public static DataSet GetFailedPayments(int FollowerId, int PaymentId, int PaymentMethodId, out int returnResult)
        {
            DataSet myDataSet = null;
            returnResult = 0;
            SqlParameter[] parameters ={

                new SqlParameter("@FollowerId",FollowerId),
                new SqlParameter("@PaymentId",PaymentId),
                new SqlParameter("@PaymentMethodId",PaymentMethodId)

                };

            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FailedPaymentTransactionCronJob", parameters);
                returnResult = 1;
                return myDataSet;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PaymentMethodData", "GetFailedPayments");
                returnResult = -1;
                return myDataSet;
            }
        }
        public static DataSet RecurringFailedPaymentTransactionUpdate(FailTransactionCronjobModel request, out int returnResult)
        {
            DataSet myDataSet = null;
            returnResult = 0;
            SqlParameter[] parameters ={

                new SqlParameter("@FollowerId",request.FollowerId),
                new SqlParameter("@PaymentId",request.PaymentId),
                new SqlParameter("@PaymentMethodId",request.PaymentMethodId),
                new SqlParameter("@TransactionId",request.TransactionId),
                new SqlParameter("@Status",request.Status),
                new SqlParameter("@ErrorCode",request.ErrorCode),
                new SqlParameter("@ErrorText",request.ErrorText)

                };

            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_PaymentTransactionSingleUpdate", parameters);
                returnResult = 1;
                return myDataSet;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PaymentMethodData", "GetRecurringPayments");
                returnResult = -1;
                return myDataSet;
            }
        }

        public static void GetAuthorizeKeys(out string ApiLogin, out string TransactionKey)
        {
            ApiLogin = string.Empty;
            TransactionKey = string.Empty;
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                new SqlParameter("@Type",0),
                };

            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_GetAPIKeys", parameters);
                if(myDataSet != null && myDataSet.Tables.Count>0 && myDataSet.Tables[0].Rows.Count > 0)
                {
                    ApiLogin = StaticEncryption.Security.Decrypt(Convert.ToString(myDataSet.Tables[0].Rows[0]["API_Key"]));
                    TransactionKey = StaticEncryption.Security.Decrypt(Convert.ToString(myDataSet.Tables[0].Rows[0]["API_Transaction"]));
                }
               // return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PaymentMethodData", "GetAuthorizeKeys");
                //return myDataSet;
            }
        }
        public static int PaymentMethodSetPrimary(SetPrimaryRequest request)
        {
            int ReturnMessage = 0;
            SqlParameter[] parameters ={

                new SqlParameter("@FollowerId",request.FollowerId),
                new SqlParameter("@SessionToken",request.SessionToken),
                new SqlParameter("@PaymentMethodId",request.PaymentMethodId)
                };

            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_PaymentMethodSetPrimary", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PaymentMethodData", "CustomerProfileDelete");
                return ReturnMessage;
            }
        }
        public static int PaymentTransactionStatusUpdate(string PaymentIds)
        {
            int ReturnMessage = 0;
            SqlParameter[] parameters ={

                new SqlParameter("@PaymentIds",PaymentIds)
                };

            try
            {
                object obj = SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.FP_PaymentTransactionStatusUpdate", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PaymentMethodData", "PaymentTransactionStatusUpdate");
                return ReturnMessage;
            }
        }
        
        public static DataSet GetTransactions(out int returnResult)
        {
            DataSet myDataSet = null;
            returnResult = 0;
            SqlParameter[] parameters ={

                new SqlParameter("@Status","Paid")
                //new SqlParameter("@SessionToken",request.SessionToken)

                };

            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_GetPaymentTransactions", parameters);
                returnResult = 1;
                return myDataSet;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PaymentMethodData", "GetTransactions");
                returnResult = -1;
                return myDataSet;
            }
        }

    }
}
