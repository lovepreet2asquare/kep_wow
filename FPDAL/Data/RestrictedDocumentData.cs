﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using Infotech.ClassLibrary;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class RestrictedDocumentData:ConnectionObject
    {
        public DataSet Add(RestrictedDocumentModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.EncryptedSessionToken),
                                            new SqlParameter("@DocumentPath",model.RDocumentPath),
                                            new SqlParameter("@DocumentName",model.RDocumentName),
                                            new SqlParameter("@ModifiedBy",model.ModifiedBy),
                                            new SqlParameter("@ModifiedByDate",model.ModifiedByDate),
                                            new SqlParameter("@ThumbnailPath",model.ThumbnailPath),
                                            new SqlParameter("@DocumentId",model.RDocumentId),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_RestrictedDocumentUpsert", parameters);
                ReturnResult = Convert.ToInt32(parameters[8].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "RestriectedDocumentData", "Add");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }
        public DataSet SelectAll(RestrictedDocumentModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.EncryptedSessionToken ),
                                            new SqlParameter("@Status",model.Status=="0"||string.IsNullOrEmpty(model.Status)?string.Empty:model.Status),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_RestrictedDocumentSelectAll", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "RestriectedDocumentData", "DocumentModel SelectAll");
                return myDataSet;
            }
            finally
            {
            }
        }
        public DataSet LogSelectAll(RestrictedDocumentModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.EncryptedSessionToken ),
                                            new SqlParameter("@Status",model.Status=="0"||string.IsNullOrEmpty(model.Status)?string.Empty:model.Status),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_RestrictedDocLogSelectAll]", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "RestriectedDocumentData", "DocumentModel SelectAll");
                return myDataSet;
            }
            finally
            {
            }
        }
        public int Delete(RestrictedDocumentModel model)
        {
            int ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@DocumentId",model.RDocumentId) ,
                                            new SqlParameter("@SessionToken",model.EncryptedSessionToken),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_RestrictedDocumentDelete]", parameters);
                ReturnResult = Convert.ToInt32(obj);
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "RestriectedDocumentData", "Delete");
                ReturnResult = -1;
                return ReturnResult;
            }
            finally

            {
            }
        }
    }
}
