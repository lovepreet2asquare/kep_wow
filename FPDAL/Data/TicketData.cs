﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class TicketData : ConnectionObject
    {
        public DataSet SelectAll(TicketModel model, out Int32 returnResult)
        {
            returnResult = -1;
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@Field",model.Field??"") ,
                                            new SqlParameter("@TicketReasonId",model.TicketReasonId??0),
                                            new SqlParameter("@AssignedTo",model.AssignedTo??0),
                                            new SqlParameter("@TaggedTo",model.TaggedTo??0),
                                            new SqlParameter("@Priority",model.Priority??"") ,
                                            new SqlParameter("@Status",model.Status??"-1") ,
                                            new SqlParameter("@DateFrom",model.DateFrom) ,
                                            new SqlParameter("@DateTo",model.DateTo) ,
                                            new SqlParameter("@CreatedBy",model.CreatedBy??0) ,
                                            new SqlParameter("@LoggedUserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new System.Data.SqlClient.SqlParameter("@ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Output, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_TicketSelectAll", parameters);
                returnResult = Convert.ToInt32(parameters[11].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "TicketData", "SelectAll");
                return null;
            }
            finally
            {
            }
        }

        public DataSet UnassignSelectAll(TicketModel model, out Int32 returnResult)
        {
            returnResult = -1;
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@Field",model.Field??"") ,
                                            new SqlParameter("@TicketReasonId",model.TicketReasonId??0),
                                            new SqlParameter("@Priority",model.Priority??"") ,
                                            new SqlParameter("@Status",model.Status??"-1") ,
                                            new SqlParameter("@DateFrom",model.DateFrom) ,
                                            new SqlParameter("@DateTo",model.DateTo) ,
                                            new SqlParameter("@LoggedUserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new System.Data.SqlClient.SqlParameter("@ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Output, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.HP_TicketUnassignSelectAll", parameters);
                returnResult = Convert.ToInt32(parameters[8].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "TicketData", "SelectAll");
                return null;
            }
            finally
            {
            }
        }
        public DataSet TicketSearch(TicketModel model)
        {
            if (model.DateTo != null)
            {
                model.DateTo = model.DateTo + " 23:59:59";
            }

            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@Field",model.Field??"") ,
                                            new SqlParameter("@ProfileId",-1),
                                            new SqlParameter("@TicketReasonId",model.TicketReasonId??-1),
                                            new SqlParameter("@AssignedTo",model.AssignedTo??-1),
                                            new SqlParameter("@TaggedTo",model.TaggedTo??-1),
                                            new SqlParameter("@Priority",model.Priority??"-1") ,
                                            new SqlParameter("@Status",model.Status??"-1") ,
                                            new SqlParameter("@DateFrom",model.DateFrom) ,
                                            new SqlParameter("@DateTo",model.DateTo) ,
                                            new SqlParameter("@CreatedBy",model.CreatedBy??0) ,
                                            new SqlParameter("@LoggedUserId",1),
                                           // new SqlParameter("@UtcMinutes",EmployeeAccess.UtcMinutes()),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.HP_TicketsSelect", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketData", "SelectAll");
                return null;
            }
            finally
            {
            }
        }
        public DataSet SelectById(TicketModel model, out Int32 returnResult)
        {
            returnResult = -1;
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@TicketId",model.TicketId ?? 0) ,
                                            new SqlParameter("@LoggedUserId",model.UserId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                           // new SqlParameter("@UtcMinutes",EmployeeAccess.UtcMinutes()),
                                            new System.Data.SqlClient.SqlParameter("@ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Output, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_TicketSelectById]", parameters);
                returnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "TicketsData", "SelectById");
                return null;
            }
            finally
            {
            }
        }
        public void AssignTicket(TicketModel model, out Int32 returnResult)
        {
            returnResult = -1;
            SqlParameter[] parameters ={
                                            new SqlParameter("@TicketId",model.TicketId ?? 0) ,
                                            new SqlParameter("@AssignTo",model.AssignedTo ?? 0) ,
                                            new SqlParameter("@LoggedUserId",model.UserId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new System.Data.SqlClient.SqlParameter("@ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Output, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {
                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.HP_TicketAssign", parameters);
                returnResult = Convert.ToInt32(parameters[4].Value);

            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "TicketsData", "AssignTicket");
            }
            finally
            {
            }
        }
        public DataSet AddOrEdit(TicketModel model, DataTable dtImages, out Int32 returnResult)
        {
            returnResult = -1;
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
          
                                            new SqlParameter("@TicketId",model.TicketId??0),
                                            new SqlParameter("@TicketNumber",model.TicketNumber??0),
                                            new SqlParameter("@TicketReasonId",model.TicketReasonId),
                                            new SqlParameter("@Priority",model.Priority),
                                            new SqlParameter("@Subject",model.Subject),
                                            new SqlParameter("@Status",model.Status),
                                            new SqlParameter("@AssignedTo",model.AssignedTo),
                                            new SqlParameter("@TaggedTo",model.TaggedTo),
                                            new SqlParameter("@FollowUpDate",model.FollowUpDate),
                                            new SqlParameter("@FollowUpText",model.FollowUpText),
                                            new SqlParameter("@Description",model.Description),
                                            new SqlParameter("@TaggedToIDs",model.TaggedToIDs),
                                            new SqlParameter("@ContactName",model.ContactName),
                                            new SqlParameter("@ContactNumber",model.ContactNumber),
                                            new SqlParameter("@ISDCode",model.ISDCode),
                                            new SqlParameter("@ReasonName",model.ReasonName),
                                            new SqlParameter("@LoggedUserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@NJ_ImageType",dtImages),
                                            new System.Data.SqlClient.SqlParameter("@ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Output, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_TicketUpsert]", parameters);
                returnResult = Convert.ToInt32(parameters[19].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "TicketData", "AddOrEdit");
                return null;
            }
            finally
            {
            }
        }
        public DataSet ValidateReasonName(String ReasonName)
        {
            //object myInt = 0;
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@ReasonName",ReasonName),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.HP_ValidateTicketReasons", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketData", "ValidateReasonName");
                return null;
            }
            finally
            {
            }
        }

        public DataSet UserListByTitle(Int32 TitleId)
        {
            //object myInt = 0;
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@TitleId",TitleId),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.HP_UserListByTitle", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketData", "UserListByTitle");
                return null;
            }
            finally
            {
            }
        }
        public DataSet CommunityList(string search)
        {
            //object myInt = 0;
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@searchText",search),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.HP_CommunityList", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketData", "CommunityList");
                return null;
            }
            finally
            {
            }
        }


       

        #region Community_Detail_TicketId
        public DataSet CommunityDetailByTicketId(Int64? TicketId)
        {
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@TicketId",TicketId),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.HP_CommunityDetailByTicketId", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketData", "CommunityDetailByTicketId");
                return null;
            }
            finally
            {
            }
        }
        #endregion

        #region community_ticketReport
        public DataSet Search(TicketModel model)
        {
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@Field",model.Field??"") ,
                                            new SqlParameter("@CommunityId",model.CommunityId??0),
                                            new SqlParameter("@TicketReasonId",model.TicketReasonId??0),
                                            new SqlParameter("@AssignedTo",model.AssignedTo??0),
                                            new SqlParameter("@TaggedTo",model.TaggedTo??0),
                                            new SqlParameter("@Priority",model.Priority??"-1") ,
                                            new SqlParameter("@Status",model.Status??"-1") ,
                                            new SqlParameter("@DateFrom",model.DateFrom) ,
                                            new SqlParameter("@DateTo",model.DateTo) ,
                                            new SqlParameter("@LoggedUserId",1),
                                           // new SqlParameter("@UtcMinutes",EmployeeAccess.UtcMinutes()),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.HP_TicketsSearch", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "TicketData", "Search");
                return null;
            }
            finally
            {
            }
        }

        #endregion kitchen_ticketreport
    }
}

