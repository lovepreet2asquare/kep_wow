﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FPDAL.Data
{

    public class UserData : ConnectionObject
    {
        public DataSet Login(LoginModel request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@Email",request.Email),
                                            //new SqlParameter("@Timezone",request.Timezone??0),
                                        };
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_WebUserLogin", parameters);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "UserAccessData", "Login");
            }

            return ds;
        }
        public int CreateAndSaveToken(LoginModel request, int userId, string sessionToken)
        {

            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",userId),
                                            new SqlParameter("@SessionToken",sessionToken),
                                            new SqlParameter("@DeviceToken",request.DeviceToken ?? ""),
                                            new SqlParameter("@DeviceType",request.DeviceType),
                                            //new SqlParameter("@CustomerId",request.CustomerId)
                };
                object obj = SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_UserTokenInsert]", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "UserData", "CreateAndSaveToken");
            }

            return returnResult;
        }
        public DataSet Signup(SignupModel model, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@Email",model.Email),
                                            new SqlParameter("@MobileNumber",model.MobileNumber),
                                            new SqlParameter("@Password",model.Password),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Output, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_UserSignup", parameters);
                returnResult = Convert.ToInt32(parameters[3].Value);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "UserData", "Signup");
            }

            return ds;
        }

        public static DataTable ForgotPassword(string EmailId, out int returnResult)
        {
            returnResult = 0;
            // int userId = 0;
            string firstName = string.Empty;
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@Email",EmailId)
                                           };
                DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.GS_UserForgotPassword", parameters);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    dt = ds.Tables[0];
                    returnResult = 1;
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "UserData", "ForgotPassword");
            }

            return dt;
        }
        public DataTable IsEmailExist(string email)
        {
            DataTable myDataTable = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@Email",email),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {

                myDataTable = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "NJ_UserIsEmailExist", parameters).Tables[0];
                return myDataTable;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "USerData", "IsEmailExist");
                return null;
            }
            finally
            {
            }
        }

        public static Int32 ResetPassword(ResetPasswordModel model)
        {
            Int32 myInt = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@Password", model.Password),
                                            new System.Data.SqlClient.SqlParameter("@ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Output, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {

                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_UserResetPassword", parameters);
                myInt = Convert.ToInt32(parameters[2].Value);
                return myInt;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserData", "ResetPassword");
                return -1;
            }
            finally
            {
            }
        }
        public static DataSet UserSelectAll(UserModel model)
        {
            Int32 myInt = 0;
            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            new SqlParameter("@sessionToken", model.SessionToken ),
                                            new SqlParameter("@search",!String.IsNullOrEmpty(model.Search)?model.Search:string.Empty),
                                            new SqlParameter("@TitleSearch",!String.IsNullOrEmpty(model.TitleSearch)?model.TitleSearch:string.Empty),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_UserSelectAll", parameters);
                //myInt = Convert.ToInt32(parameters[2].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserData", "UserSelectAll");
                return ds;
            }
            finally
            {
            }
        }

        public static DataSet UserSelectNewAll(UserModel model)
        {
            Int32 myInt = 0;
            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            new SqlParameter("@sessionToken", model.SessionToken ),
                                            new SqlParameter("@search",!String.IsNullOrEmpty(model.Search)?model.Search:string.Empty),
                                            new SqlParameter("@TitleSearch",!String.IsNullOrEmpty(model.TitleSearch)?model.TitleSearch:string.Empty),
                                            new SqlParameter("@Status","Active"),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_UserSelectAll", parameters);
                //myInt = Convert.ToInt32(parameters[2].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserData", "UserSelectAll");
                return ds;
            }
            finally
            {
            }
        }

        public static DataSet UserVerificationSelect(UserModel model)
        {
            Int32 myInt = 0;
            DataSet ds = new DataSet();
            object res;
            SqlParameter[] parameters = {
                                          new SqlParameter("@sessionToken", model.SessionToken ),
                                          new SqlParameter("@UserID",!String.IsNullOrEmpty(model.EncryptedUserId)?model.EncryptedUserId:string.Empty),
                                          new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                 ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_UserVerificationSelect", parameters);
                //myInt = Convert.ToInt32(parameters[2].Value);
                //return Convert.ToBoolean(res);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserData", "UserSelectAll");
                return ds;
            }
            finally
            {
            }
        }

        public static int UserVerificationupsert(UserModel model)
        {
            Int32 myInt = 0;
            DataSet ds = new DataSet();
            object res;
            SqlParameter[] parameters = {
                                          new SqlParameter("@sessionToken", model.SessionToken ),
                                          new SqlParameter("@UserID",!String.IsNullOrEmpty(model.EncryptedUserId)?model.EncryptedUserId:string.Empty),
                                          new SqlParameter("@IsEnabled",model.IsEnabledId),
                                          new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                res = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_UserVerificationUpsert", parameters);
                //myInt = Convert.ToInt32(parameters[2].Value);
                return Convert.ToInt32(res);
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserData", "UserSelectAll");
                return -1;
            }
            finally
            {
            }
        }
        public static DataSet UserMenuPermission(UserModel model)
        {
            Int32 myInt = 0;
            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            new SqlParameter("@sessionToken", model.SessionToken ),
                                            new SqlParameter("@search",!String.IsNullOrEmpty(model.Search)?model.Search:string.Empty),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_UserMenuPermission]", parameters);
                //myInt = Convert.ToInt32(parameters[2].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserData", "UserSelectAll");
                return ds;
            }
            finally
            {
            }
        }

        public DataSet SelectById(UserModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@userId",model.UserId) ,
                                            new SqlParameter("@sessionToken",model.SessionToken),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_UserSelectById", parameters);
                ReturnResult = Convert.ToInt32(parameters[2].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserData", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }

        public Int32 AddOrEditUser(UserModel model, DataTable dt, out Int32 ReturnResult)
        {
            int myInt = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@EncryptedUserId",model.EncryptedUserId),
                                            new SqlParameter("@FirstName",model.FirstName),
                                            new SqlParameter("@LastName",model.LastName),
                                            new SqlParameter("@MI",model.MI),
                                            new SqlParameter("@Email",model.Email),
                                            new SqlParameter("@Mobile",model.Mobile),
                                            new SqlParameter("@Password",model.Password),
                                            new SqlParameter("@titleId",model.TitleId),
                                            new SqlParameter("@Title",model.Title),
                                            new SqlParameter("@ISD",model.ISDCode),
                                            new SqlParameter("@ProfilePic",string.Empty),
                                            new SqlParameter("@NJ_MenuPermissionType",dt),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null)
            };
            try
            {
                myInt = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_UserUpsert]", parameters));
                ReturnResult = Convert.ToInt32(parameters[13].Value);
                return myInt;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserData", "AddOrEdit");
                ReturnResult = -1;
                return myInt;

            }
        }
        public static int TimeZoneCronJobUpdate(int timeZoneOffSet)
        {
            int myInt = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@TimeZoneOffSet",timeZoneOffSet)
            };
            try
            {
                myInt = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_UserTimeZoneOffSetUpdateCronJob", parameters));
                
                return myInt;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserData", "TimeZoneCronJobUpdate");
                myInt = -1;
                return myInt;

            }
        }

        public DataSet PermissionByTitle(Int32 TitleId)
        {
            DataSet myDataSet = null;            
            SqlParameter[] parameters ={
                                            new SqlParameter("@TitleId",TitleId) ,                                            
                                            new SqlParameter("ReturnResult", SqlDbType.Int, 4, ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_MenuPermissionByRole", parameters);                
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserData", "PermissionByRole");                
                return null;
            }
            finally

            {
            }
        }

        public Int32 EditUserProfile(UserModel model, out Int32 ReturnResult)
        {
            int myInt = 0;
            SqlParameter[] parameters ={
                new SqlParameter("@sessionToken",model.SessionToken),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@FirstName",model.FirstName),
                                            new SqlParameter("@LastName",model.LastName),
                                            new SqlParameter("@MI",model.MI),
                                            new SqlParameter("@Email",model.Email),
                                            new SqlParameter("@ISD",model.ISDCode),
                                            new SqlParameter("@Mobile",model.Mobile),
                                            new SqlParameter("@AddLine1",model.AddressLine1),
                                            new SqlParameter("@AddLine2",model.AddressLine2),
                                            new SqlParameter("@CityName",model.CityName),
                                            new SqlParameter("@StateId",model.StateId),
                                            new SqlParameter("@TimeZoneId",model.TimeZone),
                                            new SqlParameter("@Zip",model.Zip),
                                            new SqlParameter("@TimeZoneOffSet",model.TimeOffSet),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null)
            };
            try
            {
                myInt = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_UserEditProfile]", parameters));
                ReturnResult = Convert.ToInt32(parameters[15].Value);
                return myInt;
            }
            catch (Exception ex)
                {
                ApplicationLogger.LogError(ex, "UserData", "AddOrEdit");
                ReturnResult = -1;
                return myInt;

            }
        }
        public int NewProfileEmail(UserModel model, out int ReturnResult)
        {
            int myInt = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@sessionToken",model.SessionToken),
                                            new SqlParameter("@Email",model.Email),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null)
            };
            try
            {
                myInt = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_UserEmailProfile]", parameters));
                ReturnResult = Convert.ToInt32(parameters[13].Value);
                return myInt;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserData", "AddOrEdit");
                ReturnResult = -1;
                return myInt;

            }
        }
        public int Delete(UserModel model, out int ReturnValue)
        {
            ReturnValue = -1;
            Object myObject = -1;
            SqlParameter[] parameters ={
                                           new SqlParameter("@UserId",model.UserId) ,
                                           new SqlParameter("@sessionToken",model.SessionToken) ,
                                           new SqlParameter("ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {
                myObject = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_UserDelete]", parameters);
                ReturnValue = Convert.ToInt32(parameters[2].Value);
                return Convert.ToInt32(myObject);
            }
            catch (Exception ex)
            {
                ReturnValue = -1;
                ApplicationLogger.LogError(ex, "UserData", "Delete");
                return -1;
            }
            finally
            {

            }
        }

        public int Disable(UserModel model, out int ReturnValue)
        {
            ReturnValue = -1;
            Object myObject = -1;
            SqlParameter[] parameters ={
                                           new SqlParameter("@UserId",model.UserId) ,
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {
                myObject = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_UserDisable]", parameters);
                ReturnValue = Convert.ToInt32(parameters[2].Value);
                return Convert.ToInt32(myObject);
            }
            catch (Exception ex)
            {
                ReturnValue = -1;
                ApplicationLogger.LogError(ex, "UserData", "Disable");
                return -1;
            }
            finally
            {

            }
        }
        public static int ProfilePicUpdate(int userId, string sessionToken, string profilePicPath)
        {

            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",userId),
                                            new SqlParameter("@SessionToken",sessionToken),
                                            new SqlParameter("@ProfilePic",profilePicPath)
                                        };
                object obj = SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_UserProfilePicUpdate", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "UserData", "ProfilePicUpdate");
            }

            return returnResult;
        }

        public static DataSet ChangePassword(UserModel model)
        {
            DataSet myDataSet = null;
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@Password",model.Password),
                                            //new SqlParameter("@OldPassword",model.OldPassword),
                                            new SqlParameter("@SessionToken",model.EncryptedSessionToken??"CMGROAVH0J2Y1KXKYQOI")
                                        };



                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_ChangePassword", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChangePasswordData", "ChangePassword");
                return null;
            }
            finally
            {
            }
        }
        public static DataSet selectUserById(Int32 EncryptedUserId)
        {
            DataSet myDataSet = null;
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",EncryptedUserId),
                                        };



                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_selectUserById", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChangePasswordData", "ChangePassword");
                return null;
            }
            finally
            {
            }
        }

        public static DataSet MenuPermissionByRole(Int32 RoleId)
        {
            Int32 myInt = 0;
            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            new SqlParameter("@RoleId", RoleId ),                                           
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_UserSelectAll", parameters);
                //myInt = Convert.ToInt32(parameters[2].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserData", "UserSelectAll");
                return ds;
            }
            finally
            {
            }
        }

    }
}
