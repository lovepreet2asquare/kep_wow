﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Infotech.ClassLibrary;
using FPModels.Models;

namespace FPDAL.Data
{
    public class VendorsData : ConnectionObject
    {
        public DataSet SelectAll(VendorsModel vendorsModel)
        {
            int myint = 0;
            DataSet ds = new DataSet();
            SqlParameter[] parameters = {
                new SqlParameter("@SessionToken",vendorsModel.SessionToken),
                new SqlParameter("@Search",String.IsNullOrEmpty(vendorsModel.Search)?string.Empty:vendorsModel.Search)
            };
            try
            {
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_VendorSelectAll", parameters);
                myint = Convert.ToInt32(parameters[1].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorsData", "SelectAll");
                return ds;
            }
            finally
            {

            }
        }

        public DataSet SelectById(VendorsModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                          
                                            new SqlParameter("@VendorId",model.VendorId),
                                            new SqlParameter("@loggeduserid",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_VendorSelectById", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "vendorData", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }


        public DataSet AddorEdit(VendorsModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@loggeduserid",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@VendorId",model.VendorId),
                                            new SqlParameter("@VendorName",model.VendorName),
                                            new SqlParameter("@OfferDescription",model.OfferDescription??string.Empty),
                                            new SqlParameter("@Websitelink",model.Websitelink),
                                            new SqlParameter("@status",model.StatusId??"0"),
                                            new SqlParameter("@ImagePath",model.LogoPath),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_VendorUpsert", parameters);
                ReturnResult = Convert.ToInt32(parameters[8].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorsData", "AddorEdit");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }

        public int Delete(VendorsModel model)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@VendorId",model.VendorId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_VendorsDelete]", parameters);
                returnResult = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "VendorsData", "Delete");
            }
            finally
            { }
            return returnResult;
        }
    }
}
