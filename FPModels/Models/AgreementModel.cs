﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class AgreementResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; }
        public AgreementModel _agreementModel { get; set; }
    }
    public class AgreementModel
    {
        public string EncryptedSessionToken { get; set; }
        public string EncryptedAgreementId{ get; set; }
        public string SessionToken { get; set; }
        public string Search { get; set; }
        [Required(ErrorMessage="Please enter Title")]
        public string Title { get; set; }
        public int Privacy { get; set; }
        public string ModifyDate { get; set; }
        public string ModifyTime { get; set; }
        [Required(ErrorMessage = "Please enter Description")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Please select Date")]
        public string DateTime { get; set; }
        public string EncryptedUserId { get; set; }
        public string DocumentName { get; set; }
        public string ThumbnailPath { get; set; }
        public string NoFile { get; set; }
        public string DocumentPath { get; set; }
        public HttpPostedFileBase HttpPostedFile { get; set; }
        public int UserId { get; set; }
        public int AgreementId { get; set; }
        public List<SelectListItem> _StatusList { get; set; }
        public List<AgreementModel> _agreementlist { get; set; }
    }
}
