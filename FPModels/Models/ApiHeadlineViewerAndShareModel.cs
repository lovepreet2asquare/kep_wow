﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class ApiHeadlineViewerAndShareModel
    {
        public int FollowerId { get; set; }
        public int HeadlineId { get; set; }
    }
    public class FP_HeadlineViewerAndShareRequest
    {
        public string SessionToken { get; set; }
        public string FollowerId { get; set; }
        public string HeadlineId { get; set; }
    }
    
}
