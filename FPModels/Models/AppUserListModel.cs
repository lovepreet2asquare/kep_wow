﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FPModels.Models
{
    public class AppUserListModel
    {
        public string Search { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string Lastname { get; set; }
        public string Status { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string ZIPCode { get; set; }
        public string Occupation { get; set; }
        public string Employer { get; set; }
        public int UserId { get; set; }
        public string EncryptedUserId { get; set; }
        public string SessionToken { get; set; }
        public string EncryptedSessionToken { get; set; }
        public List<AppUserListModel> ApUserModelList { get; set; }

    }
    public class AppUserListResponse
    {
        public int ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public AppUserListModel ApUserModel { get; set; }
    }

    //public class InviteModel
    //{
    //    public int SrNo { get; set; }
    //    public string Email { get; set; }
    //    public string FirstName { get; set; }
    //    public string LasName { get; set; }
    //    public HttpPostedFileBase FilePath { get; set; }
    //    public List<InviteModel> _UserList { get; set; }

    //}



    public class InviteResponse
    {
        public int ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public InviteDataFileModel InviteDataFileModel { get; set; }
    }
    public class InviteDataFileModel
    {
        public String InviteDataFileId { get; set; }       
        public String FileNumber { get; set; }
        public String FilePath { get; set; }
        public HttpPostedFileBase HttpPostedFile { get; set; }
        public String CreateDate { get; set; }
        public String UserId { get; set; }
        public List<InviteDataModel> _InviteData { get; set; }
        public DataTable dtInviteData { get; set; }
    }

    public class InviteDataModel
    {
        public Int64 InviteDataId { get; set; }
        public Int32 InviteDataFileId { get; set; }
        public String Email { get; set; }
        public String FirstName { get; set; }
        public String MiddleInitial { get; set; }
        public String LastName { get; set; }
        public Boolean IsRegister { get; set; }
        public String Status { get; set; }

        public String FileNumber { get; set; }
        public String CreateDate { get; set; }

    }
}
