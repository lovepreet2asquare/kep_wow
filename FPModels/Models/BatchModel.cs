﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class BatchModel
    {
        public string EncryptedSessionToken { get; set; }
        public string UserId { get; set; }
        public string EncryptedEventId { get; set; }
        public Int32 BatchId { get; set; }
        public string BatchNumber { get; set; }

        [Required()]
        public string PurposeId { get; set; }
        public string CreatedBy { get; set; }
        public string CreateDate { get; set; }
        public string TransactionType { get; set; }

        [Required()]
        public string Status { get; set; }
        public string DateRange { get; set; }

        [Required()]
        public string FileTypeId { get; set; }
        public string FileType { get; set; }
        public string PurposeText { get; set; }
       // public string PurposeAdd { get; set; }


        public string SessionToken { get; set; }
        public string EncryptedUserId { get; set; }

        [Required()]
        public string DateFrom { get; set; }

        [Required()]
        public string DateTo { get; set; }
        public string Search { get; set; }
        public string IsView { get; set; }

        public bool payment { get; set; }
        public bool Refund { get; set; }

        public string FilePath { get; set; }

        public int NoOfRefund { get; set; }
        public int NoOfPayment { get; set; }
        
        public List<SelectListItem> _PurposeList { get; set; }
        public List<SelectListItem> _FileTypeList { get; set; }
        public List<SelectListItem> _StatusList { get; set; }
        public List<BatchModel> _BatchList { get; set; }
       

    }
    public class BatchResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; }
        public BatchModel _BatchModel { get; set; }
    }
}
