﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class ChangePasswordModel
    {
        public string UserId { get; set; }
        public Int32 ReturnValue { get; set; }
        public int ISEmailSend { get; set; }
        public string SessionToken { get; set; }
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 8, ErrorMessage = "Password cannot be longer than 20 characters and less than 8 characters")]
        [RegularExpression("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^\\da-zA-Z])(.{8,15})$", ErrorMessage = "Your password must contain: minimum 8 characters, at least 1 number, 1 uppercase character and 1 special character.")]
        [DisplayName("OldPassword")]
        public string OldPassWord { get; set; }
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 8, ErrorMessage = "Password cannot be longer than 20 characters and less than 8 characters")]
        [RegularExpression("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^\\da-zA-Z])(.{8,15})$", ErrorMessage = "Your password must contain: minimum 8 characters, at least 1 number, 1 uppercase character and 1 special character.")]
        [DisplayName("Password")]
        public String Password { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [StringLength(20, ErrorMessage = "Max 20 characters")]
        [System.ComponentModel.DataAnnotations.CompareAttribute("Password")]
        [DisplayName("Confirm Password")]
        public String ConfirmPassword { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string MobileNumber { get; set; }
        public int ReturnValues { get; set; }
    }

    public class ChangePasswordResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
        public ChangePasswordModel ChangePasswordModel { get; set; }
    }
}
