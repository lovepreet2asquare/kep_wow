﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public abstract class ConnectionObject
    {
        public static String ConnectionString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["fpCon"].ConnectionString;
            }
        }
    }
}
