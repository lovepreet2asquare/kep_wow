﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class ContactUsApiModel
    {
        public int ContactId { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string CreateDate { get; set; }
        public string IsdCode { get; set; }
        public string Mobile { get; set; }
        public string Message { get; set; }
        public  bool IsEmailRespnse { get; set; }
    }
    public class EPContactUsRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string IsdCode { get; set; }
        public string Mobile { get; set; }
        public string Message { get; set; }
        public string IsEmailRespnse { get; set; }
    }
    public class ChangePasswordApiModel
    {
        public string EncryptedFollowerId { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string Password { get; set; }
    }
}
