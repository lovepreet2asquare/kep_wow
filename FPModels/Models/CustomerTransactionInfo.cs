﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{

    public class CustomerTransactionInfo
    {
        
       // public int BookingId { get; set; }
        
        public string CustomerProfileId { get; set; }
        
        public string CustomerPaymentProfileId { get; set; }
        
        public int FollowerId { get; set; }
        
        public string TransactionID { get; set; }

        
        public string RefId { get; set; }
        
        public string ResultCode { get; set; }
        
        public string ResponseCode { get; set; }
        
        public string ResponseDescription { get; set; }

        
        public string ErrorCode { get; set; }
        
        public string ErrorText { get; set; }
        
        public string MessageCode { get; set; }
        
        public string MessageText { get; set; }
        
        public decimal Amount { get; set; }


    }
}
