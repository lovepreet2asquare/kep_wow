﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class DonateAPIModel
    {
        public string CustomerProfileId { get; set; }
        public string CustomerPaymentProfileId { get; set; }
        public string SessionToken { get; set; }
        public string FollowerId { get; set; }
        public string TransactionId { get; set; }
        public string PaymentMethodId { get; set; }
        public decimal Amount { get; set; }
        public string SpouseName { get; set; }
        public int DonationTypeId { get; set; }
        public int RecurringTypeId { get; set; }
        public string TransactionDate { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorText { get; set; }
        public string IsRecurring { get; set; }
        public string Accupation { get; set; }
        public string Employer { get; set; }
        public string IsHostingEvent { get; set; }
        public string IsVolunteer { get; set; }
        public string CurrentDateTime { get; set; }
    }
    public class TransactionCronjobModel
    {
        public string CustomerProfileId { get; set; }
        public string CustomerPaymentProfileId { get; set; }
        public int FollowerId { get; set; }
        public int PaymentId { get; set; }
        public string Status { get; set; }
        public decimal Amount { get; set; }

        public string ErrorCode { get; set; }
        public string ErrorText { get; set; }
        public string TransactionId { get; set; }
    }
    public class FailTransactionCronjobModel
    {
        public string PaymentMethodId { get; set; }
        public int FollowerId { get; set; }
        public int PaymentId { get; set; }
        public string Status { get; set; }
        
        public string ErrorCode { get; set; }
        public string ErrorText { get; set; }
        public string TransactionId { get; set; }
    }
}
