﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class DonationModel
    {
        public int PaymentMethodId { get; set; }
        public string EncryptedPaymentId { get; set; }
        public Int32 PaymentId { get; set; }
        public string TransactionId { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionStatus{ get; set; }
        public string UserName { get; set; }
        public string FilePath { get; set; }
        public int ReturnCode { get; set; }
        public bool IsSkip { get; set; }
        public string[] SearchStatus { get; set; }
        public string MultiStatus { get; set; }
        public string Search { get; set; }
        public string Time { get; set; }
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string EncryptedUserId { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string CardHolderName { get; set; }
        public string Email { get; set; }
        public string Occupation { get; set; }
        public string MobileNumber { get; set; }
        public string SpouseName { get; set; }
        public string ISDCode { get; set; }
        public string Employer { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string Field { get; set; }
        public string AddressLine1 { get; set; }
        public string UserId { get; set; }
        public string SessionToken { get; set; }
        public String AddressLine2 { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string cityName { get; set; }
        public String zipcode { get; set; }
        public string DonationType { get; set; }
        public string TransctionDate { get; set; }
        public string TransactionTime { get; set; }
        public string FollowerId { get; set; }
        public string EncryptedFollowerId { get; set; }
        public string RecurringType { get; set; }
        public string Duration { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string Status { get; set; }
        public string ExpMonth {get; set;}
        public string Volunteer { get; set; }
        public string Hosting { get; set; }
        public string DonationStatus { get; set; }
        [Required(ErrorMessage =("Please enter amount"))]
        public string Amount1 { get; set; }
        public string ExpYear { get; set; }
        public bool IsRefund{ get; set; }
        public List<SelectListItem> _StatusAIList { get; set; }
        public Decimal Amount { get; set; }
        public List<DonationModel> _DonationModel { get; set; }
        public List<DonationModel> _DonorDetail{ get; set; }
        public DonationWeekMMYY donationWeekMMYY { get; set; }
        public List<DonationTypeModel> _DonationTypeModel { get; set; }
        public List<SelectListItem> _StatusList { get; set; }
        public DonationAmount donationAmount { get; set; }
        public List<GraphData> _GraphData { get; set; }
        [Required(ErrorMessage = ("Please enter comment"))]
        public string Comment { get; set; }
        [Required(ErrorMessage="Please select")]
        public string FullRefund { get; set; }
        public string PartialRefund { get; set; }
        public string RefundStatus { get; set; }
        public string RefundAmount { get; set; }
        
        [Required]
        public int RefundReasonId { get; set; }
        public string RefundReasonAdd { get; set; }
        public List<SelectListItem> _ReasonList { get; set; }
    }
    public class DonationTypeModel
    {
        public string DonationType { get; set; }
        public decimal DonationPercentage { get; set; }
    }
    public class DonationAmount
    {
        public string DonationAmountTotal { get; set; }
        public string RefundAmount { get; set; }
    }
    public class DonationWeekMMYY
    {
        public string DonationWeekMMYYName { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }

    public class GraphsDataModel
    {
        public string Day { get; set; }
        public Decimal Amount { get; set; }

        public List<GraphsDataModel> _GraphData { get; set; }
        public DonationWeekMMYY DonationWeekMMYY { get; set; }
        public DonationAmount DonationAmount { get; set; }
        public List<DonationTypeModel> _DonationTypeModel { get; set; }
    }
    public class RecurringTyperesponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
        public RecurringTypeModel RecurringTypeModel { get; set; }
    }
    public class RecurringTypeModel
    {
        public int RecurringTypeId { get; set; }
        public string RecurringType { get; set; }
        [Required(ErrorMessage = "Recurring Type is required")]
        [MaxLength(60, ErrorMessage = "Recurring Type cannot be longer than 30 characters.")]
        public string RecurringType2 { get; set; }
        public string RecurringDate { get; set; }
        public bool NoEndDate{ get; set; }
        public Int32 ReturnValue { get; set; }
        public string Fields { get; set; }
        [Required]
        public Int32 StatusId { get; set; }
        public List<SelectListItem> _StatusAIList { get; set; }
        public string UserId { get; set; }
        [Required]
        public string RecurringTypeDate { get; set; }
        public string SessionToken { get; set; }
        public string Status { get; set; }
        public List<RecurringTypeModel> _RecurringTypeModel { get; set; }
    }
    public class DonationResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
        public string AuthTransactionId {get; set;}
        public DonationModel DonationModel { get; set; } // Object
    }
}
