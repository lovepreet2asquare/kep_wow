﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Text;
using System.Threading.Tasks;
using static FPModels.Models.CustomValidator;
using System.Web;

namespace FPModels.Models
{
    public class EventModel
    {
        public int TimeZone { get; set; }
        public int EventId { get; set; }
        public int Edit { get; set; }
        public string FilePath { get; set; }
        public string FileType { get; set; }
        public int Create { get; set; }
        public int CreatedAgenda { get; set; }
        public string EncryptedEventId { get; set; }
        [Required(ErrorMessage = "Please enter Title")]
        public string Title { get; set; }
        public string Local { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MI { get; set; }
        public int UserId { get; set; }
        public string IsView { get; set; }
        public string EncryptedUserId { get; set; }
        //[Required(ErrorMessage = "Write here")]
       // [MaxLength(50,ErrorMessage = "Maximum 50 words")]   
        public string Content { get; set; }
        [Required(ErrorMessage = "Please enter Agenda")]
        public string AgendaAdd { get; set; }
        public string IsAccepted { get; set; }
        public string AttendeeCount { get; set; }
        public string Declined { get; set; }
        public List<SelectListItem> _AgendaList{ get; set; }
        [Required(ErrorMessage = "Please select Agenda")]
        public string AgendaId{ get; set; }
        public string Location { get; set; }
        public string RejectReason { get; set; }

        [Required(ErrorMessage = "Please select")]
        public string Status { get; set; }
        public string Draft { get; set; }
        [Required(ErrorMessage = "Please select")]
        public string EventDate { get; set; }
        public string EventName { get; set; }
        public string EventDateMessage { get; set; }
        public string EventTime { get; set; }
        [Required(ErrorMessage = "Please select")]
        public string InvitationType { get; set; }
        public int EventDayLeft { get; set; }
        public string PublishDate { get; set; }
        public string Agenda { get; set; }

        public Boolean IsEventLoc { get; set; }

        [RequiredIf("IsEventLoc", true, ErrorMessage = "Event city is required")]
        public string EventCity { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string SessionToken { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string Search { get; set; }
        public List<EventModel> _EventList { get; set; }
        [Required(ErrorMessage = "Please enter Address Line 1")]
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Summary { get; set; }
        public string URL { get; set; }
        public String LogoPath { get; set; }
        public HttpPostedFileBase HttpPostedFile { get; set; }
        public string NoFile { get; set; }
        public string ImageBase64 { get; set; }
        [Required(ErrorMessage = "Please enter City")]
        public string CityName { get; set; }
        [Required(ErrorMessage = "Please enter City")]
        public string EventCityName { get; set; }
        [Required(ErrorMessage = "Please enter ZIP")]
        public string Zip { get; set; }
        [Required(ErrorMessage = "Please select Audience")]
        public string SelectAudience { get; set; }
        public String[] CountyId{ get; set; }
        public String SelectedCountyId{ get; set; }
        public String[] LocalId{ get; set; }
        public String SelectedLocalId { get; set; }
        public String[] IndividualId{ get; set; }
        public string SelectedIndividualId { get; set; }
        public string StateName { get; set; }
        public string EventStateName { get; set; }
        public string CountryName { get; set; }
        public string AudienceId { get; set; }
        public string Confirmation { get; set; }
        [Required(ErrorMessage = "Please enter Audience")]
        public string ApproxAudience { get; set; }
        public bool IndividualIsChecked{ get; set; }
        public bool LocalIsChecked{ get; set; }
        public bool CountyIsChecked{ get; set; }
        public int UserTitle { get; set; }
        public List<SelectListItem> _StatusListforAdmin { get; set; }
        public List<SelectListItem> _CountyList { get; set; }
        public List<SelectListItem> _LocalList { get; set; }
        public List<SelectListItem> _IndividualList { get; set; }
        public List<SelectListItem> _StatusList { get; set; }
        [Required(ErrorMessage = "Please select Country")]
        public int CountryId { get; set; }
        public List<SelectListItem> _CountryList { get; set; }
        public List<SelectListItem> _Audience{ get; set; }
        public List<SelectListItem> _EventCountryList { get; set; }
        public int EventCountryId { get; set; }
        [Required(ErrorMessage = "Please select State")]
        public int StateId { get; set; }
        [Required(ErrorMessage = "Please select State")]
        public int EventStateId { get; set; }
        public List<SelectListItem> _StateList { get; set; }
        public List<SelectListItem> _EventStateList { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
       
        public List<SelectListItem> Radius{get; set;}

        //[RequiredIf("IsEventLoc", true, ErrorMessage = "Radius is required")]
        public int? SelectedRadius { get; set; }
        public string PublishCalDate { get; set; }
        public string EventCalDate { get; set; }
    }
    public class EventResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; }
        public  string _FilePath { get; set; }
        public EventModel _EventModel { get; set; }
    }
}

