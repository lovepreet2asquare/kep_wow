﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class FAQModel
    {
        public string SessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public string EncryptedFaqId { get; set; }
        public int UserId { get; set; }
        public int FaqId { get; set; }
        public string Title { get; set; }
        public string Response { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string CreateDate { get; set; }
        public string ModifyDate { get; set; }
        public string Search { get; set; }

        public List<FAQModel> _FAQList { get; set; }
    }
    public class FAQResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage

        public FAQModel _FAQModel { get; set; }
    }
}
