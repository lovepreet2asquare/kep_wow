﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class FPRequests
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
    }
    public class LoginRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceType { get; set; }
        public string DeviceId { get; set; }

    }
    public class ForgotRequest
    {

        public string Email { get; set; }
    }
    public class FollowerNewsRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string StartIndex { get; set; }
        public string EndIndex { get; set; }

    }
    public class FolloerNewsDetailRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string NewsId { get; set; }
    }
    
    public class FollowerHeadlinesRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string StartIndex { get; set; }
        public string EndIndex { get; set; }
    }

    public class FollowerHeadlineDetailRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public int HeadlineId { get; set; }
   
    }
    public class GetInvolvedRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ISDCode { get; set; }
        public string Mobile { get; set; }
        public Int32 Countryid { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string CityName { get; set; }
        public Int32 Stateid { get; set; }
        public string ZipCode { get; set; }
    }
    public class AccountDeleteRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public bool IsDelete { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
    }
    public class LogoutRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
    }
    public class SetPrimaryRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string PaymentMethodId { get; set; }
    }
    public class FPInviteRequests
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string ContactList { get; set; }
    }
}
