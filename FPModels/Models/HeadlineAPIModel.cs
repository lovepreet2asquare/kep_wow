﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class HeadlineAPIModel
    {
        public string HeadlineId { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public string PublishDate { get; set; }
        public string PublishTime { get; set; }
        public string Content { get; set; }
        public string ShareLink { get; set; }
        public bool IsUpdated { get; set; }
        public bool IsDonate { get; set; }

        public string DonateURL { get; set; }
    }
    public class FPHeadlineAPIResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<HeadlineAPIModel> HeadlineList { get; set; }
    }
    public class FPHeadlineDetailAPIResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public HeadlineAPIModel HeadlineDetail { get; set; }
    }
}
