﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class InAppMessageResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
        public InAppMessageModel InAppMessageModel { get; set; } // Object
    }
    public class InAppMessageModel
    {
        public int TimeZone { get; set; }
        [Required(ErrorMessage = "Please enter the title")]
        // [MaxLength(50, ErrorMessage = "Maximum 50 words")]
        public string Title { get; set; }
        public int? ApproxAudience { get; set; }
        [Required(ErrorMessage = "  Please enter agenda")]
        // [MaxLength(1000,ErrorMessage = "Maximum 1000 words")]
        public string Content { get; set; }

        public string StatusPublish{ get; set; }

        public string StatusDraft { get; set; }
        public string StatusSchedule { get; set; }
        [Required(ErrorMessage = "Please select one option")]
        public string Time { get; set; }
        //[Required(ErrorMessage = "Please select one option")]
        public string IsDonate { get; set; }
        public string URL { get; set; }
        public string Button { get; set; }
        //[Required(ErrorMessage = "Please select radius")]
        public int? SelectedRadius { get; set; }
        public string SelectAudience { get; set; }
        public String[] CountyId { get; set; }
        public String SelectedCountyId { get; set; }
        public String[] LocalId { get; set; }
        public bool LocalIsChecked { get; set; }
        public bool IndividualIsChecked { get; set; }
        public bool CountyIsChecked { get; set; }
        public String SelectedLocalId { get; set; }
        public String[] IndividualId { get; set; }
        public string SelectedIndividualId { get; set; }
        public List<SelectListItem> _CountyList { get; set; }
        public List<SelectListItem> _LocalList { get; set; }
        public List<SelectListItem> _IndividualList { get; set; }
        public string isview { get; set; }
        public List<SelectListItem> Radius { get; set; }
        public string PublishCalDate { get; set; }
        public string Search { get; set; }
        public bool IsDonateNo { get; set; }
        public bool IsDonateYes { get; set; }
        [Required(ErrorMessage = "Please select one option")]
        public string Status { get; set; }
        public int Create { get; set; }
        public string RejectReason { get; set; }
        [Required(ErrorMessage = "Please select one option")]
        public List<SelectListItem> _StatusListforAdmin { get; set; }
        public string SearchStatus { get; set; }
        public string Draft { get; set; }
        public int UserTitle{ get; set; }
        public List<SelectListItem> StatusList { get; set; }
        public Int32 Shares { get; set; }
        public Int32 View { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public string SessionToken { get; set; }
        public int UserId { get; set; }
        public string MessageId { get; set; }
        public string CreateTime { get; set; }
        public string CreateDate { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        //[Required(ErrorMessage = "Please select city")]
        public string CityName { get; set; }
        public string ModifyDate { get; set; }
        //[Required(ErrorMessage = "Please select Date")]
        public string PublishDate { get; set; }
        public string DateFilterText { get; set; }
        public string DateFilterSelect { get; set; }
        public List<SelectListItem> _DateFilterList { get; set; }
        public List<InAppMessageModel> _messagelist { get; set; }
        public string DateTo { get; set; }
        public int Edit{ get; set; }
        public string DateFrom { get; set; }

        // [Required(ErrorMessage = "Please select country")]
        public int CountryId { get; set; }
        public List<SelectListItem> _CountryList { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }

        // [Required(ErrorMessage = "Please select state")]
        public int StateId { get; set; }

        // [Required(ErrorMessage = "Please enter address line 1")]
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }

        // [Required(ErrorMessage = "Please enter zipcode")]
        public string Zip { get; set; }

        public List<SelectListItem> _StateList { get; set; }

    }
}
