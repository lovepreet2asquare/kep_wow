﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
   public class InvolvedApiModel
    {
        public string FirstName { get;set;}
        public int InvolvedId { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ISDCode { get; set; }
        public string Mobile { get; set; }
        public string CountryName { get; set; }
        public Int32 Countryid { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string CityName { get; set; }
        public string CreateDate { get; set; }
        public Int32 Stateid { get; set; }
        public string ZipCode { get; set; }
        public string stateName { get; set; }
        public bool Status { get; set; }
    }

    public class FP_InvolvedApiResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; }
        public InvolvedApiModel involvedApiModel { get; set; }
    }

    public class FP_InvolvedApiRequest
    {
        public string SessionToken { get; set; }
        public string FollowerId { get; set; }
        public string Email { get; set; }
    }
}
