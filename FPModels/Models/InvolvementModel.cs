﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class InvolvementModel
    {
        public Int32 InvolvementId { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ISDCode { get; set; }
        public List<SelectListItem> _StatusAIList { get; set; }
        public string Mobile { get; set; }
        public string StdMobile { get; set; }
        public string Field { get; set; }
        public string UserId { get; set; }
        public string SessionToken { get; set; }
        public string FromDate { get; set; }
        public string Duration { get; set; }
        public string Todate { get; set; }
        public Int32 CountryId { get; set; }
        public string CountryName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string CityName { get; set; }
        public string StateId { get; set; }
        public string StateName { get; set; }
        public string ZipCode { get; set; }
        public Int32 SrNO { get; set; }
        public string Time { get; set; }
        public string CreateDate { get; set; }
        public string Status { get; set; }
        public Int32 CountCity { get; set; }
        public List<InvolvementModel> _lnvolvementList { get; set; }
        public List<CityNameModel> _CityNameModel { get; set; }
        public GraphsModel graphsModel { get; set; }
        public ThisMonthYearMonth thisMonthYearMonth { get; set; }
        public List<GraphData> _GraphData { get; set; }
    }
    public class CityNameModel
    {
        public string ProgressbarClass { get; set; }
        public string ClassName { get; set; }
        public string CityName { get; set; }
        public Int32 CountCity { get; set; }
        public Decimal CityCountPrercentage { get; set; }
    }
    public class GraphData
    {
        public string Day { get; set; }
        public Int32 Involvement { get; set; }
      
        public List<GraphData> _GraphData { get; set; }
        public AfterDropDownChange AfterDropDownChangeModel { get; set; }
        public GraphsModel GraphsModel { get; set; }
        public List<CityNameModel> _CityNameModel { get; set; }
    }
    public class AfterDropDownChange
    {
        public string Name { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
    public class ThisMonthYearMonth
    {
        public string ThisWeakMMYY { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
    public class GraphsModel
    {
        public decimal Totalcount { get; set; }
    }
}
