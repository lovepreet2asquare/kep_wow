﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class NewsModel
    {
        public int NewsId { get; set; }
        public string EncryptedNewsId { get; set; }
        [Required(ErrorMessage = "Please enter the title")]
        public string Title { get; set; }
        public string Location { get; set; }
        public string RejectReason { get; set; }
        public string County { get; set; }
        public string IsDonate { get; set; }
        [Required(ErrorMessage = "Write here")]
        public string Content { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string URL { get; set; }

        public string PublishDate { get; set; }
        public string Size { get; set; }
        public string Extension { get; set; }
        public string PublishCalDate { get; set; }

        [Required(ErrorMessage = "Please select file.")]
        [Display(Name = "Browse File")]
        public HttpPostedFileBase[] files { get; set; }
        public string ImagePath { get; set; }
        public string PublishTime { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string SessionToken { get; set; }
        public int TimeZone { get; set; }
        public string EncryptedUserId { get; set; }
        public int UserId { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string IsView { get; set; }
        public string Search { get; set; }
        public int ViewCount { get; set; }
        [Required(ErrorMessage = "Please select")]
        public string Status { get; set; }
        public int Edit{ get; set; }
        public string Draft { get; set; }
        public int SelectedUserId { get; set; }
        public int UserTitle { get; set; }
        public int Create { get; set; }
        public int SelectedStatusId { get; set; }
        public List<SelectListItem> _UserList { get; set; }
        public List<SelectListItem> _StatusList { get; set; }
        public List<NewsModel> _NewsList { get; set; }
        public List<ImageListModel> _ImagePathList { get; set; }
        public List<NewsModel> _ReadMorelist { get; set; }
        public List<SelectListItem> _StatusListforAdmin { get; set; }
        public Int32 ShareCount { get; set; }
        public string UserName { get; set; }

       
    }
    public class ImageListModel
    {
        public int ImageId { get; set; }
        public string ImagePath { get; set; }
        public string ImageBase64 { get; set; }
        public HttpPostedFileBase HttpPostedFileBase{ get; set; }
    }
    public class NewsDetailModel
    {
        public int NewsId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string PublishDate { get; set; }
        public string ImagePath { get; set; }
        public string PublishTime { get; set; }
        public List<NewsImage> newsImageList { get; set; }
    }
    public class NewsImageModel
    {
        public int NewsId { get; set; }
        public string ImagePath { get; set; }
    }
    public class NewsDeatilResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; }
        public List<NewsDetailModel> _NewsDetailModel { get; set; }
    }
    public class NewsResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; }
        public NewsModel _newsModel { get; set; }
    }
}