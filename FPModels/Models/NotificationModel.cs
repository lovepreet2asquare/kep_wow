﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class NotificationResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
        public NotificationModel NotificationModel { get; set; } // Object
    }
    public class NotificationModel
    {
        public string SessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public Int32 UserId { get; set; }
        public string DeviceType { get; set; }
        public int NotificationId { get; set; }
        public string Message { get; set; }
        public string Type { get; set; }
        public string CreateUtcDate { get; set; }
        public string CreateDate { get; set; }
        public string CreateTime { get; set; }
        public string Time { get; set; }
        public bool IsView { get; set; }
        public String IsViewForDisplay { get; set; }
        public string NotificationIds { get; set; }
        public String NotifyDay { get; set; }
        public String FirstName { get; set; }
        public List<NotificationModel> _NotificationList { get; set; }
    }
}
