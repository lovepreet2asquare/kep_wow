﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class SystemTimezones
    {
        public string TimeZoneId { get; set; }
        public string StandardName { get; set; }
        //public string DisplayName { get; set; }
    }
}
