﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class TicketResponse
    {
        public int ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public TicketModel TicketModel { get; set; }

    }
    public class TicketModel
    {
        public Nullable<Int64> TicketId { get; set; }

        public Nullable<Int64> TicketNumber { get; set; }
        public Nullable<Int32> CommunityId { get; set; }

        [Required(ErrorMessage = "Please select reason")]
        public Nullable<Int32> TicketReasonId { get; set; }

        public Nullable<Int32> AssignedTo { get; set; }
        public Nullable<Int32> TaggedTo { get; set; }
        public Nullable<Int32> TicketDetailId { get; set; }
        public Nullable<Int32> CreatedBy { get; set; }

        public String ReasonName { get; set; }
        [Required(ErrorMessage = "Please enter Subject")]
        public String Subject { get; set; }
        public String DReasonName { get; set; }

        [Required(ErrorMessage = "Please priority reason")]
        public String Priority { get; set; }

        //[Required(ErrorMessage = "Please select status")]
        public String Status { get; set; }

        [Required(ErrorMessage = "Please enter description")]
        public String Description { get; set; }
       // public String ImagePath { get; set; }
        public Nullable<Int32> UserId { get; set; }

        public Int32 SrNo { get; set; }
        public String SearchText { get; set; }
        //[Required(ErrorMessage = "Please search community")]

        public String EncryptedTicketId { get; set; }
        public String CommunityName { get; set; }
        public String AccountNumber { get; set; }

        public String FollowUpDate { get; set; }

       // [MaxLength(40, ErrorMessage = "Maximum 200 characters")]
        public String FollowUpText { get; set; }

        public String DateFrom { get; set; }
        public String DateTo { get; set; }
        public String TicketDate { get; set; }
        public String TicketTime { get; set; }
        public String CreatedByName { get; set; }
        public String AssignToName { get; set; }
        public String TaggedToName { get; set; }
        public Int16 ReturnValue { get; set; }
        public String Field { get; set; }
        public String[] SelectedTaggedToIDs { get; set; }

        public String TaggedToIDs { get; set; }

        [Required(ErrorMessage = "Please enter contact person name")]
        public String ContactName { get; set; }

        public String ISDCode { get; set; }
        public String ContactNumber { get; set; }
        public String CtrlAction { get; set; }
        public String QuestionerName { get; set; }
        public String QuestionerEmail { get; set; }
        public String EncryptedUserId { get; set; }
        public String SessionToken { get; set; }
        public HttpPostedFileBase HttpPostedFileBase { get; set; }
        public List<TicketModel> _Tickets { get; set; }
        public List<SelectListItem> _PriorityList { get; set; }
        public List<SelectListItem> _StatusList { get; set; }
        public List<SelectListItem> _TicketReasonList { get; set; }
        public List<SelectListItem> _UserList { get; set; }

        public List<TicketDetail> _TicketDetail { get; set; }
        public List<SelectListItem> _TagToList { get; set; }
        public Nullable<Int32> TitleId { get; set; }
        public List<SelectListItem> _UserTitleList { get; set; }
        public List<ImageModel> _Images { get; set; }


    }
    public class TicketDetail
    {
        public Int32 SrNo { get; set; }
        public Int32 UserId { get; set; }
        public String UserName { get; set; }
        public String Description { get; set; }
        public String SysDate { get; set; }
        public String TicketDetailStatus { get; set; }
        public String ContactName { get; set; }
        public String ContactNumber { get; set; }
        public String UserType { get; set; }
        public List<ImageModel> _Images { get; set; }
    }

    public class ImageModel
    {
        public Int64 ImageId { get; set; }
        public Int64 TicketDetailId { get; set; }
        public String ImagePath { get; set; }
        // public HttpPostedFileBase HttpPostedFileBase { get; set; }
    }
}
