﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

namespace FPModels.Models
{
    public class UserModel
    {
        [Required(ErrorMessage = "please enter first name")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Special characters & Space are not allowed")]
        public string FirstName { get; set; }

        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Special characters & Space are not allowed")]
        public string MI { get; set; }

        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Special characters & Space are not allowed")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "please enter email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter valid email")]
        public string Email { get; set; }

        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter valid email")]
        public string OldEmail { get; set; }
        //[Required(ErrorMessage = "Please enter mobile number")]
        [MinLength(14,ErrorMessage ="Minimum 10 digits are required")]
        public string Mobile { get; set; }
        public string UniqueId { get; set; }

        [Required(ErrorMessage = "Please select country")]
        public int CountryId { get; set; }
        [Required(ErrorMessage = "Please select Time Zone")]
        public string TimeZone { get; set; }

        [Required(ErrorMessage = "Please select state")]
        public int StateId { get; set; }
        [Required(ErrorMessage = "Please enter city")]
        public string CityName { get; set; }
        [Required(ErrorMessage = "Please enter Address Line1")]
        public string AddressLine1 { get; set; }
        public string RoleType { get; set; }
       
        public string OldPasswordCompare { get; set; }
        public string OldPasswordCompare1 { get; set; }
        public string AddressLine2 { get; set; }

        [Required(ErrorMessage = "Please enter ZIP")]
        public string Zip { get; set; }
        public string ProfilePic {get; set;}
        public string EncryptedSessionToken { get; set; }
        public string EncryptedUserId { get; set; }

        public int New{ get; set; }
        public bool IsEnabled  { get; set; }
        public int IsEnabledId  { get; set; }
        public string SessionToken { get; set; }
        public string ISDCode { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [StringLength(40, ErrorMessage = "Max 40 characters")]
        [DisplayName("Password :")]
        public string Password { get; set; }

        public string OldPassword { get; set; }
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [StringLength(40, ErrorMessage = "Max 40 characters")]
        [System.ComponentModel.DataAnnotations.CompareAttribute("Password")]
        [DisplayName("Confirm Password :")]
        public string ConfirmPassword { get; set; }

        public string CurrentPassword { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }

        public string Title { get; set; }

        public string UserTitle { get; set; }
        [Required(ErrorMessage = "Required")]
        public int TitleId { get; set; }
        public int TimeOffSet { get; set; }
        public List<SelectListItem> _TitleList { get; set; }

        public string Status { get; set; }

        public int MemberTypeId { get; set; }
        public string MemberType { get; set; }
        public bool IsProfileComplete { get; set; }
        public string UserCode { get; set; }
        public string Search { get; set; }
        public string TitleSearch { get; set; }
        public List<UserModel> _UserList { get; set; }
        public List<MenuModel> _MenuList { get; set; }
        public List<PermissionModel> _PermissionList { get; set; }
        
        public List<SelectListItem> _CountryList { get; set; }
        public List<SelectListItem> _StateList { get; set; }
        public List<SelectListItem> _TimeZoneList { get; set; }
        public List<SelectListItem> _ISDCodeList { get; set; }
        public ChangePasswordModell ChangePassworMMOdel { get; set; }
    }
    public class LoginModel
    {
        [DataMember]
        [Required(ErrorMessage = "Please enter email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter valid email")]
        public string Email { get; set; }


        [DataMember]
        [Required(ErrorMessage = "Please enter OTP")]
        //[RegularExpression("^[0-9]$", ErrorMessage = "Please enter valid OTP")]
        public string OTP { get; set; }


        public bool Remember { get; set; }
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        // [MembershipPassword(MinRequiredNonAlphanumericCharacters = 1, MinNonAlphanumericCharactersError = "Your password needs to contain at least one symbol (!, @, #, etc).", ErrorMessage = "Your password must be 8 characters long and contain at least one symbol (!, @, #, etc).")]
        public string Password { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceType { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public string OtpMessage { get; set; }
    }
    public class ForgotPasswordModel
    {
        public Int32 UserId { get; set; }
        public Int32 ReturnValue { get; set; }
        public String Username { get; set; }
        [Required(ErrorMessage = "Required")]
        [StringLength(100, ErrorMessage = "Max 100 characters")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Invalid email")]
        public String Email { get; set; }
        public String Name { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string EncryptedUserId { get; set; }
    }
    public class ResetPasswordModel
    {
        public Int32 ReturnValue { get; set; }

        [Required(ErrorMessage = "Required")]
        public Nullable<Int32> UserId { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [StringLength(40, ErrorMessage = "Max 40 characters")]
        [DisplayName("Password :")]
        public String Password { get; set; }
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [StringLength(40, ErrorMessage = "Max 40 characters")]
        [System.ComponentModel.DataAnnotations.CompareAttribute("Password")]
        [DisplayName("Confirm Password :")]
        public String ConfirmPassword { get; set; }
    }

    public class UserResponse
    {
        public int ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public UserModel UserModel { get; set; }
        
    }
    public class UserVerificationResponse
    {
        public int ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public UserModel UserModel { get; set; }

    }


    public class ChangePasswordModell
    {
        public string UserId { get; set; }
        public Int32 ReturnValue { get; set; }
        public int ISEmailSend { get; set; }
        public string SessionToken { get; set; }
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DisplayName("OldPassword")]
        public string OldPassWord { get; set; }
        [Required(ErrorMessage = "Required")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DisplayName("Password")]
        public String Password { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [System.ComponentModel.DataAnnotations.CompareAttribute("Password")]
        [DisplayName("Confirm Password")]
        public String ConfirmPassword { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string MobileNumber { get; set; }
        public int ReturnValues { get; set; }
    }
    public class SignupRespone
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public SignupModel SignupModel { get; set; }

    }
    public class SignupModel
    {
        [DataMember]
        [Required(ErrorMessage = "Please enter email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter valid email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter mobile")]
        public string MobileNumber { get; set; }

        [Required(ErrorMessage = "Please enter password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }
    }
    public class LoginRespone
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public UserModel UserDetail { get; set; }
        public UserModel UserInfo { get; set; }
    }
    public class ForgotPasswordResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
    }
    public class DashboardRequest
    {
        public string UserId { get; set; }
        public string SessionToken { get; set; }
    }
    public class PermissionModel
    {
        public Int32 MenuId { get; set; }
        public Int32 UserId { get; set; }
        public bool Access { get; set; } //View
        public bool Create { get; set; } //Insert
        public bool Edit { get; set; }  //Update
        public bool Delete { get; set; } //Delete
    }

    public class ImageCropModel
    {
        public String BaseData { get; set; }
    }
}
