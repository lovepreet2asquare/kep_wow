﻿using System.Web;
using System.Web.Optimization;

namespace FamousPerson
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-3.3.1.min.js"                                                 
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      //"~/Scripts/bootstrap.js",                      
                      "~/Scripts/jquery-ui.min.js",
                      "~/Scripts/jquery.validate.min.js",
                      "~/Scripts/jquery.validate.unobtrusive.min.js",
                     "~/Scripts/custom.js",
                     "~/content/js/vendors.bundle.js",
                      "~/content/js/scripts.bundle.js",
                      "~/Content/js/datatables.bundle.js",
                      //"~/Content/js/paginations.js",
                      "~/content/js/fullcalendar.bundle.js",
                        //"~/content/js/dashboard.js",D:\BitBucket\Famous_Person\FamousPerson\Content\js\paginations.js
                        //"~/content/js/morris-charts.js",
                        // "~/content/js/userdashboard.js",
                        //"~/Content/js/column-rendering.js",
                        "~/Scripts/sweetalert-dev.js",
                      "~/content/js/basic.js",
                      "~/Scripts/FP.functions.js",
                       "~/Scripts/widgets/bootstrap-datetimepicker.js",
                       "~/Scripts/Calender.js",
                       "~/Content/wickedpicker/wickedpicker.min.js",
                         "~/Content/wickedpicker/wickoptions.js"

                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/bootstrap-datetimepicker.css",
                      "~/Content/css/vendors.bundle.css",
                      "~/Content/css/style.bundle.css",
                      "~/Content/css/fullcalendar.bundle.css",
                      "~/Content/css/style.css",
                      "~/Content/css/mobile_style.css",
                      "~/Content/css/datatables.bundle.css",
                     "~/Content/css/sweetalert.css",
                     "~/Content/css/multiple-select.css",
                     "~/Content/wickedpicker/wickedpicker.css",
                     "~/Content/css/jquery-ui.min.css",
                      "~/rcrop/dist/rcrop.min.css",
                     "~/rcrop/dist/rcrop-custom.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/rcrop").Include(
                     // "~/rcrop/libs/jquery.js",
                     "~/rcrop/dist/rcrop.min.js",
                     "~/rcrop/dist/rcrop-custom.js"
                     ));
        }
    }
}
