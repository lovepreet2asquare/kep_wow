﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using FPModels.Models;
using FPBAL.Business;
using FPBAL.Interface;
using System.Web;
using System.Threading.Tasks;

namespace FamousPerson.Areas.API.Controllers
{
    [RoutePrefix("apis/1/user")]
    public class FPAPIController : ApiController
    {
        [Route("signup")]
        [HttpPost]
        [ResponseType(typeof(SignUpRespone))]
        public IHttpActionResult SignUp(SignUpAPIModel request)
        {
            IAppUser user = new AppUserAccess();
            SignUpRespone serviceResponse = user.SignUp(request);
            return Ok<SignUpRespone>(serviceResponse);
        }

        [Route("login")]
        [HttpPost]
        [ResponseType(typeof(SignUpRespone))]
        public IHttpActionResult Login(LoginRequest request)
        {
            IAppUser user = new AppUserAccess();
            SignUpRespone serviceResponse = user.Login(request);
            return Ok<SignUpRespone>(serviceResponse);
        }

        [Route("forgotpassword")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult ForgotPassword(ForgotRequest request)
        {
            IAppUser user = new AppUserAccess();
            FPResponse serviceResponse = user.ForgotPassword(request.Email);

            return Ok<FPResponse>(serviceResponse);
        }



        [Route("newsdetail")]
        [HttpPost]
        [ResponseType(typeof(FPFollowerNewsDetailResponse))]
        public IHttpActionResult FollowerNewsDetail(FolloerNewsDetailRequest request)
        {
            INewsAPI user = new NewsAPIAccess();
            FPFollowerNewsDetailResponse serviceResponse = user.FollowerNewsDetail(request);
            return Ok<FPFollowerNewsDetailResponse>(serviceResponse);
        }


        [Route("newslist")]
        [HttpPost]
        [ResponseType(typeof(FPFollowerNewsResponse))]
        public IHttpActionResult FollowerNewsList(FollowerNewsRequest request)
        {

            INewsAPI user = new NewsAPIAccess();
            FPFollowerNewsResponse serviceResponse = user.FollowerNewsList(request);
            return Ok<FPFollowerNewsResponse>(serviceResponse);

        }

        [Route("exclusiveheadlines")]
        [HttpPost]
        [ResponseType(typeof(FPHeadlineAPIResponse))]
        public IHttpActionResult FollowerHeadlines(FollowerHeadlinesRequest request)
        {
            IAppHeadlines user = new AppHeadlineAccess();
            FPHeadlineAPIResponse serviceResponse = user.FollowerHeadline(request);

            return Ok<FPHeadlineAPIResponse>(serviceResponse);
        }
        [Route("headlinedetail")]
        [HttpPost]
        [ResponseType(typeof(FPHeadlineDetailAPIResponse))]
        public IHttpActionResult FollowerHeadlineDetail(FollowerHeadlineDetailRequest request)
        {
            IAppHeadlines user = new AppHeadlineAccess();
            FPHeadlineDetailAPIResponse serviceResponse = user.FollowerHeadlineDetail(request);

            return Ok<FPHeadlineDetailAPIResponse>(serviceResponse);
        }
        [Route("getcommondata")]
        [HttpGet]
        [ResponseType(typeof(CommonDataRespone))]
        public IHttpActionResult GetCommonData()
        {
            IAppUser user = new AppUserAccess();
            CommonDataRespone serviceResponse = user.GetCommonData();
            return Ok<CommonDataRespone>(serviceResponse);
        }

        [Route("profilepicupload")]
        //[HttpPost]
        [AllowAnonymous]
        [ResponseType(typeof(HttpResponseMessage))]
        public async Task<HttpResponseMessage> ProfilePicUpload(string FollowerId)
        {
            //userId = userId.Replace(" ", "+");
            //sessionToken = sessionToken.Replace(" ", "+");
            FollowerId = FollowerId.Replace(" ", "+");
            //userId = UtilityAccess.Decrypt(userId);
            //sessionToken = UtilityAccess.Decrypt(sessionToken);
            FollowerId = UtilityAccess.Decrypt(FollowerId);
            string rootPath = string.Empty;
            int returnResult = 0;
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    string filename = "";//DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + Convert.ToString(userId);

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB  
                        //string baseRoot = HttpContext.Current.Server.MapPath("~/ProfilePics/");
                        //string root = baseRoot + userId.ToString();
                        string baseRoot = HttpContext.Current.Server.MapPath("~/Follower/");
                        string root = baseRoot + FollowerId + "/" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                        rootPath = "Follower/" + FollowerId + "/" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "/";
                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");

                            //dict.Add("error", message);
                            dict.Add("ReturnCode", "0");
                            dict.Add("ReturnMessage", message);
                            dict.Add("FilePath", rootPath);
                            return Request.CreateResponse(HttpStatusCode.OK, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {
                            var message = string.Format("Please Upload a file upto 1 mb.");

                            //dict.Add("error", message);
                            dict.Add("ReturnCode", "0");
                            dict.Add("ReturnMessage", message);
                            dict.Add("FilePath", rootPath);
                            return Request.CreateResponse(HttpStatusCode.OK, dict);
                        }
                        else
                        {
                            if (!System.IO.Directory.Exists(root))
                                System.IO.Directory.CreateDirectory(root);
                            //filename = filename + extension;
                            var filePath = root + "\\" + postedFile.FileName; // postedFile.FileName;
                            postedFile.SaveAs(filePath);

                            filename = rootPath + postedFile.FileName;
                            returnResult = 1;
                            //returnResult = UserAccessLayer.ProfilePicUpdate(Convert.ToInt32(userId), sessionToken, filename);
                        }
                    }
                    if (returnResult == 1)
                    {
                        var message1 = string.Format("Uploaded Successfully.");
                        dict.Add("ReturnCode", "1");
                        dict.Add("ReturnMessage", message1);
                        dict.Add("FilePath", rootPath);
                    }
                    else
                    {
                        dict.Add("ReturnCode", "0");
                        dict.Add("ReturnMessage", "");
                        dict.Add("FilePath", "");
                    }

                    //return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
                    return Request.CreateResponse(HttpStatusCode.OK, dict);
                }
                var res = string.Format("Please Upload a image.");
                //dict.Add("error", res);
                dict.Add("ReturnCode", "0");
                dict.Add("ReturnMessage", res);
                dict.Add("FilePath", "");
                return Request.CreateResponse(HttpStatusCode.OK, dict);
            }
            catch (Exception ex)
            {
                var res = string.Format(ex.Message);
                //dict.Add("error", res);
                dict.Add("ReturnCode", "-1");
                dict.Add("ReturnMessage", res);
                dict.Add("FilePath", "");
                return Request.CreateResponse(HttpStatusCode.OK, dict);
            }
        }
        [Route("uploadprofilepic")]
        //[HttpPost]
        [AllowAnonymous]
        [ResponseType(typeof(HttpResponseMessage))]
        public async Task<HttpResponseMessage> UploadProfilePic(string FollowerId)
        {
            //userId = userId.Replace(" ", "+");
            //sessionToken = sessionToken.Replace(" ", "+");
            FollowerId = FollowerId.Replace(" ", "+");
            //userId = UtilityAccess.Decrypt(userId);
            //sessionToken = UtilityAccess.Decrypt(sessionToken);
            FollowerId = UtilityAccess.Decrypt(FollowerId);
            string rootPath = string.Empty;
            int returnResult = 0;
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    string filename = "";//DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + Convert.ToString(userId);

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB  
                        //string baseRoot = HttpContext.Current.Server.MapPath("~/ProfilePics/");
                        //string root = baseRoot + userId.ToString();
                        string baseRoot = HttpContext.Current.Server.MapPath("~/Follower/");
                        string root = baseRoot + FollowerId + "/" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                        rootPath = "Follower/" + FollowerId + "/" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "/";
                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");

                            //dict.Add("error", message);
                            dict.Add("ReturnCode", "0");
                            dict.Add("ReturnMessage", message);
                            dict.Add("FilePath", rootPath);
                            return Request.CreateResponse(HttpStatusCode.OK, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {
                            var message = string.Format("Please Upload a file upto 1 mb.");

                            //dict.Add("error", message);
                            dict.Add("ReturnCode", "0");
                            dict.Add("ReturnMessage", message);
                            dict.Add("FilePath", rootPath);
                            return Request.CreateResponse(HttpStatusCode.OK, dict);
                        }
                        else
                        {
                            if (!System.IO.Directory.Exists(root))
                                System.IO.Directory.CreateDirectory(root);
                            //filename = filename + extension;
                            var filePath = root + "\\" + postedFile.FileName; // postedFile.FileName;
                            postedFile.SaveAs(filePath);

                            filename = rootPath + postedFile.FileName;
                            returnResult = 1;
                            IAppUser user = new AppUserAccess();
                            ProfilePicAPIModel request = new ProfilePicAPIModel();
                            request.FollowerId = FollowerId;
                            request.ProfilePic = filename;
                            ProfileUpdateAPIRespone responseUpload = user.UploadProfilePic(request);
                            returnResult = Convert.ToInt32(responseUpload.ReturnCode);
                            
                        }
                    }
                    if (returnResult == 1)
                    {
                        var message1 = string.Format("Uploaded Successfully.");
                        dict.Add("ReturnCode", "1");
                        dict.Add("ReturnMessage", message1);
                        dict.Add("FilePath", filename);
                    }
                    else if (returnResult == -1)
                    {
                        dict.Add("ReturnCode", "-1");
                        dict.Add("ReturnMessage", "Technical error.");
                        dict.Add("FilePath", "");
                    }
                    else if (returnResult == -5)
                    {
                        dict.Add("ReturnCode", "0");
                        dict.Add("ReturnMessage", "Authentication failed.");
                        dict.Add("FilePath", "");
                    }
                    else if (returnResult == 0)
                    {
                        dict.Add("ReturnCode", "0");
                        dict.Add("ReturnMessage", "We hit a snag, please try again after some time.");
                        dict.Add("FilePath", "");
                    }

                    //return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
                    return Request.CreateResponse(HttpStatusCode.OK, dict);
                }
                var res = string.Format("Please Upload a image.");
                //dict.Add("error", res);
                dict.Add("ReturnCode", "0");
                dict.Add("ReturnMessage", res);
                dict.Add("FilePath", "");
                return Request.CreateResponse(HttpStatusCode.OK, dict);
            }
            catch (Exception ex)
            {
                var res = string.Format(ex.Message);
                //dict.Add("error", res);
                dict.Add("ReturnCode", "-1");
                dict.Add("ReturnMessage", res);
                dict.Add("FilePath", "");
                return Request.CreateResponse(HttpStatusCode.OK, dict);
            }
        }
        [Route("profileupdate")]
        [HttpPost]
        [ResponseType(typeof(ProfileUpdateAPIRespone))]
        public IHttpActionResult ProfileUpdate(UserAPIModel request)
        {
            IAppUser user = new AppUserAccess();
            ProfileUpdateAPIRespone serviceResponse = user.ProfileUpdate(request);
            return Ok<ProfileUpdateAPIRespone>(serviceResponse);
        }

        [Route("getinvolvedadd")]
        [HttpPost]
        [ResponseType(typeof(FPInvolvedResponse))]
        public IHttpActionResult AddInvolved(GetInvolvedRequest request)
        {
            IAppInvolved user = new InvolvedApiAccess();
            FPInvolvedResponse serviceResponse = user.AddInvolved(request);

            return Ok<FPInvolvedResponse>(serviceResponse);
        }

        [Route("deleteaccount")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult DeleteAccount(AccountDeleteRequest request)
        {
            IAppUser user = new AppUserAccess();
            FPResponse serviceResponse = user.DeleteAccount(request);

            return Ok<FPResponse>(serviceResponse);
        }

        [Route("logout")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult Logout(LogoutRequest request)
        {
            IAppUser user = new AppUserAccess();
            FPResponse serviceResponse = user.Logout(request);

            return Ok<FPResponse>(serviceResponse);
        }

        [Route("getevents")]
        [HttpPost]
        [ResponseType(typeof(FPEventResponse))]
        public IHttpActionResult EventSelect(FPEventRequest request)
        {
            IEvent user = new EventApiAccess();
            FPEventResponse serviceResponse = user.EventSelect(request);

            return Ok<FPEventResponse>(serviceResponse);
        }

        [Route("getcampaignlocations")]
        [HttpPost]
        [ResponseType(typeof(FPOfficeLocationResponse))]
        public IHttpActionResult GetCampaignLocations(EpAboutRequest request)
        {
            IEvent user = new EventApiAccess();
            FPOfficeLocationResponse serviceResponse = user.OfficeLocationSelect(request);

            return Ok<FPOfficeLocationResponse>(serviceResponse);
        }

        [Route("getaboutdetail")]
        [HttpPost]
        [ResponseType(typeof(EPAboutResponse))]
        public IHttpActionResult getaboutdetail(EpAboutRequest request)
        {
            IEvent user = new EventApiAccess();
            EPAboutResponse serviceResponse = user.aboutSelect(request);

            return Ok<EPAboutResponse>(serviceResponse);
        }

        [Route("contactus")]
        [HttpPost]
        [ResponseType(typeof(FPInvolvedResponse))]
        public IHttpActionResult AddContactUs(EPContactUsRequest request)
        {
            IContactUs user = new ContactUsAccess();
            FPInvolvedResponse serviceResponse = user.AddContactUs(request);

            return Ok<FPInvolvedResponse>(serviceResponse);
        }
        [Route("changepassword")]
        [HttpPost]
        [ResponseType(typeof(FPInvolvedResponse))]
        public IHttpActionResult ChangePassword([FromBody] ChangePasswordApiModel request)
        {
            IContactUs user = new ContactUsAccess();
            FPInvolvedResponse serviceResponse = user.ChangePassword(request);

            return Ok<FPInvolvedResponse>(serviceResponse);
        }
        [Route("notificationsetting")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult NotificationSetting(UpdateSettingRequest request)
        {
            INotificationAPI user = new NotificationApiAccess();
            FPResponse serviceResponse = user.UpdateNotification(request);

            return Ok<FPResponse>(serviceResponse);
        }
        [Route("getinboxmessages")]
        [HttpPost]
        [ResponseType(typeof(InboxApiResponse))]
        public IHttpActionResult GetInboxMessage(FPInboxApiRequest request)
        {
            IInbox user = new InboxApiAccess();
            InboxApiResponse serviceResponse = user.InboxMessageSelect(request);

            return Ok<InboxApiResponse>(serviceResponse);
        }
        [Route("getmessagedetail")]
        [HttpPost]
        [ResponseType(typeof(InboxDetailApiResponse))]
        public IHttpActionResult InAppMessageDetail(InAppMessageDetailApiRequest request)
        {
            IInbox user = new InboxApiAccess();
            InboxDetailApiResponse serviceResponse = user.InAppMessageDetail(request);

            return Ok<InboxDetailApiResponse>(serviceResponse);
        }

        [Route("sharemessagecount")]
        [HttpPost]
        [ResponseType(typeof(FP_ShareAndOpenApiResponse))]
        public IHttpActionResult ShareMessage(FP_ShareAndOpenApiRequest request)
        {
            IShareAndOpenApi user = new ShareAndOpenApiAccess();
            FP_ShareAndOpenApiResponse serviceResponse = user.ShareCount(request);

            return Ok<FP_ShareAndOpenApiResponse>(serviceResponse);
        }

        [Route("openinboxmessage")]
        [HttpPost]
        [ResponseType(typeof(FP_ShareAndOpenApiResponse))]
        public IHttpActionResult OpenMessage(FP_ShareAndOpenApiRequest request)
        {
            IShareAndOpenApi user = new ShareAndOpenApiAccess();
            FP_ShareAndOpenApiResponse serviceResponse = user.OpenMessage(request);

            return Ok<FP_ShareAndOpenApiResponse>(serviceResponse);
        }

        [Route("headlineviewercount")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult HeadLineViewerInsert(FP_HeadlineViewerAndShareRequest request)
        {
            IApiHeadlineViewerAndShare user = new ApiHeadlineViewerAndShareAccess();
            FPResponse serviceResponse = user.HeadLineViewerInsert(request);

            return Ok<FPResponse>(serviceResponse);
        }

        [Route("headlinesharecount")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult HeadLineShareInsert(FP_HeadlineViewerAndShareRequest request)
        {
            IApiHeadlineViewerAndShare user = new ApiHeadlineViewerAndShareAccess();
            FPResponse serviceResponse = user.HeadLineShareInsert(request);

            return Ok<FPResponse>(serviceResponse);
        }

        [Route("newsviewercount")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult NewsViewerInsert(FPNewsViewerRequest request)
        {
            INewsViewer user = new NewsViewerApiAccess();
            FPResponse serviceResponse = user.NewsViewerInsert(request);

            return Ok<FPResponse>(serviceResponse);
        }

        [Route("newssharecount")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult NewsShareInsert(FPNewsViewerRequest request)
        {
            INewsViewer user = new NewsViewerApiAccess();
            FPResponse serviceResponse = user.NewsShareInsert(request);

            return Ok<FPResponse>(serviceResponse);
        }
        [Route("getfollowerinfo")]
        [HttpPost]
        [ResponseType(typeof(SignUpRespone))]
        public IHttpActionResult GetFollowerInfo(LogoutRequest request)
        {
            IAppUser user = new AppUserAccess();
            SignUpRespone serviceResponse = user.GetFollowerInfo(request);

            return Ok<SignUpRespone>(serviceResponse);
        }

        [Route("getinvolvebyemail")]
        [HttpPost]
        [ResponseType(typeof(FP_InvolvedApiResponse))]
        public IHttpActionResult GetInvolveByEmail(FP_InvolvedApiRequest request)
        {
            IAppInvolved user = new InvolvedApiAccess();
            FP_InvolvedApiResponse serviceResponse = user.GetInvolveByEmail(request);

            return Ok<FP_InvolvedApiResponse>(serviceResponse);
        }

        [Route("paymentmethodinsert")]
        [HttpPost]
        [ResponseType(typeof(PaymentMethodResponse))]
        public IHttpActionResult PaymentMethodInsert(CustProfileInfo request)
        {
            IAuthCustomerProfile obj = new AuthCustomerProfileAccess();
            PaymentMethodResponse serviceResponse = obj.PaymentMethodInsert(request);

            return Ok<PaymentMethodResponse>(serviceResponse);
        }
        [Route("getpaymentmethods")]
        [HttpPost]
        [ResponseType(typeof(PaymentMethodsResponse))]
        public IHttpActionResult GetPaymentMethods(FPRequests request)
        {
            IAuthCustomerProfile obj = new AuthCustomerProfileAccess();
            PaymentMethodsResponse serviceResponse = obj.GetPaymentMethods(request);

            return Ok<PaymentMethodsResponse>(serviceResponse);
        }
        [Route("paymentmethoddelete")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult PaymentMethodDelete(DeletePaymentMethodRequest request)
        {
            IAuthCustomerProfile obj = new AuthCustomerProfileAccess();
            FPResponse serviceResponse = obj.PaymentMethodDelete(request);

            return Ok<FPResponse>(serviceResponse);
        }
        [Route("paymentmethodsetprimary")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult PaymentMethodSetPrimary(SetPrimaryRequest request)
        {
            IAuthCustomerProfile obj = new AuthCustomerProfileAccess();
            FPResponse serviceResponse = obj.PaymentMethodSetPrimary(request);

            return Ok<FPResponse>(serviceResponse);
        }

        [Route("donate")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult  Donate(DonateAPIModel request)
        {
            IAuthCustomerProfile obj = new AuthCustomerProfileAccess();
            FPResponse serviceResponse = obj.Donate(request);

            return Ok<FPResponse>(serviceResponse);
        }


        [Route("sendfriendinvitation")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult SendFriendInvitations(FPInviteRequests request)
        {
            IAppUser user = new AppUserAccess();
            FPResponse serviceResponse = user.SendFriendInvitations(request);

            return Ok<FPResponse>(serviceResponse);
        }

        [Route("validateemail")]
        [HttpPost]
        [ResponseType(typeof(ProfileUpdateAPIRespone))]
        public IHttpActionResult ValidateEmail(FPRequests request)
        {
            IAppUser user = new AppUserAccess();
            ProfileUpdateAPIRespone serviceResponse = user.ValidateEmail(request);

            return Ok<ProfileUpdateAPIRespone>(serviceResponse);
        }

        [Route("SendApnsNotification")]
        [HttpGet]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult ApnsNotification(string deviceToken, string Message)
        {
            IAppUser user = new AppUserAccess();
            FPResponse serviceResponse = user.ApnsNotification(deviceToken, Message);

            return Ok<FPResponse>(serviceResponse);
        }


        

    }

}
