//////dashboard-second nav -extednd
//$('select').on("change", function () {
//    //alert($(this).attr('id'));
//    
//    var myVal = $(this).val();
//    $(this).find('option:selected').removeProp("selected");
//    //$(this).find('option[value="' + myVal + '"]').attr("selected", "selected");
//   // $(this).find('option[value="' + $(this).val() + '"]').prop("selected", "selected");
//});

////dashboard-second nav -extednd
//$(document).ready(function () {
//    //setTimeout(function () {
//    //debugger;
//    if ($('.formatDate').length > 0) {
//        $('.formatDate').each(function () {
//            $(this).text(GetLocalTimeFromUTC($(this).text()));
//        });
//    }
//    //}, 500);
//});
//$(document).on("click", ".ui-datepicker-close", function (event) {
//    //debugger;
//    var aa = event.target;
//    //var clickedBtnID = $(this).attr('id'); // or var clickedBtnID = this.id
//    //alert('you clicked on button #' + clickedBtnID);
//});
$(window).load(function () {
    if ($('.formatDate').length > 0) {
        $('.formatDate').each(function () {
            $(this).text(GetLocalTimeFromUTC($(this).text()));
        });
    }
});
$(document).on('keyup', '.int', function (event) {
    this.value = this.value.replace(/[^0-9]/g, '');
});

$(document).on('blur', '.decimal', function (btnEvent) {
    var num = parseFloat($(this).val());
    var num_ = $(this).val();
    var nan = isNaN(num);
    if (nan == false) {
        if (num > 999999) {
            num = num_.substring(0, 6);
            num = parseFloat(num);
        }
        var cleanNum = num.toFixed(2);
        $(this).val(cleanNum);
    }
});

$(document).on('keypress', '.decimal', function (event) {
    var myVal = parseInt($(this).val());
    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 16 || event.keyCode == 37 || event.keyCode == 39) { }
    else if (myVal > 999999) event.preventDefault();
    else {
        if (((event.which != 46 || (event.which == 46 && $(this).val() == '')) ||
            $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    }
});
//    .on('paste', function (event) {
//    event.preventDefault();
//});

var minutes;
function calculate_time_zone() {
    //
    //if (localStorage["LocalTime"] != null)
    //{ var hdnVal_ = localStorage["LocalTime"]; }

    //if (hdnVal_.length == 0) {
    localStorage["TimeZone"] != null;
    var rightNow = new Date();
    var jan1 = new Date(rightNow.getFullYear(), 0, 1, 0, 0, 0, 0);  // jan 1st
    var june1 = new Date(rightNow.getFullYear(), 6, 1, 0, 0, 0, 0); // june 1st
    var temp = jan1.toGMTString();
    var jan2 = new Date(temp.substring(0, temp.lastIndexOf(" ") - 1));
    temp = june1.toGMTString();
    var june2 = new Date(temp.substring(0, temp.lastIndexOf(" ") - 1));
    var std_time_offset = (jan1 - jan2) / (1000 * 60 * 60);
    var daylight_time_offset = (june1 - june2) / (1000 * 60 * 60);
    var dst;
    if (std_time_offset == daylight_time_offset) {
        dst = "0"; // daylight savings time is NOT observed
    } else {
        // positive is southern, negative is northern hemisphere
        var hemisphere = daylight_time_offset - std_time_offset;
        if (hemisphere >= 0) {
            std_time_offset = daylight_time_offset;
            dst = "1"; // daylight savings time is observed
        }
    }
    var i;
    // Here set the value of hidden field to the ClientTimeZone.
    return minutes = localStorage["LocalTime"] = convert(std_time_offset);
    //TimeField = document.getElementById("<%=HiddenFieldClientTime.ClientID %>");
    //TimeField.value = minutes;
    //document.getElementById("<%=OffsetTimeButton.ClientID %>").click();
    //("<%=HiddenFieldClientTime.ClientID %>")     
    //}
}

function formatDate(dateVal) {
    var newDate = new Date(dateVal);

    //var sMonth = padValue(newDate.getMonth() + 1);
    var dtMonth = padValue(newDate.getMonth() + 1);
    var sDay = padValue(newDate.getDate());
    var sYear = newDate.getFullYear();
    var sHour = newDate.getHours();
    var sMinute = padValue(newDate.getMinutes());

    if (isNaN(dtMonth))
        return;
    var sAMPM = "AM";

    var iHourCheck = parseInt(sHour);

    if (iHourCheck > 12) {
        sAMPM = "PM";
        sHour = iHourCheck - 12;
    }
    else if (iHourCheck === 0) {
        sHour = "12";
    }

    sHour = padValue(sHour);

    //var monthsArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    //var selectedMonthName = monthsArray.indexOf[sMonth];    
    switch (dtMonth) {
        case '01':
            dtMonth = 'Jan';
            break;
        case '02':
            dtMonth = 'Feb';
            break;
        case '03':
            dtMonth = 'Mar';
            break;
        case '04':
            dtMonth = 'Apr';
            break;
        case '05':
            dtMonth = 'May';
            break;
        case '06':
            dtMonth = 'Jun';
            break;
        case '07':
            dtMonth = 'Jul';
            break;
        case '08':
            dtMonth = 'Aug';
            break;
        case '09':
            dtMonth = 'Sep';
            break;
        case '10':
            dtMonth = 'Oct';
            break;
        case '11':
            dtMonth = 'Nov';
            break;
        case '12':
            dtMonth = 'Dec';
            break;
    }
    return sDay + "-" + dtMonth + "-" + sYear + " " + sHour + ":" + sMinute + " " + sAMPM;
}

function padValue(value) {
    return (value < 10) ? "0" + value : value;
}
// This function is to convert the timezoneoffset to Standard format
function convert(value) {
    var hours = parseInt(value);
    value -= parseInt(value);
    value *= 60;
    var mins = parseInt(value);
    value -= parseInt(value);
    value *= 60;
    var secs = parseInt(value);
    var display_hours = hours;
    // handle GMT case (00:00)
    if (hours == 0) {
        display_hours = "00";
    } else if (hours > 0) {
        // add a plus sign and perhaps an extra 0
        display_hours = (hours < 10) ? "+0" + hours : "+" + hours;
    } else {
        // add an extra 0 if needed
        display_hours = (hours > -10) ? "-0" + Math.abs(hours) : hours;
    }
    mins = (mins < 10) ? "0" + mins : mins;
    return display_hours + ":" + mins;
}
function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}
function convertOffset(gmt_offset) {
    var time = gmt_offset.toString().split(":");
    var hour = parseInt(time[0]);
    var minute = parseInt(time[1]);
    var preVal = hour < 0 ? "-" : "+";
    hour = Math.abs(hour) < 10 ? "0" + Math.abs(hour) : Math.abs(hour);
    //hour = negative ? "-" + hour : "+" + hour;
    return preVal + (parseInt(hour * 60) + parseInt(minute));
}
function GetLocalTimeFromUTC(date) {
    //debugger;
    var dateArr = date.split('-');
    var val = dateArr[1] + '/' + dateArr[0] + '/' + dateArr[2];
    var dt_ = new Date(val);
    if (localStorage["LocalTime"] == null) {
        calculate_time_zone();
    }

    var TimeZoneHours = convertOffset(localStorage["LocalTime"]);
    //var TimeZone_firstString = MILLISECS_PER_HOUR.charAt(0);
    //var TimeZoneHours = parseInt(MILLISECS_PER_HOUR.substring(1));
    var MILLISECS_PER_HOUR = TimeZoneHours /* min/hour */ * 60 /* sec/min */ * 1000 /* ms/s */;
    var newDate_ = new Date(+dt_ + MILLISECS_PER_HOUR);
    return formatDate(newDate_);
}
//setTimeout(function () {
//    //debugger;
//    if ($('.formatDate').length > 0) {
//        $('.formatDate').each(function () {
//            $(this).text(GetLocalTimeFromUTC($(this).text()));
//        });
//    }
//}, 500);



//$("body").on("input propertychange", ".USMobileFormat", function (e) {
//    $(this).attr('maxlength', '14');
//    $(this).mask('(000) 000-0000');
//});


$(document).ready(function () {
   

    if (readCookie("time_zone") == null) {
        calculate_time_zone();
        createCookie("time_zone", localStorage["LocalTime"], 100);
    }
    var m = 0;
    $(keepAliveFunc());
    function keepAliveFunc() {
        setTimeout(function () { keepAlive(); }, 1080000);
    };

    function keepAlive() {
        $.get('/Home/KeepAlive', {}, function (msg) { m++; if (m < 3) { keepAliveFunc(); } });
    };

    if ($('.USMobileFormat').length > 0) {
        $('.USMobileFormat').attr('maxlength', '14');
        var phone = document.getElementsByClassName("USMobileFormat");
        for (var i = 0; i < phone.length; i++) {
            phone[i].addEventListener('input', function (e) {
                var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
                e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
            });
        }
    }
    var Cur_date = new Date();
    $('.header').css("z-index", "1");
    var currentDateTime_ = new Date();
    var currentYear_ = currentDateTime_.getFullYear();
    var YearsBack_18 = currentYear_ - 18;
    var nextYear_ = currentYear_ + 1;
    //myCalendar start
    function getDOBYear() {
        // ;
        var currentDateTime_ = new Date();
        var currentYear_ = currentDateTime_.getFullYear();
        var YearBack_18 = currentYear_ - 18;
        return YearBack_18;
    }
    //$(function () {
    //var YearBackVal_ = getDOBYear();
    $("body").on("mouseenter", "input.empDOB", function () {
        var topHeight = $(this).offset().top + $(this).height() + 10;
        var offsetLeft = $(this).offset().left;
        $(this).datepicker({
            dateFormat: "dd-M-yy",
            defaultDate: '01-Jan-2003',
            minDate: "01-Jan-1930",
            yearRange: "-87:-14",
            //maxDate: "31/01/2100",
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            //maxDate: '0',
            maxDate: "31-Dec-2003",
            closeText: 'Clear',
            beforeShow: function (input, inst) {
                setTimeout(function () {
                    inst.dpDiv.css({
                        top: topHeight,
                        left: offsetLeft
                    });
                }, 0)
            },
            //showOn: "button",
            //buttonImage: "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            //buttonImageOnly: true,
            onClose: function (selectedDate) {
                var event = arguments.callee.caller.caller.arguments[0];
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    //if ($(window.event.srcElement).hasClass('ui-datepicker-close')){
                    $(this).val('');
                }  
                if (selectedDate.length > 0) {
                    var dt_ = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('option', 'minDate');
                    var maxDate = $(this).datepicker('option', 'maxDate');
                    var minDate_ = new Date(minDate);
                    var maxDate_ = new Date(maxDate);
                    if (!isDate(selectedDate)) {
                        alert('Invalid Date');
                        $(this).val(''); return;
                    }
                    if (dt_ < minDate_)
                        $(this).datepicker('setDate', minDate);
                    if (dt_ > maxDate_)
                        $(this).datepicker('setDate', maxDate);

                }
            }
            //showOn: "button",
            //buttonImage: "images/calendar.gif",
            //buttonImageOnly: true,
            //timeFormat: "hh:mm tt"
        });
    });

    $("body").on("mouseenter", "input.empDOJ", function () {
        var topHeight = $(this).offset().top + $(this).height() + 10;
        var offsetLeft = $(this).offset().left;
        $(this).datepicker({
            dateFormat: "dd-M-yy",
            yearRange: "" + currentYear_ + ":" + currentYear_ + "",
            minDate: "-14",
            maxDate: '14',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            closeText: 'Clear',
            beforeShow: function (input, inst) {
                setTimeout(function () {
                    inst.dpDiv.css({
                        top: topHeight,
                        left: offsetLeft
                    });
                }, 0)
            },
            onClose: function (selectedDate) {
                var event = arguments.callee.caller.caller.arguments[0];
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    //if ($(window.event.srcElement).hasClass('ui-datepicker-close')){
                    $(this).val('');
                }   
                if (selectedDate.length > 0) {
                    var dt_ = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('option', 'minDate');
                    var maxDate = $(this).datepicker('option', 'maxDate');
                    var prev15 = new Date();
                    prev15.setDate(currentDateTime_.getDate() - 15);
                    var next15 = new Date();
                    next15.setDate(currentDateTime_.getDate() + 15);
                    if (dt_ < prev15)
                        $(this).datepicker('setDate', minDate);
                    if (dt_ > next15)
                        $(this).datepicker('setDate', maxDate);
                    if (!isDate(selectedDate)) {
                        alert('Invalid Date');
                        $(this).val('');
                    }
                    //setTimeout(function () { $("#" + Current_Caledar_txtBox).focus() }, 300);
                }
            }

        });
    });
    var date = new Date();
    //$(".todayDate").text('Today | ' + $.datepicker.formatDate('M dd, yy', new Date()));//((date.getMonth() + 1) + "-" + date.getDate() + "-" + date.getFullYear()));
    $(".todayDate").text('Today | ' + $.datepicker.formatDate('mm/dd/yy', new Date()));
    //debugger;   
    if ($.trim($(".todayDate").val()) == '')
        $(".todayDate").val($.datepicker.formatDate('mm/dd/yy', new Date()));// for textbox
    $(".transDate").text($.datepicker.formatDate('mm/dd/yy', new Date()));
    $(".transDate").val($.datepicker.formatDate('mm/dd/yy', new Date()));

    $("body").on("mouseenter", "input.StartDate", function () {
        $(this).datepicker("destroy");
        var topHeight = $(this).offset().top + $(this).height() + 10;
        var offsetLeft = $(this).offset().left;
        $(this).datepicker({
            dateFormat: "dd-M-yy",
            yearRange: "2017:" + currentYear_ + "",
            minDate: "01-Jan-2017",
            maxDate: '0',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            closeText: 'Clear',
            beforeShow: function (input, inst) {
                setTimeout(function () {
                    inst.dpDiv.css({
                        top: topHeight,
                        left: offsetLeft
                    });
                }, 0)
            },
            onClose: function (selectedDate) {
                var event = arguments.callee.caller.caller.arguments[0];
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    //if ($(window.event.srcElement).hasClass('ui-datepicker-close')){
                    $(this).val('');
                }  
                if (selectedDate.length > 0) {
                    var dt_ = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('option', 'minDate');
                    var maxDate = $(this).datepicker('option', 'maxDate');
                    var endDate = new Date($('.EndDate').val());
                    //if (dt_ > endDate) {
                    //    alert('End date can not be less than start date');
                    //    $(this).val('');
                    //}
                    if (!isDate(selectedDate)) {
                        alert('Invalid Date');
                        $(this).val('');
                    }
                    //setTimeout(function () { $("#" + Current_Caledar_txtBox).focus() }, 300);
                }
            }

        });
    });
    $("body").on("mouseenter", "input.EndDate", function () {
        //debugger;
        $(this).datepicker("destroy");
        var d = '01-Jan-2017';
        var startDate_ = '';
        //new Date($('.StartDate').val());
        if ($('.StartDate').val() != '') {
            //var d = new Date($('.StartDate').val());
            var dateArr = $('.StartDate').val().split('-');
            var val = dateArr[1] + '/' + dateArr[0] + '/' + dateArr[2];
            var d = new Date(val);

            var Start_Date = d.getDate();
            var Start_month = d.getMonth();
            var Start_year = d.getFullYear();
            startDate_ = Start_Date + "-" + m_names[Start_month] + "-" + Start_year;
        }
        var topHeight = $(this).offset().top + $(this).height() + 10;
        var offsetLeft = $(this).offset().left;
        $(this).datepicker({
            dateFormat: "dd-M-yy",
            yearRange: "2017:" + currentYear_ + "",
            minDate: startDate_,
            maxDate: '0',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            closeText: 'Clear',
            beforeShow: function (input, inst) {
                setTimeout(function () {
                    inst.dpDiv.css({
                        top: topHeight,
                        left: offsetLeft
                    });
                }, 0)
            },
            onClose: function (selectedDate) {
                var event = arguments.callee.caller.caller.arguments[0];
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    //if ($(window.event.srcElement).hasClass('ui-datepicker-close')){
                    $(this).val('');
                }  
                if (selectedDate.length > 0) {
                    var dt_ = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('option', 'minDate');
                    var maxDate = $(this).datepicker('option', 'maxDate');
                    var startDate = new Date($('.StartDate').val());
                    if (dt_ < startDate) {
                        alert('End date can not be less than start date');
                        $(this).val('');
                    }
                    if (!isDate(selectedDate)) {
                        alert('Invalid Date');
                        $(this).val('');
                    }
                    //setTimeout(function () { $("#" + Current_Caledar_txtBox).focus() }, 300);
                }
            }

        });
    });


    $("body").on("mouseenter", "input.ShiftStartDate", function () {
        //debugger;
        $(this).datepicker("destroy");
        var topHeight = $(this).offset().top + $(this).height() + 10;
        var offsetLeft = $(this).offset().left;
        $(this).datepicker({
            dateFormat: "dd-M-yy",
            yearRange: "2017:" + currentYear_ + "",
            minDate: "01-Jan-2017",
            maxDate: '60',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            closeText: 'Clear',
            beforeShow: function (input, inst) {
                setTimeout(function () {
                    inst.dpDiv.css({
                        top: topHeight,
                        left: offsetLeft
                    });
                }, 0)
            },
            onClose: function (selectedDate) {
                var event = arguments.callee.caller.caller.arguments[0];
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    //if ($(window.event.srcElement).hasClass('ui-datepicker-close')){
                    $(this).val('');
                }  
                if (selectedDate.length > 0) {
                    var dt_ = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('option', 'minDate');
                    var maxDate = $(this).datepicker('option', 'maxDate');
                    var endDate = new Date($('.ShiftEndDate').val());
                    if (dt_ > endDate) {
                        alert('End date can not be less than start date');
                        $(this).val('');
                    }
                    if (!isDate(selectedDate)) {
                        alert('Invalid Date');
                        $(this).val('');
                    }
                    //setTimeout(function () { $("#" + Current_Caledar_txtBox).focus() }, 300);
                }
            }

        });
    });

    $("body").on("mouseenter", "input.ShiftEndDate", function () {
        //debugger;
        $(this).datepicker("destroy");
        var d = '01-Jan-2017';
        var startDate_ = '';
        //new Date($('.StartDate').val());
        if ($('.ShiftStartDate').val() != '') {
            //var d = new Date($('.ShiftStartDate').val());
            var dateArr = $('.ShiftStartDate').val().split('-');
            var val = dateArr[1] + '/' + dateArr[0] + '/' + dateArr[2];
            var d = new Date(val);

            var Start_Date = d.getDate();
            var Start_month = d.getMonth();
            var Start_year = d.getFullYear();
            startDate_ = Start_Date + "-" + m_names[Start_month] + "-" + Start_year;
        }
        var topHeight = $(this).offset().top + $(this).height() + 10;
        var offsetLeft = $(this).offset().left;
        $(this).datepicker({
            dateFormat: "dd-M-yy",
            yearRange: "2017:" + currentYear_ + "",
            minDate: startDate_,
            maxDate: '60',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            closeText: 'Clear',
            beforeShow: function (input, inst) {
                setTimeout(function () {
                    inst.dpDiv.css({
                        top: topHeight,
                        left: offsetLeft
                    });
                }, 0)
            },
            onClose: function (selectedDate) {
                var event = arguments.callee.caller.caller.arguments[0];
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    //if ($(window.event.srcElement).hasClass('ui-datepicker-close')){
                    $(this).val('');
                }  
                if (selectedDate.length > 0) {
                    var dt_ = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('option', 'minDate');
                    var maxDate = $(this).datepicker('option', 'maxDate');
                    var startDate = new Date($('.ShiftStartDate').val());
                    if (dt_ < startDate) {
                        alert('End date can not be less than start date');
                        $(this).val('');
                    }
                    if (!isDate(selectedDate)) {
                        alert('Invalid Date');
                        $(this).val('');
                    }
                    //setTimeout(function () { $("#" + Current_Caledar_txtBox).focus() }, 300);
                }
            }

        });
    });

    $("body").on("mouseenter", "input.CurrentStartDate", function () {
        $(this).datepicker("destroy");
        var topHeight = $(this).offset().top + $(this).height() + 10;
        var offsetLeft = $(this).offset().left;
        $(this).datepicker({
            dateFormat: "dd-M-yy",
            yearRange: "" + currentYear_ + ":" + nextYear_ + "",
            minDate: "0",
            //maxDate: '0',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            closeText: 'Clear',
            beforeShow: function (input, inst) {
                setTimeout(function () {
                    inst.dpDiv.css({
                        top: topHeight,
                        left: offsetLeft
                    });
                }, 0)
            },
            onClose: function (selectedDate) {
                var event = arguments.callee.caller.caller.arguments[0];
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    //if ($(window.event.srcElement).hasClass('ui-datepicker-close')){
                    $(this).val('');
                }  
                if (selectedDate.length > 0) {
                    var dt_ = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('option', 'minDate');
                    var maxDate = $(this).datepicker('option', 'maxDate');
                    var endDate = new Date($('.CurrentEndDate').val());
                    if (dt_ > endDate) {
                        alert('End date can not be less than start date');
                        $(this).val('');
                    }
                    if (!isDate(selectedDate)) {
                        alert('Invalid Date');
                        $(this).val('');
                    }
                    //setTimeout(function () { $("#" + Current_Caledar_txtBox).focus() }, 300);
                }
            }

        });
    });

    $("body").on("mouseenter", "input.CurrentEndDate", function () {
        $(this).datepicker("destroy");
        //new Date($('.StartDate').val());
        var startDate_ = '';
        if ($('.CurrentStartDate').val() != '') {
            //var d = new Date($('.ShiftStartDate').val());
            var dateArr = $('.CurrentStartDate').val().split('-');
            var val = dateArr[1] + '/' + dateArr[0] + '/' + dateArr[2];
            var d = new Date(val);

            var Start_Date = d.getDate();
            var Start_month = d.getMonth();
            var Start_year = d.getFullYear();
            startDate_ = Start_Date + "-" + m_names[Start_month] + "-" + Start_year;
        }
        var topHeight = $(this).offset().top + $(this).height() + 10;
        var offsetLeft = $(this).offset().left;
        $(this).datepicker({
            dateFormat: "dd-M-yy",
            yearRange: "" + currentYear_ + ":" + nextYear_ + "",
            minDate: "0",
            //maxDate: '60',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            closeText: 'Clear',
            beforeShow: function (input, inst) {
                setTimeout(function () {
                    inst.dpDiv.css({
                        top: topHeight,
                        left: offsetLeft
                    });
                }, 0)
            },
            onClose: function (selectedDate) {
                var event = arguments.callee.caller.caller.arguments[0];
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    //if ($(window.event.srcElement).hasClass('ui-datepicker-close')){
                    $(this).val('');
                }
                if (selectedDate.length > 0) {
                    var dt_ = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('option', 'minDate');
                    var maxDate = $(this).datepicker('option', 'maxDate');
                    var startDate = new Date($('.CurrentStartDate').val());
                    if (dt_ < startDate) {
                        alert('End date can not be less than start date');
                        $(this).val('');
                    }
                    if (!isDate(selectedDate)) {
                        alert('Invalid Date');
                        $(this).val('');
                    }
                    //setTimeout(function () { $("#" + Current_Caledar_txtBox).focus() }, 300);
                }
            }
        });
    });
    $("body").on("mouseenter", "input.calendarWithNoPastDate", function (evt) {
        var topHeight = $(this).offset().top + $(this).height();
        var offsetLeft = $(this).offset().left;
        ////debugger;
        $(this).datepicker({
            dateFormat: "dd-M-yy",
            //timeFormat: "h:mm tt",
            minDate: '0',
            maxDate: '30',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            closeText: 'Clear',
            onClose: function (dateText, inst) {
                var event = arguments.callee.caller.caller.arguments[0];
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    //if ($(window.event.srcElement).hasClass('ui-datepicker-close')){
                    $(this).val('');
                }
            },
            beforeShow: function (input, inst) {
                setTimeout(function () {
                    inst.dpDiv.css({
                        top: $(evt.target).offset().top + 35,
                        left: $(evt.target).offset().left
                    });
                }, 0);
            }
            //showOn: "button",
            //buttonImage: "images/calendar.gif",
            //buttonImageOnly: true,
            //timeFormat: "hh:mm tt"
        });
    });
    $("body").on("mouseenter", "input.calendarDateTimeWithNoPastDate", function (evt) {
        try {
            var topHeight = $(this).offset().top + $(this).height();
            var offsetLeft = $(this).offset().left;
            //var todayDate = new Date().getDate();
            ////debugger;
            $(this).datetimepicker({
                dateFormat: "dd-M-yy",
                timeFormat: "h:mm tt",
                minDate: 0,
                maxDate: 30,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                closeText: 'Clear',
                onClose: function (dateText, inst) {
                    var event = arguments.callee.caller.caller.caller.arguments[0];
                    if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                        //if ($(window.event.srcElement).hasClass('ui-datepicker-close')){
                        $(this).val('');
                    }
                },
                beforeShow: function (input, inst) {
                    setTimeout(function () {
                        inst.dpDiv.css({
                            top: $(evt.target).offset().top + 35,
                            left: $(evt.target).offset().left
                        });
                    }, 0);
                }
                //showOn: "button",
                //buttonImage: "images/calendar.gif",
                //buttonImageOnly: true,
                //timeFormat: "hh:mm tt"
            });
        } catch (ex) {
            //alert(ex.message);
        }
    });

});


$('.marketvalue').on('input propertychange', function (evt) {
    var value1 = parseInt($(this).val());
    if (value1 <= 100) {
        if (this.value.length > 4) {
            this.value = this.value.slice(0, 4);
        }
    }
    else
        $(this).val('');
});

$('.HighestEmployee_').on('input propertychange', function (evt) {
    var value1 = parseInt($(this).val());
    if (value1 <= 50000) {
        //if (this.value.length > 4) {
        //    this.value = this.value.slice(0, 4);
        //}
    }
    else
        $(this).val('');
});


$(".NumbersOnly").keypress(function (e) {

    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57 || $(this).val().length >= 25)) {
        return false;
        //alert('Invalid input');
    }
});

$("body").on("input propertychange", ".CardNumber", function (e) {
    //
    $(this).attr('maxlength', '19');
    $(this).mask('0000-0000-0000-0999');
});
$("body").on("input propertychange", ".CardNumberExpDate", function (e) {
    //
    $(this).attr('maxlength', '7');
    $(this).mask('00/0000');
});




function validateEmail(email) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (reg.test(email)) {

        return true;
    }
    else {
        return false;
    }
}

$('.nav-side-menu-inner').scrollbar();

jQuery(document).ready(function ($) {
    window.prettyPrint && prettyPrint();
    $('.scrollbar-dynamic').scrollbar();

    //$("img").on("error", function () {
    //    $(this).attr("src", "../empprofilepics/noimage.jpg");
    //})

    // length validation start
    $("body").on("input focus", "input", function (e) {
        $(this).attr('data-placement', 'right');
        var Val = $(this).val();
        var myAttr = $(this).attr('data-val-maxlength-max');
        if (typeof myAttr == typeof undefined || myAttr == false) {
            $(this).attr('data-val-maxlength-max', 100);
        }
        CharLimit(this, myAttr);
    });

    var arrKeys = new Array(8, 9, 16, 17, 18, 20, 35, 36, 37, 39, 46);
    $('.form-control').keyup(function (e) {
        var attrWarning = $(this).attr('warning');
        var maxlength = 100;
        if ($(this).attr('data-val-maxlength-max') > 0)
            maxlength = $(this).attr('data-val-maxlength-max');
        if ($(this).val().length >= parseInt(maxlength)) {
            if (typeof attrWarning !== 'undefined' && attrWarning !== false) {
                if ($.inArray(e.keyCode, arrKeys) == -1) {
                    $(this).attr('data-original-title', 'Max ' + $(this).attr('data-val-maxlength-max') + ' characters');
                    $(this).tooltip('show');
                    return false;
                }
            }
            else {
                $(this).attr('warning', 'warning');
                return false;
            }
        } else {
            $(this).removeAttr('warning');
            $('.tooltip').not(this).hide();
        }
    });
    $('.form-control').blur(function (e) {
        $(this).removeAttr('warning');
        $(this).attr('data-original-title', '');
    });
    $('textarea').on('input propertychange', function () {
        var myLength = $(this).attr('size');
        if (typeof myLength == typeof undefined || myLength == false) {
            myLength = 500;
        }
        CharLimit(this, myLength);
        var Val = $(this).val();
    });
    // length validation start
});
function CharLimit(input, maxChar) {
    var len = $(input).val().length;
    if (len > maxChar) {
        $(input).val($(input).val().substring(0, maxChar));
    }
}

function getPageName() {
    //var index = window.location.href.lastIndexOf("/") + 1,
    //    filenameWithExtension = window.location.href.substr(index),               
    //    filename = filenameWithExtension.split(".")[0];
    var url = window.location.pathname.split("/");
    var filename = '/' + url[1] + '/' + url[2];
    return filename;
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toUTCString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

$("body").on("mouseenter", "ul#menu-content li a.active,ul#menu-content li.active", function () {
    $(this).css('cursor', 'default !important');
});
jQuery(document).ready(function ($) {
    var url = window.location.href.replace('#', '');
    var getQuestionMark = url.indexOf("?");
    if (getQuestionMark != -1) {
        var getQueryString = url.substring(getQuestionMark);
        var replacedQueryString = url.replace(getQueryString, "");
        url = replacedQueryString;
    }
    var oldUrl;
    var target;
    var oldtarget;
    var newurlfound = 0;
    //$("body").on("click", "ul.collapse li a[href!='#']", function () {
    $("body").on("click", "ul#menu-content li a[href!='#']", function (evt) {
        //if ($(this).hasClass('active')) {
        //    //evt.preventDefault();
        //} 
        //else {
        if (oldUrl != null) { eraseCookie('ssUrlCookie'); }
        createCookie('ssUrlCookie', this.href.replace('#', ''), 1);
        $("ul.collapse li").each(function () { $(this).removeClass('active'); });
        $(this).closest('li').addClass("active");
        //}
    });
    oldUrl = readCookie('ssUrlCookie');
    $("ul#menu-content li,ul#menu-content li a").each(function () {
        $('ul[class="in"]').removeClass('in');
        //$(this).closest('ul.sub-menu').addClass('in');
    });
    //$("ul.menu-content li a[href!='#'],ul.menu-content ul li a[href!='#']").each(function () {
    $("ul#menu-content li a[href]").filter(function () {
        return this.href.toLowerCase() == url.toLowerCase();
    }).each(function () {
        //$("ul.menu-content li a[href='" + url + " i'],ul.menu-content ul li a[href='" + url + " i']").each(function () {
        //$("ul.menu-content li a,ul.menu-content ul li a").each(function () {    
        // checks if its the same on the address bar
        //if (url.toLowerCase() == (this.href).toLowerCase()) {        
        //eraseCookie('ssUrlCookie');
        $(this).addClass("active").closest('li').addClass("active");
        //$(this).closest("ul").addClass('sub-menu collapse in');
        target = $(this).closest("ul").addClass('collapse in collapsein').attr('id');
        target = "#" + target;
        $("ul#menu-content li[data-target='" + target + "']")
            .removeClass('collapsed').addClass('active');//.addClass('collapseIn')
        //.find('a').append('<i class="glyphicon glyphicon-arrow-down clsRight myGlyphicon"></i>')
        //.addClass('active');
        newurlfound = 1;
        return;
        //}
    });
    if (newurlfound != 1 && oldUrl != null) {
        newurlfound = 0;
        //$("ul.menu-content li a[href!='#'],ul.menu-content ul li a[href!='#']").each(function () {
        //$("ul.menu-content li a[href!='#'],ul.menu-content ul li a[href!='#']").each(function () {
        $("ul#menu-content li a[href]").filter(function () {
            return this.href.toLowerCase() == oldUrl.toLowerCase();
        }).each(function () {
            //if (oldUrl.toLowerCase() == (this.href).toLowerCase()) {             
            $(this).addClass("active").closest('li').addClass("active");
            //$(this).closest("ul").addClass('sub-menu collapse in');
            oldtarget = $(this).closest("ul").addClass('collapse in collapsein').attr('id');
            oldtarget = "#" + oldtarget;
            $("ul#menu-content li[data-target='" + oldtarget + "']")
                .removeClass('collapsed').addClass('active');//.addClass('collapseIn')
            //.find('a').append('<i class="glyphicon glyphicon-arrow-down clsRight myGlyphicon"></i>')
            //.addClass('active');
            return;
            //}
        });
    }

    var pathname = window.location.pathname;
    if (pathname == '/home/mgroverview')
        $("ul#menu-content li:first").addClass('active');
    if ($("ul#menu-content li.active").length > 0) { }
    else
        $("ul#menu-content li:first").addClass('active');
});
$('a[href="#"]').click(function (event) {
    event.preventDefault();
});
