var MorrisChartsDemo = {
    init: function () {
        new Morris.Line({
            element: "m_chart_trends_stats", data: [{
                y: "2006", a: 100, b: 90
            }
                , {
                y: "2007", a: 75, b: 65
            }
                , {
                y: "2008", a: 50, b: 40
            }
                , {
                y: "2009", a: 75, b: 65
            }
                , {
                y: "2010", a: 50, b: 40
            }
                , {
                y: "2011", a: 75, b: 65
            }
                , {
                y: "2012", a: 100, b: 90
            }
            ], xkey: "y", ykeys: ["a", "b"], labels: ["Values A", "Values B"]
        }
        )//,
        //    new Morris.Area({
        //    element: "m_chart_bandwidth2", data: [{
        //            y: "2006", a: 100, b: 90
        //        }
        //            , {
        //            y: "2007", a: 75, b: 65
        //        }
        //            , {
        //            y: "2008", a: 50, b: 40
        //        }
        //            , {
        //            y: "2009", a: 75, b: 65
        //        }
        //            , {
        //            y: "2010", a: 50, b: 40
        //        }
        //            , {
        //            y: "2011", a: 75, b: 65
        //        }
        //            , {
        //            y: "2012", a: 100, b: 90
        //        }
        //        ], xkey: "y", ykeys: ["a", "b"], labels: ["Series A", "Series B"]
        //    }
        //    ),
        //    new Morris.Bar({
        //    element: "m_chart_bandwidth1", data: [{
        //            y: "2006", a: 100, b: 90
        //        }
        //            , {
        //            y: "2007", a: 75, b: 65
        //        }
        //            , {
        //            y: "2008", a: 50, b: 40
        //        }
        //            , {
        //            y: "2009", a: 75, b: 65
        //        }
        //            , {
        //            y: "2010", a: 50, b: 40
        //        }
        //            , {
        //            y: "2011", a: 75, b: 65
        //        }
        //            , {
        //            y: "2012", a: 100, b: 90
        //        }
        //        ], xkey: "y", ykeys: ["a", "b"], labels: ["Series A", "Series B"]
        //    }
        //    )//,
            //new Morris.Donut({
            //    element: "m_morris_4", data: [{
            //        label: "Download Sales", value: 12
            //    }
            //        , {
            //        label: "In-Store Sales", value: 30
            //    }
            //        , {
            //        label: "Mail-Order Sales", value: 20
            //    }
            //    ]
            //}
            //)
    }
}

    ;
jQuery(document).ready(function () {
    MorrisChartsDemo.init()
}

);


//[{
//    Lebel: "2006", Value: 100, Value1: 90, Value2: 100
//}
//    , {
//        Lebel: "2007", Value: 75, Value1: 65, Value2: 20
//    }
//    , {
//        Lebel: "2008", Value: 50, Value1: 40, Value2: 15
//    }
//    , {
//        Lebel: "2009", Value: 75, Value1: 65, Value2: 25
//    }
//    , {
//        Lebel: "2010", Value: 50, Value1: 40, Value2: 95
//    }
//    , {
//        Lebel: "2011", Value: 75, Value1: 65, Value2: 45
//    }
//    , {
//        Lebel: "2012", Value: 100, Value1: 90, Value2: 65
//    }
//]