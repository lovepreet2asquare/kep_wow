﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class AboutController : Controller
    {
        IAgreement agreementaccess = new AgreementAccess();
        AgreementModel model = new AgreementModel();
        // GET: Condition
        public ActionResult PrivacyPolicy()
        {
            model.Title = "Privacy Policy";
            model.Privacy = 1;
            model.AgreementId = 1;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            AgreementResponse result = agreementaccess.SelectAgreements(model);
            return View(result._agreementModel);
        }
        public ActionResult Eula()
        {
            model.Title = "EULA";
            model.Privacy = 1;
            model.AgreementId = 3;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            AgreementResponse result = agreementaccess.SelectAgreements(model);
            return View(result._agreementModel);
        }
        public ActionResult TermsAndCondition()
        {
            model.Title = "Terms & Conditions";
            model.Privacy = 1;
            model.AgreementId = 2;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            AgreementResponse result = agreementaccess.SelectAgreements(model);
            return View(result._agreementModel);
        }
        public ActionResult About()
        {
            return View();
        }  public ActionResult Invitation()
        {
            return View();
        }
        public ActionResult Support()
        {
            return View();
        } public ActionResult PurchaseLicense()
        {
            return View();
        }public ActionResult supportcenter()
        {
            return View();
        }
        public ActionResult Exclusive_NewsDetail()
        {
            return View();
        }

        public ActionResult Donationstatement()
        {
            return View();
        }
        public ActionResult Download()
        {
            return View();
        }
    }
}