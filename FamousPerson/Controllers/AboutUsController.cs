﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class AboutUsController : BaseController
    {
        IAboutUs aboutusAccess = new AboutUsAccess();

        // GET: About
        public ActionResult Index()

        {
            AboutUsModel model = new AboutUsModel();
            AboutUsResponse response = new AboutUsResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            AboutUsResponse result = aboutusAccess.Select(model);
            if (result.AboutUsModel != null)
            {
                //TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.AboutUsModel;
                TempData["ImagePath"] = result.AboutUsModel.ImagePath;
                return View(result.AboutUsModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        
        [HttpPost]
        public ActionResult Index(AboutUsModel model)
        {
            
            AboutUsResponse response = new AboutUsResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            AboutUsResponse result = aboutusAccess.Select(model);
            if (result.AboutUsModel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;

                return View(result.AboutUsModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        public ActionResult CreateOrEdit()
       {
            AboutUsModel model = new AboutUsModel();
            
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            var Result = TempData["ImagePath"];
            model.ImagePath = Convert.ToString(Result);
            AboutUsResponse result = aboutusAccess.Select(model);
           
            //InAppMessageResponse result = appMessageAccess.AddOrEdit(model);
            if (result != null)
            {
                result.AboutUsModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                result.AboutUsModel.DateTo = UtilityAccess.FromDate(model.DateTo);
                return View(result.AboutUsModel);
            }
            return View();
        }
        [HttpPost]
        public ActionResult CreateOrEdit(AboutUsModel model)
        {
          
            AboutUsResponse result = new AboutUsResponse();
            
            model.EncryptedAboutId = UtilityAccess.Decrypt(model.AboutId);
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            String UserId = UtilityAccess.Decrypt(model.EncryptedUserId);


            if (!String.IsNullOrEmpty(model.ImageBase64))
                model.ImagePath = Function.UploadCropImage(model.ImageBase64, "Headlines");

            result = aboutusAccess.AddOrEdit(model);
            
            if (result.AboutUsModel != null)
            {
                TempData["AboutUsModel"] = result.AboutUsModel;
                return RedirectToAction("index", "aboutus");
            }
            else
                return RedirectToAction("error", "home");
        }

        public ActionResult Create()
        {
            AboutUsModel model = new AboutUsModel();

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            AboutUsResponse result = aboutusAccess.Select(model);

            if (result.AboutUsModel.AboutId != null)
            {
                result.AboutUsModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                result.AboutUsModel.DateTo = UtilityAccess.FromDate(model.DateTo);
                TempData["AboutUsModel"] = result.AboutUsModel;
                //return View(result.AboutUsModel);
                return RedirectToAction("Index", "AboutUs");
            }
            else
            {
                result.AboutUsModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                result.AboutUsModel.DateTo = UtilityAccess.FromDate(model.DateTo);
                return View(result.AboutUsModel);
            }
            //return View();
        }
        [HttpPost]
        public ActionResult Create(AboutUsModel model)
        {

            //AboutUsResponse response = new AboutUsResponse();
            AboutUsResponse result = new AboutUsResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            String UserId = UtilityAccess.Decrypt(model.EncryptedUserId);

            if (!String.IsNullOrEmpty(model.ImageBase64))
                model.ImagePath = Function.UploadCropImage(model.ImageBase64, "AboutUs");

            result = aboutusAccess.AddOrEdit(model);

            if (result.AboutUsModel != null)
            {
                TempData["AboutUsModel"] = result.AboutUsModel;
                return RedirectToAction("index", "aboutus");
            }
            else
                return RedirectToAction("error", "home");
        }

        public ActionResult Delete(String aid)
        {
            AboutUsModel model = new AboutUsModel(); 
            model.AboutId = aid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            AboutUsResponse result = aboutusAccess.Delete(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                //TempData["href"] = "/about/index";
            }

            return RedirectToAction("create", "aboutus", new { aid = aid });
        }
    }
}