﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
//using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class AccountController : Controller
    {
        IUser userAccess = new UserAccess();
        INews newsAccess = new NewsAccess();
        IHeadline headLineAccess = new HeadlineAccess();
        // GET: Account
        public ActionResult Login()
        {
            LoginModel model = new LoginModel();
            string DecryptedPassword = UtilityAccess.Decrypt("6BURvpITCAOC4JGUWCIBQJbGlFQ5TREH+3oKS0AS63nUjpi+LQDp3daIx62nmnIx");
            string Password = UtilityAccess.Encrypt("njpba@321");

            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            if (Function.Authenticate())
            {
                return View(model);
                //return RedirectToAction("login", "account");
            }
            else
            {
                if (!String.IsNullOrEmpty(Function.ReadCookie("FPRemember")))
                {
                    model.Remember = true;
                    model.Email = Function.ReadCookie("FPRememberEmail");
                    model.Password = UtilityAccess.Decrypt(Function.ReadCookie("FPRememberPassword"));
                }
                return View(model);

            }

        }
        [HttpPost]
      
        public ActionResult Login(LoginModel model)
        {

            LoginRespone result = LoginValidate(model);
            
            if (result.UserInfo != null && result.ReturnCode == "1")
            {


                model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
                model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
                string isadmin = Convert.ToString(Session["IsAdmin"]);
                bool IsEnabled = Convert.ToBoolean(Session["IsEnabled"]);
                int returnResult = 0;
                if (isadmin == "1" && IsEnabled==true)
                {
                    string mobile = Convert.ToString(Session["Mobile"]).Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "");

                    Session["otp"] = SendOTP.SendSMS(mobile);

                   int res= userAccess.sendotpmail();
                    ViewData["otpsent"]="OTP has been sent to your mobile"+ Convert.ToString(Session["Mobile"])+ "  and email "+ Convert.ToString(Session["Email"]) +"";
                        
                    //return RedirectToAction("index", "dashboard");
                    return RedirectToAction("otp");
                }
                else
                {
                    return RedirectToAction("index", "dashboard");
                }

                }
            else if (result.UserInfo != null && result.ReturnCode == "1")
            {
                string isadmin = Convert.ToString(Session["IsAdmin"]);
                bool IsEnabled = Convert.ToBoolean(Session["IsEnabled"]);
                if (isadmin == "1" && IsEnabled == true)
                {
                    string mobile = Convert.ToString(Session["Mobile"]).Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "");
                    Session["otp"] = SendOTP.SendSMS(mobile);
                  
                    //return RedirectToAction("index", "dashboard");
                    return RedirectToAction("otp");
                }
                else
                {
                    return RedirectToAction("index", "dashboard");
                }
            }
            else
            {
                ViewBag.Message = result.ReturnMessage;
                return View();
            }
        }
        [HttpPost]
        public LoginRespone LoginValidate(LoginModel model)

        {
            model.DeviceToken = "web";
            model.DeviceType = "pc";
            LoginRespone result = userAccess.Login(model);
            // store value into cookie
            if (result.UserInfo != null && result.ReturnCode == "1")
            {
                Function.WriteCookie("EncryptedSessionToken", result.UserInfo.EncryptedSessionToken);
                Function.WriteCookie("EncryptedUserId", result.UserInfo.EncryptedUserId);
                Function.WriteCookie("FP_LoggedUserName", result.UserInfo.FirstName + " " + result.UserInfo.LastName);
                //Function.WriteCookie("timezoneid", result.UserInfo.TimeZone);
                //Session["UserIsProfileComplete"] = result.UserInfo.IsProfileComplete;
                Session["Password"] = result.UserInfo.Password;
                Session["Mobile"]= result.UserInfo.Mobile;
                Session["Email"] = result.UserInfo.Email;
                Session["IsAdmin"] = result.UserInfo.TitleId;
                Session["IsEnabled"] = result.UserInfo.IsEnabled;
                if (model.Remember)
                {
                    Function.WriteCookie("FPRemember", Convert.ToString(model.Remember), 100);
                    Function.WriteCookie("FPRememberEmail", Convert.ToString(model.Email), 100);
                    Function.WriteCookie("FPRememberPassword", UtilityAccess.Encrypt(Convert.ToString(model.Password)), 100);
                }
                else
                {
                    Function.DeleteCookie("FPRemember");
                    Function.DeleteCookie("FPRememberEmail");
                    Function.DeleteCookie("FPRememberPassword");

                }
            }

            //    if (!result.UserInfo.IsProfileComplete)
            //    {
            //        return RedirectToAction("commingsoon", "dashboard");
            //    }
            //    else
            //    {
            //        return RedirectToAction("myprofile", "user", new { pf = "uc" });
            //    }
            //}

            //ViewBag.Message = result.ReturnMessage;
            // return View(result);
            return result;
            
        }
        public ActionResult LogOut()
        {
            Function.DeleteCookie("EncryptedUserId");
            Function.DeleteCookie("EncryptedStationId");
            Function.DeleteCookie("EncryptedSessionToken");


            return RedirectToAction("login", "account");
        }        //[AllowAnonymous]
        //[HttpGet]
        public ActionResult ForgotPassword()
        {
            ViewBag.ReturnCode = "0";
            ViewBag.Message = "";
            return View();
        }


        [HttpPost]
        //[AllowAnonymous]
        public ActionResult ForgotPassword(ForgotPasswordModel model)
        {
            ViewBag.Message = "";
            ViewBag.ReturnCode = "0";
            try
            {
                if (ModelState.IsValid)
                {
                    ForgotPasswordModel _result = new ForgotPasswordModel();
                    _result = userAccess.IsEmailExist(model);

                    if (_result.ReturnValue == -1)
                        ViewBag.Message = "Sorry, a technical error occurred. Please try again later.";
                    else if (_result.ReturnValue == 0)
                        ViewBag.Message = "Account Not found.";
                    else
                    {
                        ViewBag.ReturnCode = "1";
                        ViewBag.Message = "Password reset link email has been sent to your email address.";
                    }
                }
                return View(model);
            }
            catch (Exception ex)
            {
                //ApplicationLogger.LogError(ex, "AccountController", "ForgotPassword");
                return View(model);
            }
        }
        public ActionResult ResetPassword()
        {
            ViewBag.Message = "";
            ResetPasswordModel model = new ResetPasswordModel();
            try
            {
                if (Request["ur"] != null && Request["tm"] != null)
                {
                    if (String.IsNullOrEmpty(Request["ur"]) == false && String.IsNullOrEmpty(Request["tm"]) == false)
                    {

                        double _time = Convert.ToDouble(UtilityAccess.Decrypt(Request["tm"]));
                        DateTime _date = UtilityAccess.UnixTimeStampToDateTime(_time);
                        DateTime _now = DateTime.Now;
                        if ((_now - _date).TotalMinutes < 60)
                        {
                            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(Request["ur"]));
                        }
                        else
                        {
                            ViewBag.Message = "Password reset link has been expired.";
                        }
                    }
                }
                else
                    return Redirect(UserAccess.IsAuthorize()); ;

                return View(model);
            }
            catch (Exception ex)
            {
                // ApplicationLogger.LogError(ex, "AccountController", "ResetPassword");
                return View(model);
            }

        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            ViewBag.Message = "";
            try
            {
                if (ModelState.IsValid)
                {
                    Int32 _result = UserAccess.ResetPassword(model);
                    if (_result == 2)
                        ViewBag.Message = "Password changed successfully. Click <a style=color:#2196F3;     href=\"/account/login\"> here </a> to login.";
                    else
                        ViewBag.Message = "Sorry, a technical error occurred. Please try again later.";
                }

                return View(model);
            }
            catch (Exception ex)
            {
                // ApplicationLogger.LogError(ex, "AccountController", "ResetPassword");
                return View(model);
            }
        }
        public ActionResult Signup()
        {
            ModelState.Clear();
            ViewBag.ReturnCode = "";
            ViewBag.Message = "";
            return View();
        }


        [HttpPost]
        //[AllowAnonymous]
        public ActionResult Signup(SignupModel model)
        {
            ViewBag.Message = "";
            ViewBag.ReturnCode = "0";
            try
            {
                if (ModelState.IsValid)
                {
                    SignupRespone result = userAccess.Signup(model);

                    if (result.ReturnCode == -1)
                        ViewBag.Message = "Sorry, a technical error occurred. Please try again later.";

                    else if (result.ReturnCode == -3)
                    {
                        ViewBag.ReturnCode = "";
                        ViewBag.Message = "Email already exists.";
                    }
                    else
                    {
                        ViewBag.ReturnCode = "1";
                        ViewBag.Message = "Verification link has been sent to your email address.";
                    }
                }
                return View(model);
            }
            catch (Exception ex)
            {
                //ApplicationLogger.LogError(ex, "AccountController", "ForgotPassword");
                return View(model);
            }
        }
        public ActionResult DetailLink(string nid)
        {
            //if (nid == "" || nid == null)
            //{
            //    nid = "QTbomPE1Mn9AXn5rH1WTFQ==";
            //}
            //nid = UtilityAccess.Encrypt(nid);
            NewsModel model = new NewsModel();
            model.EncryptedNewsId = nid;
           // model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
           // model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            //NewsResponse result = newsAccess.NewsSelectAll(model);
            // model.NewsId = Convert.ToInt32(nid);

            NewsResponse result = newsAccess.NewsDetailById(model);
            //ViewBag.PageHeader = "Basic Info";
            //return View(result.UserModel);
            if (result._newsModel != null)
            {
                result._newsModel.EncryptedNewsId = nid;
                TempData["NewsModel"] = result._newsModel;
                return View(result._newsModel);
            }
            else
                return RedirectToAction("error", "home");
            //return View();
        }
        public ActionResult HDetailLink(string hid)
        {
            //if (hid == "" || hid == null)
            //{
            //    hid = "NEO6oc4hkie4i/Zm00wcPQ==";

            //}
           // hid = UtilityAccess.Encrypt(hid);
            HeadlineModel model = new HeadlineModel();
            model.EncryptedHeadLineId = hid;
           // model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
          //  model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            HeadlineResponse result = headLineAccess.DetailById(model);
            ViewBag.PageHeader = "Basic Info";
            return View(result._HeadlineModel);
        }
        public ActionResult MessageDetail(string mid)
        {
            IAppMessage appMessageAccess = new InAppMessageAccess();
            InAppMessageModel model = new InAppMessageModel();
            InAppMessageResponse response = new InAppMessageResponse();
            model.MessageId = mid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

            InAppMessageResponse result = appMessageAccess.DetailView(model);

            return View(result.InAppMessageModel);

        }
        public ActionResult MemberDetail(string mid)
        {
            IMemberBenefit memberBenefitAccess = new MemberBenefitAccess();
            MemberBenefitModel model = new MemberBenefitModel();
            model.EncryptedMemberBenefitId = mid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

            MemberBenefitResponse result = memberBenefitAccess.DetailById(model);

            return View(result.MemberBenefit);

        }

        public ActionResult otp()
        {
            //string abc= String.Format("{0:(###) ###-####}", 8005551212);
            ViewData["otpsent"] = "OTP has been sent to your mobile " + Convert.ToString(Session["Mobile"]) + "  and email " + Convert.ToString(Session["Email"]) + "";
            //ViewData["otpsent"] = "OTP has been sent to your mobile (946)984-4489 and email freeman@keplerbase.com";

            return View();
        }

        [HttpPost]
        public ActionResult otp(LoginModel model)
        {
            string mobile = Convert.ToString(Session["Mobile"]).Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "");
            string OTP = Convert.ToString(Session["otp"]);
            if(model.OTP!="" && model.OTP== OTP)
            {
                Session["otp"] = null;
                return RedirectToAction("index", "dashboard");

            }
            else
            {
                ViewBag.Message = "Please enter valid verification code.";
                return View();
            }
        }

        [HttpPost]
        public JsonResult otpverification()
        {
            string mobile = Convert.ToString(Session["Mobile"]).Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "");
            Session["otp"] = SendOTP.SendSMS(mobile);
            ForgotPasswordResponse res = new ForgotPasswordResponse();
            string otp = Convert.ToString(Session["otp"]);
            res.ReturnMessage = "";
            if (!string.IsNullOrWhiteSpace(otp))
                res.ReturnCode = "1";
            else
                res.ReturnCode = "0";

            return Json(res, JsonRequestBehavior.AllowGet);
        }

    }
}