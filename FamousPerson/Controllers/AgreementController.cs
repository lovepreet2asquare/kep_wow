﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class AgreementController : BaseController
    {
        IAgreement agreementaccess = new AgreementAccess();
        // GET: Agreement
        public ActionResult Index()
        {
            AgreementModel model = new AgreementModel();

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            AgreementResponse result = agreementaccess.SelectAll(model);
            return View(result._agreementModel);
        }
        [HttpPost]
        public ActionResult Index(AgreementModel model)
        {
            
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            AgreementResponse result = agreementaccess.SelectAll(model);
            return View(result._agreementModel);
        }
        public ActionResult Create(string aid)
            {
            AgreementModel model = new AgreementModel();

            AgreementResponse result = new AgreementResponse();
            model.EncryptedAgreementId = aid;
            model.NoFile = null;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
                result= agreementaccess.Select(model);
            //result._agreementModel.AgreementId = 0;
            return View(result._agreementModel);
        }
        public ActionResult Edit(string aid)
        {
            AgreementModel model = new AgreementModel();
            model.EncryptedAgreementId = aid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            AgreementResponse result = agreementaccess.Select(model);
            return View(result._agreementModel);
        }
        public ActionResult Detail(string aid)
        {
            AgreementModel model = new AgreementModel();
            model.EncryptedAgreementId = aid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            AgreementResponse result = agreementaccess.Select(model);
            return View(result._agreementModel);
        }
        public ActionResult View(string aid)
        {
            AgreementModel model = new AgreementModel();
            model.EncryptedAgreementId = aid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            AgreementResponse result = agreementaccess.Select(model);
            return View(result._agreementModel);
        }
        [HttpPost]
        public ActionResult CreateorEdit(AgreementModel model)
        {
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            HttpPostedFileBase httpPostedFileBase = null;
            if (Request.Files.Count > 0)
            {
                //save picture
                httpPostedFileBase = Request.Files[0] as HttpPostedFileBase;
                if (httpPostedFileBase != null && httpPostedFileBase.ContentLength > 0)
                {
                    string timeStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                    string fileName = Path.GetFileName(httpPostedFileBase.FileName);
                    string ext = System.IO.Path.GetExtension(httpPostedFileBase.FileName);

                    string thumbnailFilePath = string.Empty;
                    //Set thumbnail for documents for app use
                    if (ext.ToLower() == ".docx" || ext.ToLower() == ".doc")
                        thumbnailFilePath = "/thumbnail/word.png";
                    else if (ext.ToLower() == ".xlsx" || ext.ToLower() == ".xls")
                        thumbnailFilePath = "/thumbnail/excel.png";
                    else if (ext.ToLower() == ".jpeg" || ext.ToLower() == ".jpg" || ext.ToLower() == ".png")
                        thumbnailFilePath = "/thumbnail/png.png";
                    else if (ext.ToLower() == ".pdf")
                        thumbnailFilePath = "/thumbnail/pdf.png";
                    else
                    {
                        model.NoFile = "Please enter file with specified extensions!";

                    }
                    if (model.NoFile == null)
                    {
                        model.ThumbnailPath = thumbnailFilePath;
                        string filePath = "Upload/Agreement/" + timeStamp + "/" + fileName;
                        model.DocumentName = fileName;

                        model.DocumentPath = filePath;
                        Directory.CreateDirectory(Server.MapPath("~/" + filePath));
                        httpPostedFileBase.SaveAs(Server.MapPath("~/" + filePath) + "/" + fileName);
                    }
                }
            }
            if (model.NoFile != null)
            {

                return View(model);
            }
            AgreementResponse result = agreementaccess.AddOrEdit(model);
            if (result._agreementModel!= null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                TempData["AgreementModel"] = result._agreementModel;
                return RedirectToAction("index", "agreement");
            }
            else
            {
                return RedirectToAction("error", "home");
            }
            
        }
    }
}