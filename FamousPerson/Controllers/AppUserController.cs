﻿using FPBAL.Interface;
using FPBAL.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPModels.Models;
using FamousPerson.Models;

namespace FamousPerson.Controllers
{
    public class AppUserController : BaseController
    {
        // GET: AppUser
        IAppuserListAccess apAccess = new AppuserListAccess();

        public ActionResult Index()
        {
            AppUserListModel model = new AppUserListModel();
            AppUserListResponse request = new AppUserListResponse();
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");

            AppUserListResponse result = apAccess.AppUserSelectAll(model);
            //result.UserModel.EncryptedUserId = model.EncryptedUserId;
          
            ViewBag.PageHeader = "App User";
            return View(result.ApUserModel);
        }

        [HttpPost]
        public ActionResult Index(AppUserListModel model)
        {
            AppUserListResponse request = new AppUserListResponse();
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            AppUserListResponse result = apAccess.AppUserSelectAll(model);
            //result.UserModel.EncryptedUserId = model.EncryptedUserId;
            ViewBag.PageHeader = "App User";
            return View(result.ApUserModel);
        }


        #region InviteUser
        public ActionResult InviteUser(String fid)
        {
            if (!String.IsNullOrEmpty(fid))
                fid = UtilityAccess.Decrypt(fid);
            else
                fid = "0";

            InviteResponse inviteResponse = apAccess.ExcellDataSelectByFileId(fid);

            if (inviteResponse.InviteDataFileModel != null)
            {
                if (String.IsNullOrEmpty(inviteResponse.InviteDataFileModel.InviteDataFileId))
                    inviteResponse.InviteDataFileModel.InviteDataFileId = UtilityAccess.Encrypt("-1");

                return View(inviteResponse.InviteDataFileModel);
            }
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public ActionResult InviteUser(InviteDataFileModel model)
        {
            InviteResponse inviteResponse = apAccess.UploadHttpPostedFile(model);
            if (inviteResponse.InviteDataFileModel != null)
                return View(inviteResponse.InviteDataFileModel);
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public JsonResult SendInvitation(String fid)
        {
            InviteResponse inviteResponse = new InviteResponse();
            inviteResponse.ReturnCode = 0;
            inviteResponse.ReturnMessage = "Success";
            if (!String.IsNullOrEmpty(fid) && fid!= "undefined")
            {
                inviteResponse = apAccess.MultiInvite(UtilityAccess.Decrypt(fid));
                return Json(inviteResponse, JsonRequestBehavior.AllowGet);
            }

            return Json(inviteResponse, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SingleInvite(String did, String name, String email)
        {
            InviteResponse inviteResponse = new InviteResponse();
            inviteResponse.ReturnCode = 0;
            inviteResponse.ReturnMessage = "Success";
            if (!String.IsNullOrEmpty(did) )
            {
                inviteResponse = apAccess.SingleInvite(Convert.ToInt32(did),name,email);
                return Json(inviteResponse, JsonRequestBehavior.AllowGet);
            }

            return Json(inviteResponse, JsonRequestBehavior.AllowGet);
        }


        //public ActionResult SendInvitation(String fid)
        //{
        //    InviteResponse inviteResponse = new InviteResponse();
        //    if (!String.IsNullOrEmpty(fid))
        //    {
        //        inviteResponse = apAccess.SendInvitation(UtilityAccess.Decrypt(fid));
        //        if (inviteResponse.InviteDataFileModel != null)
        //            return View(inviteResponse.InviteDataFileModel);
        //        else
        //            return RedirectToAction("error", "home");
        //    }
        //    else
        //        return View(inviteResponse);
        //}


        //[HttpPost]
        //public ActionResult Invite(AppUserListModel model)
        //{
        //    AppUserListResponse request = new AppUserListResponse();
        //    model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
        //    model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
        //    AppUserListResponse result = apAccess.SendBulkEmail(model);
        //    //result.UserModel.EncryptedUserId = model.EncryptedUserId;
        //    ViewBag.PageHeader = "App User";
        //    //if (result.ReturnCode > 0)
        //    //{
        //    //    TempData["ReturnCode"] = result.ReturnCode;
        //    //    TempData["ReturnMessage"] = result.ReturnMessage;
        //    //}
        //    return Json(result.ReturnCode, JsonRequestBehavior.AllowGet);
        //    //return RedirectToAction("index", "appuser");
        //}
        #endregion InviteUser
    }
}