﻿using FamousPerson.Models;
using FPBAL.Business;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class BaseController : Controller
    {
        String MenuId = "0";

        MenuAccess menuAccess = new MenuAccess();
        protected override void OnException(ExceptionContext context)
         {
            //dont interfere if the exception is already handled
            if (context.ExceptionHandled)
                return;

            context.Result = new ViewResult
            {
                ViewName = "~/Views/Shared/Error"
            };

            context.ExceptionHandled = true;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                 //if (!Function.Authenticate())
                  //  filterContext.Result = new RedirectResult(Url.Action("logout", "account"));
               

                //if (!string.IsNullOrEmpty(UserAccess.SessionValue("IMS_LoggedUserId")))
                 //{                  
                string url = MvcApplication.getControllerNameFromUrl();
                var siteMenuManager = new MenuModel();
                MenuModel result = new MenuModel();
                if (url == "home/error404" || url == "mailbox/setlocaltimezone")
                {
                    if (Session["Result"] != null)
                        result = (MenuModel)Session["Result"];
                }
                else
                {                   
                    if (Request["mid"] != null && !String.IsNullOrEmpty(Request["mid"]))
                        MenuId = UtilityAccess.Decrypt(Request["mid"].Replace(" ", "+"));

                    Int32 UserId = 0;
                    //Int32 StationId = 0;

                    if (!String.IsNullOrEmpty(Function.ReadCookie("EncryptedUserId")))
                        Int32.TryParse(UtilityAccess.Decrypt(Function.ReadCookie("EncryptedUserId")), out UserId);
                    
                    result = menuAccess.MenusByUser(url, UserId, MenuId);

                    Session["Result"] = result;
                }

                ViewBag.TopMenuList = result._TopMenus;
                ViewBag.LeftMenuList = result._LeftMenus;
                ViewBag.NotificationList = result._NotificationList;
                ViewBag.NotificationCount = result.CountNotification;
                ViewBag.menuCountdetails = result._menuCountList;
                ViewBag.Famous = "NJSPBA";



                //Activity Center Start              
                if (Request.Cookies["activity-menu-show"] != null && Convert.ToString(Request.Cookies["activity-menu-show"].Value) == "true")
                    ViewBag.ActivityCenter = "activity-menu-show";
                else
                    ViewBag.ActivityCenter = "";
                //Activity Center End

                base.OnActionExecuting(filterContext);
                // }
            }
            catch (Exception ex) { }
        }
    }
}