﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class CampaignController : BaseController
    {
        ICampaign campaignAccess = new CampaignAccess();
        // GET: Campaign
        public ActionResult Index()
        {
            CampaignModel model = new CampaignModel();
            CampaignResponse response = new CampaignResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            //model.DateTo = UtilityAccess.ToDate(model.DateTo);
            //model.DateFrom = UtilityAccess.FromDate(model.DateFrom);

            CampaignResponse result = campaignAccess.SelectAll(model);
            if (result.CampaignModel != null)
            {
                //result.CampaignModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                //result.CampaignModel.DateTo = UtilityAccess.FromDate(model.DateTo);
                //TempData["ReturnCode"] = result.ReturnCode;
                //TempData["ReturnMessage"] = result.ReturnMessage;
                return View(result.CampaignModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpPost]
        public ActionResult Index(CampaignModel model,FormCollection frm )
        {
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            CampaignResponse result = new CampaignResponse();
            result.CampaignModel = new CampaignModel();
            model.DateFrom = UtilityAccess.FromDate(model.DateFrom);
            model.DateTo = UtilityAccess.ToDate(model.DateTo);
            result = campaignAccess.SelectAll(model);
            if (result != null)
            {

                return View(result.CampaignModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        public ActionResult Create()

        {
            CampaignModel model = new CampaignModel();
            //model.DayId = Convert.ToInt32(UtilityAccess.Decrypt(oid));
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            CampaignResponse result = campaignAccess.Select(model);
            //InAppMessageResponse result = appMessageAccess.AddOrEdit(model);
            if (result != null)
            {
                result.CampaignModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                result.CampaignModel.DateTo = UtilityAccess.FromDate(model.DateTo);
                return View(result.CampaignModel);
            }
            return View();
        }
        [HttpPost]
        public ActionResult Create(CampaignModel model)
        {
            CampaignResponse result = new CampaignResponse();
            //var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
            //if(ModelState.IsValid)
            //{
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            //model.IsChecked = Convert.ToBoolean("true");
            //model.ISDCode = model.ISDCode.Replace("+", "");

            result = campaignAccess.AddOrEdit(model);
            //}
            TempData["ReturnCode"] = result.ReturnCode;
            TempData["ReturnMessage"] = result.ReturnMessage;
            return RedirectToAction("index", "Campaign");
        }
        
            public ActionResult CreateOrEdit(string oid)
        {
            CampaignModel model = new CampaignModel();
            model.OfficeId = oid;
            //model.DayId = Convert.ToInt32(UtilityAccess.Decrypt(oid));
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            CampaignResponse result = campaignAccess.Select(model);
            //InAppMessageResponse result = appMessageAccess.AddOrEdit(model);
            if (result != null)
            {
                //if(!String.IsNullOrEmpty(result.CampaignModel.ISDCode))
                //    result.CampaignModel.ISDCode = result.CampaignModel.ISDCode.Replace("+", "");

                result.CampaignModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                result.CampaignModel.DateTo = UtilityAccess.FromDate(model.DateTo);
                return View(result.CampaignModel);
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateOrEdit(CampaignModel model)
         {
            CampaignResponse result = new CampaignResponse();
            //var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
            //if(ModelState.IsValid)
            //{
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            //model.IsChecked = Convert.ToBoolean("true");

            //model.ISDCode = model.ISDCode.Replace("+", "");
            result = campaignAccess.AddOrEdit(model);
            //}
            TempData["ReturnCode"] = result.ReturnCode;
            TempData["ReturnMessage"] = result.ReturnMessage;
            return RedirectToAction("index", "Campaign");
        }

        public ActionResult Detail(string oid)
        {
            CampaignModel model = new CampaignModel();
            CampaignResponse response = new CampaignResponse();
            model.OfficeId = oid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            CampaignResponse result = campaignAccess.Select(model);
            if (result.CampaignModel != null)
            {
                result.CampaignModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                result.CampaignModel.DateTo = UtilityAccess.FromDate(model.DateTo);
                return View(result.CampaignModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpPost]
        public ActionResult Detail(string oid, CampaignModel model)
        {
            CampaignResponse response = new CampaignResponse();
            model.OfficeId = Convert.ToString(oid);
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

            //InAppMessageResponse result = appMessageAccess.DetailView(model);
            //if (result.InAppMessageModel != null)
            //{
            //    result.InAppMessageModel.DateFilterSelect = model.DateFilterSelect;
            //    result.InAppMessageModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
            //    result.InAppMessageModel.DateTo = UtilityAccess.FromDate(model.DateTo);
            //    return View(result.InAppMessageModel);
            //}
            //else
            return RedirectToAction("createoredit", "Campaign");
        }
        public ActionResult Delete(string oid)
        {
            CampaignModel model = new CampaignModel();
            model.OfficeId = oid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            CampaignResponse result = campaignAccess.Delete(model);

            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "Campaign");
        }

    }
}