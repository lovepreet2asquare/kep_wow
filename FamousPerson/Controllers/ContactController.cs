﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class ContactController : BaseController
    {

        IContact contactAccess = new ContactAccess();
        // GET: Contact
        public ActionResult Index()

        {
            ContactModel model = new ContactModel();
            ContactResponse response = new ContactResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
          
            ContactResponse result = contactAccess.SelectAll(model);
            if (result.ContactModel != null)
            {
                return View(result.ContactModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpPost]
        public ActionResult Index(ContactModel model)
        {

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            ContactResponse result = new ContactResponse();
            result.ContactModel = new ContactModel();
            ContactResponse response = new ContactResponse();
            //model.DateFrom = UtilityAccess.FromDate(model.DateFrom);
            //model.DateTo = UtilityAccess.ToDate(model.DateTo);
            
            result = contactAccess.SelectAll(model);
            
            return View(result.ContactModel);


        }
        public ActionResult Detail(string cid)
        {
            ContactModel model = new ContactModel();
            model.ContactId = cid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            ContactResponse result = contactAccess.Select(model);
            if (result.ContactModel != null)
            {
                result.ContactModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                result.ContactModel.DateTo = UtilityAccess.FromDate(model.DateTo);
                //TempData["ReturnCode"] = result.ReturnCode;
                //TempData["ReturnMessage"] = result.ReturnMessage;
                return View(result.ContactModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpPost]
        public ActionResult Detail(ContactModel model)
        {
            
            
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            ContactResponse response = new ContactResponse();

            response = contactAccess.AddOrEdit(model);
            TempData["ReturnCode"] = response.ReturnCode;
            TempData["ReturnMessage"] = response.ReturnMessage;
            return RedirectToAction("Index", "Contact");
            
        }
    }
}