﻿using FamousPerson.Models;
using FPBAL.Business;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class DashboardController : BaseController
    {
        DashboardAccess dashboardAccess = new DashboardAccess();
        // GET: Dashboard
        
        public ActionResult index()
        {
            DashboardSearch model = new DashboardSearch();
            model.UserId= Function.ReadCookie("EncryptedUserId");
            model.Duration = "0";
            DashboardResponse result = dashboardAccess.LoadData(model);
            if (result.ReturnCode == -1)
                return RedirectToAction("error", "home");
            else
            {
                 Session["DbTrendArticleModel"] = result.DashboardModel._TrendArticle;
                return View(result.DashboardModel);
            }
        }
        [HttpGet]
        public ActionResult ExtendSession()
        {
            System.Web.Security.FormsAuthentication.SetAuthCookie(User.Identity.Name, false);
            var data = new { IsSuccess = true };
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadData(DashboardSearch model)
        {
            model.UserId = Function.ReadCookie("EncryptedUserId");
            DashboardResponse result = dashboardAccess.LoadData(model);
            
            return Json(result, JsonRequestBehavior.AllowGet);


        }
        public ActionResult PrintArticle(String sid, String type, String filter)
        {
            DbTrendArticleModel result = new DbTrendArticleModel();
            Int32 retVal = -1;
            result.ReturnCode = -1;

            List<DbTrendArticleModel> _List  = (List<DbTrendArticleModel>)Session["DbTrendArticleModel"];
            if (_List != null && _List.Count > 0)
                _List = _List.Where(x => x.DurationType.ToLower().Contains(filter??"this")).ToList();

            result.FilePath = dashboardAccess.PdfPrintArticle(_List, out retVal);
            if (!String.IsNullOrEmpty(result.FilePath))
                result.FilePath = UtilityAccess.Encrypt(result.FilePath);

            result.ReturnCode = retVal;

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}