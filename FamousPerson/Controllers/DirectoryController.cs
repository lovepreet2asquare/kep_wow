﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class DirectoryController : BaseController
    {
        IDirectory directoryAccess = new DirectoryAccess();
        // GET: Directory
        //public ActionResult Index()
        //{ 
        //    DirectoryModel model = new DirectoryModel();
        //    DirectoryResponse request = new DirectoryResponse();
        //    model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
        //    model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
        //    DirectoryResponse result = directoryAccess.SelectAll(model);
        //    result._directoryModel.TotalCount = Convert.ToInt32(TempData["TotalCount"]);
        //    result._directoryModel.InsertedCount = Convert.ToInt32(TempData["InsertedCount"]);
        //    result._directoryModel.UpdatedCount = Convert.ToInt32(TempData["UpdatedCount"]);
        //    result._directoryModel.PendingCount = Convert.ToInt32(TempData["PendingCount"]);
        //    result._directoryModel.DuplicateCount = Convert.ToInt32(TempData["DuplicateCount"]);
        //    result._directoryModel.FileNotAdded = Convert.ToInt32(TempData["FileNotAdded"]);
        //    result._directoryModel.ErrorMessage = Convert.ToString(TempData["ErrorMessage"]);
            

        //    return View(result._directoryModel);
            
        //}
        public ActionResult Index()
        {
            int maxRows = 10;
            DirectoryModel model = new DirectoryModel();
            DirectoryResponse request = new DirectoryResponse();
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.StartIndex = "1";
            model.EndIndex = maxRows.ToString();
            DirectoryResponse result = directoryAccess.SelectAll(model);
            result._directoryModel.TotalCount = Convert.ToInt32(TempData["TotalCount"]);
            result._directoryModel.InsertedCount = Convert.ToInt32(TempData["InsertedCount"]);
            result._directoryModel.UpdatedCount = Convert.ToInt32(TempData["UpdatedCount"]);
            result._directoryModel.PendingCount = Convert.ToInt32(TempData["PendingCount"]);
            result._directoryModel.DuplicateCount = Convert.ToInt32(TempData["DuplicateCount"]);
            result._directoryModel.FileNotAdded = Convert.ToInt32(TempData["FileNotAdded"]);
            result._directoryModel.ErrorMessage = Convert.ToString(TempData["ErrorMessage"]);


            result._directoryModel.TotalRecords = result._directoryModel.PageCount;

            double pageCount = (double)((decimal)result._directoryModel.PageCount / Convert.ToDecimal(maxRows));
            result._directoryModel.PageCount = (int)Math.Ceiling(pageCount);

            result._directoryModel.CurrentPageIndex = 1;
            result._directoryModel.CurrentPageIndex1 = 1;
            result._directoryModel.StartIndex = "1";
            result._directoryModel.EndIndex = model.EndIndex;
            result._directoryModel.TotalCountlist = maxRows;
            return View(result._directoryModel);

        }
        private DirectoryModel Getdirectory(DirectoryModel model)
        {
            int maxRows = model.TotalCountlist;
          
            DirectoryResponse request = new DirectoryResponse();
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            int max = maxRows - 1;


            if (model.CurrentPageIndex==1 || model.CurrentPageIndex1==1)
            {
                model.StartIndex = "1";
                model.EndIndex = maxRows.ToString();
            }
            else
            {
                int endindex = model.CurrentPageIndex * maxRows;
                model.EndIndex = endindex.ToString();
                int startindex = endindex - max;
                model.StartIndex = startindex.ToString();
            }
            DirectoryResponse result = directoryAccess.SelectAll(model);
            ViewBag.PageHeader = "View Users";
            ViewBag.Message = result.ReturnMessage;
            result._directoryModel.TotalRecords= result._directoryModel.PageCount;
            double pageCount = (double)((decimal)result._directoryModel.PageCount / Convert.ToDecimal(maxRows));
            result._directoryModel.PageCount = (int)Math.Ceiling(pageCount);
            result._directoryModel.CurrentPageIndex =model.CurrentPageIndex;
            result._directoryModel.CurrentPageIndex1 =model.CurrentPageIndex;
            result._directoryModel.StartIndex = model.StartIndex;
            result._directoryModel.EndIndex = model.EndIndex;
            result._directoryModel.TotalCountlist = model.TotalCountlist;
            return result._directoryModel;
           
        }
        [HttpPost]
        public ActionResult Index(DirectoryModel model)
        {
            
            return View(Getdirectory(model));

        }
         //public ActionResult Index(DirectoryModel model)
        //{
        //    DirectoryResponse request = new DirectoryResponse();
        //    model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
        //    model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
        //    DirectoryResponse result = directoryAccess.SelectAll(model);
        //    ViewBag.PageHeader = "View Users";
        //    ViewBag.Message = result.ReturnMessage;
        //    return View(result._directoryModel);
        //}
        [HttpPost]
        public ActionResult CreateOrEdit(DirectoryModel model)
        {
           
            model.EncryptedLoggedId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            DirectoryResponse result = null;
            if (!String.IsNullOrEmpty(model.ImageBase64))
                model.LogoPath = Function.UploadCropImage(model.ImageBase64, "Directory");
            result = directoryAccess.AddorEdit(model);
            if (result._directoryModel!= null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                TempData["DirectoryModel"] = result._directoryModel;
                return RedirectToAction("index", "directory");
            }
            else
            {
                return RedirectToAction("error", "home");
            }

        }
        [HttpGet]
        public ActionResult Create(string uid)
        {
            //ModelState.Clear();

            DirectoryModel model = new DirectoryModel();
            DirectoryResponse request = new DirectoryResponse();
            model.EncryptedLoggedId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            DirectoryResponse result = directoryAccess.SelectById(model);
            ViewBag.PageHeader = "Basic Info";
            if (result._directoryModel!= null)
            {
                TempData["DirectoryModel"] = result._directoryModel;
                return View(result._directoryModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpGet]
        public ActionResult Edit(String uid)
        {
            DirectoryModel model = new DirectoryModel();
            DirectoryResponse request = new DirectoryResponse();
            model.EncryptedUserId =uid;
            model.EncryptedLoggedId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            DirectoryResponse result = directoryAccess.SelectById(model);
            if (result._directoryModel != null)
            {
                 TempData["DirectoryModel"] = result._directoryModel;
                return View(result._directoryModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        public ActionResult Detail(string uid)
        {
            DirectoryModel model = new DirectoryModel();
            model.EncryptedUserId = uid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedLoggedId = Function.ReadCookie("EncryptedUserId");
            DirectoryResponse result = directoryAccess.SelectById(model);
            return View(result._directoryModel);
        }
        public ActionResult LogBook()
        {
            DirectoryModel model = new DirectoryModel();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            DirectoryResponse result = directoryAccess.LogSelectAll(model);
            return View(result._directoryModel);
        }
        [HttpPost]
        public ActionResult LogBook(DirectoryModel model)
        {
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            DirectoryResponse result = directoryAccess.LogSelectAll(model);
            return View(result._directoryModel);
        }
        public PartialViewResult ImportExcel()
        {
            ///model.FilePath = HttpPostedFile;
            DirectoryModel model = new DirectoryModel();
            DirectoryResponse response = new DirectoryResponse(); //directoryAccess.UploadHttpPostedFile(model);
            return PartialView("_ImportExcel");
        }
        public JsonResult _ImportComplete(int TotalCount,int InsertedCount, int DuplicateCount,int PendingCount, int UpdatedCount)
        {
            DirectoryResponse response = new DirectoryResponse();
            DirectoryModel model = new DirectoryModel();
            model.TotalCount = TotalCount;
            model.InsertedCount = InsertedCount;
            model.DuplicateCount = DuplicateCount;
            model.PendingCount = PendingCount;
            model.UpdatedCount = UpdatedCount;


            return Json(model,JsonRequestBehavior.AllowGet);
        }
       
        [HttpPost]
        public ActionResult ImportExcel(DirectoryModel model)
        {
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedLoggedId = Function.ReadCookie("EncryptedUserId");
            
            DirectoryResponse response = directoryAccess.UploadHttpPostedFile(model);
            TempData["TotalCount"] = response._directoryModel.TotalCount;
            TempData["InsertedCount"] = response._directoryModel.InsertedCount;
            TempData["PendingCount"] = response._directoryModel.PendingCount;
            TempData["UpdatedCount"] = response._directoryModel.UpdatedCount;
            TempData["DuplicateCount"] = response._directoryModel.DuplicateCount;
            TempData["FileNotAdded"] = response._directoryModel.FileNotAdded;

            TempData["ErrorMessage"] = response._directoryModel.ErrorMessage;

            //TempData["ReturnCode"] = response.ReturnCode;
            //TempData["ReturnMessage"] = response.ReturnMessage;

            if (response._directoryModel != null)
            {
                return RedirectToAction("index", "directory");
            }
            else
            {
                //return PartialView("_importcomplete", response._directoryModel);
                return RedirectToAction("index", "directory");
            }
        }
       // [HttpPost]
        public JsonResult multipleleInvite(DirectoryModel directoryModel)
        {
            DirectoryModel model = new DirectoryModel();
            DirectoryResponse request = new DirectoryResponse();
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            DirectoryResponse result =directoryAccess.SelectAll1(model);
            DirectoryResponse response = new DirectoryResponse();
            response.ReturnCode = 1;
            response.ReturnMessage = "Success";
            if (result != null )
            {
                HostingEnvironment.QueueBackgroundWorkItem(cancellationToken => new DirectoryAccess().MultiInvite(result._directoryModel, cancellationToken));
                //response = directoryAccess.MultiInvite(result._directoryModel);
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
     
        //[HttpPost]

        public JsonResult SingleInvite(String did,string FName, String Email)
        {
            InviteModel model = new InviteModel();
            DirectoryResponse response = new DirectoryResponse();
            did = UtilityAccess.Decrypt(did);
            response.ReturnCode = 0;
            response.ReturnMessage = "Success";
              if (!String.IsNullOrEmpty(did))
            {
                response = directoryAccess.SingleInvite(Convert.ToInt32(did), FName, Email);
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(String lid)
        {
            DirectoryModel model = new DirectoryModel();
            model.EncryptedLoggedId = lid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            DirectoryResponse result = directoryAccess.Delete(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "directory");
        }
    }
}