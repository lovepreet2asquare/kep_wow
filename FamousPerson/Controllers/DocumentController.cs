﻿using FamousPerson.Models;
using FPBAL.Business;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class DocumentController : BaseController
    {
        DocumentAccess documentAccess = new  DocumentAccess();
        // GET: Document
        public ActionResult Index()
        {
            DocumentModel model = new DocumentModel();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            DocumentResponse result = documentAccess.SelectAll(model);
            return View(result._documentModel);
        }
        [HttpPost]
        public ActionResult Index(DocumentModel model)
        {
            
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            DocumentResponse result = documentAccess.SelectAll(model);
            return View(result._documentModel);
        }
        public ActionResult Create(string did)
        {
            DocumentModel model = new DocumentModel();
            DocumentResponse result = new DocumentResponse();
            model.EncryptedDocumentId = did;
            model.NoFile = null;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.DocumentId = 0;
            result._documentModel = model;
            return View(result._documentModel);
        }
        [HttpPost]
        public ActionResult Create(DocumentModel model)
        {
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            DocumentResponse result = new DocumentResponse();
            HttpPostedFileBase httpPostedFileBase = null;
            if (Request.Files.Count > 0)
            {
                //save picture
                httpPostedFileBase = Request.Files[0] as HttpPostedFileBase;
                if (httpPostedFileBase != null && httpPostedFileBase.ContentLength > 0)
                {
                    string timeStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                    string fileName = Path.GetFileName(httpPostedFileBase.FileName);
                    string ext = System.IO.Path.GetExtension(httpPostedFileBase.FileName);
                    fileName = fileName.Replace(" ","");
                    string thumbnailFilePath = string.Empty;
                    //Set thumbnail for documents for app use
                    if (ext.ToLower() == ".docx" || ext.ToLower() == ".doc")
                        thumbnailFilePath = "/thumbnail/word.png";
                    else if (ext.ToLower() == ".xlsx" || ext.ToLower() == ".xls")
                        thumbnailFilePath = "/thumbnail/excel.png";
                    else if (ext.ToLower() == ".jpeg" || ext.ToLower() == ".jpg" || ext.ToLower() == ".png")
                        thumbnailFilePath = "/thumbnail/png.png";
                    else if (ext.ToLower() == ".pdf")
                        thumbnailFilePath = "/thumbnail/pdf.png";
                    else 
                    {
                        model.NoFile = "Please enter file with specified extensions!";
                        
                    }

                    if (model.NoFile == null)
                    {
                        model.ThumbnailPath = thumbnailFilePath;
                        string filePath = "Upload/Document/" + timeStamp;
                        model.DocumentName = fileName;

                        model.DocumentPath = filePath+"/"+fileName;
                        Directory.CreateDirectory(Server.MapPath("~/" + filePath));
                        httpPostedFileBase.SaveAs(Server.MapPath("~/" + filePath) + "/" + fileName);
                        result = documentAccess.Add(model);
                    }

                }
                
            }
            if(model.NoFile!=null)
            {
                
                return View(model);
            }
             
            if (result._documentModel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                TempData["DocumentModel"] = result._documentModel;
                return RedirectToAction("index", "Document");
            }
            else
            {
                return RedirectToAction("error", "home");
            }
        }

        public ActionResult Delete(string did)
        {
            DocumentModel model = new DocumentModel();
            model.EncryptedDocumentId = did;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            DocumentResponse result = documentAccess.Delete(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "document");
        }
        public ActionResult LogBook()
        {
            DocumentModel model = new DocumentModel();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            DocumentResponse result = documentAccess.LogSelectAll(model);
            return View(result._documentModel);
        }
        [HttpPost]
        public ActionResult LogBook(DocumentModel model)
        {
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            DocumentResponse result = documentAccess.LogSelectAll(model);
            return View(result._documentModel);
        }
    }
}