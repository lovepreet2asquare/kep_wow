﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class DonationController : BaseController
    {
        // GET: Donation
        IDonation donationAccess = new DonationAccess();
        public ActionResult Index(string f)
        {
            //if (EmployeeAccess.IsAuthorize() != "")
            //    return Redirect(EmployeeAccess.IsAuthorize()); 

            DonationModel model = new DonationModel();
            //model.FromDate = UtilityAccess.FromDate();
            //model.ToDate = UtilityAccess.ToDate();
            model.UserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            //string type = "";
            //string duration = "7";
            DonationModel result = donationAccess.DonationSelectAll(model);
            if (result != null)
            {
                //if (result.FromDate == null || result.ToDate == null)
                //{
                //    result.FromDate = DateTime.Now.ToShortDateString();
                //    result.ToDate = DateTime.Now.ToShortDateString();
                //}
                TempData["DonationModel"] = result._DonationModel;
                return View(result);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpPost]
        public ActionResult Index(DonationModel model)
        {
            model.ToDate = UtilityAccess.ToDate(model.ToDate);
            model.UserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            //model.FromDate = UtilityAccess.FromDate();
            //model.ToDate = UtilityAccess.ToDate();
            DonationModel result = donationAccess.DonationSelectAll(model);
            if (result != null)
            {
                result.FromDate = model.FromDate;
                result.ToDate = UtilityAccess.FromDate(model.ToDate);
                TempData["DonationModel"] = result._DonationModel;
                return View(result);
            }
            else
                return RedirectToAction("error", "home");
        }
        public JsonResult DonationGraphData(string duration)
        {
            GraphsDataModel JsonData = donationAccess.DonationGraphData(duration);
            return Json(JsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult NotificationSetting()
        {
            RecurringTypeModel model = new RecurringTypeModel();

            model.UserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            RecurringTypeModel result = donationAccess.RecurringTypeSelectAll(model);
            if (result != null)
                return View(result);
            else
                return RedirectToAction("error", "home");
        }
        [HttpPost]
        public ActionResult NotificationSetting(RecurringTypeModel model)
        {
            model.UserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            RecurringTypeModel result = donationAccess.RecurringTypeSelectAll(model);
            if (result != null)
            {
                return View(result);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpGet]
        public ActionResult AddDonationType(String uid)
        {
            //pid = String.IsNullOrEmpty(pid) ? "0" : Cypher.Decrypt(pid);
            uid = String.IsNullOrEmpty(uid) ? "0" : UtilityAccess.Decrypt(uid);

            RecurringTypeModel result = donationAccess.RecurringTypeById(uid);

            return PartialView("_RecurringType", result);
        }
        [HttpPost]
        public ActionResult AddDonationType(RecurringTypeModel model)
        {
            model.UserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            RecurringTypeModel modell = new RecurringTypeModel();
            RecurringTyperesponse response = new RecurringTyperesponse();
            response = donationAccess.RecurringTypeInsert(model);

            TempData["ReturnCode"] = response.ReturnCode;
            TempData["ReturnMessage"] = response.ReturnMessage;
            TempData["href"] = "/donation/notificationsetting";
            return RedirectToAction("notificationsetting", "donation");
            //if (result != null)
            //    return View(result);
            //else
            //    return RedirectToAction("error", "home");
        }

        [HttpGet]
        public ActionResult EditDonationType(String uid)
        {
            //pid = String.IsNullOrEmpty(pid) ? "0" : Cypher.Decrypt(pid);
            uid = String.IsNullOrEmpty(uid) ? "0" : UtilityAccess.Decrypt(uid);

            RecurringTypeModel result = donationAccess.RecurringTypeById(uid);

            return PartialView("_EditRecuringType", result);
        }
        [HttpPost]
        public ActionResult EditDonationType(RecurringTypeModel model)
        {
            model.UserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            RecurringTypeModel modell = new RecurringTypeModel();
            RecurringTyperesponse response = new RecurringTyperesponse();
            response = donationAccess.RecurringTypeInsert(model);

            TempData["ReturnCode"] = response.ReturnCode;
            TempData["ReturnMessage"] = response.ReturnMessage;
            TempData["href"] = "/donation/notificationsetting";
            return RedirectToAction("notificationsetting", "donation");
        }
        public ActionResult Delete(String uid)
        {
            RecurringTypeModel model = new RecurringTypeModel();
            //uid = String.IsNullOrEmpty(uid) ? "0" : UtilityAccess.Encrypt(uid);
            model.RecurringTypeId =Convert.ToInt32(uid);
            model.UserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

            RecurringTyperesponse result = donationAccess.Delete(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                //TempData["href"] = "/about/index";
            }

            return RedirectToAction("notificationsetting", "donation", new { uid = uid });
        }

 

        public ActionResult DonorDetail(string fid,string pid)
        {

            DonationModel model = new DonationModel();
            //model.FromDate = UtilityAccess.FromDate();
            if (pid==null)
            {
              var eid = TempData["EncryptedPaymentId"];
                model.EncryptedPaymentId =Convert.ToString(eid);
            }
            else
            {
                model.EncryptedPaymentId = pid;
            }
            if (model.SearchStatus != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Join(",", model.SearchStatus));
                model.MultiStatus = sb.ToString();
            }
            model.EncryptedFollowerId = fid;            
            
            model.FollowerId = UtilityAccess.Decrypt(fid);
           //model.ToDate = UtilityAccess.ToDate();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.UserId = UtilityAccess.Decrypt(model.EncryptedUserId);
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.MultiStatus = "Paid,Refunded,Scheduled,Unsettled";
            //string type = "";
            //string duration = "7";
            DonationResponse result = donationAccess.DetailSelectAll(model);
            // for print
            Session["DonationModelPrint"] = result.DonationModel;

            if (result != null)
            {
                if (result.DonationModel.FromDate == null || result.DonationModel.ToDate == null)
                {
                    //result.DonationModel.FromDate = DateTime.Now.ToShortDateString();
                    //result.DonationModel.ToDate = DateTime.Now.ToShortDateString();
                }
                Session["DonationModel"] = result.DonationModel._DonorDetail;
                Session["DonationModel1"] = result.DonationModel;

                TempData["EncryptedPaymentId"] = result.DonationModel.EncryptedPaymentId;
                return View(result.DonationModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpPost]
        public ActionResult DonorDetail(DonationModel model)
        {
            //model.FromDate = UtilityAccess.FromDate();
            //model.ToDate = UtilityAccess.ToDate();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            if (model.SearchStatus != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Join(",", model.SearchStatus));
                model.MultiStatus = sb.ToString();
            }
            else
            {
                model.MultiStatus = "Paid,Refunded,Scheduled,Unsettled";

            }
            DonationResponse result = donationAccess.DetailSelectAll(model);
            result.DonationModel.FromDate = model.FromDate;
            result.DonationModel.ToDate = model.ToDate;
            Session["DonationModelPrint"] = result.DonationModel;
            if (result != null)
                return View(result.DonationModel);
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public JsonResult AmountChange(String id, String amount, String pid, String fid)
        {
            DonationModel model = new DonationModel();
            //if(!String.IsNullOrEmpty(id))
            //    model.PaymentId =Convert.ToInt32(id);
            if (!String.IsNullOrEmpty(amount))
                model.Amount = Convert.ToDecimal(amount);

            //model.Amount = amount;
            //model.EncryptedFollowerId = fid;
            model.EncryptedPaymentId = pid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            Int32 result = donationAccess.AmountChange(model);
           // if (result != null)
                return Json(result, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("DonorDetail", "Donation",new { fid = model.EncryptedFollowerId, pid = model.EncryptedPaymentId });
            //  else
            //return RedirectToAction("error", "home");
        }

        [HttpPost]
        //public ActionResult DonorRefund(DonationModel model)
        public ActionResult DonorRefund(DonationModel model)
        {
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedFollowerId = UtilityAccess.Encrypt(model.FollowerId);
            model.EncryptedPaymentId =UtilityAccess.Encrypt(Convert.ToString(model.PaymentId));
            DonationResponse result = donationAccess.DonorRefund(model);
            if(result.ReturnCode==25)
            {
                TempData["ReturnCode"] = -5;
                TempData["ReturnMessage"] = result.ReturnMessage;
                return RedirectToAction("DonorDetail", "donation", new { fid = model.EncryptedFollowerId , pid = model.EncryptedPaymentId });

            }
            else if (result != null)
            {
                result.DonationModel = new DonationModel();
                result.DonationModel.EncryptedPaymentId = UtilityAccess.Encrypt(model.PaymentId.ToString());
                 result.DonationModel.EncryptedFollowerId = UtilityAccess.Encrypt(model.FollowerId.ToString());
                //return RedirectToAction("donordetail", "donation", new { pid = model.EncryptedPaymentId });
                //return Json(result, JsonRequestBehavior.AllowGet);
                TempData["ReturnCode"] = 26;
                TempData["ReturnMessage"] = result.ReturnMessage;
                //return RedirectToAction("ViewRefund", "Donation",new {pid = result.DonationModel.EncryptedPaymentId });
                return RedirectToAction("index", "Donation");
                }

            else
                return RedirectToAction("error", "home");
            //return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SkipPayment(DonationModel model)
        {
            model.UserId = UtilityAccess.Decrypt(Function.ReadCookie("EncryptedUserId"));
            model.SessionToken = UtilityAccess.Decrypt(Function.ReadCookie("EncryptedSessionToken"));
            //get pid

            Int32 result = donationAccess.DonorSkip(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult InvoicePrint(String sid, String type, String email)
        {
            DonationModel result = new DonationModel();
            Int32 retVal = -1;
            result.ReturnCode = -1;
            
            List<DonationModel> model = (List<DonationModel>)Session["DonationModel"] ;
            DonationModel modell = (DonationModel)Session["DonationModel1"];
            //model[1].UserId = Function.ReadCookie("EncryptedUserId");
            //model[1].UserId= UtilityAccess.Decrypt(model[1].EncryptedUserId);
            result.FilePath = donationAccess.PdfPrintShare(model, modell, type, Function.ReadCookie("EncryptedUserId"), email, out retVal);
            if (!String.IsNullOrEmpty(result.FilePath))
                result.FilePath = UtilityAccess.Encrypt(result.FilePath);

            result.ReturnCode = retVal;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PrintDonationMain()
        {
            DonationModel data = new DonationModel();
            if (Session["DonationModelPrint"] != null)
                data = (DonationModel)Session["DonationModelPrint"];
            
            return PartialView("_PrintDonationMain", data);
        }


        [HttpPost]
        public ActionResult PrintDonation(DonationModel data)
        {
            Session["DonationModel2"] = data;
            return PartialView("_PrintDonation", data);
        }

        [HttpPost]
        public ActionResult Printdonationreceipt(String sid, String type, String email)
        {
            DonationModel result = new DonationModel();
            Int32 retVal = -1;
            result.ReturnCode = -1;

            DonationModel modell = (DonationModel)Session["DonationModel2"];

            //model[1].UserId = Function.ReadCookie("EncryptedUserId");
            //model[1].UserId= UtilityAccess.Decrypt(model[1].EncryptedUserId);
            result.FilePath = donationAccess.PdfPrintreceipt(modell, type, Function.ReadCookie("EncryptedUserId"), email, out retVal);
            if (!String.IsNullOrEmpty(result.FilePath))
                result.FilePath = UtilityAccess.Encrypt(result.FilePath);

            result.ReturnCode = retVal;

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult IssueRefund(String pid,string fid,String mid,string yid,String tid,string cid,string eid, string nid)
        {
            DonationModel model = new DonationModel();
            if (!String.IsNullOrEmpty(pid))
                model.PaymentId = Convert.ToInt32(UtilityAccess.Decrypt(pid));
            if (!String.IsNullOrEmpty(fid))
                model.FollowerId = UtilityAccess.Decrypt(fid);
            if (!String.IsNullOrEmpty(tid))
                model.TransactionId = UtilityAccess.Decrypt(tid);
            model.Email = UtilityAccess.Decrypt(eid);
            model.FirstName = UtilityAccess.Decrypt(nid);
            model.TransactionId = UtilityAccess.Decrypt(tid);
            model.ExpMonth = (UtilityAccess.Decrypt(mid));
            model.ExpYear = (UtilityAccess.Decrypt(yid));
            model.CardNumber = UtilityAccess.Decrypt(cid);
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            DonationResponse result = donationAccess.IssueRefund(model);
            model.EncryptedFollowerId = UtilityAccess.Encrypt(fid);
            if (result.DonationModel != null)
            {
                return View(result.DonationModel);
                //  return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
                return RedirectToAction("error", "home");
        }
        public ActionResult ViewRefund(String pid,string pmid,string fid)
        {
            DonationModel model = new DonationModel();
            if (!String.IsNullOrEmpty(pid))
                model.PaymentId = Convert.ToInt32(UtilityAccess.Decrypt(pid));
            //if (!String.IsNullOrEmpty(pmid))
            //    model.PaymentMethodId =Convert.ToInt32(UtilityAccess.Decrypt(pmid));

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            DonationResponse result = donationAccess.ViewRefund(model);
            if (result.DonationModel != null)
            {
                return View(result.DonationModel);
                //  return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
                return RedirectToAction("error", "home");
        }

    }
}