﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;

namespace FamousPerson.Controllers
{
    public class EventController : BaseController
    {
        // GET: Event
        IEventAccess eventAccess = new EventAccess();
        
        public ActionResult Index()
        {
            EventModel model = new EventModel();
            EventResponse request = new EventResponse();
            string fid = TempData["ReturnCode"] == null ? "1" : TempData["ReturnCode"].ToString();
            model.IsView = fid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            
            EventResponse result = eventAccess.EventSelectAll(model);
            
            ViewBag.PageHeader = "Event";
            return View(result._EventModel);
        }

        [HttpPost]
        public ActionResult Index(EventModel model)
        {
            EventResponse request = new EventResponse();
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            EventResponse result = eventAccess.EventSelectAll(model);
            //result.UserModel.EncryptedUserId = model.EncryptedUserId;
            ViewBag.PageHeader = "Event";
            return View(result._EventModel);
        }

        public ActionResult Detail(string eid)
        {
            EventModel model = new EventModel();
            model.EncryptedEventId= eid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            //NewsResponse result = newsAccess.NewsSelectAll(model);
            model.EventId = Convert.ToInt32(UtilityAccess.Decrypt(eid));

            EventResponse result = eventAccess.EventDetail(model);
            //ViewBag.PageHeader = "Basic Info";
            //return View(result.UserModel);
            if (result._EventModel != null)
            {
                result._EventModel.EncryptedEventId= eid;
                TempData["EventModel"] = result._EventModel;
                return View(result._EventModel);
            }
            else
                return RedirectToAction("error", "home");
            //return View();
        }

        public ActionResult Create(string eid)


        {
            EventModel model = new EventModel();
            model.EncryptedEventId= eid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            EventResponse result = eventAccess.EventDetail(model);
            result._EventModel.Status = "Published";
            result._EventModel.EncryptedEventId = model.EncryptedEventId;
            if (result._EventModel != null)
            {
                TempData["EventModel"] = result._EventModel;
                result._EventModel.Create = 0;
                return View(result._EventModel);
            }
            else
                return RedirectToAction("error", "home");
        }


        [HttpPost]
        public ActionResult Create(EventModel model, string[] CountyId, string[] LocalID, string[] IndividualId)
        {
            //,string[] CountyId,string[] LocalID,string[] IndividualId
            model.EventId = model.EventId;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            EventResponse result = null;
            
                if (CountyId != null)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(string.Join(",", CountyId));
                    model.SelectedCountyId = sb.ToString();
                }
                else if (LocalID != null)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(string.Join(",", LocalID));
                    model.SelectedLocalId = sb.ToString();
                }
                else if (IndividualId != null)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(string.Join(",", IndividualId));
                    model.SelectedIndividualId =sb.ToString();
                }

            HttpPostedFileBase httpPostedFileBase = null;
            if (Request.Files.Count > 0)
            {
                //save picture
                httpPostedFileBase = Request.Files[0] as HttpPostedFileBase;
                if (httpPostedFileBase != null && httpPostedFileBase.ContentLength > 0)
                {
                    string timeStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                    string fileName = Path.GetFileName(httpPostedFileBase.FileName);
                    string ext = System.IO.Path.GetExtension(httpPostedFileBase.FileName);
                    fileName = fileName.Replace(" ", "");
                    string fileType = string.Empty;
                    ext = ext.ToLower();
                    //validate files
                    if (ext == ".docx" || ext == ".doc" || ext == ".jpeg" || ext == ".jpg" || ext == ".png" || ext == ".pdf")
                        fileType = ext.Replace(".", "");
                    else
                    {
                        model.NoFile = "Please enter file with specified extensions!";

                    }

                    //if (model.NoFile == null)
                    {
                        model.FileType = fileType;
                        string filePath = "Upload/Event/" + timeStamp;
                        
                        model.FilePath = filePath + "/" + fileName;
                        model.LogoPath = filePath + "/" + fileName;
                        Directory.CreateDirectory(Server.MapPath("~/" + filePath));
                        httpPostedFileBase.SaveAs(Server.MapPath("~/" + filePath) + "/" + fileName);
                        
                    }

                }

            }

            result = eventAccess.AddorEdit(model);
            if (result._EventModel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                TempData["EventModel"] = result._EventModel;
                return RedirectToAction("index", "event");
            }
            else
            {
                return RedirectToAction("error", "home");
            }
           
        }
        [HttpPost]
        public ActionResult  AddAgenda(String Text)
        {
            EventModel model = new EventModel();
            model.AgendaAdd = Text;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            EventResponse result = eventAccess.AddAgenda(model);

            return Json(result, JsonRequestBehavior.AllowGet);
            //return PartialView("_AddAgenda", result._EventModel);

        }
        public ActionResult Edit(string eid,string nid)
        {
            EventModel model = new EventModel();
            model.EncryptedEventId = eid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            
            EventResponse result = eventAccess.EventDetail(model);

            result._EventModel.EventDateMessage= UtilityAccess.Decrypt(nid);
            if (result._EventModel != null)
            {
                //if (result._EventModel.Status != "Scheduled")
                //{
                //    result._EventModel.PublishDate = string.Empty;
                //}
                TempData["EventModel"] = result._EventModel;
                TempData["PublishDate"] = result._EventModel.PublishCalDate;
                TempData["eid"] = result._EventModel.EventId;
                result._EventModel.Edit = 1;
                if ((result._EventModel.Status == "Scheduled")&& (result._EventModel.PublishDate == ""))
                {
                    
                        result._EventModel.PublishDate = result._EventModel.Status == "Scheduled" ? result._EventModel.PublishCalDate : "";
                    
                }
                else if ((result._EventModel.Status == "Requested")&& (result._EventModel.PublishDate == ""))
                {
                   
                        result._EventModel.PublishDate = result._EventModel.Status == "Requested" ? result._EventModel.PublishCalDate : "";
                   
                }
                if (result._EventModel.Status == "Requested")
                {
                    result._EventModel.Status = "Scheduled";
                }
                TempData["ReturnMessage"] = TempData["Message"];
                return View(result._EventModel);

            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpPost]
        public ActionResult Edit(EventModel model, string[] CountyId, string[] LocalID, string[] IndividualId)
        {
            
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            var Result = Convert.ToDateTime(TempData["PublishDate"]);
            var EventDate = Convert.ToDateTime(model.EventDate);
            if (CountyId != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Join(",", CountyId));
                model.SelectedCountyId = sb.ToString();
            }
            else if (LocalID != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Join(",", LocalID));
                model.SelectedLocalId = sb.ToString();
            }
            else if (IndividualId != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Join(",", IndividualId));
                model.SelectedIndividualId = sb.ToString();
            }
            if (Result > EventDate)
            {
                 model.EventDateMessage = "Event Date must refer to future";
                 return RedirectToAction("edit", "event", new { eid = UtilityAccess.Encrypt(Convert.ToString(model.EventId)),nid = UtilityAccess.Encrypt(Convert.ToString(model.EventDateMessage))});
                //return View(model.EventDateMessage);
            }

            EventResponse result = null;
            //if (ModelState.IsValid)
            //{
            //    if (!String.IsNullOrEmpty(model.ImageBase64))
            //model.LogoPath = Function.UploadCropImage(model.ImageBase64, "Event");
            HttpPostedFileBase httpPostedFileBase = null;
            if (Request.Files.Count > 0)
            {
                //save picture
                httpPostedFileBase = Request.Files[0] as HttpPostedFileBase;
                if (httpPostedFileBase != null && httpPostedFileBase.ContentLength > 0)
                {
                    string timeStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                    string fileName = Path.GetFileName(httpPostedFileBase.FileName);
                    string ext = System.IO.Path.GetExtension(httpPostedFileBase.FileName);
                    fileName = fileName.Replace(" ", "");
                    string fileType = string.Empty;
                    ext = ext.ToLower();
                    //validate files
                    if (ext == ".docx" || ext == ".doc" || ext == ".jpeg" || ext == ".jpg" || ext == ".png" || ext == ".pdf")
                        fileType = ext.Replace(".", "");
                    else
                    {
                        model.NoFile = "Please enter file with specified extensions!";

                    }

                    //if (model.NoFile == null)
                    {
                        model.FileType = fileType;
                        string filePath = "Upload/Event/" + timeStamp;

                        model.FilePath = filePath + "/" + fileName;
                        model.LogoPath = filePath + "/" + fileName;
                        Directory.CreateDirectory(Server.MapPath("~/" + filePath));
                        httpPostedFileBase.SaveAs(Server.MapPath("~/" + filePath) + "/" + fileName);

                    }

                }

            }

            result = eventAccess.AddorEdit(model);
                if (result._EventModel != null)
                {
                    TempData["ReturnCode"] = result.ReturnCode;
                    TempData["ReturnMessage"] = result.ReturnMessage;
                    TempData["EventModel"] = result._EventModel;
                    return RedirectToAction("index", "event");
                }
                else
                    return RedirectToAction("error", "home");
            //}
            //else
            //{
                //model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
                //model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
                //result = eventAccess.EventDetail(model);
                //return View(result._EventModel);
            //}
        }
        public ActionResult Delete(string eid)
        {
            EventModel model = new EventModel();
            model.EncryptedEventId = eid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            EventResponse result = eventAccess.Delete(model);
            //ViewBag.PageHeader = "Delete headline";
            //return View(result._HeadlineModel);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "event");
        }
        public ActionResult Archive(string eid)
        {
            EventModel model = new EventModel();
            model.EncryptedEventId = eid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            EventResponse result = eventAccess.Archive(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }
            return RedirectToAction("index", "event");
        }
        public ActionResult Preview(EventModel model)
        {
            EventResponse result = new EventResponse();
            result._EventModel= new EventModel();
            result._EventModel.EventDayLeft = model.EventDayLeft;
            result._EventModel.PublishDate = model.PublishDate;
            result._EventModel.Agenda = model.Agenda;
            result._EventModel.Title = model.Title;
            result._EventModel.Content = model.Content;
            result._EventModel.LogoPath = model.LogoPath;
            result._EventModel.AgendaId = model.AgendaId;
            result._EventModel.PublishDate = model.PublishDate;
            result._EventModel.InvitationType = model.InvitationType;
            result._EventModel.AddressLine1 = model.AddressLine1;
            result._EventModel.AddressLine2 = model.AddressLine2;
            result._EventModel.StateName = model.StateName;
            result._EventModel.Summary = model.Summary;
            result._EventModel.EventTime = model.EventTime;
            result._EventModel.EventDate = model.EventDate;
            ViewBag.PageHeader = "Basic Info";
            //return View(result._HeadlineModel);
            return PartialView("_Preview", result._EventModel);
        }
        public ActionResult Count(string eid)
        {
            EventModel model = new EventModel();
            EventResponse request = new EventResponse();
            model.EncryptedEventId = eid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");

            EventResponse result = eventAccess.EventCount(model);
            Session["EventModel"] = result._EventModel._EventList;
            Session["EventModel1"] = result._EventModel;
            return View(result._EventModel);
        }
        [HttpPost]
        public ActionResult Download(String sid, String type, String email,String name,String title,String count)
        {
            EventResponse response= new EventResponse();
            EventModel result= new EventModel();
            Int32 retVal = -1;
            response.ReturnCode = -1;
            result.Email = email;
            result.FirstName = name;
            result.Title = title;
            result.AttendeeCount = count;
            List<EventModel> model = (List<EventModel>)Session["EventModel"];
            EventModel Model = (EventModel)Session["EventModel1"];
            result.FilePath = eventAccess.PdfPrintShare(model, Model, type, Function.ReadCookie("EncryptedUserId"), email, out retVal);

            if (!String.IsNullOrEmpty(result.FilePath))
                result.FilePath = UtilityAccess.Encrypt(result.FilePath);
                String file= result.FilePath;

            
            response._EventModel = result;
            response._FilePath = file;
            response.ReturnCode = retVal;

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}