﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class FAQController : BaseController
    {
        
        IFAQ faqAccess = new FAQAccess();
        public ActionResult Index()
        {

            FAQModel model = new FAQModel();
            FAQResponse response = new FAQResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            FAQResponse result = faqAccess.SelectAll(model);
            if (result._FAQModel != null)
            {

                return View(result._FAQModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpPost]
        public ActionResult Index(FAQModel model)
        {
            
            FAQResponse response = new FAQResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            FAQResponse result = faqAccess.SelectAll(model);
            if (result._FAQModel != null)
            {

                return View(result._FAQModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpPost]
        public ActionResult Addfaq(String Text)
        {
            FAQModel model = new FAQModel();
            model.Title = Text;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            FAQResponse result = faqAccess.AddFaq(model);

            return Json(result, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult AddResponse(String Res,String Fid)
        {
            FAQModel model = new FAQModel();
            if(Res=="No")
            {
                model.Response = "0";
            }
            else
            {
                model.Response = "1";
            }
            model.EncryptedFaqId = Fid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            FAQResponse result = faqAccess.AddRes(model);

            return Json(result, JsonRequestBehavior.AllowGet);

        }
    }
}