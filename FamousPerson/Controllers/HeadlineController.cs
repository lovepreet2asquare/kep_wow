﻿using FamousPerson.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPModels.Models;
using FPBAL.Business;
using FPBAL.Interface;
using System.IO;
namespace FamousPerson.Controllers
{
    public class HeadlineController : BaseController
    {
        IHeadline headLineAccess = new HeadlineAccess();
        // GET: Headline
        public ActionResult Index()
        {
            HeadlineModel model = new HeadlineModel();
            
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            string fid = TempData["ReturnCode"] == null ? "1" : TempData["ReturnCode"].ToString();
            model.isView = fid;
            
            HeadlineResponse result = headLineAccess.SelectAll(model);
           
            ViewBag.Header = "Exclusive Headlines";
            return View(result._HeadlineModel);
        }

        [HttpPost]
        public ActionResult Index(HeadlineModel model)
        {
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            
            HeadlineResponse result = headLineAccess.SelectAll(model);
            ViewBag.Header = "Exclusive Headlines";
            return View(result._HeadlineModel);
        }

        public ActionResult Create(string hid)
        {
            HeadlineModel model = new HeadlineModel();
            model.EncryptedHeadLineId = hid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            HeadlineResponse result = headLineAccess.Select(model);
            result._HeadlineModel.Status = "Published";
            result._HeadlineModel.Create = 1;
            if (result._HeadlineModel != null)
            {
                result._HeadlineModel._HImagePathList = new List<ImageListModel>();
                for (int i = 0; i < 5; i++)
                {
                    result._HeadlineModel._HImagePathList.Add(new ImageListModel { });
                }
            }
            
            ViewBag.PageHeader = "Basic Info";
            return View(result._HeadlineModel);
        }
        public ActionResult Edit(string hid)
        {
            HeadlineModel model = new HeadlineModel();
            model.EncryptedHeadLineId = hid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            HeadlineResponse result = headLineAccess.Select(model);
            if (result._HeadlineModel._HImagePathList == null)
            {
                result._HeadlineModel._HImagePathList = new List<ImageListModel>();
                for (int i = 0; i < 5; i++)
                {
                    result._HeadlineModel._HImagePathList.Add(new ImageListModel { });
                }
            }

            var Result = TempData["HeadlineModel"];
            TempData["HeadlineModel"] = result._HeadlineModel;
            if (result._HeadlineModel.Status == "Scheduled")
            {
                result._HeadlineModel.PublishDate = result._HeadlineModel.Status == "Scheduled" ? result._HeadlineModel.PublishCalDate : "";
            }
            else if(result._HeadlineModel.Status == "Requested")
            {
                result._HeadlineModel.PublishDate = result._HeadlineModel.Status == "Requested" ? result._HeadlineModel.PublishCalDate : "";

            }
            if (result._HeadlineModel.Status == "Requested")
            {
                result._HeadlineModel.Status = "Scheduled";
            }
            result._HeadlineModel.Edit = 1;
            return View(result._HeadlineModel);
        }

        [HttpPost]
        public ActionResult CreateorEdit(HeadlineModel model)
        {
            Function function = new Function();
            if(model.UserTitle!=1)
            {
                if(model.Status=="Scheduled")
                {
                    model.Status = "Requested";
                }
            }
            else
            {
                if (model.Status == "Requested")
                {
                    model.Status = "Scheduled";
                }

            }
            if (model.PublishDate == "undefined")
            {
                model.PublishDate = null;
            }
            if (model.Draft != null)
            {
                model.Status = model.Draft;
            }
            model.HeadLineId = model.HeadLineId;
            string Message = "Maximum file size is 2 MB";
            string MessageExt = "Only .jpeg,.jpg,.png format";
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

            if (model._HImagePathList != null && model._HImagePathList.Count > 0)
            {
                for (int i = 0; i < model._HImagePathList.Count; i++)
                {
                    if (!String.IsNullOrEmpty(model._HImagePathList[i].ImageBase64))
                    {
                        model._HImagePathList[i].ImagePath = Function.UploadCropImage(model._HImagePathList[i].ImageBase64, "Headline");
                    }

                }
            }
           
            if (model.Size != Message && model.Size != MessageExt)
            {

                HeadlineResponse result = headLineAccess.AddorEdit(model);
                if (result._HeadlineModel != null)
                {
                    TempData["ReturnCode"] = result.ReturnCode;
                    TempData["ReturnMessage"] = result.ReturnMessage;
                    TempData["HeadlineModel"] = result._HeadlineModel;
                    return RedirectToAction("index", "headline");
                }
            }
           
            return View(model);
        }

        public ActionResult Delete(string hid)
        {
            HeadlineModel model = new HeadlineModel();
            model.EncryptedHeadLineId = hid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            HeadlineResponse result = headLineAccess.Delete(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "headline");
        }
        public ActionResult Archive(string id)
        {
            HeadlineModel model = new HeadlineModel();
            model.EncryptedHeadLineId = id;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            HeadlineResponse result = headLineAccess.Archive(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }
            return RedirectToAction("index", "headline");
            
        }

        public ActionResult Detail(string hid)
        {
            HeadlineModel model = new HeadlineModel();
            model.EncryptedHeadLineId = hid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            HeadlineResponse result = headLineAccess.Select(model);
            ViewBag.PageHeader = "Basic Info";
            return View(result._HeadlineModel);
        }
       

        public ActionResult ReadMore(string hid)
        {
            HeadlineModel model = new HeadlineModel();
            model.EncryptedHeadLineId = hid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            HeadlineResponse result = headLineAccess.Select(model);
            ViewBag.PageHeader = "Basic Info";
            return View(result._HeadlineModel);
        }
        public ActionResult Preview(HeadlineModel model)
        {
            //model = new HeadlineModel();
            //model.EncryptedHeadLineId = hid;
            //model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            //model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            //HeadlineResponse result = headLineAccess.Select(model);

            HeadlineResponse result = new HeadlineResponse();
            result._HeadlineModel = new HeadlineModel();
            //result._HeadlineModel.HeadLineId = 0;
            //model.Status = "Published";
            //result._HeadlineModel.Status = model.Status;
            result._HeadlineModel.PublishDate = model.PublishDate;
            result._HeadlineModel.PublishTime = model.PublishTime;
            result._HeadlineModel.Title = model.Title;
            result._HeadlineModel.Content = model.Content;
            result._HeadlineModel.ImagePath = model.ImagePath;
            if (result._HeadlineModel != null)
            {
                result._HeadlineModel._HImagePathList = new List<ImageListModel>();
                for (int i = 0; i < 5; i++)
                {
                    result._HeadlineModel._HImagePathList.Add(new ImageListModel { });
                }
            }

            ViewBag.PageHeader = "Basic Info";
            //return View(result._HeadlineModel);
            return PartialView("_Preview", result._HeadlineModel);
        }
    }
}