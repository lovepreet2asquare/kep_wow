﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class ITSupportController : BaseController
    {
        // GET: ITSupport
        public ActionResult Tickets()
        {
            return View();
        }
        public ActionResult Invoice()
        {
            return View();
        }
        public ActionResult PaidService()
        {
            return View();
        }
    }
}