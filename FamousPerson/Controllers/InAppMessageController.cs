﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class InAppMessageController : BaseController
    {

        IAppMessage appMessageAccess = new InAppMessageAccess();
        // GET: InAppMessage
        public ActionResult Edit(string mid)
        {
            InAppMessageModel model = new InAppMessageModel();
            model.MessageId = mid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            InAppMessageResponse result = appMessageAccess.DetailView(model);
            result.InAppMessageModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
            result.InAppMessageModel.DateTo = UtilityAccess.FromDate(model.DateTo);
            result.InAppMessageModel.Edit = 1;
            if (result.InAppMessageModel.Status == "Scheduled")
            {
                result.InAppMessageModel.PublishDate = result.InAppMessageModel.Status == "Scheduled" ? result.InAppMessageModel.PublishCalDate : "";
            }
            else if (result.InAppMessageModel.Status == "Requested")
            {
                result.InAppMessageModel.PublishDate = result.InAppMessageModel.Status == "Requested" ? result.InAppMessageModel.PublishCalDate : "";

            }
            if (result.InAppMessageModel.Status == "Requested")
            {
                result.InAppMessageModel.Status = "Scheduled";
            }
            if (result != null)
            {

                return View(result.InAppMessageModel);
            }

            return View();
        }
        [HttpPost]
        public ActionResult Edit(InAppMessageModel model, string[] CountyId, string[] LocalID, string[] IndividualId)
        {
            InAppMessageResponse result = new InAppMessageResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            if(CountyId != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Join(",", CountyId));
                model.SelectedCountyId = sb.ToString();
            }
            else if (LocalID != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Join(",", LocalID));
                model.SelectedLocalId = sb.ToString();
            }
            else if (IndividualId != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Join(",", IndividualId));
                model.SelectedIndividualId = sb.ToString();
            }
            result = appMessageAccess.AddOrEdit(model);
            //}
            TempData["ReturnCode"] = result.ReturnCode;
            TempData["ReturnMessage"] = result.ReturnMessage;
            return RedirectToAction("index", "InAppMessage");
        }
        public ActionResult Index()
        {
            InAppMessageModel model = new InAppMessageModel();
            InAppMessageResponse response = new InAppMessageResponse();
            string fid = TempData["ReturnCode"] == null ? "1" : TempData["ReturnCode"].ToString();
            model.isview = fid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
           
            InAppMessageResponse result = appMessageAccess.Detail(model);
            if (result.InAppMessageModel != null)
            {
                
                return View(result.InAppMessageModel);
            }
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public ActionResult Index(InAppMessageModel model)
        {

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            InAppMessageResponse result = new InAppMessageResponse();
            result.InAppMessageModel = new InAppMessageModel();
            result = appMessageAccess.Detail(model);
            if (result != null)
            {

                return View(result.InAppMessageModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        public ActionResult DetailView(string mid)
        {
            InAppMessageModel model = new InAppMessageModel();
            InAppMessageResponse response = new InAppMessageResponse();
            model.MessageId = mid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

            InAppMessageResponse result = appMessageAccess.DetailView(model);
            if (result.InAppMessageModel != null)
            {
                result.InAppMessageModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                result.InAppMessageModel.DateTo = UtilityAccess.FromDate(model.DateTo);
                return View(result.InAppMessageModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpPost]
        public ActionResult DetailView(string mid, InAppMessageModel model)
        {
            InAppMessageResponse response = new InAppMessageResponse();
            model.MessageId = Convert.ToString(mid);
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

          
            return RedirectToAction("create", "inappmessage");
        }
        public ActionResult _Dashboard()
        {
            InAppMessageModel model = new InAppMessageModel();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            InAppMessageResponse result = appMessageAccess.DetailView(model);
            result.InAppMessageModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
            result.InAppMessageModel.DateTo = UtilityAccess.FromDate(model.DateTo);
          
            if (result != null)
            {

                return View(result.InAppMessageModel);
            }

            return View();
        }
        public ActionResult Create(string mid)
        {
            InAppMessageModel model = new InAppMessageModel();
            model.MessageId = mid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            InAppMessageResponse result = appMessageAccess.DetailView(model);
            result.InAppMessageModel.Status = "Published";
            result.InAppMessageModel.Create = 1;
            result.InAppMessageModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
            result.InAppMessageModel.DateTo = UtilityAccess.FromDate(model.DateTo);
             if (result != null)
            {

                return View(result.InAppMessageModel);
            }

            return View();

        }
        [HttpPost]
        public ActionResult Create(InAppMessageModel model, string[] CountyId, string[] LocalID, string[] IndividualId)
        {
            InAppMessageResponse result = new InAppMessageResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            if (CountyId != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Join(",", CountyId));
                model.SelectedCountyId = sb.ToString();
            }
            else if (LocalID != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Join(",", LocalID));
                model.SelectedLocalId = sb.ToString();
            }
            else if (IndividualId != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Join(",", IndividualId));
                model.SelectedIndividualId = sb.ToString();
            }
            result = appMessageAccess.AddOrEdit(model);
            if (result.InAppMessageModel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }
            return RedirectToAction("index", "InAppMessage");

        }

        public ActionResult Delete(string mid)
        {
            InAppMessageModel model = new InAppMessageModel();
            model.MessageId = mid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            InAppMessageResponse result = appMessageAccess.Delete(model);

            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "inappmessage");
        }
        public ActionResult Archive(string mid)
        {
            InAppMessageModel model = new InAppMessageModel();
            model.MessageId = mid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            InAppMessageResponse result = appMessageAccess.Archive(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }
            return RedirectToAction("index", "inappmessage");
        }
        public ActionResult Preview(InAppMessageModel model)
        {
            InAppMessageResponse result = new InAppMessageResponse();
            result.InAppMessageModel = new InAppMessageModel();
            result.InAppMessageModel.PublishDate = model.PublishDate;
            result.InAppMessageModel.Title = model.Title;
            result.InAppMessageModel.Time = model.Time;
            result.InAppMessageModel.Content = model.Content;
            result.InAppMessageModel.StatusSchedule = model.StatusSchedule;
            result.InAppMessageModel.StatusPublish = model.StatusPublish;
            result.InAppMessageModel.StatusDraft = model.StatusDraft;
            ViewBag.PageHeader = "Basic Info";
            return PartialView("_Preview", result.InAppMessageModel);
        }
    }
}