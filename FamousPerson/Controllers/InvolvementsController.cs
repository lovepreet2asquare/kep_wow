﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class InvolvementsController : BaseController
    {
        // GET: Involvements
        IInvolvement involveAccess = new InvolvementAccess();
        public ActionResult Index(string f)
        {
            //if (EmployeeAccess.IsAuthorize() != "")
            //    return Redirect(EmployeeAccess.IsAuthorize()); ;

            InvolvementModel model = new InvolvementModel();
            //model.FromDate = UtilityAccess.FromDate();
            //model.Todate = UtilityAccess.ToDate();
            model.UserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            string type="";
            string duration = "7";
            InvolvementModel result = involveAccess.InvolvementGraphsData(model);
            if (result != null)
            {
                //result.FromDate = UtilityAccess.FromDate();
                //result.Todate = UtilityAccess.FromDate();
                return View(result);
            }
                
            else
                return RedirectToAction("error", "home");

        }
        //[HttpPost]
        //public ActionResult Index(string Type, String Duration)
        //{
        //    //model.Todate = UtilityAccess.ToDate(model.Todate);
        //    InvolvementModel result = involveAccess.InvolvementGraphsData(Type,Duration);
        //    if (result != null)
        //    {
        //        return View(result);
        //    }
        //    else
        //        return RedirectToAction("error", "home");

        //}

        
        [HttpPost]
        public ActionResult Index(InvolvementModel model)
        {
            model.Todate= UtilityAccess.ToDate(model.Todate);
            model.UserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            InvolvementModel result = involveAccess.InvolvementGraphsData(model);
            if (result != null)
            {
                result.FromDate = model.FromDate;
                result.Todate = UtilityAccess.FromDate(model.Todate);
                return View(result);
            }
            else
                return RedirectToAction("error", "home");
        }

        public JsonResult GetChartData(string duration)
        {
            GraphData JsonData = involveAccess.GraphData(duration);
            return Json(JsonData, JsonRequestBehavior.AllowGet);
        }
    }
}