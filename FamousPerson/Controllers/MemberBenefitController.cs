﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FamousPerson.Models;
using System.Web.Mvc;
using FPModels.Models;
using FPBAL.Business;
using FPBAL.Interface;

namespace FamousPerson.Controllers
{
    public class MemberBenefitController : BaseController
    {
        IMemberBenefit memberBenefitAccess = new MemberBenefitAccess();
        // GET: MemberBenefit
        public ActionResult Index()
        {
            MemberBenefitModel model = new MemberBenefitModel();

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            MemberBenefitResponse result = memberBenefitAccess.SelectAll(model);

            ViewBag.Header = "LPP";
            return View(result.MemberBenefit);
        }
        [HttpPost]
        public ActionResult Index(MemberBenefitModel model)
        {
            //MemberBenefitModel model = new MemberBenefitModel();

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            MemberBenefitResponse result = memberBenefitAccess.SelectAll(model);

            ViewBag.Header = "LPP";
            return View(result.MemberBenefit);
        }
        public ActionResult Create(string mid)
        {
            MemberBenefitModel model = new MemberBenefitModel();
            model.EncryptedMemberBenefitId = mid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.MemberBenefitId = 0;
            MemberBenefitResponse result = new MemberBenefitResponse();
            result.MemberBenefit = model;

            ViewBag.PageHeader = "Basic Info";
            return View(result.MemberBenefit);
        }
        public ActionResult Edit(string mid)
        {
            MemberBenefitModel model = new MemberBenefitModel();
            model.EncryptedMemberBenefitId = mid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            MemberBenefitResponse result = memberBenefitAccess.Select(model);
            TempData["MemberBenefitModel"] = result.MemberBenefit;
            ViewBag.PageHeader = "Basic Info";
            
            return View(result.MemberBenefit);
        }
        public ActionResult Detail(string mid)
        {
            MemberBenefitModel model = new MemberBenefitModel();
            model.EncryptedMemberBenefitId = mid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            MemberBenefitResponse result = memberBenefitAccess.Select(model);
            TempData["MemberBenefitModel"] = result.MemberBenefit;
            ViewBag.PageHeader = "Basic Info";

            return View(result.MemberBenefit);
        }
        [HttpPost]
        public ActionResult CreateorEdit(MemberBenefitModel model)
        {
            Function function = new Function();
            
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            {
                MemberBenefitResponse result = memberBenefitAccess.AddorEdit(model);
                if (result.MemberBenefit != null)
                {
                    TempData["ReturnCode"] = result.ReturnCode;
                    TempData["ReturnMessage"] = result.ReturnMessage;
                    TempData["MemberBenefitModel"] = result.MemberBenefit;
                    return RedirectToAction("index", "memberbenefit");
                }
            }
            return View(model);
        }
        //[HttpPost]
        public ActionResult Delete(string mid)
        {
            //Function function = new Function();
            MemberBenefitModel model = new MemberBenefitModel();
            model.EncryptedMemberBenefitId = mid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            {
                MemberBenefitResponse result = memberBenefitAccess.Delete(model);
                if (result.MemberBenefit != null)
                {
                    TempData["ReturnCode"] = result.ReturnCode;
                    TempData["ReturnMessage"] = result.ReturnMessage;
                    TempData["MemberBenefitModel"] = result.MemberBenefit;
                    return RedirectToAction("index", "memberbenefit");
                }
            }
            return View(model);
        }

    }
}