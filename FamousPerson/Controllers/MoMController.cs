﻿using FamousPerson.Models;
using System.Web.Mvc;
using FPModels.Models;
using FPBAL.Business;
using FPBAL.Interface;
using System.Web;
using System;
using System.IO;

namespace FamousPerson.Controllers
{
    public class MoMController : BaseController
    {
        IMoM momAccess = new MoMAccess();

        public ActionResult Index()
        {
            //FPResponse res = momAccess.ApnsNotification("c46cd1b392787bbc321e980ab3cd593c7b3d0c1bea138e6860b18c84e387460f", "this is testing only");

            MoMModel model = new MoMModel();
            model.Month = "-1";
            model.Year = -1;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            MoMResponse result = momAccess.SelectAll(model);

            ViewBag.Header = "Minutes of Meeting";
            return View(result.MinutesOfMeeting);
        }
        [HttpPost]
        public ActionResult Index(MoMModel model)
        {
            

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            MoMResponse result = momAccess.SelectAll(model);

            ViewBag.Header = "Minutes of Meeting";
            return View(result.MinutesOfMeeting);
        }
        public ActionResult Create(string mid)
        {
            MoMModel model = new MoMModel();
            model.EncryptedMoMId= mid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.MoMId = 0;
            model.MonthList = UtilityAccess.MonthList(1);
            model.YearList = UtilityAccess.YearList(1);
            MoMResponse result = new MoMResponse();
            result.MinutesOfMeeting = model;

            ViewBag.PageHeader = "Basic Info";
            return View(result.MinutesOfMeeting);
        }
        [HttpPost]
        public ActionResult CreateorEdit(MoMModel model)
        {
            Function function = new Function();
            bool IsFile = false;
            //string UserId = UtilityAccess.Decrypt(model.EncryptedUserId);
            //.xlsx, .xls, .doc, .docx, .pdf, .jpg, .png

            HttpPostedFileBase httpPostedFileBase = model.file;

            //httpPostedFileBase = Request.Files[0] as HttpPostedFileBase;

            string fileName = string.Empty, filePath = string.Empty;
            if (Request.Files.Count > 0)
            {
                //save picture
                httpPostedFileBase = Request.Files[0] as HttpPostedFileBase;
                if (httpPostedFileBase != null && httpPostedFileBase.ContentLength > 0)
                {
                    string timeStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                    fileName = Path.GetFileName(httpPostedFileBase.FileName);
                    string ext = System.IO.Path.GetExtension(httpPostedFileBase.FileName);
                    filePath = "/Upload/MOM/" + model.Year.ToString() + "/"+ model.Month;
                    fileName = fileName.Replace(" ", "");
                    string thumbnailFilePath = string.Empty;
                    //Set thumbnail for documents for app use
                    if (ext.ToLower() == ".docx" || ext.ToLower() == ".doc")
                        thumbnailFilePath = "/thumbnail/word.png";
                    else
                    if (ext.ToLower() == ".xlsx" || ext.ToLower() == ".xls")
                        thumbnailFilePath = "/thumbnail/excel.png";
                    else
                    if (ext.ToLower() == ".jpeg" || ext.ToLower() == ".jpg" || ext.ToLower() == ".png")
                        thumbnailFilePath = "/thumbnail/png.png";
                    else
                    if (ext.ToLower() == ".pdf")
                        thumbnailFilePath = "/thumbnail/pdf.png";
                    else { thumbnailFilePath = "/thumbnail/png.png"; }

                    model.ThumbnailPath = thumbnailFilePath;
                    model.DocumentName = fileName;
                    
                    model.DocumentPath = filePath+"/"+fileName;
                    IsFile = true;

                    
                }
            }


            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            {
                MoMResponse result = momAccess.AddorEdit(model);
                if(IsFile)
                {
                    Directory.CreateDirectory(Server.MapPath("~/" + filePath));
                    httpPostedFileBase.SaveAs(Server.MapPath("~/" + filePath) + "/" + fileName);

                }
                if (result.MinutesOfMeeting != null)
                {
                    TempData["ReturnCode"] = result.ReturnCode;
                    TempData["ReturnMessage"] = result.ReturnMessage;
                    TempData["MoMModel"] = result.MinutesOfMeeting;
                   // return RedirectToAction("index", "mom");
                }
            }
            return View(model);
        }
        [HttpPost]
        public JsonResult ValidateMOM(string duration)
        {
            string[] arr = duration.Split(',');
            string month = arr[0];
            string year = arr[1];

            MoMValidateResponse JsonData = momAccess.ValidateMoM(month, year);
            return Json(JsonData, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        public ActionResult Delete(string mid)
        {
            //Function function = new Function();
            MoMModel model = new MoMModel();
            model.EncryptedMoMId = mid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            {
                MoMResponse result = momAccess.Delete(model);
                if (result.MinutesOfMeeting != null)
                {
                    TempData["ReturnCode"] = result.ReturnCode;
                    TempData["ReturnMessage"] = result.ReturnMessage;
                    TempData["MoMModel"] = result.MinutesOfMeeting;
                    return RedirectToAction("index", "mom");
                }
            }
            return View(model);
        }
        public ActionResult LogBook()
        {
            MoMModel model = new MoMModel();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            MoMResponse result = momAccess.LogSelectAll(model);
            return View(result.MinutesOfMeeting);
        }
        [HttpPost]
        public ActionResult LogBook(MoMModel model)
        {

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            MoMResponse result = momAccess.LogSelectAll(model);
            return View(result.MinutesOfMeeting);
        }


    }
}