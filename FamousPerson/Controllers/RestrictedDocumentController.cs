﻿using FamousPerson.Models;
using FPBAL.Business;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class RestrictedDocumentController : BaseController
    {
        RestrictedDocumentAccess restrictedDocumentAccess = new RestrictedDocumentAccess();
        // GET: RestrictedDocument
        public ActionResult Index()
        {
            RestrictedDocumentModel model = new RestrictedDocumentModel();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            RestrictedDocumentResponse result = restrictedDocumentAccess.SelectAll(model);
            return View(result._restrictedDocumentModel);
        }
        [HttpPost]
        public ActionResult Index(RestrictedDocumentModel model)
        {

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            RestrictedDocumentResponse result = restrictedDocumentAccess.SelectAll(model);
            return View(result._restrictedDocumentModel);
        }
        public ActionResult Create(string did)
        {
            RestrictedDocumentModel model = new RestrictedDocumentModel();
            RestrictedDocumentResponse result = new RestrictedDocumentResponse();
            model.EncryptedDocumentId = did;
            model.NoFile = null;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.RDocumentId = 0;
            result._restrictedDocumentModel = model;
            return View(result._restrictedDocumentModel);
        }
        [HttpPost]
        public ActionResult Create(RestrictedDocumentModel model)
        {
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            RestrictedDocumentResponse result = new RestrictedDocumentResponse();
            HttpPostedFileBase httpPostedFileBase = null;
            if (Request.Files.Count > 0)
            {
                //save picture
                httpPostedFileBase = Request.Files[0] as HttpPostedFileBase;
                if (httpPostedFileBase != null && httpPostedFileBase.ContentLength > 0)
                {
                    string timeStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                    string fileName = Path.GetFileName(httpPostedFileBase.FileName);
                    string ext = System.IO.Path.GetExtension(httpPostedFileBase.FileName);
                    fileName = fileName.Replace(" ", "");
                    string thumbnailFilePath = string.Empty;
                    //Set thumbnail for documents for app use
                    if (ext.ToLower() == ".docx" || ext.ToLower() == ".doc")
                        thumbnailFilePath = "/thumbnail/word.png";
                    else if (ext.ToLower() == ".xlsx" || ext.ToLower() == ".xls")
                        thumbnailFilePath = "/thumbnail/excel.png";
                    else if (ext.ToLower() == ".jpeg" || ext.ToLower() == ".jpg" || ext.ToLower() == ".png")
                        thumbnailFilePath = "/thumbnail/png.png";
                    else if (ext.ToLower() == ".pdf")
                        thumbnailFilePath = "/thumbnail/pdf.png";
                    else
                    {
                        model.NoFile = "Please enter file with specified extensions!";

                    }

                    if (model.NoFile == null)
                    {
                        model.ThumbnailPath = thumbnailFilePath;
                        string filePath = "Upload/RestrictedDocument/" + timeStamp;
                        model.RDocumentName = fileName;

                        model.RDocumentPath = filePath+"/"+fileName; 
                        Directory.CreateDirectory(Server.MapPath("~/" + filePath));
                        httpPostedFileBase.SaveAs(Server.MapPath("~/" + filePath) + "/" + fileName);
                        result = restrictedDocumentAccess.Add(model);
                    }
                }
            }
            if (model.NoFile != null)
            {

                return View(model);
            }


            if (result._restrictedDocumentModel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                TempData["DocumentModel"] = result._restrictedDocumentModel;
                return RedirectToAction("index", "RestrictedDocument");
            }
            else
            {
                return RedirectToAction("error", "home");
            }
        }

        public ActionResult Delete(string did)
        {
            RestrictedDocumentModel model = new RestrictedDocumentModel();
            model.EncryptedDocumentId = did;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            RestrictedDocumentResponse result = restrictedDocumentAccess.Delete(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "RestrictedDocument");
        }
        public ActionResult LogBook()
        {
            RestrictedDocumentModel model = new RestrictedDocumentModel();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            RestrictedDocumentResponse result = restrictedDocumentAccess.LogSelectAll(model);
            return View(result._restrictedDocumentModel);
        }
        [HttpPost]
        public ActionResult LogBook(RestrictedDocumentModel model)
        {
            
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            RestrictedDocumentResponse result = restrictedDocumentAccess.LogSelectAll(model);
            return View(result._restrictedDocumentModel);
        }
    }
}