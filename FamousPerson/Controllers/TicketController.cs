﻿using FamousPerson.Models;
using FPBAL.Business;
using FPModels.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class TicketController : BaseController
    {
        // GET: Ticket
        TicketAccess ticketAccess = new TicketAccess();
        #region Manage_Ticket
        public ActionResult List()
        {
            TicketModel model = new TicketModel();
            UserResponse request = new UserResponse();
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.DateFrom = "2019/01/01";
            model.DateTo = UtilityAccess.ToDate();

            TicketResponse result = ticketAccess.SelectAll(model);
            if (result.TicketModel != null)
            {
                
                return View(result.TicketModel);
            }
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public ActionResult List(TicketModel model)
        {
            model.DateTo = UtilityAccess.ToDate(model.DateTo);
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            if (model.DateFrom == null)
            {
                model.DateFrom = "2019/01/01";
            }
            TicketResponse result = ticketAccess.SelectAll(model);

            if (result != null)
            {
                // community detail
                //ViewBag.AccountNumber = result.TicketModel.AccountNumber;
                if (!String.IsNullOrEmpty(result.TicketModel.CommunityName))
                    ViewBag.CommunityName = result.TicketModel.CommunityName + " (" + result.TicketModel.AccountNumber + ")";

                //ViewBag.CommunityName = result.TicketModel.CommunityName + " (" + result.TicketModel.AccountNumber + ")";

                return View(result.TicketModel);
            }
            else
                return RedirectToAction("error", "home");
        }

     
      
        public ActionResult Create()
        {
            
            TicketModel model = new TicketModel();
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            // access call
            TicketResponse result = ticketAccess.SelectById(model);

            if (result.TicketModel != null)
            {
                return View(result.TicketModel);
            }
            else
                return RedirectToAction("error", "home");

        }

        [HttpPost]
        public ActionResult Create(TicketModel model, string[] SelectedTaggedToIDs)
        {

            

            if (ModelState.IsValid)
            {
                model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
                model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");

                if (SelectedTaggedToIDs != null)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(string.Join(",", SelectedTaggedToIDs));
                    model.TaggedToIDs = sb.ToString();
                }
                if (model.AssignedTo == null || model.AssignedTo == -1)
                {
                    model.AssignedTo = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                }
                if (model.ReasonName != null)
                {
                    model.TicketReasonId = null;
                }
                if (Request.Files != null && Request.Files.Count > 0)
                {
                    model._Images = new List<ImageModel>();
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        model._Images.Add(new ImageModel
                        {
                            ImagePath = Function.UserUploadFile((Request.Files[i] as HttpPostedFileBase), "TicketImage")
                        
                        });
                        
                    }
                }

                TicketResponse result = ticketAccess.AddOrEdit(model);
                if (result != null)
                {
                    TempData["ReturnCode"] = result.ReturnCode;
                    TempData["ReturnMessage"] = result.ReturnMessage;
                    TempData["href"] = "/ticket/list?cid=" + model.EncryptedTicketId;
                    if (result.ReturnCode == 1)
                    {
                       
                    }

                    if (result.ReturnCode < 1)
                    {
                        model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
                        model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
                        result = ticketAccess.SelectById(model);
                        TempData["href"] = "";
                        // community detail
                        //ViewBag.AccountNumber = result.TicketModel.AccountNumber;


                        if (result.ReturnCode < 1)
                            return RedirectToAction("error", "home");
                    }

                    if (!String.IsNullOrEmpty(result.TicketModel.CommunityName))
                        ViewBag.CommunityName = result.TicketModel.CommunityName + " (" + result.TicketModel.AccountNumber + ")";

                    return View(result.TicketModel);

                }
                else
                    return RedirectToAction("error", "home");
            }

            //get encrypted data
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            TicketResponse response = ticketAccess.SelectById(model);
            // model.ProfileId = KitchenAccess.ProfileIdGet();
            return View(response.TicketModel);
        }

        public ActionResult Edit(String tid, String cid, String mid)
        {
            
            TicketModel model = new TicketModel();
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedTicketId = cid;

            if (!String.IsNullOrEmpty(tid))
                model.TicketId = Convert.ToInt32(UtilityAccess.Decrypt(tid));

            // access call
            TicketResponse result = ticketAccess.SelectById(model);

            if (result != null)
            {
                
                return View(result.TicketModel);
            }
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]

        public ActionResult Edit(TicketModel model, string[] SelectedTaggedToIDs)
        {

            if (ModelState.IsValid)
            {
                model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
                model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
                if (SelectedTaggedToIDs != null)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(string.Join(",", SelectedTaggedToIDs));
                    model.TaggedToIDs = sb.ToString();
                }
                if (model.AssignedTo == null)
                {
                    model.AssignedTo = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                }
                if (model.ReasonName != null)
                {
                    model.TicketReasonId = null;
                }
                if (Request.Files != null && Request.Files.Count > 0)
                {
                    model._Images = new List<ImageModel>();
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        model._Images.Add(new ImageModel
                        {
                            ImagePath = Function.UserUploadFile((Request.Files[i] as HttpPostedFileBase), "TicketImage")

                        });

                    }
                }
                TicketResponse result = ticketAccess.AddOrEdit(model);
                if (result != null)
                {
                    TempData["ReturnCode"] = result.ReturnCode;
                    TempData["ReturnMessage"] = result.ReturnMessage;
                    TempData["href"] = "/ticket/list?cid=" + model.EncryptedTicketId;
                    if (result.ReturnCode == 2 && result.TicketModel.Status == "Closed")
                    {
                   
                    }
                    return View(result.TicketModel);
                }
                else
                    return RedirectToAction("error", "home");
            }


            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            TicketResponse response = ticketAccess.SelectById(model);

            // community detail            
            ViewBag.CommunityName = response.TicketModel.CommunityName + " (" + response.TicketModel.AccountNumber + ")";


            return View(response.TicketModel);
        }


       

        public ActionResult Detail(String tid)
        {
            TicketModel model = new TicketModel();
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");

            TicketResponse result = ticketAccess.SelectById(model);
            return PartialView(result);

        }

    
        public JsonResult ValidateReasonName(String ReasonName)
        {
            var result = ticketAccess.ValidateReasonName(ReasonName);
            string JSONresult;
            JSONresult = JsonConvert.SerializeObject(result);
            return Json(JSONresult, JsonRequestBehavior.AllowGet);
        }
        #endregion Manage_Ticket

        

      
    }
}