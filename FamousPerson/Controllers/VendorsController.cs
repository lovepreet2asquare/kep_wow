﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPModels.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FamousPerson.Models;

namespace FamousPerson.Controllers
{
    public class VendorsController : BaseController
    {
        IVendors vendorsaccess = new VendorsAccess();
        // GET: Vendors
        public ActionResult Index()
        {
            VendorsModel model = new VendorsModel();

            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            VendorsResponse result = vendorsaccess.SelectAll(model);

            ViewBag.Header = "Vendors";
            return View(result.Vendorsmodel);
        }
        [HttpPost]
        public ActionResult Index(VendorsModel model)
        {
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            VendorsResponse result = vendorsaccess.SelectAll(model);

            ViewBag.Header = "Vendors";
            return View(result.Vendorsmodel);
        }

        [HttpGet]
        public ActionResult Create(string vid)
        {
            //ModelState.Clear();

            VendorsModel model = new VendorsModel();
            DirectoryResponse request = new DirectoryResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedVendorId = vid;
            VendorsResponse result = vendorsaccess.SelectById(model);
            ViewBag.PageHeader = "Vendor Info";
            if (result.Vendorsmodel != null)
            {
             
                return View(result.Vendorsmodel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpGet]
        public ActionResult Edit(String vid)
        {

            VendorsModel model = new VendorsModel();
            DirectoryResponse request = new DirectoryResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedVendorId = vid;
            VendorsResponse result = vendorsaccess.SelectById(model);
            ViewBag.PageHeader = "Vendor Info";
            if (result.Vendorsmodel != null)
            {

                return View(result.Vendorsmodel);
            }
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public ActionResult CreateOrEdit(VendorsModel model)
        {

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            VendorsResponse result = null;
            if (!String.IsNullOrEmpty(model.ImageBase64))
                model.LogoPath = Function.UploadCropImage(model.ImageBase64, "Vendor");
            result = vendorsaccess.AddorEdit(model);
            if (result.Vendorsmodel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                return RedirectToAction("index", "vendors");
            }
            else
            {
                return RedirectToAction("error", "home");
            }

        }
        public ActionResult Detail(string vid)
        {
            VendorsModel model = new VendorsModel();
            DirectoryResponse request = new DirectoryResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedVendorId = vid;
            VendorsResponse result = vendorsaccess.SelectById(model);
            ViewBag.PageHeader = "Vendor Info";
            if (result.Vendorsmodel != null)
            {

                return View(result.Vendorsmodel);
            }
            else
                return RedirectToAction("error", "home");
        }

        public ActionResult Delete(string vid)
        {
            VendorsModel model = new VendorsModel();
            model.EncryptedVendorId = vid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            VendorsResponse result = vendorsaccess.Delete(model);
            //ViewBag.PageHeader = "Delete headline";
            //return View(result._HeadlineModel);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "Vendors");
        }
    }
}