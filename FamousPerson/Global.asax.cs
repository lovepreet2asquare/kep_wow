﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FamousPerson
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        public static string getControllerNameFromUrl()
        {
            HttpContextBase context = new HttpContextWrapper(HttpContext.Current);
            RouteData rd = RouteTable.Routes.GetRouteData(context);
            String _controller = String.Empty;

            if (rd != null)
            {
                _controller = rd.GetRequiredString("controller");
                _controller += "/" + rd.GetRequiredString("action");

                //_controller += "/" + rd.GetRequiredString("action");
            }
            return _controller;
        }

        protected void Application_Error()
        {
            HttpContext httpContext = HttpContext.Current;
            if (httpContext != null)
            {
                RequestContext requestContext = ((MvcHandler)httpContext.CurrentHandler).RequestContext;
                /* When the request is ajax the system can automatically handle a mistake with a JSON response. 
                   Then overwrites the default response */
                if (requestContext.HttpContext.Request.IsAjaxRequest())
                {
                    httpContext.Response.Clear();
                    string controllerName = requestContext.RouteData.GetRequiredString("controller");
                    IControllerFactory factory = ControllerBuilder.Current.GetControllerFactory();
                    IController controller = factory.CreateController(requestContext, controllerName);
                    ControllerContext controllerContext = new ControllerContext(requestContext, (ControllerBase)controller);

                    JsonResult jsonResult = new JsonResult
                    {
                        Data = new { success = false, serverError = "500" },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                    jsonResult.ExecuteResult(controllerContext);
                    httpContext.Response.End();
                }
                else
                {
                    httpContext.Response.Redirect("~/home/error");
                }
            }
        }
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // routes.MapRoute("news",                                           // Route name
            //    "news/index",                            // URL with parameters
            //     "news/list",                                                //new url
            //    new { controller = "news", action = "index" }   // Parameter defaults
            //);

            routes.MapRoute("list", "news/index", new { controller = "news", action = "index" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "account", action = "login", id = UrlParameter.Optional }
            );
            //routes.MapRoute("Pages3", "{url1}/{url2}/{url3}", MVC.Page.RedirectTo(), new { url1 = "", url2 = "", url3 = "" });
            //routes.MapRoute("Pages2", "{url1}/{url2}", MVC.Page.RedirectTo(), new { url1 = "", url2 = "", url3 = "" });
            //routes.MapRoute("Pages1", "{url1}", MVC.Page.RedirectTo(), new { url1 = "", url2 = "", url3 = "" });
        }
    }
}
