﻿if ($('.USMobileFormat').length > 0) {
    $('.USMobileFormat').attr('maxlength', '14');
    var phone = document.getElementsByClassName("USMobileFormat");
    for (var i = 0; i < phone.length; i++) {
        phone[i].addEventListener('input', function (e) {
            var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
            e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
        });
    }
}
function Check(MyStr) {

   
}
$(document).on("keypress", ".numbersOnly", function (e) {

    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57 || $(this).val().length >= 25)) {
        return false;
        //alert('Invalid input');
    }
});

$(document).on("keypress", ".ZipOnly", function (e) {

    if (e.which != 8 && e.which != 0 && ((e.which == 47 && e.which == 48 && e.which == 44 )&& e.which < 43 || e.which > 57 || $(this).val().length >= 25)) {
        return false;
        //alert('Invalid input');
    }
});

$(document).ready(function () {
    $(".AlphabetOnly").keypress(function (e) {
        var key = e.keyCode;
        if (key >= 48 && key <= 57) {
            e.preventDefault();
        }
    });
});
//$(".AlphabetOnly").keypress(function (e) {
//    $(document).on("keypress", ".AlphabetOnly", function (e) {

//    var key = e.keyCode;
//    //if (key >= 48 && key <= 57) {
//        if ((c < 65) || (c > 90 && c < 97) || (c > 122))
//        e.preventDefault();
//    }

//    //var c = e.get_keyCode();
//    //if ((c < 65) || (c > 90 && c < 97) || (c > 122))
//    //    return false;

//    //eventArgs.set_cancel(true);
//);

//$(document).on("keypress", ".WordLimit", function (e) {
    $(".WordLimit").on('keyup', function () {
        var words = this.value.match(/\S+/g).length;

        if (words > 1000) {
            // Split the string on first 200 words and rejoin on spaces
            var trimmed = $(this).val().split(/\s+/, 50).join(" ");
            // Add a space at the end to make sure more typing creates new words
            $(this).val(trimmed + " ");
        }
        else {
            $('.limit').text("Maximum 1000 words allowed");
            //$('#word_left').text(200 - words);
        }
});

$(".WordLimit100").on('keyup', function () {
    var words = this.value.match(/\S+/g).length;

    if (words > 100) {
        // Split the string on first 200 words and rejoin on spaces
        var trimmed = $(this).val().split(/\s+/, 50).join(" ");
        // Add a space at the end to make sure more typing creates new words
        $(this).val(trimmed + " ");
    }
    else {
        $('.limit').text("Maximum 100 words allowed");
        //$('#word_left').text(200 - words);
    }
});


function getNextYear() {
    var currentDateTime_ = new Date();
    var currentYear_ = currentDateTime_.getFullYear();
    var nextYear_ = currentYear_ + 1;
    return nextYear_;
    var hr = currentDateTime_.getHours();
    var mn = currentDateTime_.getMinutes();
}
//$(function () {
var NextYearVal_ = getNextYear();
$("body").on("mouseenter", "input.myCalendar", function () {
    var topHeight = $(this).offset().top + $(this).height() + 10;
    var offsetLeft = $(this).offset().left;
    $(this).datepicker({
        dateFormat: "mm/dd/yy",
        // minDate: "01-Jan-1970",
        yearRange: "1970:" + NextYearVal_ + "",
        //maxDate: "31/01/2100",
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        beforeShow: function (input, inst) {
            setTimeout(function () {
                inst.dpDiv.css({
                    top: topHeight,
                    left: offsetLeft
                });
            }, 0)
        }
        //showOn: "button",
        //buttonImage: "images/calendar.gif",
        //buttonImageOnly: true,
        //timeFormat: "hh:mm tt"
    });
});

function RectangleRatio(w,h) {
    var r = gcd(w, h);
    return "Image aspect ratio should be in a range 9:3 to 9:4 (e.g. 300x100, 300x90, 600x200 and etc)). Current image ratio is " + (w / r) + ':' + parseInt(h / r) + '.'
}

function SquareRatio(w, h) {
    var r = gcd(w, h);
    return "Image aspect ratio should be in a range 9:7 to 7:9 (e.g. 150x150, 150x130, 130x150 and etc). Current image ratio is " + (w / r) + ':' + parseInt(h / r) + '.'
}

function gcd(w, h) {
    if (h == 0)
        return w

    return gcd(h, w % h)
}