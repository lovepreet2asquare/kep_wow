﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net;
using System.IO;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

/// <summary>
/// Summary description for SendOTP
/// </summary>
public class SendOTP
{
    public SendOTP()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static string GenerateOTP()
    {
        string strPassword = "";
        Random rnd = new Random();
        string data = "1234567890";
        for (int i = 0; i < 6; i++)
        {
            strPassword += data[rnd.Next(0, data.Length - 1)];
        }
        

        return strPassword;
    }
    
    public static string SendSMS(string msisdn)
    {
        string returnResult = string.Empty;
        string returnValue = "0";
        string username = "parkurcity";
        string password = "Mj03201989";

        string OTP = GenerateOTP();

        string message = OTP + " is your NJSPBA verification code.";//message.Replace("@OTP", OTP);

        //what if no numbers & error ocurres

        // msisdn = "447123123123";
        if (!string.IsNullOrEmpty(msisdn))
        {
            string url = "http://usa.bulksms.com/eapi/submission/send_sms/2/2.0";
            string seven_bit_msg = message;
            Hashtable result;
            
            string data = seven_bit_message(username, password, msisdn, seven_bit_msg);
            result = send_sms(data, url);
            if ((int)result["success"] == 1)
            {
                returnResult = formatted_server_response(result);
                returnValue = "1";
            }
            else
            {
                returnValue = "-1";
                returnResult = formatted_server_response(result);
            }
        }
        else
        {
            returnResult = "0";
        }

        return OTP; // + "|" + returnValue;
    }
    public static string formatted_server_response(Hashtable result)
    {
        string ret_string = "";
        if ((int)result["success"] == 1)
        {
            ret_string += "Success: batch ID " + (string)result["api_batch_id"] + "API message: " + (string)result["api_message"] + "\nFull details " + (string)result["details"];
        }
        else
        {
            ret_string += "Fatal error: HTTP status " + (string)result["http_status_code"] + " API status " + (string)result["api_status_code"] + " API message " + (string)result["api_message"] + "\nFull details " + (string)result["details"];
        }

        return ret_string;
    }

    public static Hashtable send_sms(string data, string url)
    {
        string sms_result = Post(url, data);

        Hashtable result_hash = new Hashtable();

        string tmp = "";
        tmp += "Response from server: " + sms_result + "\n";
        string[] parts = sms_result.Split('|');

        string statusCode = parts[0];
        string statusString = parts[1];

        result_hash.Add("api_status_code", statusCode);
        result_hash.Add("api_message", statusString);

        if (parts.Length != 3)
        {
            tmp += "Error: could not parse valid return data from server.\n";
        }
        else
        {
            if (statusCode.Equals("0"))
            {
                result_hash.Add("success", 1);
                result_hash.Add("api_batch_id", parts[2]);
                tmp += "Message sent - batch ID " + parts[2] + "\n";
            }
            else if (statusCode.Equals("1"))
            {
                // Success: scheduled for later sending.
                result_hash.Add("success", 1);
                result_hash.Add("api_batch_id", parts[2]);
            }
            else
            {
                result_hash.Add("success", 0);
                tmp += "Error sending: status code " + parts[0] + " description: " + parts[1] + "\n";
            }
        }
        result_hash.Add("details", tmp);
        return result_hash;
    }
    public static string Post(string url, string data)
    {
        string result = null;
        try
        {
            byte[] buffer = Encoding.Default.GetBytes(data);

            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(url);
            WebReq.Method = "POST";
            WebReq.ContentType = "application/x-www-form-urlencoded";
            WebReq.ContentLength = buffer.Length;
            Stream PostData = WebReq.GetRequestStream();
            PostData.Write(buffer, 0, buffer.Length);
            PostData.Close();
            HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

            //Console.WriteLine(WebResp.StatusCode);

            Stream Response = WebResp.GetResponseStream();
            StreamReader _Response = new StreamReader(Response);
            result = _Response.ReadToEnd();
        }
        catch (Exception ex)
        {
            //Console.WriteLine(ex.Message);
            result = ex.Message;
        }
        return result.Trim() + "\n";
    }

    public static string seven_bit_message(string username, string password, string msisdn, string message)
    {

        /********************************************************************
        * Construct data                                                    *
        *********************************************************************/
        /*
        * Note the suggested encoding for the some parameters, notably
        * the username, password and especially the message.  ISO-8859-1
        * is essentially the character set that we use for message bodies,
        * with a few exceptions for e.g. Greek characters. For a full list,
        * see: http://developer.bulksms.com/eapi/submission/character-encoding/
        */

        string data = "";
        data += "username=" + HttpUtility.UrlEncode(username, System.Text.Encoding.GetEncoding("ISO-8859-1"));
        data += "&password=" + HttpUtility.UrlEncode(password, System.Text.Encoding.GetEncoding("ISO-8859-1"));
        data += "&message=" + HttpUtility.UrlEncode(character_resolve(message), System.Text.Encoding.GetEncoding("ISO-8859-1"));
        data += "&msisdn=" + msisdn;
        data += "&want_report=1";

        return data;
    }
    public static string character_resolve(string body)
    {
        Hashtable chrs = new Hashtable();
        chrs.Add('Ω', "Û");
        chrs.Add('Θ', "Ô");
        chrs.Add('Δ', "Ð");
        chrs.Add('Φ', "Þ");
        chrs.Add('Γ', "¬");
        chrs.Add('Λ', "Â");
        chrs.Add('Π', "º");
        chrs.Add('Ψ', "Ý");
        chrs.Add('Σ', "Ê");
        chrs.Add('Ξ', "±");

        string ret_str = "";
        foreach (char c in body)
        {
            if (chrs.ContainsKey(c))
            {
                ret_str += chrs[c];
            }
            else
            {
                ret_str += c;
            }
        }
        return ret_str;
    }

}