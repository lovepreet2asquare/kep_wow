﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;
using FPBAL.Business;

namespace FamousPerson
{
    public partial class reset : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            lblMsg.Text = "";
            if (!Page.IsPostBack)
            {
                GetUserInfo();
            }
        }
        protected void GetUserInfo()
        {
            if (Request.QueryString["q"] != null && Request.QueryString["token"] != null)
            {
                int FollowerId = 0;

                string FollowerIdEncrypted = Request.QueryString["q"];
                FollowerIdEncrypted = FollowerIdEncrypted.Replace(" ", "+");
                string FollowerIdDecrypted = UtilityAccess.Decrypt(FollowerIdEncrypted);

                if (int.TryParse(FollowerIdDecrypted, out FollowerId))
                {

                    DataTable dtblInfo = new DataTable();
                    using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["fpCon"].ConnectionString))
                    {
                        using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter("dbo.FP_FollowerInfo", sqlConnection))
                        {
                            try
                            {
                                sqlDataAdapter.SelectCommand.Parameters.Add("@FollowerId", SqlDbType.Int).Value = FollowerId;
                                sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                                sqlDataAdapter.Fill(dtblInfo);
                                if (dtblInfo.Rows.Count > 0)
                                {

                                    string strEmail = dtblInfo.Rows[0]["Email"].ToString();
                                    string strPassword = dtblInfo.Rows[0]["pwd"].ToString();
                                    int IsExpired = Convert.ToInt32(dtblInfo.Rows[0]["IsExpired"]);
                                    bool TokenUsed = Convert.ToBoolean(dtblInfo.Rows[0]["TokenUsed"]);
                                    ViewState["FirstName"] = dtblInfo.Rows[0]["FirstName"].ToString();
                                    // generate MD5 by @password+@email+@puid
                                    string strToken = UtilityAccess.CalculateMD5Hash(strPassword + strEmail.ToLower() + FollowerId);
                                    if (strToken == Request.QueryString["token"] && IsExpired > 0)
                                    {
                                        if (!TokenUsed)
                                        {
                                            // Valid Link
                                            txtNewPassword.Focus();
                                            lblEmail.InnerText = strEmail;
                                            ViewState["valid"] = true;
                                            ViewState["FollowerId"] = FollowerId;
                                            pnlForm.Visible = true;
                                            noresult.Visible = false;
                                            pnlMsg.Visible = false;
                                            // pnlInvalid.Visible = false;
                                        }
                                        else
                                        {
                                            // Invalid token
                                            pnlForm.Visible = false;
                                            noresult.Visible = true;
                                            pnlMsg.Visible = true;

                                        }
                                    }
                                    else
                                    {
                                        // Invalid token
                                        pnlForm.Visible = false;
                                        noresult.Visible = true;
                                        pnlMsg.Visible = true;
                                        //pnlInvalid.Visible = true;
                                    }

                                }
                                else
                                {
                                    // invalid puid
                                    pnlForm.Visible = false;
                                    noresult.Visible = true;
                                    pnlMsg.Visible = true;
                                    //pnlInvalid.Visible = true;
                                }

                            }
                            catch (Exception ex)
                            {
                                //ApplicationLogger.LogError(ex, "reset.aspx", "GetBistroInfo()");
                                //ExceptionUtility.LogException(ex, "ForgotPassword");
                            }
                            finally
                            {
                                sqlConnection.Close();
                            }
                        }
                    }
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            bool isValid = false;
            if (Page.IsValid)
            {
                string procedureName = "";
                string firstParam = "";
                int firstParamValue = 0;

                isValid = true;
                procedureName = "dbo.FP_FollowerResetPassword";
                firstParam = "@FollowerId";
                firstParamValue = Convert.ToInt32(ViewState["FollowerId"]);

                if (isValid && txtNewPassword.Text.Trim() != "" && txtConfirmPassword.Text.Trim() != "")
                {
                    if (txtNewPassword.Text == txtConfirmPassword.Text)
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["fpCon"].ConnectionString))
                        {
                            using (SqlCommand sqlCommand = new SqlCommand(procedureName, sqlConnection))
                            {
                                try
                                {
                                    sqlCommand.Parameters.Add(firstParam, SqlDbType.Int).Value = firstParamValue;
                                    sqlCommand.Parameters.Add("@PWD", SqlDbType.NVarChar, 300).Value = UtilityAccess.Encrypt(txtNewPassword.Text);
                                    sqlCommand.CommandType = CommandType.StoredProcedure;
                                    if (sqlConnection.State == ConnectionState.Closed)
                                    {
                                        sqlConnection.Open();
                                    }
                                    if (sqlCommand.ExecuteNonQuery() > 0)
                                    {
                                        pnlForm.Visible = false;
                                        pnlcontainer.Visible = false;
                                        pnlSuccess.Visible = true;
                                        AppUserAccess userAccess = new AppUserAccess();
                                        string firstName = ViewState["FirstName"] as string;
                                        userAccess.SendEmailOnPasswordUpdate(firstName, lblEmail.InnerText);
                                    }
                                    else
                                    {
                                        lblMsg.Text = "Oops! Something went wrong. Please try again later.";
                                    }
                                }
                                catch (Exception ex)
                                {
                                    // ApplicationLogger.LogError(ex, "REST.aspx", "RESET");
                                    // ExceptionUtility.LogException(ex, "ForgotPassword");
                                }
                                finally
                                {
                                    sqlConnection.Close();
                                }
                            }
                        }
                    }
                    else
                    {
                        lblMsg.Text = "Sorry! Mismatch passwords.";
                    }
                }
            }
        }

    }
}